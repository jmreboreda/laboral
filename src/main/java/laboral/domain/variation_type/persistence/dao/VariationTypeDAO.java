package laboral.domain.variation_type.persistence.dao;


import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.utilities.HibernateUtil;
import laboral.domain.variation_type.persistence.dbo.VariationTypeDBO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class VariationTypeDAO implements GenericDAO<VariationTypeDBO, Integer> {

    private SessionFactory sessionFactory;
    private Session session;

    public VariationTypeDAO() {
    }

    public static class VariationTypeDAOFactory {

        private static VariationTypeDAO variationTypeDAO;

        public static VariationTypeDAO getInstance() {
            if(variationTypeDAO == null) {
                variationTypeDAO = new VariationTypeDAO(HibernateUtil.retrieveGlobalSession());
            }
            return variationTypeDAO;
        }
    }

    public VariationTypeDAO(Session session) {
        this.session = session;
    }

    @Override
    public Integer create(VariationTypeDBO variationTypeDBO) {
        try {
            session.beginTransaction();
            session.saveOrUpdate(variationTypeDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }
        return variationTypeDBO.getId();
    }

    @Override
    public VariationTypeDBO findById(Integer variationTypeId) {
        TypedQuery<VariationTypeDBO> query = session.createNamedQuery(VariationTypeDBO.FIND_VARIATION_TYPE_BY_ID, VariationTypeDBO.class);
        query.setParameter("variationTypeId", variationTypeId);

        return query.getSingleResult();
    }

    @Override
    public Integer update(VariationTypeDBO variationTypeDBO) {

        return null;
    }

    @Override
    public Boolean delete(VariationTypeDBO variationTypeDBO) {

        return null;
    }

    @Override
    public List<VariationTypeDBO> findAll() {
        TypedQuery<VariationTypeDBO> query = session.createNamedQuery(VariationTypeDBO.FIND_ALL, VariationTypeDBO.class);

        return query.getResultList();
    }

    public VariationTypeDBO findByVariationTypeCode(Integer variationTypeCode){
        TypedQuery<VariationTypeDBO> query = session.createNamedQuery(VariationTypeDBO.FIND_VARIATION_TYPE_BY_VARIATION_CODE, VariationTypeDBO.class);
        query.setParameter("variationCode", variationTypeCode);
        return query.getSingleResult();
    }
}
