package laboral.domain.variation_type;

public class VariationType {

    private Integer id;
    private Integer variationCode;
    private String variationDescription;
    private Boolean extinction;
    private Boolean conversion;
    private Boolean special;
    private Boolean specialInitial;
    private Boolean specialFinal;
    private Boolean extension;
    private Boolean category;
    private Boolean initial;
    private Boolean reincorporation;
    private Boolean workingDay;
    private Boolean idcRequired;

    public VariationType(Integer id,
                         Integer variationCode,
                         String variationDescription,
                         Boolean extinction,
                         Boolean conversion,
                         Boolean special,
                         Boolean specialInitial,
                         Boolean specialFinal,
                         Boolean extension,
                         Boolean category,
                         Boolean initial,
                         Boolean reincorporation,
                         Boolean workingDay,
                         Boolean idcRequired) {
        this.id = id;
        this.variationCode = variationCode;
        this.variationDescription = variationDescription;
        this.extinction = extinction;
        this.conversion = conversion;
        this.special = special;
        this.specialInitial = specialInitial;
        this.specialFinal = specialFinal;
        this.extension = extension;
        this.category = category;
        this.initial = initial;
        this.reincorporation = reincorporation;
        this.workingDay= workingDay;
        this.idcRequired = idcRequired;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVariationCode() {
        return variationCode;
    }

    public void setVariationCode(Integer variationCode) {
        this.variationCode = variationCode;
    }

    public String getVariationDescription() {
        return variationDescription;
    }

    public void setVariationDescription(String variationDescription) {
        this.variationDescription = variationDescription;
    }

    public Boolean getExtinction() {
        return extinction;
    }

    public void setExtinction(Boolean extinction) {
        this.extinction = extinction;
    }

    public Boolean getConversion() {
        return conversion;
    }

    public void setConversion(Boolean conversion) {
        this.conversion = conversion;
    }

    public Boolean getSpecial() {
        return special;
    }

    public void setSpecial(Boolean special) {
        this.special = special;
    }

    public Boolean getSpecialInitial() {
        return specialInitial;
    }

    public void setSpecialInitial(Boolean specialInitial) {
        this.specialInitial = specialInitial;
    }

    public Boolean getSpecialFinal() {
        return specialFinal;
    }

    public void setSpecialFinal(Boolean specialFinal) {
        this.specialFinal = specialFinal;
    }

    public Boolean getExtension() {
        return extension;
    }

    public void setExtension(Boolean extension) {
        this.extension = extension;
    }

    public Boolean getCategory() {
        return category;
    }

    public void setCategory(Boolean category) {
        this.category = category;
    }

    public Boolean getInitial() {
        return initial;
    }

    public void setInitial(Boolean initial) {
        this.initial = initial;
    }

    public Boolean getReincorporation() {
        return reincorporation;
    }

    public void setReincorporation(Boolean reincorporation) {
        this.reincorporation = reincorporation;
    }

    public Boolean getWorkingDay() {
        return workingDay;
    }

    public void setWorkingDay(Boolean workingDay) {
        this.workingDay = workingDay;
    }

    public Boolean getIdcRequired() {
        return idcRequired;
    }

    public void setIdcRequired(Boolean idcRequired) {
        this.idcRequired = idcRequired;
    }

    public String toString(){
        return this.variationDescription;
    }

    public static final class VariationTypeBuilder {
        private Integer id;
        private Integer variationCode;
        private String variationDescription;
        private Boolean extinction;
        private Boolean conversion;
        private Boolean special;
        private Boolean specialInitial;
        private Boolean specialFinal;
        private Boolean extension;
        private Boolean category;
        private Boolean initial;
        private Boolean reincorporation;
        private Boolean workingDay;
        private Boolean idcRequired;

//        private VariationTypeBuilder() {
//        }

        public static VariationTypeBuilder create() {
            return new VariationTypeBuilder();
        }

        public VariationTypeBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public VariationTypeBuilder withVariationCode(Integer variationCode) {
            this.variationCode = variationCode;
            return this;
        }

        public VariationTypeBuilder withVariationDescription(String variationDescription) {
            this.variationDescription = variationDescription;
            return this;
        }

        public VariationTypeBuilder withExtinction(Boolean extinction) {
            this.extinction = extinction;
            return this;
        }

        public VariationTypeBuilder withConversion(Boolean conversion) {
            this.conversion = conversion;
            return this;
        }

        public VariationTypeBuilder withSpecial(Boolean special) {
            this.special = special;
            return this;
        }

        public VariationTypeBuilder withSpecialInitial(Boolean specialInitial) {
            this.specialInitial = specialInitial;
            return this;
        }

        public VariationTypeBuilder withSpecialFinal(Boolean specialFinal) {
            this.specialFinal = specialFinal;
            return this;
        }

        public VariationTypeBuilder withExtension(Boolean extension) {
            this.extension = extension;
            return this;
        }

        public VariationTypeBuilder withCategory(Boolean category) {
            this.category = category;
            return this;
        }

        public VariationTypeBuilder withInitial(Boolean initial) {
            this.initial = initial;
            return this;
        }

        public VariationTypeBuilder withReincorporation(Boolean reincorporation) {
            this.reincorporation = reincorporation;
            return this;
        }

        public VariationTypeBuilder withWorkingDay(Boolean workingDay) {
            this.workingDay = workingDay;
            return this;
        }

        public VariationTypeBuilder withIdcRequired(Boolean idcRequired) {
            this.idcRequired = idcRequired;
            return this;
        }

        public VariationType build() {
            return new VariationType(id, variationCode, variationDescription, extinction, conversion, special, specialInitial, specialFinal, extension, category, initial, reincorporation, workingDay, idcRequired);
        }
    }
}
