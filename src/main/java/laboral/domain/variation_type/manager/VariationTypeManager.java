package laboral.domain.variation_type.manager;


import laboral.domain.variation_type.VariationType;
import laboral.domain.variation_type.mapper.MapperVariationTypeDBOToVariationType;
import laboral.domain.variation_type.persistence.dao.VariationTypeDAO;
import laboral.domain.variation_type.persistence.dbo.VariationTypeDBO;

import java.util.ArrayList;
import java.util.List;

public class VariationTypeManager {

    private VariationTypeDAO variationTypeDAO = VariationTypeDAO.VariationTypeDAOFactory.getInstance();


    public VariationTypeManager() {
    }

    public List<VariationType> findAllVariationType(){

        List<VariationType> VariationTypeList = new ArrayList<>();

        List<VariationTypeDBO> variationTypeDBOList = variationTypeDAO.findAll();
        for(VariationTypeDBO variationTypeDBO : variationTypeDBOList){
            VariationType variationType = VariationType.VariationTypeBuilder.create()
                    .withId(variationTypeDBO.getId())
                    .withVariationCode(variationTypeDBO.getVariationCode())
                    .withVariationDescription(variationTypeDBO.getVariationDescription())
                    .withExtinction(variationTypeDBO.getExtinction())
                    .withConversion(variationTypeDBO.getConversion())
                    .withSpecial(variationTypeDBO.getSpecial())
                    .withSpecialInitial(variationTypeDBO.getSpecialInitial())
                    .withSpecialFinal(variationTypeDBO.getSpecialFinal())
                    .withExtension(variationTypeDBO.getExtension())
                    .withCategory(variationTypeDBO.getCategory())
                    .withInitial(variationTypeDBO.getInitial())
                    .withReincorporation(variationTypeDBO.getReincorporation())
                    .withWorkingDay(variationTypeDBO.getWorkingDay())
                    .build();
            VariationTypeList.add(variationType);
        }

        return VariationTypeList;
    }

    public VariationType findVariationTypeById(Integer typesContractVariationId){

        VariationTypeDBO variationTypeDBO = variationTypeDAO.findById(typesContractVariationId);

        return VariationType.VariationTypeBuilder.create()
                .withId(variationTypeDBO.getId())
                .withVariationDescription(variationTypeDBO.getVariationDescription())
                .withExtinction(variationTypeDBO.getExtinction())
                .withConversion(variationTypeDBO.getConversion())
                .withSpecial(variationTypeDBO.getSpecial())
                .withSpecialInitial(variationTypeDBO.getSpecialInitial())
                .withSpecialFinal(variationTypeDBO.getSpecialFinal())
                .withExtension(variationTypeDBO.getExtension())
                .withCategory(variationTypeDBO.getCategory())
                .withInitial(variationTypeDBO.getInitial())
                .withReincorporation(variationTypeDBO.getReincorporation())
                .withWorkingDay(variationTypeDBO.getWorkingDay())
                .withIdcRequired(variationTypeDBO.getIdcRequired())
                .build();
    }

    public VariationType findVariationTypeByVariationCode(Integer variationTypeCode){

        VariationTypeDBO variationTypeDBO = variationTypeDAO.findByVariationTypeCode(variationTypeCode);
        MapperVariationTypeDBOToVariationType mapperVariationTypeDBOToVariationType = new MapperVariationTypeDBOToVariationType();

        return mapperVariationTypeDBOToVariationType.map(variationTypeDBO);
    }
}
