package laboral.domain.variation_type.controller;

import laboral.domain.variation_type.VariationType;
import laboral.domain.variation_type.manager.VariationTypeManager;

import java.util.List;

public class VariationTypeController {

    private VariationTypeManager variationTypeManager = new VariationTypeManager();


    public VariationTypeController() {
    }

//    public String findVariationDescriptionById(int variationId){
//
//        return variationTypeManager.findVariationDescriptionById(variationId);
//    }

    public VariationType findVariationTypeById(Integer variationTypeId){

        return variationTypeManager.findVariationTypeById(variationTypeId);
    }

    public List<VariationType> findAllTypesContractVariations(){

        return variationTypeManager.findAllVariationType();
    }

    public VariationType findVariationTypeByVariationCode(Integer variationTypeCode){

        return variationTypeManager.findVariationTypeByVariationCode(variationTypeCode);
    }
}
