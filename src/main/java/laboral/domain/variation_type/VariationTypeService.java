package laboral.domain.variation_type;

import laboral.domain.variation_type.persistence.dao.VariationTypeDAO;
import laboral.domain.variation_type.persistence.dbo.VariationTypeDBO;

public class VariationTypeService {

    VariationTypeDAO variationTypeDAO = VariationTypeDAO.VariationTypeDAOFactory.getInstance();

    private VariationTypeService() {
    }

    public static class VariationTypeServiceFactory {

        private static VariationTypeService variationTypeService;

        public static VariationTypeService getInstance() {
            if(variationTypeService == null) {
                variationTypeService = new VariationTypeService();
            }
            return variationTypeService;
        }
    }

    public VariationTypeDBO findByVariationTypeCode(Integer variationTypeCode){

        return variationTypeDAO.findByVariationTypeCode(variationTypeCode);
    }
}
