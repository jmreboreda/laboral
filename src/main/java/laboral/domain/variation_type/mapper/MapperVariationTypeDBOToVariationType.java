package laboral.domain.variation_type.mapper;

import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.variation_type.VariationType;
import laboral.domain.variation_type.persistence.dbo.VariationTypeDBO;

public class MapperVariationTypeDBOToVariationType implements GenericMapper<VariationTypeDBO, VariationType> {
    @Override
    public VariationType map(VariationTypeDBO variationTypeDBO) {

        return VariationType.VariationTypeBuilder.create()
                .withId(variationTypeDBO.getId())
                .withVariationCode(variationTypeDBO.getVariationCode())
                .withCategory(variationTypeDBO.getCategory())
                .withConversion(variationTypeDBO.getConversion())
                .withExtension(variationTypeDBO.getExtension())
                .withExtinction(variationTypeDBO.getExtinction())
                .withIdcRequired(variationTypeDBO.getIdcRequired())
                .withInitial(variationTypeDBO.getInitial())
                .withReincorporation(variationTypeDBO.getReincorporation())
                .withSpecial(variationTypeDBO.getSpecial())
                .withSpecialInitial(variationTypeDBO.getSpecialInitial())
                .withSpecialFinal(variationTypeDBO.getSpecialFinal())
                .withVariationDescription(variationTypeDBO.getVariationDescription())
                .withWorkingDay(variationTypeDBO.getWorkingDay())
                .build();
    }
}
