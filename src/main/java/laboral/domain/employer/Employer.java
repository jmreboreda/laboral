package laboral.domain.employer;

import laboral.domain.activity_period.manager.ActivityPeriodService;
import laboral.domain.client.Client;
import laboral.domain.quote_account_code.QuoteAccountCode;

import java.time.LocalDate;
import java.util.Set;

public class Employer {

    private Integer id;
    private Set<QuoteAccountCode> quoteAccountCodes;
    private Client client;

    public Employer(Integer id, Set<QuoteAccountCode> quoteAccountCodes, Client client) {
        this.id = id;
        this.quoteAccountCodes = quoteAccountCodes;
        this.client = client;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<QuoteAccountCode> getQuoteAccountCodes() {
        return quoteAccountCodes;
    }

    public void setQuoteAccountCodes(Set<QuoteAccountCode> quoteAccountCodes) {
        this.quoteAccountCodes = quoteAccountCodes;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String toAlphabeticalName(){
        return getClient().toAlphabeticalName();
    }

    public String toString() {
            return toAlphabeticalName();
    }

    public Boolean isActiveClientAtDate(Employer employer, LocalDate date){
        ActivityPeriodService activityPeriodService = ActivityPeriodService.ActivityPeriodServiceFactory.getInstance();

        return activityPeriodService.isActiveClientAtDate(employer, date);
    }

    public static final class EmployerBuilder {
        private Integer id;
        private Set<QuoteAccountCode> quoteAccountCodes;
        private Client client;

        private EmployerBuilder() {
        }

        public static EmployerBuilder create() {
            return new EmployerBuilder();
        }

        public EmployerBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public EmployerBuilder withQuoteAccountCodes(Set<QuoteAccountCode> quoteAccountCodes) {
            this.quoteAccountCodes = quoteAccountCodes;
            return this;
        }

        public EmployerBuilder withClient(Client client) {
            this.client = client;
            return this;
        }

        public Employer build() {
            return new Employer(id, quoteAccountCodes, client);
        }
    }
}
