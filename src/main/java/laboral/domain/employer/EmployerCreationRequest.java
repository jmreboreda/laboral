package laboral.domain.employer;

import laboral.domain.client.ClientService;
import laboral.domain.client.persistence.dbo.ClientDBO;

public class EmployerCreationRequest{

    Integer id;
    ClientDBO clientDBO;

    public EmployerCreationRequest() {
    }

    public EmployerCreationRequest(ClientDBO clientDBO) {
        this.clientDBO = clientDBO;
    }

    public ClientDBO getClientDBO() {
        return clientDBO;
    }

    public EmployerCreationRequest create (Employer employer){

        ClientService clientService = ClientService.ClientServiceFactory.getInstance();

        ClientDBO clientDBO = clientService.findClientDBOById(employer.getClient().getClientId());

        return new EmployerCreationRequest(clientDBO);
    }
}
