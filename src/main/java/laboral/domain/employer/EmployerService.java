package laboral.domain.employer;

import laboral.domain.client.Client;
import laboral.domain.employer.controller.EmployerController;
import laboral.domain.employer.persistence.dao.EmployerDAO;
import laboral.domain.employer.persistence.dbo.EmployerDBO;

public class EmployerService {

    EmployerDAO employerDAO = EmployerDAO.EmployerDAOFactory.getInstance();

    private final EmployerController employerController = new EmployerController();

    private EmployerService() {
    }

    public static class EmployerServiceFactory {

        private static EmployerService employerService;

        public static EmployerService getInstance() {
            if(employerService == null) {
                employerService = new EmployerService();
            }
            return employerService;
        }
    }

    public EmployerDBO findById(Integer employerId){

        return employerDAO.findById(employerId);
    }

    public EmployerDBO findByClientID(Integer clientId){

        return employerDAO.findByClientId(clientId);
    }

    public EmployerDBO  findByClientId(Integer clientId){

        return employerDAO.findByClientId(clientId);
    }

    public Boolean isEmployer(Client client){
        return employerController.isEmployer(client);
    }
}
