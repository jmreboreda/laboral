package laboral.domain.employer.mapper;

import laboral.domain.client.mapper.MapperClientDBOToClient;
import laboral.domain.client.persistence.dbo.ClientDBO;
import laboral.domain.contract.WorkContract;
import laboral.domain.contract.work_contract.mapper.MapperWorkContractDBOToWorkContract;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.employer.Employer;
import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.quote_account_code.mapper.MapperQuoteAccountCodeDBOToQuoteAccountCode;
import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

public class MapperEmployerDBOToEmployer implements GenericMapper<EmployerDBO, Employer> {

    @Override
    public Employer map(EmployerDBO employerDBO) {

        MapperQuoteAccountCodeDBOToQuoteAccountCode mapperQuoteAccountCodeDBOToQuoteAccountCode = new MapperQuoteAccountCodeDBOToQuoteAccountCode();
        MapperClientDBOToClient mapperClientDBOToClient = new MapperClientDBOToClient();

        Set<QuoteAccountCode> quoteAccountCodeSet = employerDBO.getQuoteAccountCodes().stream()
                .map(mapperQuoteAccountCodeDBOToQuoteAccountCode::map)
                .collect(toSet());

        Integer clientId = employerDBO.getClientDBO().getId();

        return Employer.EmployerBuilder.create()
                .withQuoteAccountCodes(quoteAccountCodeSet)
                .withClient(mapperClientDBOToClient.map(employerDBO.getClientDBO()))
                .withId(employerDBO.getId())
                .build();
    }
}
