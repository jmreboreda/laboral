package laboral.domain.employer.manager;

import laboral.domain.contract.work_contract.WorkContractService;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.time_record.dto.EmployerForSelectorInTimeRecord;

import java.text.Collator;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class EmployerFinderForSelectorInTimeRecord {

    public List<EmployerForSelectorInTimeRecord> findAllEmployerWithWorkContractWithTimeRecordInMonth(LocalDate startDate, LocalDate endDate){

        WorkContractService workContractService = WorkContractService.WorkContractServiceFactory.getInstance();

        List<EmployerForSelectorInTimeRecord> employerForSelectorInTimeRecordList = new ArrayList<>();

        List<WorkContractDBO> workContractDBOList = workContractService.findAllWorkContractDBOWithTimeRecordInMonth(startDate, endDate);
        for(WorkContractDBO workContractDBO : workContractDBOList){
            EmployerForSelectorInTimeRecord employerForSelectorInTimeRecord = EmployerForSelectorInTimeRecord.EmployerForSelectorInTimeRecordBuilder.create()
                    .withEmployerId(workContractDBO.getEmployerDBO().getId())
                    .withEmployerFullName(workContractDBO.getEmployerDBO().toAlphabeticalName())
                    .build();

            employerForSelectorInTimeRecordList.add(employerForSelectorInTimeRecord);
        }

        return retrieveEmployerForSelectorInTimeRecordListWithoutDuplicates(employerForSelectorInTimeRecordList);
    }

    private List<EmployerForSelectorInTimeRecord> retrieveEmployerForSelectorInTimeRecordListWithoutDuplicates(List<EmployerForSelectorInTimeRecord> employerForSelectorInTimeRecordList){

        List<EmployerForSelectorInTimeRecord> employerForSelectorInTimeRecordListWithoutDuplicates = new ArrayList<>();

        Map<Integer, EmployerForSelectorInTimeRecord> employerMap = new HashMap<>();

        for (EmployerForSelectorInTimeRecord employerForSelector : employerForSelectorInTimeRecordList) {
            employerMap.put(employerForSelector.getEmployerId(), employerForSelector);
        }

        for (Map.Entry<Integer, EmployerForSelectorInTimeRecord> itemMap : employerMap.entrySet()) {
            employerForSelectorInTimeRecordListWithoutDuplicates.add(itemMap.getValue());
        }

        Collator primaryCollator = Collator.getInstance(new Locale("es","ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        return employerForSelectorInTimeRecordListWithoutDuplicates
                .stream()
                .sorted(Comparator.comparing(EmployerForSelectorInTimeRecord::getEmployerFullName, primaryCollator)).collect(Collectors.toList());
    }
}
