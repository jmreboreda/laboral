package laboral.domain.employer.manager;

import laboral.domain.client.Client;
import laboral.domain.employer.Employer;
import laboral.domain.employer.EmployerCreationRequest;
import laboral.domain.employer.mapper.MapperEmployerDBOToEmployer;
import laboral.domain.employer.persistence.dao.EmployerDAO;
import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.quote_account_code.QuoteAccountCodeService;
import laboral.domain.quote_account_code.persistence.dbo.QuoteAccountCodeDBO;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class EmployerManager {

    public EmployerManager() {
    }

    EmployerDAO employerDAO = EmployerDAO.EmployerDAOFactory.getInstance();

    public Integer createEmployer(EmployerCreationRequest employerCreationRequest){

        EmployerDBO employerDBO = EmployerDBO.EmployerDBOBuilder.create()
//                .withContracts(null)
                .withClientDBO(employerCreationRequest.getClientDBO())
                .withQuoteAccountCodes(null)
                .withId(null)
                .build();

        return employerDAO.create(employerDBO);
    }

    public Integer updateEmployer(Employer employer) {

        QuoteAccountCodeService quoteAccountCodeService = new QuoteAccountCodeService();

        EmployerDBO employerDBOToUpdate = employerDAO.getSession().load(EmployerDBO.class, employer.getId());

        Set<QuoteAccountCodeDBO> quoteAccountCodeDBOSet = new HashSet<>();
        for (QuoteAccountCode quoteAccountCode : employer.getQuoteAccountCodes()) {
            if (quoteAccountCode.getId() != null) {
                QuoteAccountCodeDBO persistedQuoteAccountCodeDBO = quoteAccountCodeService.findById(quoteAccountCode.getId());
                persistedQuoteAccountCodeDBO.setRegime(quoteAccountCode.getRegimeCode());
                persistedQuoteAccountCodeDBO.setProvince(quoteAccountCode.getProvince());
                persistedQuoteAccountCodeDBO.setQuoteAccountNumber(quoteAccountCode.getQuoteAccountCodeNumber());
                persistedQuoteAccountCodeDBO.setControlDigit(quoteAccountCode.getControlDigit());
                persistedQuoteAccountCodeDBO.setActivityEpigraphNumber(quoteAccountCode.getActivityEpigraphNumber());
                persistedQuoteAccountCodeDBO.setActivityDescription(quoteAccountCode.getActivityDescription());
                quoteAccountCodeDBOSet.add(persistedQuoteAccountCodeDBO);
            } else {
                QuoteAccountCodeDBO quoteAccountCodeDBO = QuoteAccountCodeDBO.QuoteAccountCodeDBOBuilder.create()
                        .withId(null)
                        .withRegime(quoteAccountCode.getRegimeCode())
                        .withProvince(quoteAccountCode.getProvince())
                        .withQuoteAccountNumber(quoteAccountCode.getQuoteAccountCodeNumber())
                        .withControlDigit(quoteAccountCode.getControlDigit())
                        .withActivityEpigraphNumber(quoteAccountCode.getActivityEpigraphNumber())
                        .withActivityDescription(quoteAccountCode.getActivityDescription())
                        .build();
                quoteAccountCodeDBOSet.add(quoteAccountCodeDBO);
            }
        }
        employerDBOToUpdate.setQuoteAccountCodes(quoteAccountCodeDBOSet);

        return employerDAO.update(employerDBOToUpdate);
    }

    public Employer findById(Integer employerId) {

        EmployerDBO employerDBO = employerDAO.findById(employerId);

        MapperEmployerDBOToEmployer mapperEmployerDBOToEmployer = new MapperEmployerDBOToEmployer();

        return mapperEmployerDBOToEmployer.map(employerDBO);
    }

    public List<Employer> findAll() {

        MapperEmployerDBOToEmployer mapperEmployerDBOToEmployer = new MapperEmployerDBOToEmployer();

        List<Employer> employerList = new ArrayList<>();
        List<EmployerDBO> employerDBOList = employerDAO.findAll();
        for (EmployerDBO employerDBO : employerDBOList) {
            Employer employer = mapperEmployerDBOToEmployer.map(employerDBO);
            employerList.add(employer);
        }

        return employerList;
    }

    public Employer findEmployerByClientId(Integer clientId) {

        EmployerDBO employerDBO = employerDAO.findByClientId(clientId);
        if (employerDBO == null) {
            return null;
        }

        MapperEmployerDBOToEmployer mapperEmployerDBOToEmployer = new MapperEmployerDBOToEmployer();
        return mapperEmployerDBOToEmployer.map(employerDAO.findByClientId(clientId));
    }

    public List<Employer> findActiveEmployerByNamePattern(String pattern) {

        List<EmployerDBO> allEmployerWithPatternDBOList = employerDAO.findActiveEmployerByPattern(pattern);

        List<Employer> employerList = new ArrayList<>();
        MapperEmployerDBOToEmployer mapperEmployerDBOToEmployer = new MapperEmployerDBOToEmployer();
        for (EmployerDBO employerDBO : allEmployerWithPatternDBOList) {
//            if(employerDBO.isActive()){
            Employer employer = mapperEmployerDBOToEmployer.map(employerDBO);
            employerList.add(employer);
//            }
        }

        return employerList;
    }

    public Boolean isEmployer(Client client){

        return employerDAO.findByClientId(client.getClientId()) != null
                ? Boolean.TRUE
                : Boolean.FALSE;
    }
}
