package laboral.domain.employer.controller;

import laboral.domain.client.Client;
import laboral.domain.employer.Employer;
import laboral.domain.employer.EmployerCreationRequest;
import laboral.domain.employer.manager.EmployerManager;

import java.util.List;

public class EmployerController {

    public EmployerController() {
    }

    EmployerManager employerManager = new EmployerManager();

    public Integer createEmployer(EmployerCreationRequest employerCreationRequest){
        return employerManager.createEmployer(employerCreationRequest);
    }

    public Integer updateEmployer(Employer employer){

        return employerManager.updateEmployer(employer);
    }

    public Employer findById(Integer employerId){

        return employerManager.findById(employerId);
    }

    public Employer findByClientId(Integer clientId){

        return employerManager.findEmployerByClientId(clientId);
    }

    public List<Employer> findAll(){

        return employerManager.findAll();
    }

    public List<Employer> findActiveEmployerByNamePattern(String pattern){

        return employerManager.findActiveEmployerByNamePattern(pattern);
    }
    public Boolean isEmployer(Client client){
        return employerManager.isEmployer(client);
    }
}
