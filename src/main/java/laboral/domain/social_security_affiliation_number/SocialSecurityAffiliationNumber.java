package laboral.domain.social_security_affiliation_number;

import java.util.regex.Pattern;

public class SocialSecurityAffiliationNumber {

    private String socialSecurityAffiliationNumber;
    private String province;
    private String affiliationNumber;
    private String controlDigit;

    public SocialSecurityAffiliationNumber() {
    }

    public SocialSecurityAffiliationNumber(String socialSecurityAffiliationNumber) {
        this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getAffiliationNumber() {
        return affiliationNumber;
    }

    public void setAffiliationNumber(String affiliationNumber) {
        this.affiliationNumber = affiliationNumber;
    }

    public String getControlDigit() {
        return controlDigit;
    }

    public void setControlDigit(String controlDigit) {
        this.controlDigit = controlDigit;
    }

    public String toString(){
//        return getProvince().concat(getAffiliationNumber()).concat(getControlDigit());
        return socialSecurityAffiliationNumber;
    }

    public String getSocialSecurityNumber(){
        return socialSecurityAffiliationNumber;
    }

    public Boolean isValid(){

        Pattern SSANPattern = Pattern.compile("\\d{12}");
        if(SSANPattern.matcher(socialSecurityAffiliationNumber).matches()){

            this.province = socialSecurityAffiliationNumber.substring(0, 2);
            this.affiliationNumber = socialSecurityAffiliationNumber.substring(2, 10);
            this.controlDigit = socialSecurityAffiliationNumber.substring(10);

            Long introducedProvince = Long.parseLong(getProvince());
            Long introducedAffiliationNumber = Long.parseLong(getAffiliationNumber());

            Long calculatedControlDigit;
            String calculatedControlDigitAsString;

            if (introducedProvince < 1 || (introducedProvince > 53 && introducedProvince != 66)) {
                return false;
            }

            if (introducedAffiliationNumber < 10000000) {
                calculatedControlDigit = (introducedProvince * 10000000 + introducedAffiliationNumber) % 97;

            } else {
                Long numberNASSWithoutControlDigit = Long.parseLong(introducedProvince.toString().concat(introducedAffiliationNumber.toString()));
                calculatedControlDigit = numberNASSWithoutControlDigit % 97;
            }

            if (calculatedControlDigit <= 9) {
                calculatedControlDigitAsString = "0".concat(calculatedControlDigit.toString());
            } else {
                calculatedControlDigitAsString = calculatedControlDigit.toString();
            }

            if (calculatedControlDigitAsString.equals(getControlDigit())) {
                return true;
            }
        }

        return false;
    }
}
