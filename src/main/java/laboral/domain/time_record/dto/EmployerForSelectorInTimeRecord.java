package laboral.domain.time_record.dto;


public class EmployerForSelectorInTimeRecord {

    private Integer employerId;
    private String employerFullName;

    public EmployerForSelectorInTimeRecord() {
    }

    public EmployerForSelectorInTimeRecord(Integer employerId, String employerFullName) {
        this.employerId = employerId;
        this.employerFullName = employerFullName;
    }

    public Integer getEmployerId() {
        return employerId;
    }

    public void setEmployerId(Integer employerId) {
        this.employerId = employerId;
    }

    public String getEmployerFullName() {
        return employerFullName;
    }

    public void setEmployerFullName(String employerFullName) {
        this.employerFullName = employerFullName;
    }

    @Override
    public String toString() {
        return getEmployerFullName();
    }

    public static final class EmployerForSelectorInTimeRecordBuilder {
        private Integer employerId;
        private String employerFullName;

        private EmployerForSelectorInTimeRecordBuilder() {
        }

        public static EmployerForSelectorInTimeRecordBuilder create() {
            return new EmployerForSelectorInTimeRecordBuilder();
        }

        public EmployerForSelectorInTimeRecordBuilder withEmployerId(Integer employerId) {
            this.employerId = employerId;
            return this;
        }

        public EmployerForSelectorInTimeRecordBuilder withEmployerFullName(String employerFullName) {
            this.employerFullName = employerFullName;
            return this;
        }

        public EmployerForSelectorInTimeRecord build() {
            EmployerForSelectorInTimeRecord employerForSelectorInTimeRecord = new EmployerForSelectorInTimeRecord();
            employerForSelectorInTimeRecord.setEmployerId(employerId);
            employerForSelectorInTimeRecord.setEmployerFullName(employerFullName);
            return employerForSelectorInTimeRecord;
        }
    }
}