package laboral.domain.client.controller;

import laboral.domain.client.Client;
import laboral.domain.client.ClientCreationRequest;
import laboral.domain.client.manager.ClientManager;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.Person;

import java.time.LocalDate;
import java.util.List;

public class ClientController {

    private ClientManager clientManager = new ClientManager();

    public ClientController() {
    }

    public Integer clientCreator(ClientCreationRequest clientCreationRequest){

        return clientManager.clientCreator(clientCreationRequest);
    }

    public Integer updateClient(Client client){

        return clientManager.updateClient(client);
    }

    public Client findClientById(Integer clientId){

        return clientManager.findById(clientId);
    }

    public Client findClientByPersonId(Integer personId){

        return clientManager.findClientByPersonId(personId);
    }

    public List<Client> findAll(){

        return clientManager.findAll();
    }

    public List<Client> findAllActiveClient(){

        return clientManager.findAllActiveClient();
    }

    public Client findClientByNieNif(NieNif nieNif){

        return clientManager.findClientByNieNif(nieNif);
    }
}
