package laboral.domain.client.manager;

import laboral.domain.activity_period.ActivityPeriod;
import laboral.domain.activity_period.mapper.MapperActivityPeriodToActivityPeriodDBO;
import laboral.domain.activity_period.persistence.dbo.ActivityPeriodDBO;
import laboral.domain.address.persistence.dao.AddressDAO;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.client.Client;
import laboral.domain.client.ClientCreationRequest;
import laboral.domain.client.mapper.MapperClientDBOToClient;
import laboral.domain.client.persistence.dao.ClientDAO;
import laboral.domain.client.persistence.dbo.ClientDBO;
import laboral.domain.contract.WorkContract;
import laboral.domain.contract.work_contract.WorkContractService;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.employer.Employer;
import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.Person;
import laboral.domain.person.persistence.dao.PersonDAO;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.service_gm.ServiceGM;
import laboral.domain.service_gm.mapper.MapperServiceGMToServiceGMDBO;
import laboral.domain.service_gm.persistence.dbo.ServiceGMDBO;
import laboral.domain.work_center.WorkCenter;
import laboral.domain.work_center.mapper.MapperWorkCenterToWorkCenterDBO;
import laboral.domain.work_center.persistence.dao.WorkCenterDAO;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;

import java.sql.Date;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ClientManager {

    private ClientDAO clientDAO = ClientDAO.ClientDAOFactory.getInstance();
    private PersonDAO personDAO = PersonDAO.PersonDAOFactory.getInstance();
    private AddressDAO addressDAO = AddressDAO.AddressDAOFactory.getInstance();

    public ClientManager() {
    }

    MapperClientDBOToClient mapperClientDBOToClient = new MapperClientDBOToClient();
    MapperActivityPeriodToActivityPeriodDBO mapperActivityPeriodToActivityPeriodDBO = new MapperActivityPeriodToActivityPeriodDBO();
    MapperServiceGMToServiceGMDBO mapperServiceGMServiceGMDBO = new MapperServiceGMToServiceGMDBO();
    MapperWorkCenterToWorkCenterDBO mapperWorkCenterToWorkCenterDBO = new MapperWorkCenterToWorkCenterDBO();

    public Integer clientCreator(ClientCreationRequest clientCreationRequest){

        PersonDBO personDBO = personDAO.findById(clientCreationRequest.getPersonId());
        Set<WorkCenterDBO> workCenterDBOSet = new HashSet<>();
        for(WorkCenter workCenter : clientCreationRequest.getWorkCenters()){
            AddressDBO addressDBO = addressDAO.findById(workCenter.getId());
            WorkCenterDBO workCenterDBO = new WorkCenterDBO();
            workCenterDBO.setContracts(null);
            workCenterDBO.setAddressDBO(addressDBO);

            workCenterDBOSet.add(workCenterDBO);
        }

        Set<ActivityPeriodDBO> activityPeriodDBOSet = new HashSet<>();
        for(ActivityPeriod activityPeriod : clientCreationRequest.getActivityPeriods()){
            Date dateTo = activityPeriod.getDateTo() == null ? null : Date.valueOf(activityPeriod.getDateTo());
            Date withoutActivityDate = activityPeriod.getWithoutActivityDate() == null ? null : Date.valueOf(activityPeriod.getWithoutActivityDate());

            ActivityPeriodDBO activityPeriodDBO = new ActivityPeriodDBO();
            activityPeriodDBO.setDateFrom(Date.valueOf(activityPeriod.getDateFrom()));
            activityPeriodDBO.setDateTo(dateTo);
            activityPeriodDBO.setWithoutActivityDate(withoutActivityDate);

            activityPeriodDBOSet.add(activityPeriodDBO);
        }

        Set<ServiceGMDBO> serviceGMDBOSet = new HashSet<>();
        for(ServiceGM serviceGM : clientCreationRequest.getServices()){
            ServiceGMDBO serviceGMDBO = mapperServiceGMServiceGMDBO.map(serviceGM);
            serviceGMDBOSet.add(serviceGMDBO);
        }

        ClientDBO clientDBO = ClientDBO.create()
                .withId(null)
                .withActivityPeriods(activityPeriodDBOSet)
                .withSg21Code(clientCreationRequest.getSg21Code())
                .withPersonDBO(personDBO)
                .withServices(serviceGMDBOSet)
                .withWorkCenters(workCenterDBOSet)
                .build();

        Integer clientId = clientDAO.create(clientDBO);

        clientDAO.getSession().refresh(clientDBO);

        Client client = findById(clientId);

        return updateClient(client);
    }

    public Integer updateClient(Client client){

        ClientDBO persistedClientDBOToUpdate = clientDAO.getSession().load(ClientDBO.class, client.getClientId());

        Set<ActivityPeriodDBO> activityPeriodDBOSet = new HashSet<>();
        for(ActivityPeriod activityPeriodInTable : client.getActivityPeriods()){
            activityPeriodInTable.setClient(persistedClientDBOToUpdate);
            ActivityPeriodDBO activityPeriodDBO = mapperActivityPeriodToActivityPeriodDBO.map(activityPeriodInTable);
            activityPeriodDBO.setClientDBO(persistedClientDBOToUpdate);
            activityPeriodDBOSet.add(activityPeriodDBO);
        }

        Set<ServiceGMDBO> serviceGMDBOSet = new HashSet<>();
        for(ServiceGM serviceGMInTable : client.getServices()){
            ServiceGMDBO serviceGMDBO = mapperServiceGMServiceGMDBO.map(serviceGMInTable);
            serviceGMDBO.setClientDBO(persistedClientDBOToUpdate);
            serviceGMDBOSet.add(serviceGMDBO);
        }

        Set<WorkCenterDBO> workCenterDBOSetToUpdate = persistedClientDBOToUpdate.getWorkCenters();
        workCenterDBOSetToUpdate.clear();
        for(WorkCenter workCenterInTable : client.getWorkCenters()){
            WorkCenterDBO workCenterDBO = mapperWorkCenterToWorkCenterDBO.map(workCenterInTable);
            workCenterDBO.setClientDBO(persistedClientDBOToUpdate);

            workCenterDBOSetToUpdate.add(workCenterDBO);
        }

        persistedClientDBOToUpdate.setSg21Code(client.getSg21Code());
        persistedClientDBOToUpdate.setActivityPeriods(activityPeriodDBOSet);
        persistedClientDBOToUpdate.setServices(serviceGMDBOSet);
        persistedClientDBOToUpdate.setWorkCenters(workCenterDBOSetToUpdate);

        return clientDAO.update(persistedClientDBOToUpdate);
    }

    public Client findById(Integer clientId){

        return mapperClientDBOToClient.map(clientDAO.findById(clientId));
    }

    public Client findClientByPersonId(Integer personId){

        ClientDBO clientDBO = clientDAO.findClientByPersonId(personId);
        if(clientDBO.getId() == null){
            return Client.ClientBuilder.create()
                    .build();
        }

        return mapperClientDBOToClient.map(clientDAO.findClientByPersonId(personId));
    }

    public List<Client> findAll(){

        List<Client> clientList = new ArrayList<>();

        List<ClientDBO> clientDBOList = clientDAO.findAll();
        for(ClientDBO clientDBO : clientDBOList){
            clientList.add(mapperClientDBOToClient.map(clientDBO));
        }

        return clientList;
    }

    public Client findClientByNieNif(NieNif nieNif){

        ClientDBO clientDBO = clientDAO.findClientByNieNif(nieNif);

        return mapperClientDBOToClient.map(clientDBO);
    }

    public List<Client> findAllActiveClient(){
        List<Client> clientList = new ArrayList<>();

        clientDAO = ClientDAO.ClientDAOFactory.getInstance();
        List<ClientDBO> activeClientDBO = clientDAO.findAllActiveClient();
        for(ClientDBO clientDBO : activeClientDBO){
            Client client = mapperClientDBOToClient.map(clientDBO);
            clientList.add(client);
        }

        return clientList;
    }

    public List<EmployerDBO> findAllEmployerWithWorkContractWithTimeRecordInMonth(LocalDate dateReceived){
        List<EmployerDBO> employerList = new ArrayList<>();

        WorkContractService workContractService = WorkContractService.WorkContractServiceFactory.getInstance();

        Integer yearReceived = dateReceived.getYear();
        Integer monthReceived = dateReceived.getMonth().getValue();

        LocalDate initialDate = LocalDate.of(yearReceived, monthReceived, 1);
        LocalDate finalDate = YearMonth.of(yearReceived, monthReceived).atEndOfMonth();

        List<WorkContractDBO> workContractList = workContractService.findAllWorkContractWithTimeRecordInMonth(initialDate, finalDate);
        for (WorkContractDBO workContractDBO : workContractList) {
            employerList.add(workContractDBO.getEmployerDBO());
//            LocalDate expectedEndDate = workContractDBO.getExpectedEndDate() != null ? workContractDBO.getExpectedEndDate() : null;
//            LocalDate modificationDate = workContractDBO.getModificationDate() != null ? workContractDBO.getModificationDate() : null;
//            LocalDate endingDate = workContractDBO.getEndDate() != null ? workContractDBO.getEndDate() : null;
//            ContractNewVersionDTO contractNewVersionDTO = ContractNewVersionDTO.create()
//                    .withId(workContractDBO.getId())
//                    .withContractNumber(workContractDBO.getContractNumber())
//                    .withStartDate(workContractDBO.getStartDate().toLocalDate())
//                    .withExpectedEndDate(expectedEndDate)
//                    .withModificationDate(modificationDate)
//                    .withEndingDate(endingDate)
//                    .withContractJsonData(workContractDBO.getContractJsonData())
//                    .build();
//
//            workContractList.add(contractNewVersionDTO);
        }

        return employerList;
    }
}
