package laboral.domain.client;

import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.quote_account_code.persistence.dbo.QuoteAccountCodeDBO;
import laboral.domain.service_gm.persistence.dbo.ServiceGMDBO;

import java.time.LocalDate;
import java.util.Set;


public class ClientDTO {

    private Integer id;
    private Integer clientId;
    private Boolean isNaturalPerson;
    private Boolean isLegalPerson;
    private Boolean isEntityWithoutLegalPersonality;
    private String nieNif;
    private String surNames;
    private String name;
    private String legalName;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private String sg21Code;
    private Boolean activeClient;
    private LocalDate withoutActivity;
    private Set<ServiceGMDBO> servicesGM;
    private Set<QuoteAccountCodeDBO> quoteAccountCodes;
    private Set<WorkContractDBO> workContracts;

    public ClientDTO() {
    }

    public ClientDTO(
            Integer id,
            Integer clientId,
            Boolean isNaturalPerson,
            Boolean isLegalPerson,
            Boolean isEntityWithoutLegalPersonality,
            String nieNif,
            String surNames,
            String name,
            String legalName,
            LocalDate dateFrom,
            LocalDate dateTo,
            String sg21Code,
            Boolean activeClient,
            LocalDate withoutActivity,
            Set<ServiceGMDBO> servicesGM,
            Set<QuoteAccountCodeDBO> quoteAccountCodes,
            Set<WorkContractDBO> workContracts) {
        this.id = id;
        this.clientId = clientId;
        this.isNaturalPerson = isNaturalPerson;
        this.isLegalPerson = isLegalPerson;
        this.isEntityWithoutLegalPersonality = isEntityWithoutLegalPersonality;
        this.nieNif = nieNif;
        this.surNames = surNames;
        this.name = name;
        this.legalName = legalName;
        this.sg21Code = sg21Code;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.activeClient = activeClient;
        this.withoutActivity = withoutActivity;
        this.servicesGM = servicesGM;
        this.quoteAccountCodes = quoteAccountCodes;
        this.workContracts = workContracts;
    }

    public Integer getId() {
        return id;
    }

    public Integer getClientId() {
        return clientId;
    }

    public Boolean getNaturalPerson() {
        return isNaturalPerson;
    }

    public Boolean getLegalPerson() {
        return isLegalPerson;
    }

    public Boolean getEntityWithoutLegalPersonality() {
        return isEntityWithoutLegalPersonality;
    }

    public String getNieNif() {
        return nieNif;
    }

    public String getSurNames() {
        return surNames;
    }

    public String getName() {
        return name;
    }

    public String getLegalName() {
        return legalName;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public String getSg21Code() {
        return sg21Code;
    }

    public Boolean getActiveClient() {
        return activeClient;
    }

    public LocalDate getWithoutActivity() {
        return withoutActivity;
    }

    public Set<ServiceGMDBO> getServicesGM() {
        return servicesGM;
    }

    public Set<QuoteAccountCodeDBO> getQuoteAccountCodes() {
        return quoteAccountCodes;
    }

    public Set<WorkContractDBO> getWorkContracts() {
        return workContracts;
    }

    public String toString() {
        if(isNaturalPerson){
            return getSurNames() + ", " + getName();
        }

        return getLegalName();
    }

    public String toNaturalName(){
        if(isNaturalPerson){
            return getName() + " " + getSurNames();
        }

        return getLegalName();
    }

    public Boolean isActiveClient(){
        if(getDateTo() == null){
            return true;
        }

        return false;
    }

    public static final class ClientDTOBuilder {
        private Integer id;
        private Integer clientId;
        private Boolean isNaturalPerson;
        private Boolean isLegalPerson;
        private Boolean isEntityWithoutLegalPersonality;
        private String nieNif;
        private String surNames;
        private String name;
        private String legalName;
        private LocalDate dateFrom;
        private LocalDate dateTo;
        private String sg21Code;
        private Boolean activeClient;
        private LocalDate withoutActivity;
        private Set<ServiceGMDBO> servicesGM;
        private Set<QuoteAccountCodeDBO> quoteAccountCodes;
        private Set<WorkContractDBO> workContracts;

        private ClientDTOBuilder() {
        }

        public static ClientDTOBuilder create() {
            return new ClientDTOBuilder();
        }

        public ClientDTOBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public ClientDTOBuilder withClientId(Integer clientId) {
            this.clientId = clientId;
            return this;
        }

        public ClientDTOBuilder withIsNaturalPerson(Boolean isNaturalPerson) {
            this.isNaturalPerson = isNaturalPerson;
            return this;
        }

        public ClientDTOBuilder withIsLegalPerson(Boolean isLegalPerson) {
            this.isLegalPerson = isLegalPerson;
            return this;
        }

        public ClientDTOBuilder withIsEntityWithoutLegalPersonality(Boolean isEntityWithoutLegalPersonality) {
            this.isEntityWithoutLegalPersonality = isEntityWithoutLegalPersonality;
            return this;
        }

        public ClientDTOBuilder withNieNif(String nieNif) {
            this.nieNif = nieNif;
            return this;
        }

        public ClientDTOBuilder withSurNames(String surNames) {
            this.surNames = surNames;
            return this;
        }

        public ClientDTOBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public ClientDTOBuilder withLegalName(String legalName) {
            this.legalName = legalName;
            return this;
        }

        public ClientDTOBuilder withDateFrom(LocalDate dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        public ClientDTOBuilder withDateTo(LocalDate dateTo) {
            this.dateTo = dateTo;
            return this;
        }

        public ClientDTOBuilder withSg21Code(String sg21Code) {
            this.sg21Code = sg21Code;
            return this;
        }

        public ClientDTOBuilder withActiveClient(Boolean activeClient) {
            this.activeClient = activeClient;
            return this;
        }

        public ClientDTOBuilder withWithoutActivity(LocalDate withoutActivity) {
            this.withoutActivity = withoutActivity;
            return this;
        }

        public ClientDTOBuilder withServicesGM(Set<ServiceGMDBO> servicesGM) {
            this.servicesGM = servicesGM;
            return this;
        }

        public ClientDTOBuilder withQuoteAccountCodes(Set<QuoteAccountCodeDBO> quoteAccountCodes) {
            this.quoteAccountCodes = quoteAccountCodes;
            return this;
        }

        public ClientDTOBuilder withWorkContracts(Set<WorkContractDBO> workContracts) {
            this.workContracts = workContracts;
            return this;
        }

        public ClientDTO build() {
            return new ClientDTO(id, clientId, isNaturalPerson, isLegalPerson, isEntityWithoutLegalPersonality, nieNif, surNames, name, legalName, dateFrom, dateTo, sg21Code, activeClient, withoutActivity, servicesGM, quoteAccountCodes, workContracts);
        }
    }
}