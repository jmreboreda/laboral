package laboral.domain.client;

import laboral.domain.client.controller.ClientController;
import laboral.domain.client.persistence.dao.ClientDAO;
import laboral.domain.client.persistence.dbo.ClientDBO;
import laboral.domain.employer.EmployerService;
import laboral.domain.service_gm.ServiceGM;
import laboral.domain.service_gm.ServiceGMService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ClientService {

    private ClientController clientController = new ClientController();
    private ClientDAO clientDAO = ClientDAO.ClientDAOFactory.getInstance();

    private ClientService() {
    }

    public static class ClientServiceFactory {

        private static ClientService clientService;

        public static ClientService getInstance() {
            if(clientService == null) {
                clientService = new ClientService();
            }
            return clientService;
        }
    }

    private static final String SERVICE_FOR_WORK_CONTRACTS = "Asesoría";



    public Client findClientById(Integer clientId){

        return clientController.findClientById(clientId);
    }

    public ClientDBO findClientDBOById(Integer id){

        return clientDAO.findById(id);
    }
//    public List<PersistedClient> findAllActiveClient(){
//
//        return clientController.findAllActiveClient();
//    }

//    public List<PersistedClient> findAllActiveClientWithContractHistory(){
//
//        return clientController.findAllActiveClientWithContractHistory();
//    }
//
//    public  List<PersistedClient> findAllClientWithContractInForceAtDate(LocalDate date){
//
//        return clientController.findAllClientWithContractInForceAtDate(date);
//    }

    public List<Client> findAllClientGMWithInvoicesToClaimInPeriod(LocalDate periodInitialDate, LocalDate periodFinalDate){
        List<Client> clientList = new ArrayList<>();

        List<ServiceGM> serviceGMList = ServiceGMService.findAllClientGMWithInvoicesToClaimInPeriod(periodInitialDate, periodFinalDate);
        for(ServiceGM serviceGM : serviceGMList){
            Integer clientId = serviceGM.getClient().getId();
            Client client = findClientById(clientId);
            clientList.add(client);
        }

        return clientList;
    }

    public Boolean isEmployer(Client client){
        EmployerService employerService = EmployerService.EmployerServiceFactory.getInstance();

        return employerService.isEmployer(client);

    }

//    public List<PersistedClient> findAllActiveClientWithAdvisoryServicesByNamePatternInAlphabeticalOrder(String pattern){
//
//        List<PersistedClient> clientDTOListWithAdvisoryServicesByNamePattern = new ArrayList<>();
//
//        ClientController clientController = new ClientController();
//        List<PersistedClient> clientDTOList = clientController.findAllActiveClientByNamePatternInAlphabeticalOrder(pattern);
//        for(PersistedClient clientDTO : clientDTOList) {
//            Set<PersistedServiceGM> serviceGMDTOSet = clientDTO.getServicesGM();
//            for (PersistedServiceGM serviceGMVO : serviceGMDTOSet) {
//                if (serviceGMVO.getService().contains(SERVICE_FOR_WORK_CONTRACTS)) {
//                    clientDTOListWithAdvisoryServicesByNamePattern.add(clientDTO);
//                }
//            }
//        }
//
//        return clientDTOListWithAdvisoryServicesByNamePattern;
//    }
}
