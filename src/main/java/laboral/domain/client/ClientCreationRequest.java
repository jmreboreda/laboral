package laboral.domain.client;

import laboral.domain.activity_period.ActivityPeriod;
import laboral.domain.address.Address;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.Person;
import laboral.domain.person_type.PersonType;
import laboral.domain.service_gm.ServiceGM;
import laboral.domain.work_center.WorkCenter;

import java.time.LocalDate;
import java.util.Set;

public class ClientCreationRequest extends Person {

    LocalDate dateFrom;
    LocalDate dateTo;
    LocalDate withoutActivity;
    String sg21Code;
    Set<ActivityPeriod> activityPeriods;
    Set<ServiceGM> services;
    Set<WorkCenter> workCenters;

    public ClientCreationRequest(Integer personId,
                                PersonType personType,
                                NieNif nieNif,
                                Set<Address> addresses,
                                LocalDate dateFrom,
                                LocalDate dateTo,
                                LocalDate withoutActivity,
                                String sg21Code,
                                Set<ActivityPeriod> activityPeriods,
                                Set<ServiceGM> services,
                                Set<WorkCenter> workCenters) {
        super(personId, personType, nieNif, addresses);

        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.withoutActivity = withoutActivity;
        this.sg21Code = sg21Code;
        this.activityPeriods = activityPeriods;
        this.services = services;
        this.workCenters = workCenters;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public LocalDate getWithoutActivity() {
        return withoutActivity;
    }

    public void setWithoutActivity(LocalDate withoutActivity) {
        this.withoutActivity = withoutActivity;
    }

    public String getSg21Code() {
        return sg21Code;
    }

    public void setSg21Code(String sg21Code) {
        this.sg21Code = sg21Code;
    }

    public Set<ActivityPeriod> getActivityPeriods() {
        return activityPeriods;
    }

    public void setActivityPeriods(Set<ActivityPeriod> activityPeriods) {
        this.activityPeriods = activityPeriods;
    }

    public Set<ServiceGM> getServices() {
        return services;
    }

    public void setServices(Set<ServiceGM> services) {
        this.services = services;
    }

    public Set<WorkCenter> getWorkCenters() {
        return workCenters;
    }

    public void setWorkCenters(Set<WorkCenter> workCenters) {
        this.workCenters = workCenters;
    }

    public static final class ClientCreationRequestBuilder {
        private LocalDate dateFrom;
        private LocalDate dateTo;
        private LocalDate withoutActivity;
        private String sg21Code;
        private Set<ActivityPeriod> activityPeriods;
        private Set<ServiceGM> services;
        private Set<WorkCenter> workCenters;
        private Integer personId;
        private PersonType personType;
        private NieNif nieNif;
        private Set<Address> addresses;

        private ClientCreationRequestBuilder() {
        }

        public static ClientCreationRequestBuilder create() {
            return new ClientCreationRequestBuilder();
        }

        public ClientCreationRequestBuilder withDateFrom(LocalDate dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        public ClientCreationRequestBuilder withDateTo(LocalDate dateTo) {
            this.dateTo = dateTo;
            return this;
        }

        public ClientCreationRequestBuilder withWithoutActivity(LocalDate withoutActivity) {
            this.withoutActivity = withoutActivity;
            return this;
        }

        public ClientCreationRequestBuilder withSg21Code(String sg21Code) {
            this.sg21Code = sg21Code;
            return this;
        }

        public ClientCreationRequestBuilder withActivityPeriods(Set<ActivityPeriod> activityPeriods) {
            this.activityPeriods = activityPeriods;
            return this;
        }

        public ClientCreationRequestBuilder withServices(Set<ServiceGM> services) {
            this.services = services;
            return this;
        }

        public ClientCreationRequestBuilder withWorkCenters(Set<WorkCenter> workCenters) {
            this.workCenters = workCenters;
            return this;
        }

        public ClientCreationRequestBuilder withPersonId(Integer personId) {
            this.personId = personId;
            return this;
        }

        public ClientCreationRequestBuilder withPersonType(PersonType personType) {
            this.personType = personType;
            return this;
        }

        public ClientCreationRequestBuilder withNieNif(NieNif nieNif) {
            this.nieNif = nieNif;
            return this;
        }

        public ClientCreationRequestBuilder withAddresses(Set<Address> addresses) {
            this.addresses = addresses;
            return this;
        }

        public ClientCreationRequest build() {
            return new ClientCreationRequest(personId, personType, nieNif, addresses, dateFrom, dateTo, withoutActivity, sg21Code, activityPeriods, services, workCenters);
        }
    }
}
