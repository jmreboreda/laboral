package laboral.domain.client.persistence.dbo;

import laboral.domain.activity_period.persistence.dbo.ActivityPeriodDBO;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.person_type.PersonType;
import laboral.domain.service_gm.persistence.dbo.ServiceGMDBO;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

@Entity
//@Table(name = "client", uniqueConstraints = {@UniqueConstraint(columnNames = {"clientId", "clientType"})})
@Table(name = "client")
@NamedQueries(value = {
        @NamedQuery(
                name = ClientDBO.FIND_ALL_CLIENT,
                query = "select p from ClientDBO p order by p.id"
        ),
        @NamedQuery(
                name = ClientDBO.FIND_CLIENT_BY_ID,
                query = "select p from ClientDBO as p where p.id = :clientId"
        ),
        @NamedQuery(
                name = ClientDBO.FIND_CLIENT_BY_NIENIF,
                query = "select p from ClientDBO as p where p.personDBO.nieNif = :nieNif"
        ),
        @NamedQuery(
                name = ClientDBO.FIND_CLIENT_BY_PERSON_ID,
                query = "select p from ClientDBO as p where p.personDBO.id = :personId"
        )
})

public class ClientDBO implements Serializable {

    public static final String FIND_ALL_ACTIVE_CLIENT = "ClientDBO.FIND_ALL_ACTIVE_CLIENT";
    public static final String FIND_ALL_CLIENT = "ClientDBO.FIND_ALL_CLIENT";
    public static final String FIND_CLIENT_BY_ID = "ClientDBO.FIND_CLIENT_BY_ID";
    public static final String FIND_CLIENT_BY_NIENIF = "ClientDBO.FIND_CLIENT_BY_NIENIF";
    public static final String FIND_CLIENT_BY_PERSON_ID = "ClientDBO.FIND_CLIENT_BY_PERSON_ID";

    @Id
    @SequenceGenerator(name = "client_id_seq", sequenceName = "client_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;

    @Column(length = 4)
    private String sg21Code;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "personId")
    private PersonDBO personDBO;

    @OneToMany(fetch = FetchType.EAGER, cascade= CascadeType.ALL)
    @JoinColumn(name = "clientId")
    private Set<ServiceGMDBO> services;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "clientId")
    private Set<ActivityPeriodDBO> activityPeriods;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "clientId")
    private Set<WorkCenterDBO> workCenters;

    public ClientDBO() {
    }

    public ClientDBO(Integer id,
                     String sg21Code,
                     PersonDBO personDBO,
                     Set<ServiceGMDBO> services,
                     Set<ActivityPeriodDBO> activityPeriods,
                     Set<WorkCenterDBO> workCenters) {
        this.id = id;
        this.sg21Code = sg21Code;
        this.personDBO = personDBO;
        this.services = services;
        this.activityPeriods = activityPeriods;
        this.workCenters = workCenters;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PersonDBO getPersonDBO() {
        return personDBO;
    }

    public void setPersonDBO(PersonDBO personDBO) {
        this.personDBO = personDBO;
    }

    public String getSg21Code() {
        return sg21Code;
    }

    public void setSg21Code(String sg21Code) {
        this.sg21Code = sg21Code;
    }

    public Set<ServiceGMDBO> getServices() {
        return services;
    }

    public void setServices(Set<ServiceGMDBO> services) {
        this.services = services;
    }

    public Set<ActivityPeriodDBO> getActivityPeriods() {
        return activityPeriods;
    }

    public void setActivityPeriods(Set<ActivityPeriodDBO> activityPeriods) {
        this.activityPeriods = activityPeriods;
    }

    public Set<WorkCenterDBO> getWorkCenters() {
        return workCenters;
    }

    public void setWorkCenters(Set<WorkCenterDBO> workCenters) {
        this.workCenters = workCenters;
    }

    public String toString(){
        return toAlphabeticalName();
    }

    public String toAlphabeticalName(){
        if(personDBO.getPersonType().equals(PersonType.NATURAL_PERSON)) {
            return personDBO.getFirstSurname().concat(" ")
                    .concat(personDBO.getSecondSurname()).concat(", ")
                    .concat(personDBO.getName());
        }

        return personDBO.getLegalName();
    }

    public String toNaturalName(){
        if(personDBO.getPersonType().equals(PersonType.NATURAL_PERSON)) {
            return personDBO.getName().concat(" ")
                    .concat(personDBO.getFirstSurname()).concat(" ")
                    .concat(personDBO.getSecondSurname());
        }

        return personDBO.getLegalName();
    }

    public Boolean isActiveAtDate(LocalDate date){
        for(ServiceGMDBO serviceGMDBO : getServices()){
            if(serviceGMDBO.isActiveAtDate(date)){

                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public static ClientDBO.ClientDBOBuilder create() {
        return new ClientDBO.ClientDBOBuilder();
    }


    public static class ClientDBOBuilder {

        private Integer id;
        private String sg21Code;
        private PersonDBO personDBO;
        private Set<ServiceGMDBO> services;
        private Set<ActivityPeriodDBO> activityPeriods;
        private Set<WorkCenterDBO> workCenters;

        public ClientDBO.ClientDBOBuilder withId(Integer id) {
            this.id = id;
            return this;
        }
        
        public ClientDBO.ClientDBOBuilder withSg21Code(String sg21Code) {
            this.sg21Code = sg21Code;
            return this;
        }

        public ClientDBO.ClientDBOBuilder withPersonDBO(PersonDBO personDBO) {
            this.personDBO = personDBO;
            return this;
        }

        public ClientDBO.ClientDBOBuilder withServices(Set<ServiceGMDBO> services) {
            this.services = services;
            return this;
        }

        public ClientDBO.ClientDBOBuilder withActivityPeriods(Set<ActivityPeriodDBO> activityPeriods) {
            this.activityPeriods = activityPeriods;
            return this;
        }

        public ClientDBO.ClientDBOBuilder withWorkCenters(Set<WorkCenterDBO> workCenters) {
            this.workCenters = workCenters;
            return this;
        }

        public ClientDBO build() {
            return new ClientDBO(this.id, this.sg21Code, this.personDBO, this.services, this.activityPeriods, this.workCenters);
        }
    }
}

