package laboral.domain.client.persistence.dao;

import laboral.domain.client.persistence.dbo.ClientDBO;
import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.nienif.NieNif;
import laboral.domain.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

public class ClientDAO implements GenericDAO<ClientDBO, Integer> {

    private SessionFactory sessionFactory;
    private Session session;

    public ClientDAO() {
    }

    public static class ClientDAOFactory {

        private static ClientDAO clientDAO;

        public static ClientDAO getInstance() {
            if(clientDAO == null) {
                clientDAO = new ClientDAO(HibernateUtil.retrieveGlobalSession());
            }
            return clientDAO;
        }

    }

    public ClientDAO(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    public Integer create(ClientDBO clientDBO) {
        try {
            session.beginTransaction();
            session.saveOrUpdate(clientDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }
        return clientDBO.getId();
    }

    @Override
    public ClientDBO findById(Integer clientId) {
        TypedQuery<ClientDBO> query = session.createNamedQuery(ClientDBO.FIND_CLIENT_BY_ID, ClientDBO.class);
        query.setParameter("clientId", clientId);

        return query.getSingleResult();
    }

    @Override
    public Integer update(ClientDBO clientDBO) {
        try {
            session.beginTransaction();
            session.merge(clientDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }

        return clientDBO.getId();
    }

    @Override
    public Boolean delete(ClientDBO clientDBO) {

        return null;
    }

    @Override
    public List<ClientDBO> findAll() {
        TypedQuery<ClientDBO> query = session.createNamedQuery(ClientDBO.FIND_ALL_CLIENT, ClientDBO.class);

        return query.getResultList();
    }

    public ClientDBO findClientByPersonId(Integer personId){
        ClientDBO clientDBORead = null;
        TypedQuery<ClientDBO> query = null;
        try {
            query = session.createNamedQuery(ClientDBO.FIND_CLIENT_BY_PERSON_ID, ClientDBO.class);
            query.setParameter("personId", personId);

            clientDBORead = query.getSingleResult();

        }catch(NoResultException nre){

        }

        if(clientDBORead == null){

            return new ClientDBO();
            }

        return query.getSingleResult();
    }

    public ClientDBO findClientByNieNif(NieNif nieNif){
        TypedQuery<ClientDBO> query = session.createNamedQuery(ClientDBO.FIND_CLIENT_BY_NIENIF, ClientDBO.class);
        query.setParameter("nieNif", nieNif.getNif());


        return query.getSingleResult();
    }

    public List<ClientDBO> findAllActiveClient(){
        TypedQuery<ClientDBO> query = session.createNamedQuery(ClientDBO.FIND_ALL_ACTIVE_CLIENT, ClientDBO.class);

        return query.getResultList();
    }
}
