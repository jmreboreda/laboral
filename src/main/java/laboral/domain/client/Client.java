package laboral.domain.client;

import laboral.domain.activity_period.ActivityPeriod;
import laboral.domain.address.Address;
import laboral.domain.person.dto.PersonDTO;
import laboral.domain.service_gm.ServiceGM;
import laboral.domain.work_center.WorkCenter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

public class Client {

    private Integer clientId;
    private String sg21Code;
    private Set<ActivityPeriod> activityPeriods;
    private Set<ServiceGM> services;
    private Set<WorkCenter> workCenters;
    private PersonDTO person;

    public Client(Integer clientId,
                  String sg21Code,
                  Set<ActivityPeriod> activityPeriods,
                  Set<ServiceGM> services,
                  Set<WorkCenter> workCenters,
                  PersonDTO person) {
        this.clientId = clientId;
        this.sg21Code = sg21Code;
        this.activityPeriods = activityPeriods;
        this.services = services;
        this.workCenters = workCenters;
        this.person = person;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public void setSg21Code(String sg21Code) {
        this.sg21Code = sg21Code;
    }

    public void setActivityPeriods(Set<ActivityPeriod> activityPeriods) {
        this.activityPeriods = activityPeriods;
    }

    public void setServices(Set<ServiceGM> services) {
        this.services = services;
    }

    public void setWorkCenters(Set<WorkCenter> workCenters) {
        this.workCenters = workCenters;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    public Integer getClientId() {
        return clientId;
    }

    public String getSg21Code() {
        return sg21Code;
    }

    public Set<ActivityPeriod> getActivityPeriods() {
        return activityPeriods;
    }

    public Set<ServiceGM> getServices() {
        return services;
    }

    public Set<WorkCenter> getWorkCenters() {
        return workCenters;
    }

    public PersonDTO getPerson() {
        return person;
    }

    public List<Address> getAddressesOfWorkCenters(){
        List<Address> addressOfWorkCentersList = new ArrayList<>();
        for(WorkCenter workCenter : getWorkCenters()){
            Address address = workCenter.getAddress();
            addressOfWorkCentersList.add(address);
        }

        Comparator<Address> addressId = Comparator.comparing(Address::getId);
        addressOfWorkCentersList.sort(addressId);

        return addressOfWorkCentersList;
    }

    public String toString(){
        return toAlphabeticalName();
    }

    public String toAlphabeticalName(){

        return person.toAlphabeticalName();
    }

    public static final class ClientBuilder {
        private Integer clientId;
        private String sg21Code;
        private Set<ActivityPeriod> activityPeriods;
        private Set<ServiceGM> services;
        private Set<WorkCenter> workCenters;
        private PersonDTO person;

        private ClientBuilder() {
        }

        public static ClientBuilder create() {
            return new ClientBuilder();
        }

        public ClientBuilder withClientId(Integer clientId) {
            this.clientId = clientId;
            return this;
        }

        public ClientBuilder withSg21Code(String sg21Code) {
            this.sg21Code = sg21Code;
            return this;
        }

        public ClientBuilder withActivityPeriods(Set<ActivityPeriod> activityPeriods) {
            this.activityPeriods = activityPeriods;
            return this;
        }

        public ClientBuilder withServices(Set<ServiceGM> services) {
            this.services = services;
            return this;
        }

        public ClientBuilder withWorkCenters(Set<WorkCenter> workCenters) {
            this.workCenters = workCenters;
            return this;
        }

        public ClientBuilder withPerson(PersonDTO person) {
            this.person = person;
            return this;
        }

        public Client build() {
            return new Client(clientId, sg21Code, activityPeriods, services, workCenters, person);
        }
    }
}
