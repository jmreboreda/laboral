package laboral.domain.contract_variation_for_table_view;

import java.time.Duration;
import java.time.LocalDate;

public class WorkContractVariationDataForTableView {

    private Integer id;
    private Integer variationTypeCode;
    private String variationDescription;
    private LocalDate startDate;
    private LocalDate expectedEndDate;
    private LocalDate modificationDate;
    private LocalDate endingDate;
    private Duration hoursWorkWeek;

    public WorkContractVariationDataForTableView(Integer id,
                                                 Integer variationTypeCode,
                                                 String variationDescription,
                                                 LocalDate startDate,
                                                 LocalDate expectedEndDate,
                                                 LocalDate modificationDate,
                                                 LocalDate endingDate,
                                                 Duration hoursWorkWeek) {
        this.id = id;
        this.variationTypeCode = variationTypeCode;
        this.variationDescription = variationDescription;
        this.startDate = startDate;
        this.expectedEndDate = expectedEndDate;
        this.modificationDate = modificationDate;
        this.endingDate = endingDate;
        this.hoursWorkWeek = hoursWorkWeek;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVariationTypeCode() {
        return variationTypeCode;
    }

    public void setVariationTypeCode(Integer variationTypeCode) {
        this.variationTypeCode = variationTypeCode;
    }

    public String getVariationDescription() {
        return variationDescription;
    }

    public void setVariationDescription(String variationDescription) {
        this.variationDescription = variationDescription;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(LocalDate expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
    }

    public LocalDate getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(LocalDate endingDate) {
        this.endingDate = endingDate;
    }

    public Duration getHoursWorkWeek() {
        return hoursWorkWeek;
    }

    public void setHoursWorkWeek(Duration hoursWorkWeek) {
        this.hoursWorkWeek = hoursWorkWeek;
    }


    public static final class WorkContractVariationBuilder {
        private Integer id;
        private Integer variationTypeCode;
        private String variationDescription;
        private LocalDate startDate;
        private LocalDate expectedEndDate;
        private LocalDate modificationDate;
        private LocalDate endingDate;
        private Duration hoursWorkWeek;

        private WorkContractVariationBuilder() {
        }

        public static WorkContractVariationBuilder create() {
            return new WorkContractVariationBuilder();
        }

        public WorkContractVariationBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public WorkContractVariationBuilder withVariationTypeCode(Integer variationTypeCode) {
            this.variationTypeCode = variationTypeCode;
            return this;
        }

        public WorkContractVariationBuilder withVariationDescription(String variationDescription) {
            this.variationDescription = variationDescription;
            return this;
        }

        public WorkContractVariationBuilder withStartDate(LocalDate startDate) {
            this.startDate = startDate;
            return this;
        }

        public WorkContractVariationBuilder withExpectedEndDate(LocalDate expectedEndDate) {
            this.expectedEndDate = expectedEndDate;
            return this;
        }

        public WorkContractVariationBuilder withModificationDate(LocalDate modificationDate) {
            this.modificationDate = modificationDate;
            return this;
        }

        public WorkContractVariationBuilder withEndingDate(LocalDate endingDate) {
            this.endingDate = endingDate;
            return this;
        }

        public WorkContractVariationBuilder withHoursWorkWeek(Duration hoursWorkWeek) {
            this.hoursWorkWeek = hoursWorkWeek;
            return this;
        }

        public WorkContractVariationBuilder but() {
            return create().withId(id).withVariationTypeCode(variationTypeCode).withVariationDescription(variationDescription).withStartDate(startDate).withExpectedEndDate(expectedEndDate).withModificationDate(modificationDate).withEndingDate(endingDate).withHoursWorkWeek(hoursWorkWeek);
        }

        public WorkContractVariationDataForTableView build() {
            return new WorkContractVariationDataForTableView(id, variationTypeCode, variationDescription, startDate, expectedEndDate, modificationDate, endingDate, hoursWorkWeek);
        }
    }
}
