package laboral.domain.activity_period;

import laboral.domain.client.Client;
import laboral.domain.client.persistence.dbo.ClientDBO;

import java.time.LocalDate;

public class PersistedActivityPeriod extends ActivityPeriod{

    private Integer id;
    private Integer clientId;

    public PersistedActivityPeriod() {
    }

    public PersistedActivityPeriod(Integer id, Integer clientId, LocalDate dateFrom, LocalDate dateTo, LocalDate withoutActivityDate, ClientDBO client) {
        super(id, dateFrom, dateTo, withoutActivityDate, client);
        this.id = id;
        this.clientId = clientId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    @Override
    public LocalDate getDateFrom() {
        return super.getDateFrom();
    }

    @Override
    public void setDateFrom(LocalDate dateFrom) {
        super.setDateFrom(dateFrom);
    }

    @Override
    public LocalDate getDateTo() {
        return super.getDateTo();
    }

    @Override
    public void setDateTo(LocalDate dateTo) {
        super.setDateTo(dateTo);
    }

    @Override
    public LocalDate getWithoutActivityDate() {
        return super.getWithoutActivityDate();
    }

    @Override
    public void setWithoutActivityDate(LocalDate withoutActivityDate) {
        super.setWithoutActivityDate(withoutActivityDate);
    }
}
