package laboral.domain.activity_period;

import laboral.domain.client.Client;
import laboral.domain.client.persistence.dbo.ClientDBO;

import java.time.LocalDate;
import java.util.List;

public class ActivityPeriod {

//    List<PersistedActivityPeriod> persistedActivityPeriodList;

    private Integer id;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private LocalDate withoutActivityDate;
    private ClientDBO client;

    public ActivityPeriod() {
    }

    public ActivityPeriod(Integer id, LocalDate dateFrom, LocalDate dateTo, LocalDate withoutActivityDate, ClientDBO client) {
        if(dateFrom != null && dateTo != null && dateFrom.isBefore(dateTo)) {
            System.out.println("New Activity Period: error -> dateTo < dateFrom");
        }
        this.id = id;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.withoutActivityDate = withoutActivityDate;
        this.client = client;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public LocalDate getWithoutActivityDate() {
        return withoutActivityDate;
    }

    public void setWithoutActivityDate(LocalDate withoutActivityDate) {
        this.withoutActivityDate = withoutActivityDate;
    }

    public ClientDBO getClient() {
        return client;
    }

    public void setClient(ClientDBO client) {
        this.client = client;
    }

    //    public Boolean validateActivityPeriodDataEntry( List<PersistedActivityPeriod> persistedActivityPeriodList, Stage stage) {
//
//        this.persistedActivityPeriodList = persistedActivityPeriodList;
//
//        for(int counter = 0; counter < persistedActivityPeriodList.size(); counter++){
//            if(persistedActivityPeriodList.get(counter).getDateFrom() == null &&  persistedActivityPeriodList.get(counter).getDateTo() == null ||
//                    persistedActivityPeriodList.get(counter).getDateFrom() == null &&  persistedActivityPeriodList.get(counter).getDateTo() != null ){
//                Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.THERE_ARE_ANY_PERIOD_OF_ACTIVITY_WITHOUT_DATES);
//
//                return Boolean.FALSE;
//            }
//        }
//
//        Integer activityPeriodsOpen = 0;
//        for (PersistedActivityPeriod persistedActivityPeriod : persistedActivityPeriodList) {
//            if (persistedActivityPeriod.getDateTo() == null && persistedActivityPeriod.getWithoutActivityDate() == null ||
//                    persistedActivityPeriod.getDateTo() == null && persistedActivityPeriod.getWithoutActivityDate() != null) {
//                activityPeriodsOpen++;
//            }
//
//            if (activityPeriodsOpen > 1) {
//                Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.TOO_MANY_OPEN_ACTIVITY_PERIODS);
//
//                return Boolean.FALSE;
//            }
//        }
//
//        for (PersistedActivityPeriod persistedActivityPeriod : persistedActivityPeriodList) {
//            if (persistedActivityPeriod.getDateTo() != null && persistedActivityPeriod.getDateFrom() != null &&
//                    persistedActivityPeriod.getDateTo().isBefore(persistedActivityPeriod.getDateFrom()) ||
//                    persistedActivityPeriod.getDateTo() != null && persistedActivityPeriod.getWithoutActivityDate() != null &&
//                            persistedActivityPeriod.getDateTo().isBefore(persistedActivityPeriod.getWithoutActivityDate()) ||
//                    persistedActivityPeriod.getDateFrom() != null && persistedActivityPeriod.getWithoutActivityDate() != null &&
//                            persistedActivityPeriod.getWithoutActivityDate().isBefore(persistedActivityPeriod.getDateFrom())
//            ) {
//                Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.THERE_ARE_INCONSISTENT_DATES_IN_ANY_PERIOD_OF_ACTIVITY);
//
//                return Boolean.FALSE;
//            }
//        }
//
//        if(persistedActivityPeriodList.size() > 1) {
//            for (int counter = 1; counter < persistedActivityPeriodList.size(); counter++) {
//                if(persistedActivityPeriodList.get(counter).getDateFrom().isBefore(persistedActivityPeriodList.get(counter-1).getDateFrom()) ||
//                        persistedActivityPeriodList.get(counter-1).getDateTo() != null && persistedActivityPeriodList.get(counter).getDateFrom().isBefore(persistedActivityPeriodList.get(counter-1).getDateTo())){
//                    Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.THERE_ARE_INCONSISTENT_DATES_IN_ANY_PERIOD_OF_ACTIVITY);
//
//                    return Boolean.FALSE;
//                }
//            }
//        }
//
//        return Boolean.TRUE;
//    }
}
