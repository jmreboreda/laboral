package laboral.domain.activity_period.persistence.dao;


import laboral.domain.activity_period.persistence.dbo.ActivityPeriodDBO;
import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.service_gm.persistence.dbo.ServiceGMDBO;
import laboral.domain.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.List;

public class ActivityPeriodDAO implements GenericDAO<ActivityPeriodDBO, Integer> {

    private SessionFactory sessionFactory;
    private Session session;

    public ActivityPeriodDAO() {
    }

    public static class ActivityPeriodDBODAOFactory {

        private static ActivityPeriodDAO activityPeriodDAO;

        public static ActivityPeriodDAO getInstance() {
            if(activityPeriodDAO == null) {
                activityPeriodDAO = new ActivityPeriodDAO(HibernateUtil.retrieveGlobalSession());
            }
            return activityPeriodDAO;
        }

    }

    public ActivityPeriodDAO(Session session) {
        this.session = session;
    }

    @Override
    public Integer create(ActivityPeriodDBO activityPeriodDBO) {
        try {
            session.beginTransaction();
            session.saveOrUpdate(activityPeriodDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }
        return activityPeriodDBO.getId();    }

    @Override
    public ActivityPeriodDBO findById(Integer integer) {
        return null;
    }

    @Override
    public Integer update(ActivityPeriodDBO entity) {

        return null;
    }

    @Override
    public Boolean delete(ActivityPeriodDBO serviceGMDBO) {

        return null;
    }

    @Override
    public List<ActivityPeriodDBO> findAll() {
        return null;
    }

    public List<ServiceGMDBO> findServiceGMByClientId(Integer clientId){
        TypedQuery<ServiceGMDBO> query = session.createNamedQuery(ServiceGMDBO.FIND_SERVICE_GM_BY_CLIENT_ID, ServiceGMDBO.class);
        query.setParameter("clientId", clientId);

        return query.getResultList();
    }

    public Boolean isActiveClientAtDate(Integer clientId, LocalDate date){
        TypedQuery<ServiceGMDBO> query = session.createNamedQuery(ServiceGMDBO.FIND_SERVICE_GM_BY_CLIENT_ID, ServiceGMDBO.class);
        query.setParameter("clientId", clientId);

        List<ServiceGMDBO> serviceGMDBOList = query.getResultList();
        for(ServiceGMDBO serviceGMDBO : serviceGMDBOList){
            if(serviceGMDBO.isActiveAtDate(date)){
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }
}
