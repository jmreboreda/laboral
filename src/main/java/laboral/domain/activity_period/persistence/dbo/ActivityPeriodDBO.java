package laboral.domain.activity_period.persistence.dbo;

import laboral.domain.client.persistence.dbo.ClientDBO;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "activity_period")
@NamedQueries(value = {
        @NamedQuery(
                name = ActivityPeriodDBO.FIND_SERVICE_GM_BY_CLIENT_ID,
                query = "select p from ActivityPeriodDBO as p where clientId = :clientId"
        ),
        @NamedQuery(
                name = ActivityPeriodDBO.FIND_ALL_CLIENT_WITH_INVOICES_TO_CLAIM_IN_PERIOD,
                query = "select p from ActivityPeriodDBO as p where (p.dateFrom is null or p.dateFrom <= :periodFinalDate) " +
                        "and (p.dateTo is null or p.dateTo >= :periodFinalDate) and claimInvoices = true"
        )
})
public class ActivityPeriodDBO implements Serializable {

    public static final String FIND_SERVICE_GM_BY_CLIENT_ID = "ActivityPeriodDBO.FIND_SERVICE_GM_BY_CLIENT_ID";
    public static final String FIND_ALL_CLIENT_WITH_INVOICES_TO_CLAIM_IN_PERIOD = "ActivityPeriodDBO.FIND_ALL_CLIENT_WITH_INVOICES_TO_CLAIM_IN_PERIOD";

    @Id
    @SequenceGenerator(name = "activity_period_id_seq", sequenceName = "activity_period_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "activity_period_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;
    private Date dateFrom;
    private Date dateTo;
    private Date withoutActivityDate;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "clientId")
    private ClientDBO clientDBO;

    public ActivityPeriodDBO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Date getWithoutActivityDate() {
        return withoutActivityDate;
    }

    public void setWithoutActivityDate(Date withoutActivityDate) {
        this.withoutActivityDate = withoutActivityDate;
    }

    public ClientDBO getClientDBO() {
        return clientDBO;
    }

    public void setClientDBO(ClientDBO clientDBO) {
        this.clientDBO = clientDBO;
    }
}
