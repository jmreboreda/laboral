package laboral.domain.activity_period.manager;

import laboral.domain.activity_period.persistence.dao.ActivityPeriodDAO;
import laboral.domain.employer.Employer;

import java.time.LocalDate;

public class ActivityPeriodService {

    ActivityPeriodDAO activityPeriodDAO = ActivityPeriodDAO.ActivityPeriodDBODAOFactory.getInstance();

    private ActivityPeriodService() {
    }

    public static class ActivityPeriodServiceFactory {

        private static ActivityPeriodService activityPeriodService;

        public static ActivityPeriodService getInstance() {
            if(activityPeriodService == null) {
                activityPeriodService = new ActivityPeriodService();
            }
            return activityPeriodService;
        }
    }

    public Boolean isActiveClientAtDate(Employer employer, LocalDate date){

        return activityPeriodDAO.isActiveClientAtDate(employer.getClient().getClientId(), date);
    }
}
