package laboral.domain.activity_period.mapper;

import laboral.domain.activity_period.PersistedActivityPeriod;
import laboral.domain.activity_period.persistence.dbo.ActivityPeriodDBO;
import laboral.domain.interface_pattern.GenericMapper;

import java.time.LocalDate;

public class MapperActivityPeriodDBOToPersistedActivityPeriod implements GenericMapper<ActivityPeriodDBO, PersistedActivityPeriod> {

    @Override
    public PersistedActivityPeriod map(ActivityPeriodDBO activityPeriodDBO) {

        LocalDate dateFrom = activityPeriodDBO.getDateFrom() == null ? null : activityPeriodDBO.getDateFrom().toLocalDate();
        LocalDate dateTo = activityPeriodDBO.getDateTo() == null ? null : activityPeriodDBO.getDateTo().toLocalDate();
        LocalDate withoutActivityDate = activityPeriodDBO.getWithoutActivityDate() == null ? null : activityPeriodDBO.getWithoutActivityDate().toLocalDate();

        PersistedActivityPeriod persistedActivityPeriod = new PersistedActivityPeriod();
        persistedActivityPeriod.setId(activityPeriodDBO.getId());
        persistedActivityPeriod.setClientId(activityPeriodDBO.getClientDBO().getId());
        persistedActivityPeriod.setDateFrom(dateFrom);
        persistedActivityPeriod.setDateTo(dateTo);
        persistedActivityPeriod.setWithoutActivityDate(withoutActivityDate);

        return persistedActivityPeriod;
    }
}
