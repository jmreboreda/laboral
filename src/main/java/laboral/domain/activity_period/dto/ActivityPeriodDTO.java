package laboral.domain.activity_period.dto;


import laboral.domain.client.ClientDTO;

import java.time.LocalDate;

public class ActivityPeriodDTO {

    private Integer id;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private LocalDate withoutActivityDate;
    private ClientDTO clientDTO;

    public ActivityPeriodDTO(Integer id, LocalDate dateFrom, LocalDate dateTo, LocalDate withoutActivityDate, ClientDTO clientDTO) {
        if(dateFrom != null && dateTo != null && dateFrom.isBefore(dateTo)) {
            System.out.println("New Activity Period: error -> dateTo < dateFrom");
        }
        this.id = id;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.withoutActivityDate = withoutActivityDate;
        this.clientDTO = clientDTO;
    }

    public Integer getId() {
        return id;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public LocalDate getWithoutActivityDate() {
        return withoutActivityDate;
    }

    public ClientDTO getClientDTO() {
        return clientDTO;
    }

    public static final class ActivityPeriodDTOBuilder {
        private Integer id;
        private LocalDate dateFrom;
        private LocalDate dateTo;
        private LocalDate withoutActivityDate;
        private ClientDTO clientDTO;

        private ActivityPeriodDTOBuilder() {
        }

        public static ActivityPeriodDTOBuilder create() {
            return new ActivityPeriodDTOBuilder();
        }

        public ActivityPeriodDTOBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public ActivityPeriodDTOBuilder withDateFrom(LocalDate dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        public ActivityPeriodDTOBuilder withDateTo(LocalDate dateTo) {
            this.dateTo = dateTo;
            return this;
        }

        public ActivityPeriodDTOBuilder withWithoutActivityDate(LocalDate withoutActivityDate) {
            this.withoutActivityDate = withoutActivityDate;
            return this;
        }

        public ActivityPeriodDTOBuilder withClientDTO(ClientDTO clientDTO) {
            this.clientDTO = clientDTO;
            return this;
        }

        public ActivityPeriodDTO build() {
            return new ActivityPeriodDTO(id, dateFrom, dateTo, withoutActivityDate, clientDTO);
        }
    }

    //    public Boolean validateActivityPeriodDataEntry( List<PersistedActivityPeriod> persistedActivityPeriodList, Stage stage) {
//
//        this.persistedActivityPeriodList = persistedActivityPeriodList;
//
//        for(int counter = 0; counter < persistedActivityPeriodList.size(); counter++){
//            if(persistedActivityPeriodList.get(counter).getDateFrom() == null &&  persistedActivityPeriodList.get(counter).getDateTo() == null ||
//                    persistedActivityPeriodList.get(counter).getDateFrom() == null &&  persistedActivityPeriodList.get(counter).getDateTo() != null ){
//                Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.THERE_ARE_ANY_PERIOD_OF_ACTIVITY_WITHOUT_DATES);
//
//                return Boolean.FALSE;
//            }
//        }
//
//        Integer activityPeriodsOpen = 0;
//        for (PersistedActivityPeriod persistedActivityPeriod : persistedActivityPeriodList) {
//            if (persistedActivityPeriod.getDateTo() == null && persistedActivityPeriod.getWithoutActivityDate() == null ||
//                    persistedActivityPeriod.getDateTo() == null && persistedActivityPeriod.getWithoutActivityDate() != null) {
//                activityPeriodsOpen++;
//            }
//
//            if (activityPeriodsOpen > 1) {
//                Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.TOO_MANY_OPEN_ACTIVITY_PERIODS);
//
//                return Boolean.FALSE;
//            }
//        }
//
//        for (PersistedActivityPeriod persistedActivityPeriod : persistedActivityPeriodList) {
//            if (persistedActivityPeriod.getDateTo() != null && persistedActivityPeriod.getDateFrom() != null &&
//                    persistedActivityPeriod.getDateTo().isBefore(persistedActivityPeriod.getDateFrom()) ||
//                    persistedActivityPeriod.getDateTo() != null && persistedActivityPeriod.getWithoutActivityDate() != null &&
//                            persistedActivityPeriod.getDateTo().isBefore(persistedActivityPeriod.getWithoutActivityDate()) ||
//                    persistedActivityPeriod.getDateFrom() != null && persistedActivityPeriod.getWithoutActivityDate() != null &&
//                            persistedActivityPeriod.getWithoutActivityDate().isBefore(persistedActivityPeriod.getDateFrom())
//            ) {
//                Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.THERE_ARE_INCONSISTENT_DATES_IN_ANY_PERIOD_OF_ACTIVITY);
//
//                return Boolean.FALSE;
//            }
//        }
//
//        if(persistedActivityPeriodList.size() > 1) {
//            for (int counter = 1; counter < persistedActivityPeriodList.size(); counter++) {
//                if(persistedActivityPeriodList.get(counter).getDateFrom().isBefore(persistedActivityPeriodList.get(counter-1).getDateFrom()) ||
//                        persistedActivityPeriodList.get(counter-1).getDateTo() != null && persistedActivityPeriodList.get(counter).getDateFrom().isBefore(persistedActivityPeriodList.get(counter-1).getDateTo())){
//                    Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.THERE_ARE_INCONSISTENT_DATES_IN_ANY_PERIOD_OF_ACTIVITY);
//
//                    return Boolean.FALSE;
//                }
//            }
//        }
//
//        return Boolean.TRUE;
//    }
}
