package laboral.domain.quote_account_code;

public enum QuoteAccountCodeRegimeEnum {

    GENERAL_REGIME(true, "Régimen general", "0111 - Régimen general","0111"),
    ARTISTS_REGIME(true, "Colectivo de artistas", "0112 - R. G. Colectivo de artistas","0112"),
    PROFESSIONAL_TOREROS_AND_ASSISTANTS_REGIME(false, "Régimen General", "0114 - R. G. Profesionales taurinos","0114"),
    RESINA_REGIME(false, "Régimen general", "0131 - R. G. S. E. Resina", "0131"),
    VEGETABLES_AND_CANNED_VEGETABLES_REGIME(true, "Régimen General", "0132 - R. G. S. E. Frutas, Hortalizas y conservas Vegetales","0132"),
    FRESH_TOMATO_REGIME(false, "Régimen general", "0134 - S. E. manipulado y empaquetado tomate fresco", "0134"),
    HOSTEL_INDUSTRY_REGIME(true, "Régimen general", "0135 - R. G. S. E. Industria Hostelera", "0135"),
    CINEMA_NIGHTCLUBS_REGIME(true, "Régimen general", "0136 - R. G. S. E. fijos discontínuos en cines, salas baile, discotecas y salas fiesta", "0136"),
    MARKET_RESEARCH_AND_PUBLIC_OPINION_REGIME(true, "Régimen General", "0137 - R. G. S. E. trabajadores discontinuos en empresas de estudios de mercado y opinión pública","0137"),
    HOME_EMPLOYERS_REGIME(true, "Régimen general", "0138 - R. G. S. E. empleados de hogar", "0138"),
    AGRARIAN_REGIME(true, "Régimen General", "0163 - R. G. S. E. Agrario. Actividad en CCC","0163"),
    SELF_EMPLOYED_REGIME(true, "Régimen Especial", "0521 - Régimen Especial Trabajadores Autónomos", "0521"),
    AGRARIAN_CERTAIN_WORKDAY_REGIME(false, "Régimen Especial", "0613 - Régimen Especial Agrario (Jornadas Reales).Integrado en Régimen General desde 01/01/2012","0613"),
    MARITIME_WORKERS_WITH_LAND_OCCUPATION_REGIME(true, "Régimen de los trabajadores del mar con ocupación en tierra", "0811 - R. E. MAR: trabajadores en tierra","0811"),
    MARITIME_WORKERS_WITH_SEA_OCCUPATION_REGIME(true, "Régimen de los trabajadores del mar con ocupación en la mar", "0814 - R. E. MAR: trabajadores a bordo","0814"),
    MARITIME_WORKERS_2A_GROUP_REGIME(true, "Régimen Especial de los trabajadores del Mar", "0812 - R. E. MAR Trabajadores por cuenta ajena grupo 2A","0812"),
    MARITIME_WORKERS_2B_GROUP_REGIME(true, "Régimen Especial de los trabajadores del Mar", "0813 - R. E. MAR Trabajadores por cuenta ajena grupo 2B","0813"),
    SHIP_OWNERS_3_GROUP_REGIME(false, "Régimen Especial de los trabajadores del Mar", "0821 - R. E. MAR Armadores y asimilados por cuenta ajena Grupo 1. Baja desde 1/1/2016","0821"),
    SHIP_OWNERS_2A_GROUP_REGIME(false, "Régimen Especial de los trabajadores del Mar", "0822 - R. E. MAR Armadores y asimilados por cuenta ajena Grupo 2A. Baja desde 1/1/2016","0822"),
    SHIP_OWNERS_2B_GROUP_REGIME(false, "Régimen Especial de los trabajadores del Mar", "0823 - R. E. MAR Armadores y asimilados por cuenta ajena Grupo 2B. Baja desde 1/1/2016","0823"),
    SELF_EMPLOYED_MARITIME_REGIME(true, "Régimen Especial de los trabajadores del Mar", "0825 - R. E. MAR: Autónomos","0825"),
    COAL_MINING_REGIME(true, "Minería del carbón", "0911 - S. E. de minería del carbón","0911"),
    CONCERTED_HEALTH_ASSISTANCE(true, "Asistencia sanitaria concertada", "3011 - Asistencia sanitaria concertada", "3011");


    private Boolean active;
    private String quoteAccountCodeRegimeDescription;
    private String quoteAccountCodeRegimeDescriptionResumed;
    private String quoteAccountCodeRegimeCode;

    QuoteAccountCodeRegimeEnum(Boolean active, String quoteAccountCodeRegimeDescription, String quoteAccountCodeRegimeDescriptionResumed, String quoteAccountCodeRegimeCode) {
        this.active = active;
        this.quoteAccountCodeRegimeDescription = quoteAccountCodeRegimeDescription;
        this.quoteAccountCodeRegimeDescriptionResumed = quoteAccountCodeRegimeDescriptionResumed;
        this.quoteAccountCodeRegimeCode = quoteAccountCodeRegimeCode;
    }

    public Boolean getActive() {
        return active;
    }

    public String getQuoteAccountCodeRegimeDescription() {
        return quoteAccountCodeRegimeDescription;
    }

    public String getQuoteAccountCodeRegimeDescriptionResumed() {
        return quoteAccountCodeRegimeDescriptionResumed;
    }

    public String getQuoteAccountCodeRegimeCode() {
        return quoteAccountCodeRegimeCode;
    }

    public String toString(){
        return getQuoteAccountCodeRegimeCode().concat("-").concat(getQuoteAccountCodeRegimeDescription());
    }
}
