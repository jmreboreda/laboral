package laboral.domain.quote_account_code.persistence.dbo;

import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.quote_account_code.QuoteAccountCodeRegimeEnum;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "quote_account_code", uniqueConstraints = {@UniqueConstraint(columnNames = {"regime", "province", "quoteAccountNumber", "controlDigit"})})
@NamedQueries(value = {
        @NamedQuery(
                name = QuoteAccountCodeDBO.FIND_ALL_QUOTE_ACCOUNT_CODE_BY_CLIENT_ID,
                query = "select p from QuoteAccountCodeDBO p where clientId = :code"
        ),
        @NamedQuery(
                name = QuoteAccountCodeDBO.FIND_QUOTE_ACCOUNT_CODE_BY_ID,
                query = "select p from QuoteAccountCodeDBO p where p.id = :quoteAccountCodeId"
        ),
        @NamedQuery(
                name = QuoteAccountCodeDBO.FIND_QUOTE_ACCOUNT_CODE_BY_QUOTE_ACCOUNT_CODE_NUMBER,
                query = "select p from QuoteAccountCodeDBO p where concat(p.regime, '-', p.province, p.quoteAccountNumber, p.controlDigit) = :quoteAccountCode"
        )
})

public class QuoteAccountCodeDBO implements Serializable{

    public static final String FIND_ALL_QUOTE_ACCOUNT_CODE_BY_CLIENT_ID = "QuoteAccountCodeDBO.FIND_ALL_QUOTE_ACCOUNT_CODE_BY_CLIENT_ID";
    public static final String FIND_QUOTE_ACCOUNT_CODE_BY_ID = "QuoteAccountCodeDBO.FIND_QUOTE_ACCOUNT_CODE_BY_ID";
    public static final String FIND_QUOTE_ACCOUNT_CODE_BY_QUOTE_ACCOUNT_CODE_NUMBER = "QuoteAccountCodeDBO.FIND_QUOTE_ACCOUNT_CODE_BY_QUOTE_ACCOUNT_CODE_NUMBER";

    @Id
    @SequenceGenerator(name = "quote_account_code_id_seq", sequenceName = "quote_account_code_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "quote_account_code_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;
    private String regime;
    @Column(length = 2)
    private String province;
    private String quoteAccountNumber;
    private String controlDigit;
    private String activityEpigraphNumber;
    private String activityDescription;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="employerId")
    private EmployerDBO employerDBO;

    public QuoteAccountCodeDBO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegime() {
        return regime;
    }

    public void setRegime(String regime) {
        this.regime = regime;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getQuoteAccountNumber() {
        return quoteAccountNumber;
    }

    public void setQuoteAccountNumber(String quoteAccountNumber) {
        this.quoteAccountNumber = quoteAccountNumber;
    }

    public String getControlDigit() {
        return controlDigit;
    }

    public void setControlDigit(String controlDigit) {
        this.controlDigit = controlDigit;
    }

    public String getActivityEpigraphNumber() {
        return activityEpigraphNumber;
    }

    public void setActivityEpigraphNumber(String activityEpigraphNumber) {
        this.activityEpigraphNumber = activityEpigraphNumber;
    }

    public String getActivityDescription() {
        return activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }

    public EmployerDBO getEmployerDBO() {
        return employerDBO;
    }

    public void setEmployerDBO(EmployerDBO clientVO) {
        this.employerDBO = clientVO;
    }

//    public WorkContractDBO getWorkContractDBO() {
//        return workContractDBO;
//    }
//
//    public void setWorkContractDBO(WorkContractDBO workContractDBO) {
//        this.workContractDBO = workContractDBO;
//    }
    public String getQuoteAccountCodeExtended(){

        return getRegime() + "-" + getProvince() + getQuoteAccountNumber() + getControlDigit();
    }

    public QuoteAccountCodeRegimeEnum getQuoteAccountCodeRegimeEnum(){
        QuoteAccountCodeRegimeEnum quoteAccountCodeRegimeEnum = null;
        for(QuoteAccountCodeRegimeEnum quoteAccountCodeRegimeEnumRead : QuoteAccountCodeRegimeEnum.values()){
            if(quoteAccountCodeRegimeEnumRead.getQuoteAccountCodeRegimeCode().equals(getRegime())){
                quoteAccountCodeRegimeEnum = quoteAccountCodeRegimeEnumRead;
            }
        }

        return quoteAccountCodeRegimeEnum;
    }


    public static final class QuoteAccountCodeDBOBuilder {
        private Integer id;
        private String regime;
        private String province;
        private String quoteAccountNumber;
        private String controlDigit;
        private String activityEpigraphNumber;
        private String activityDescription;
        private EmployerDBO employerDBO;

        private QuoteAccountCodeDBOBuilder() {
        }

        public static QuoteAccountCodeDBOBuilder create() {
            return new QuoteAccountCodeDBOBuilder();
        }

        public QuoteAccountCodeDBOBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public QuoteAccountCodeDBOBuilder withRegime(String regime) {
            this.regime = regime;
            return this;
        }

        public QuoteAccountCodeDBOBuilder withProvince(String province) {
            this.province = province;
            return this;
        }

        public QuoteAccountCodeDBOBuilder withQuoteAccountNumber(String quoteAccountNumber) {
            this.quoteAccountNumber = quoteAccountNumber;
            return this;
        }

        public QuoteAccountCodeDBOBuilder withControlDigit(String controlDigit) {
            this.controlDigit = controlDigit;
            return this;
        }

        public QuoteAccountCodeDBOBuilder withActivityEpigraphNumber(String activityEpigraphNumber) {
            this.activityEpigraphNumber = activityEpigraphNumber;
            return this;
        }

        public QuoteAccountCodeDBOBuilder withActivityDescription(String activityDescription) {
            this.activityDescription = activityDescription;
            return this;
        }

        public QuoteAccountCodeDBOBuilder withEmployerDBO(EmployerDBO employerDBO) {
            this.employerDBO = employerDBO;
            return this;
        }

        public QuoteAccountCodeDBO build() {
            QuoteAccountCodeDBO quoteAccountCodeDBO = new QuoteAccountCodeDBO();
            quoteAccountCodeDBO.setId(id);
            quoteAccountCodeDBO.setRegime(regime);
            quoteAccountCodeDBO.setProvince(province);
            quoteAccountCodeDBO.setQuoteAccountNumber(quoteAccountNumber);
            quoteAccountCodeDBO.setControlDigit(controlDigit);
            quoteAccountCodeDBO.setActivityEpigraphNumber(activityEpigraphNumber);
            quoteAccountCodeDBO.setActivityDescription(activityDescription);
            quoteAccountCodeDBO.setEmployerDBO(employerDBO);
            return quoteAccountCodeDBO;
        }
    }
}
