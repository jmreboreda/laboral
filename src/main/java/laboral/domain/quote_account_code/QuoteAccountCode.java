package laboral.domain.quote_account_code;

import java.util.regex.Pattern;

public class QuoteAccountCode {

    private Integer id;
    private QuoteAccountCodeRegimeEnum regime;
    private String province;
    private String quoteAccountCodeNumber;
    private String controlDigit;
    private String activityEpigraphNumber;
    private String activityDescription;

    public QuoteAccountCode() {
    }

    public QuoteAccountCode(Integer id, QuoteAccountCodeRegimeEnum regime, String province, String quoteAccountCodeNumber, String controlDigit, String activityEpigraphNumber, String activityDescription) {
        this.id = id;
        this.regime = regime;
        this.province = province;
        this.quoteAccountCodeNumber = quoteAccountCodeNumber;
        this.controlDigit = controlDigit;
        this.activityEpigraphNumber = activityEpigraphNumber;
        this.activityDescription = activityDescription;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public QuoteAccountCodeRegimeEnum getRegime() {
        return regime;
    }

    public String getRegimeDescription(){
        return regime.getQuoteAccountCodeRegimeDescription();
    }

    public String getRegimeCode(){
        return regime.getQuoteAccountCodeRegimeCode();
    }

    public void setRegime(QuoteAccountCodeRegimeEnum regime) {
        this.regime = regime;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getQuoteAccountCodeNumber() {
        return quoteAccountCodeNumber;
    }

    public void setQuoteAccountCodeNumber(String quoteAccountCodeNumber) {
        this.quoteAccountCodeNumber = quoteAccountCodeNumber;
    }

    public String getControlDigit() {
        return controlDigit;
    }

    public void setControlDigit(String controlDigit) {
        this.controlDigit = controlDigit;
    }

    public String getActivityEpigraphNumber() {
        return activityEpigraphNumber;
    }

    public void setActivityEpigraphNumber(String activityEpigraphNumber) {
        this.activityEpigraphNumber = activityEpigraphNumber;
    }

    public String getActivityDescription() {
        return activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }

    public String toCompleteNumber(){

        return getRegime().getQuoteAccountCodeRegimeCode().concat("-").concat(getProvince()).concat(getQuoteAccountCodeNumber()).concat(getControlDigit());
    }
    public String toString(){

        return getRegime().getQuoteAccountCodeRegimeDescriptionResumed()
                .concat(": ")
                .concat(getProvince())
                .concat(getQuoteAccountCodeNumber())
                .concat(getControlDigit())
                .concat(" [IAE: ")
                .concat(getActivityEpigraphNumber())
                .concat(" ")
                .concat(getActivityDescription())
                .concat("]");
    }

    public QuoteAccountCodeRegimeEnum getQuoteAccountCodeEnumByDescription(String quoteAccountCodeDescription){
        return QuoteAccountCodeRegimeEnum.valueOf(quoteAccountCodeDescription);
    }

    public QuoteAccountCodeRegimeEnum getQuoteAccountCodeEnumByCode(String quoteAccountCode){
        QuoteAccountCodeRegimeEnum quoteAccountCodeRegimeEnum = null;
        for (QuoteAccountCodeRegimeEnum quoteAccountCodeRegimeEnumRead : QuoteAccountCodeRegimeEnum.values()){
            if(quoteAccountCodeRegimeEnumRead.getQuoteAccountCodeRegimeCode().equals(quoteAccountCode)){
                quoteAccountCodeRegimeEnum = quoteAccountCodeRegimeEnumRead;
            }
        }
        return quoteAccountCodeRegimeEnum;
    }

    public Boolean isValid(){

        Pattern provincePattern = Pattern.compile("\\d{2}");
        Pattern numbersPattern = Pattern.compile("\\d{7}");
        Pattern digitControlPattern = Pattern.compile("\\d{2}");

        if(!provincePattern.matcher(getProvince()).matches() ||
                !numbersPattern.matcher(getQuoteAccountCodeNumber()).matches() ||
                !digitControlPattern.matcher(getControlDigit()).matches()){

            return false;
        }

        Long introducedProvince = Long.parseLong(getProvince());
        Long introducedQuoteAccountNumber = Long.parseLong(getQuoteAccountCodeNumber());

        Long calculatedControlDigit;
        String calculatedControlDigitAsString;

        if (introducedProvince < 1 || (introducedProvince > 53 && introducedProvince != 66)) {
            return false;
        }

        if (introducedQuoteAccountNumber < 10000000) {
            calculatedControlDigit = (introducedProvince * 10000000 + introducedQuoteAccountNumber) % 97;

        } else {
            Long numberNASSWithoutControlDigit = Long.parseLong(introducedProvince.toString().concat(introducedQuoteAccountNumber.toString()));
            calculatedControlDigit = numberNASSWithoutControlDigit % 97;
        }

        if (calculatedControlDigit <= 9){
                calculatedControlDigitAsString = "0".concat(calculatedControlDigit.toString());
        }else{
            calculatedControlDigitAsString = calculatedControlDigit.toString();
        }

        if(calculatedControlDigitAsString.equals(getControlDigit())){
            return true;
        }

        return false;
    }

    public static final class QuoteAccountCodeBuilder {
        private Integer id;
        private QuoteAccountCodeRegimeEnum regime;
        private String province;
        private String quoteAccountCodeNumber;
        private String controlDigit;
        private String activityEpigraphNumber;
        private String activityDescription;

        private QuoteAccountCodeBuilder() {
        }

        public static QuoteAccountCodeBuilder aQuoteAccountCode() {
            return new QuoteAccountCodeBuilder();
        }

        public QuoteAccountCodeBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public QuoteAccountCodeBuilder withRegime(QuoteAccountCodeRegimeEnum regime) {
            this.regime = regime;
            return this;
        }

        public QuoteAccountCodeBuilder withProvince(String province) {
            this.province = province;
            return this;
        }

        public QuoteAccountCodeBuilder withQuoteAccountCodeNumber(String quoteAccountCodeNumber) {
            this.quoteAccountCodeNumber = quoteAccountCodeNumber;
            return this;
        }

        public QuoteAccountCodeBuilder withControlDigit(String controlDigit) {
            this.controlDigit = controlDigit;
            return this;
        }

        public QuoteAccountCodeBuilder withActivityEpigraphNumber(String activityEpigraphNumber) {
            this.activityEpigraphNumber = activityEpigraphNumber;
            return this;
        }

        public QuoteAccountCodeBuilder withActivityDescription(String activityDescription) {
            this.activityDescription = activityDescription;
            return this;
        }

        public QuoteAccountCode build() {
            QuoteAccountCode quoteAccountCode = new QuoteAccountCode();
            quoteAccountCode.setId(id);
            quoteAccountCode.setRegime(regime);
            quoteAccountCode.setProvince(province);
            quoteAccountCode.setQuoteAccountCodeNumber(quoteAccountCodeNumber);
            quoteAccountCode.setControlDigit(controlDigit);
            quoteAccountCode.setActivityEpigraphNumber(activityEpigraphNumber);
            quoteAccountCode.setActivityDescription(activityDescription);
            return quoteAccountCode;
        }
    }
}
