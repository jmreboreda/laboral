package laboral.domain.quote_account_code.mapper;

import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.quote_account_code.persistence.dbo.QuoteAccountCodeDBO;

public class MapperQuoteAccountCodeToQuoteAccountCodeDBO implements GenericMapper<QuoteAccountCode, QuoteAccountCodeDBO> {

    @Override
    public QuoteAccountCodeDBO map(QuoteAccountCode quoteAccountCode) {

        final QuoteAccountCodeDBO quoteAccountCodeDBO = new QuoteAccountCodeDBO();

        quoteAccountCodeDBO.setRegime(quoteAccountCode.getRegimeCode());
        quoteAccountCodeDBO.setProvince(quoteAccountCode.getProvince());
        quoteAccountCodeDBO.setQuoteAccountNumber(quoteAccountCode.getQuoteAccountCodeNumber());
        quoteAccountCodeDBO.setControlDigit(quoteAccountCode.getControlDigit());
        quoteAccountCodeDBO.setActivityEpigraphNumber(quoteAccountCode.getActivityEpigraphNumber());
        quoteAccountCodeDBO.setActivityDescription(quoteAccountCode.getActivityDescription());

        return quoteAccountCodeDBO;
    }
}
