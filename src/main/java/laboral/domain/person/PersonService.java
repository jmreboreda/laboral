package laboral.domain.person;

import laboral.domain.person.manager.PersonManager;
import laboral.domain.person.persistence.dbo.PersonDBO;

public class PersonService {

    private final PersonManager personManager = new PersonManager();

    private PersonService() {
    }


    public static class PersonServiceFactory {

        private static PersonService personService;

        public static PersonService getInstance() {
            if(personService == null) {
                personService = new PersonService();
            }
            return personService;
        }
    }


    public PersonDBO findPersonById(Integer id){

        return personManager.findPersonById(id);
    }
}
