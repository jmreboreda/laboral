package laboral.domain.person.mapper;

import laboral.domain.address.Address;
import laboral.domain.address.mapper.MapperAddressDBOToAddress;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.EntityWithoutLegalPersonality;
import laboral.domain.person.LegalPerson;
import laboral.domain.person.NaturalPerson;
import laboral.domain.person.Person;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.person_type.PersonType;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class MapperPersonDBOToPerson implements GenericMapper<PersonDBO, Person> {

    @Override
    public Person map(PersonDBO personDBO) {

        MapperAddressDBOToAddress mapperAddressDBOToAddress = new MapperAddressDBOToAddress();

        Set<Address> addresses = new HashSet<>();
        if (personDBO.getAddresses() != null) {
            for (AddressDBO addressDBO : personDBO.getAddresses()) {
                Address address = mapperAddressDBOToAddress.map(addressDBO);
                addresses.add(address);
            }
        }

        if (personDBO.getPersonType().equals(PersonType.NATURAL_PERSON)) {

            LocalDate birthDate = personDBO.getBirthDate() == null ? null : personDBO.getBirthDate().toLocalDate();

            return NaturalPerson.create()
                    .withId(personDBO.getId())
                    .withPersonType(personDBO.getPersonType())
                    .withFirstSurname(personDBO.getFirstSurname())
                    .withSecondSurname(personDBO.getSecondSurname())
                    .withName(personDBO.getName())
                    .withNieNif(new NieNif(personDBO.getNieNif()))
                    .withBirthDate(birthDate)
                    .withSocialSecurityAffiliationNumber(personDBO.getSocialSecurityAffiliationNumber())
                    .withStudy(personDBO.getStudy())
                    .withNationality(personDBO.getNationality())
                    .withCivilStatus(personDBO.getCivilStatus())
                    .withAddresses(addresses)
                    .build();
        }
        else if (personDBO.getPersonType().equals(PersonType.LEGAL_PERSON)) {

            return LegalPerson.create()
                    .withId(personDBO.getId())
                    .withPersonType(personDBO.getPersonType())
                    .withLegalName(personDBO.getLegalName())
                    .withNieNif(new NieNif(personDBO.getNieNif()))
                    .withAddresses(addresses)
                    .build();
        }
        else if (personDBO.getPersonType().equals(PersonType.ENTITY_WITHOUT_LEGAL_PERSONALITY)) {

            return EntityWithoutLegalPersonality.create()
                    .withId(personDBO.getId())
                    .withPersonType(personDBO.getPersonType())
                    .withLegalName(personDBO.getLegalName())
                    .withNieNif(new NieNif(personDBO.getNieNif()))
                    .withAddresses(addresses)
                    .build();
        }

        return null;
    }
}
