package laboral.domain.person.mapper;

import laboral.domain.address.Address;
import laboral.domain.address.mapper.MapperAddressDBOToAddress;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.person_type.PersonType;

import java.util.HashSet;
import java.util.Set;

public class MapperPersonDBOPersonCreationRequest implements GenericMapper<PersonDBO, PersonCreationRequest> {

    @Override
    public PersonCreationRequest map(PersonDBO personDBO) {

        MapperAddressDBOToAddress mapperAddressDBOToAddress = new MapperAddressDBOToAddress();

        Set<Address> addresses = new HashSet<>();
        if(personDBO.getAddresses() != null){
            for(AddressDBO addressDBO : personDBO.getAddresses()){
                Address address = mapperAddressDBOToAddress.map(addressDBO);
                addresses.add(address);
            }
        }

        if(personDBO.getPersonType().equals(PersonType.NATURAL_PERSON)) {

            return PersonCreationRequest.PersonCreationRequestBuilder.create()
                    .withPersonType(personDBO.getPersonType())
                    .withFirstSurname(personDBO.getFirstSurname())
                    .withSecondSurname(personDBO.getSecondSurname())
                    .withName(personDBO.getName())
                    .withNieNif(new NieNif(personDBO.getNieNif()))
                    .withBirthDate(personDBO.getBirthDate().toLocalDate())
                    .withSocialSecurityAffiliationNumber(personDBO.getSocialSecurityAffiliationNumber())
                    .withStudy(personDBO.getStudy())
                    .withNationality(personDBO.getNationality())
                    .withCivilStatus(personDBO.getCivilStatus())
                    .withAddresses(addresses)
                    .build();
        }

        return PersonCreationRequest.PersonCreationRequestBuilder.create()
                .withPersonType(personDBO.getPersonType())
                .withLegalName(personDBO.getLegalName())
                .withNieNif(new NieNif(personDBO.getNieNif()))
                .withAddresses(addresses)
                .build();
    }
}

