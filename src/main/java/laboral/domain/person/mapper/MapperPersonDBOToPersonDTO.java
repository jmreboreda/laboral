package laboral.domain.person.mapper;

import laboral.domain.address.Address;
import laboral.domain.address.mapper.MapperAddressDBOToAddress;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.dto.PersonDTO;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.person_type.PersonType;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class MapperPersonDBOToPersonDTO implements GenericMapper<PersonDBO, PersonDTO> {

    @Override
    public PersonDTO map(PersonDBO personDBO) {

        MapperAddressDBOToAddress mapperAddressDBOToAddress = new MapperAddressDBOToAddress();

        Set<Address> addresses = new HashSet<>();
        if (personDBO.getAddresses() != null) {
            for (AddressDBO addressDBO : personDBO.getAddresses()) {
                Address address = mapperAddressDBOToAddress.map(addressDBO);
                addresses.add(address);
            }
        }

        if (personDBO.getPersonType().equals(PersonType.NATURAL_PERSON)) {

            LocalDate birthDate = personDBO.getBirthDate() == null ? null : personDBO.getBirthDate().toLocalDate();

            return PersonDTO.PersonDTOBuilder.create()
                    .withPersonType(personDBO.getPersonType())
                    .withFirstSurname(personDBO.getFirstSurname())
                    .withSecondSurname(personDBO.getSecondSurname())
                    .withName(personDBO.getName())
                    .withNieNif(new NieNif(personDBO.getNieNif()))
                    .withBirthDate(birthDate)
                    .withSocialSecurityAffiliationNumber(personDBO.getSocialSecurityAffiliationNumber())
                    .withStudy(personDBO.getStudy())
                    .withNationality(personDBO.getNationality())
                    .withCivilStatus(personDBO.getCivilStatus())
                    .withAddresses(addresses)
                    .build();
        }
        else if (personDBO.getPersonType().equals(PersonType.LEGAL_PERSON)) {

            return PersonDTO.PersonDTOBuilder.create()
                    .withPersonType(personDBO.getPersonType())
                    .withLegalName(personDBO.getLegalName())
                    .withNieNif(new NieNif(personDBO.getNieNif()))
                    .withAddresses(addresses)
                    .build();
        }
        else if (personDBO.getPersonType().equals(PersonType.ENTITY_WITHOUT_LEGAL_PERSONALITY)) {

            return PersonDTO.PersonDTOBuilder.create()
                    .withPersonType(personDBO.getPersonType())
                    .withLegalName(personDBO.getLegalName())
                    .withNieNif(new NieNif(personDBO.getNieNif()))
                    .withAddresses(addresses)
                    .build();
        }

        return null;
    }
}
