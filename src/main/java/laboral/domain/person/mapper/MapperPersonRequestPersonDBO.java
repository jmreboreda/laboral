package laboral.domain.person.mapper;

import laboral.domain.address.Address;
import laboral.domain.address.mapper.MapperAddressToAddressDBO;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.person_type.PersonType;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

public class MapperPersonRequestPersonDBO implements GenericMapper<PersonCreationRequest, PersonDBO> {

    @Override
    public PersonDBO map(PersonCreationRequest personCreationRequest) {

        Set<AddressDBO> addressesDBO = new HashSet<>();
        MapperAddressToAddressDBO mapperAddressToAddressDBO = new MapperAddressToAddressDBO();

        if (personCreationRequest.getAddresses() != null) {
            for (Address address : personCreationRequest.getAddresses()) {
                AddressDBO addressDBO = mapperAddressToAddressDBO.map(address);
                addressesDBO.add(addressDBO);
            }
        }

        if(personCreationRequest.getPersonType().equals(PersonType.NATURAL_PERSON)){

            return PersonDBO.create()
                    .withId(personCreationRequest.getPersonId())
                    .withPersonType(personCreationRequest.getPersonType())
                    .withFirstSurname(personCreationRequest.getFirstSurname())
                    .withSecondSurname(personCreationRequest.getSecondSurname())
                    .withName(personCreationRequest.getName())
                    .withNieNif(personCreationRequest.getNieNif().getNif())
                    .withBirthDate(Date.valueOf(personCreationRequest.getBirthDate()))
                    .withSocialSecurityAffiliationNumber(personCreationRequest.getSocialSecurityAffiliationNumber().toString())
                    .withStudy(personCreationRequest.getStudy())
                    .withNationality(personCreationRequest.getNationality())
                    .withCivilStatus(personCreationRequest.getCivilStatus())
                    .withAddresses(addressesDBO)
                    .build();

            }else {

            return PersonDBO.create()
                    .withId(personCreationRequest.getPersonId())
                    .withPersonType(personCreationRequest.getPersonType())
                    .withLegalName(personCreationRequest.getLegalName())
                    .withNieNif(personCreationRequest.getNieNif().getNif())
                    .withAddresses(addressesDBO)
                    .build();
        }
    }
}
