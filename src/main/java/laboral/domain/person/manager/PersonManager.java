package laboral.domain.person.manager;

import laboral.domain.address.Address;
import laboral.domain.address.mapper.MapperAddressToAddressDBO;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.person.*;
import laboral.domain.person.mapper.MapperPersonDBOToPerson;
import laboral.domain.person.mapper.MapperPersonRequestPersonDBO;
import laboral.domain.person.persistence.dao.PersonDAO;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.person_type.PersonType;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class PersonManager {

    private PersonDAO personDAO = PersonDAO.PersonDAOFactory.getInstance();
    private MapperPersonDBOToPerson mapperPersonDBOToPerson = new MapperPersonDBOToPerson();

    public PersonManager() {
    }

    public Integer personCreate(PersonCreationRequest personCreationRequest){

        MapperPersonRequestPersonDBO mapperPersonRequestPersonDBO = new MapperPersonRequestPersonDBO();

        return personDAO.create(mapperPersonRequestPersonDBO.map(personCreationRequest));
    }

    public Integer personUpdate(Person person){

        MapperAddressToAddressDBO mapperAddressToAddressDBO = new MapperAddressToAddressDBO();

        PersonDBO personDBOToUpdate = personDAO.getSession().load(PersonDBO.class, person.getPersonId());
        Set<AddressDBO> addressDBOSet = personDBOToUpdate.getAddresses();
        addressDBOSet.clear();
        for (Address address : person.getAddresses()) {
            AddressDBO addressDBO = mapperAddressToAddressDBO.map(address);
            addressDBO.setPersonDBO(personDBOToUpdate);
            addressDBOSet.add(addressDBO);
        }

        if(person.getPersonType().equals(PersonType.NATURAL_PERSON)) {
            NaturalPerson naturalPersonToUpdate = (NaturalPerson) person;
            Date birthDate = naturalPersonToUpdate.getBirthDate() == null ? null : Date.valueOf(naturalPersonToUpdate.getBirthDate());

            personDBOToUpdate.setFirstSurname(naturalPersonToUpdate.getFirstSurname());
            personDBOToUpdate.setSecondSurname(naturalPersonToUpdate.getSecondSurname());
            personDBOToUpdate.setName(naturalPersonToUpdate.getName());
            personDBOToUpdate.setNieNif(naturalPersonToUpdate.getNieNif().getNif());
            personDBOToUpdate.setSocialSecurityAffiliationNumber(naturalPersonToUpdate.getSocialSecurityAffiliationNumber());
            personDBOToUpdate.setBirthDate(birthDate);
            personDBOToUpdate.setStudy(naturalPersonToUpdate.getStudy());
            personDBOToUpdate.setNationality(naturalPersonToUpdate.getNationality());
            personDBOToUpdate.setCivilStatus(naturalPersonToUpdate.getCivilStatus());
            personDBOToUpdate.setAddresses(addressDBOSet);

        } else if (person.getPersonType().equals(PersonType.LEGAL_PERSON) ){
            LegalPerson legalPerson = (LegalPerson) person;
            personDBOToUpdate.setLegalName(legalPerson.getLegalName());
            personDBOToUpdate.setNieNif(legalPerson.getNieNif().getNif());
            personDBOToUpdate.setAddresses(addressDBOSet);
        }
        else if (person.getPersonType().equals(PersonType.ENTITY_WITHOUT_LEGAL_PERSONALITY) ){
            EntityWithoutLegalPersonality entityWithoutLegalPersonality = (EntityWithoutLegalPersonality) person;
            personDBOToUpdate.setLegalName(entityWithoutLegalPersonality.getLegalName());
            personDBOToUpdate.setNieNif(entityWithoutLegalPersonality.getNieNif().getNif());
            personDBOToUpdate.setAddresses(addressDBOSet);
        }

        return personDAO.update(personDBOToUpdate);
    }

    public PersonDBO findPersonById(Integer id){

        return personDAO.findById(id);
    }

    public List<Person> findAllNaturalPerson(){
        List<Person> persistedPersonList = new ArrayList<>();

        List<PersonDBO> personDBOList = personDAO.findAllNaturalPerson();

        for(PersonDBO personDBO : personDBOList){
            Person per = mapperPersonDBOToPerson.map(personDBO);
            persistedPersonList.add(per);
        }

        return persistedPersonList;
    }

    public List<Person> findPersonByNieNif(String nieNif){

        List<PersonDBO> personDBOList = personDAO.findPersonByNieNif(nieNif);
        List<Person> personList = new ArrayList<>();
        for(PersonDBO personDBO : personDBOList){
            Person persistedPerson = mapperPersonDBOToPerson.map(personDBO);
            personList.add(persistedPerson);
        }

        return personList;
    }

    public List<Person> findPersonByPattern(String pattern){

        List<PersonDBO> personDBOList = personDAO.findPersonByPattern(pattern);

        List<Person> personList = new ArrayList<>();
        for(PersonDBO personDBO : personDBOList){
            Person person = mapperPersonDBOToPerson.map(personDBO);
            personList.add(person);
        }

        return personList;
    }

    public List<Person> findPersonBySSAN(String socialSecurityAffiliationNumber){

        List<PersonDBO> personDBOList = personDAO.findPersonBySSAN(socialSecurityAffiliationNumber);

        MapperPersonDBOToPerson mapperPersonDBOToPerson = new MapperPersonDBOToPerson();

        List<Person> personList = new ArrayList<>();
        for(PersonDBO personDBO : personDBOList){
            Person person = mapperPersonDBOToPerson.map(personDBO);
            personList.add(person);
        }

        return personList;
    }
}
