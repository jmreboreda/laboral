package laboral.domain.person;

import laboral.domain.address.Address;
import laboral.domain.interface_pattern.LegalPersonality;
import laboral.domain.nienif.NieNif;
import laboral.domain.person_type.PersonType;

import java.util.Set;

public class EntityWithoutLegalPersonality extends Person implements LegalPersonality {

    private String legalName;

    public EntityWithoutLegalPersonality(Integer id,
                                         PersonType personType,
                                         String legalName,
                                         NieNif nieNif,
                                         Set<Address> addresses) {
        super(id, personType, nieNif, addresses);
        this.legalName = legalName;
    }

    @Override
    public Integer getPersonId() {
        return super.getPersonId();
    }

    @Override
    public void setPersonId(Integer personId) {
        super.setPersonId(personId);
    }

    @Override
    public PersonType getPersonType() {
        return super.getPersonType();
    }

    @Override
    public void setPersonType(PersonType personType) {
        super.setPersonType(personType);
    }

    @Override
    public NieNif getNieNif() {
        return super.getNieNif();
    }

    @Override
    public void setNieNif(NieNif nieNif) {
        super.setNieNif(nieNif);
    }

    @Override
    public Set<Address> getAddresses() {
        return super.getAddresses();
    }

    @Override
    public void setAddresses(Set<Address> addresses) {
        super.setAddresses(addresses);
    }

    @Override
    public String toString() {
        return getLegalName();
    }

    @Override
    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public static EntityWithoutLegalPersonalityBuilder create() {
        return new EntityWithoutLegalPersonalityBuilder();
    }

    @Override
    public String toAlphabeticalName() {
        return getLegalName();
    }

    public static class EntityWithoutLegalPersonalityBuilder {

        private Integer id;
        private String legalName;
        private PersonType personType;
        private NieNif nieNif;
        private Set<Address> addresses;

        public EntityWithoutLegalPersonalityBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public EntityWithoutLegalPersonalityBuilder withLegalName(String legalName) {
            this.legalName = legalName;
            return this;
        }

        public EntityWithoutLegalPersonalityBuilder withPersonType(PersonType personType) {
            this.personType = personType;
            return this;
        }

        public EntityWithoutLegalPersonalityBuilder withNieNif(NieNif nieNif) {
            this.nieNif = nieNif;
            return this;
        }

        public EntityWithoutLegalPersonalityBuilder withAddresses(Set<Address> addresses) {
            this.addresses = addresses;
            return this;
        }

        public EntityWithoutLegalPersonality build() {
            return new EntityWithoutLegalPersonality(this.id, this.personType, this.legalName, this.nieNif,  this.addresses);
        }
    }
}
