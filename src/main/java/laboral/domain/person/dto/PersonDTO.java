package laboral.domain.person.dto;

import laboral.domain.address.Address;
import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.interface_pattern.LegalPersonality;
import laboral.domain.interface_pattern.NaturalPersonality;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.Person;
import laboral.domain.person_type.PersonType;
import laboral.domain.study.StudyLevelType;

import java.time.LocalDate;
import java.util.Set;

public class PersonDTO extends Person implements NaturalPersonality, LegalPersonality {

    private final Integer oldClientId;
    private final  String firstSurname;
    private final  String secondSurname;
    private final  String name;
    private final  String legalName;
    private final  String socialSecurityAffiliationNumber;
    private final  LocalDate birthDate;
    private final  CivilStatusType civilStatus;
    private final  StudyLevelType study;
    private final  String nationality;

    public PersonDTO(Integer personId,
                     PersonType personType,
                     NieNif nieNif,
                     Set<Address> addresses,
                     Integer oldClientId,
                     String firstSurname,
                     String secondSurname,
                     String name,
                     String legalName,
                     String socialSecurityAffiliationNumber,
                     LocalDate birthDate,
                     CivilStatusType civilStatus,
                     StudyLevelType study,
                     String nationality) {
        super(personId, personType, nieNif, addresses);
        this.oldClientId = oldClientId;
        this.firstSurname = firstSurname;
        this.secondSurname = secondSurname;
        this.name = name;
        this.legalName = legalName;
        this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
        this.birthDate = birthDate;
        this.civilStatus = civilStatus;
        this.study = study;
        this.nationality = nationality;
    }

    public Integer getOldClientId() {
        return oldClientId;
    }

    @Override
    public String getLegalName() {
        return this.legalName;
    }

    @Override
    public String getFirstSurname() {
        return this.firstSurname;
    }

    @Override
    public String getSecondSurname() {
        return this.secondSurname;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getSocialSecurityAffiliationNumber() {
        return this.socialSecurityAffiliationNumber;
    }

    @Override
    public LocalDate getBirthDate() {
        return this.birthDate;
    }

    @Override
    public CivilStatusType getCivilStatus() {
        return this.civilStatus;
    }

    @Override
    public StudyLevelType getStudy() {
        return this.study;
    }

    @Override
    public String getNationality() {
        return this.nationality;
    }

    @Override
    public String toAlphabeticalName() {

        String alphabeticalName = null;

        if(getPersonType().equals(PersonType.NATURAL_PERSON)){
            alphabeticalName = getSecondSurname().isEmpty()
                    ? getFirstSurname() + ", " + getName()
                    : getFirstSurname() + " " + getSecondSurname() + ", " + getName();
        }else{
            alphabeticalName = getLegalName();
        }

        return alphabeticalName;
    }

    @Override
    public String toNaturalName() {

        String naturalName = null;

        if(getPersonType().equals(PersonType.NATURAL_PERSON)){
            naturalName = getSecondSurname().isEmpty()
                    ? getName() + " " + getFirstSurname()
                    : getName() + " " + getFirstSurname() + " " + getSecondSurname();
        }else{
            naturalName = getLegalName();
        }

        return naturalName;
    }

    public static final class PersonDTOBuilder {
        private Integer personId;
        private PersonType personType;
        private NieNif nieNif;
        private Set<Address> addresses;
        private Integer oldClientId;
        private String firstSurname;
        private String secondSurname;
        private String name;
        private String legalName;
        private String socialSecurityAffiliationNumber;
        private LocalDate birthDate;
        private CivilStatusType civilStatus;
        private StudyLevelType study;
        private String nationality;

        private PersonDTOBuilder() {
        }

        public static PersonDTOBuilder create() {
            return new PersonDTOBuilder();
        }

        public PersonDTOBuilder withPersonId(Integer personId) {
            this.personId = personId;
            return this;
        }

        public PersonDTOBuilder withPersonType(PersonType personType) {
            this.personType = personType;
            return this;
        }

        public PersonDTOBuilder withNieNif(NieNif nieNif) {
            this.nieNif = nieNif;
            return this;
        }

        public PersonDTOBuilder withAddresses(Set<Address> addresses) {
            this.addresses = addresses;
            return this;
        }

        public PersonDTOBuilder withOldClientId(Integer oldClientId) {
            this.oldClientId = oldClientId;
            return this;
        }

        public PersonDTOBuilder withFirstSurname(String firstSurname) {
            this.firstSurname = firstSurname;
            return this;
        }

        public PersonDTOBuilder withSecondSurname(String secondSurname) {
            this.secondSurname = secondSurname;
            return this;
        }

        public PersonDTOBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public PersonDTOBuilder withLegalName(String legalName) {
            this.legalName = legalName;
            return this;
        }

        public PersonDTOBuilder withSocialSecurityAffiliationNumber(String socialSecurityAffiliationNumber) {
            this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
            return this;
        }

        public PersonDTOBuilder withBirthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public PersonDTOBuilder withCivilStatus(CivilStatusType civilStatus) {
            this.civilStatus = civilStatus;
            return this;
        }

        public PersonDTOBuilder withStudy(StudyLevelType study) {
            this.study = study;
            return this;
        }

        public PersonDTOBuilder withNationality(String nationality) {
            this.nationality = nationality;
            return this;
        }

        public PersonDTO build() {
            return new PersonDTO(personId, personType, nieNif, addresses, oldClientId, firstSurname, secondSurname, name, legalName, socialSecurityAffiliationNumber, birthDate, civilStatus, study, nationality);
        }
    }
}
