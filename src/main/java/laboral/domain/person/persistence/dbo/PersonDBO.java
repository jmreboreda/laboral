package laboral.domain.person.persistence.dbo;

import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.person_type.PersonType;
import laboral.domain.study.StudyLevelType;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Set;


@Entity
@Table(name = "person")
@NamedQueries(value = {
        @NamedQuery(
                name = PersonDBO.FIND_ALL_PERSON,
                query = "select p from PersonDBO as p"
        ),
        @NamedQuery(
                name = PersonDBO.FIND_ALL_NATURAL_PERSON,
                query = "select p from PersonDBO as p where personType = :personType"
        ),
        @NamedQuery(
                name = PersonDBO.FIND_ALL_PERSON_IN_ALPHABETICAL_ORDER,
                query = "select p from PersonDBO as p order by p.firstSurname, p.secondSurname, p.name"
        ),
        @NamedQuery(
                name = PersonDBO.FIND_ALL_PERSONS_BY_NAME_PATTERN,
                query = "select p from PersonDBO as p where concat(lower(p.firstSurname), ' ', lower(p.secondSurname), ', ', lower(p.name)) like :code or " +
                        "lower(p.legalName) like :code"
        ),
        @NamedQuery(
                name = PersonDBO.FIND_PERSON_BY_ID,
                query = " select p from PersonDBO as p where p.id = :personId"
        ),
        @NamedQuery(
                name = PersonDBO.FIND_PERSON_BY_NIE_NIF,
                query = " select p from PersonDBO as p where p.nieNif = :nieNif"
        ),
        @NamedQuery(
                name = PersonDBO.FIND_PERSON_BY_SSAN,
                query = " select p from PersonDBO as p where p.socialSecurityAffiliationNumber = :socialSecurityAffiliationNumber"
        )
})

public class PersonDBO implements Serializable {
    public static final String FIND_ALL_PERSON = "PersonDBO.FIND_ALL_PERSON";
    public static final String FIND_ALL_NATURAL_PERSON = "PersonDBO.FIND_ALL_NATURAL_PERSON";
    public static final String FIND_ALL_PERSON_IN_ALPHABETICAL_ORDER = "PersonDBO.FIND_ALL_PERSON_IN_ALPHABETICAL_ORDER";
    public static final String FIND_ALL_PERSONS_BY_NAME_PATTERN = "PersonDBO.FIND_ALL_PERSONS_BY_NAME_PATTERN";
    public static final String FIND_PERSON_BY_ID = "PersonDBO.FIND_PERSON_BY_ID";
    public static final String FIND_PERSON_BY_NIE_NIF = "PersonDBO.FIND_PERSON_BY_NIE_NIF";
    public static final String FIND_PERSON_BY_SSAN = "PersonDBO.FIND_PERSON_BY_SSAN";

    @Id
    @SequenceGenerator(name = "person_id_seq", sequenceName = "person_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "person_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;
    private Integer oldClientId;
    private String firstSurname;
    private String secondSurname;
    private String name;
    private String legalName;
    @Enumerated(EnumType.STRING)
    private PersonType personType;
    private String nieNif;
    private String socialSecurityAffiliationNumber;
    private Date birthDate;
    @Enumerated(EnumType.STRING)
    private CivilStatusType civilStatus;
    @Enumerated(EnumType.STRING)
    private StudyLevelType study;
    @OneToMany(fetch = FetchType.EAGER, cascade= CascadeType.ALL)
    @JoinColumn(name = "personId")
    private Set<AddressDBO> addresses;
    private String nationality;

    public PersonDBO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOldClientId() {
        return oldClientId;
    }

    public void setOldClientId(Integer oldClientId) {
        this.oldClientId = oldClientId;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public void setPersonType(PersonType personType) {
        this.personType = personType;
    }

    public String getNieNif() {
        return nieNif;
    }

    public void setNieNif(String nieNif) {
        this.nieNif = nieNif;
    }

    public String getSocialSecurityAffiliationNumber() {
        return socialSecurityAffiliationNumber;
    }

    public void setSocialSecurityAffiliationNumber(String socialSecurityAffiliationNumber) {
        this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public CivilStatusType getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(CivilStatusType civilStatus) {
        this.civilStatus = civilStatus;
    }

    public StudyLevelType getStudy() {
        return study;
    }

    public void setStudy(StudyLevelType study) {
        this.study = study;
    }

    public Set<AddressDBO> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<AddressDBO> addresses) {
        this.addresses = addresses;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String toString(){
        return toAlphabeticalName();
    }

    public PersonType getPersonType(){
        return personType;
    }

    public String toAlphabeticalName(){
        if(personType.equals(PersonType.NATURAL_PERSON)) {
            return getSecondSurname() == null
                    ? getFirstSurname() + ", " + getName()
                    : getFirstSurname() + " " + getSecondSurname() + ", " + getName();
        }

        return getLegalName();
    }

    public String toNaturalName(){
        if(personType.equals(PersonType.NATURAL_PERSON)) {

            return getSecondSurname() == null
                    ? getName() + " " + getFirstSurname()
                    : getName() + " " + getFirstSurname() + " " + getSecondSurname();
        }

        return getLegalName();
    }

    public PersonDBO(Integer id,
                     Integer oldClientId,
                     String firstSurname,
                     String secondSurname,
                     String name,
                     String legalName,
                     PersonType personType,
                     String nieNif,
                     String socialSecurityAffiliationNumber,
                     Date birthDate,
                     CivilStatusType civilStatus,
                     StudyLevelType study,
                     Set<AddressDBO> addresses,
                     String nationality) {
        this.oldClientId = oldClientId;
        this.id = id;
        this.firstSurname = firstSurname;
        this.secondSurname = secondSurname;
        this.name = name;
        this.legalName = legalName;
        this.personType = personType;
        this.nieNif = nieNif;
        this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
        this.birthDate = birthDate;
        this.civilStatus = civilStatus;
        this.study = study;
        this.addresses = addresses;
        this.nationality = nationality;
    }

    public static PersonDBO.PersonDBOBuilder create() {
        return new PersonDBO.PersonDBOBuilder();
    }

    public static class PersonDBOBuilder {

        private Integer id;
        private Integer oldClientId;
        private String firstSurname;
        private String secondSurname;
        private String name;
        private String legalName;
        private PersonType personType;
        private String nieNif;
        private String socialSecurityAffiliationNumber;
        private Date birthDate;
        private CivilStatusType civilStatus;
        private StudyLevelType study;
        private Set<AddressDBO> addresses;
        private String nationality;

        public PersonDBO.PersonDBOBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public PersonDBO.PersonDBOBuilder withOldClientId(Integer oldClientId) {
            this.oldClientId = oldClientId;
            return this;
        }

        public PersonDBO.PersonDBOBuilder withFirstSurname(String firstSurname) {
            this.firstSurname = firstSurname;
            return this;
        }

        public PersonDBO.PersonDBOBuilder withSecondSurname(String secondSurname) {
            this.secondSurname = secondSurname;
            return this;
        }

        public PersonDBO.PersonDBOBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public PersonDBO.PersonDBOBuilder withLegalName(String legalName) {
            this.legalName = legalName;
            return this;
        }

        public PersonDBO.PersonDBOBuilder withPersonType(PersonType personType) {
            this.personType = personType;
            return this;
        }

        public PersonDBO.PersonDBOBuilder withNieNif(String nieNif) {
            this.nieNif = nieNif;
            return this;
        }

        public PersonDBO.PersonDBOBuilder withSocialSecurityAffiliationNumber(String socialSecurityAffiliationNumber) {
            this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
            return this;
        }

        public PersonDBO.PersonDBOBuilder withBirthDate(Date birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public PersonDBO.PersonDBOBuilder withCivilStatus(CivilStatusType civilStatus) {
            this.civilStatus = civilStatus;
            return this;
        }

        public PersonDBO.PersonDBOBuilder withStudy(StudyLevelType study) {
            this.study = study;
            return this;
        }

        public PersonDBO.PersonDBOBuilder withAddresses(Set<AddressDBO> addresses) {
            this.addresses = addresses;
            return this;
        }

        public PersonDBO.PersonDBOBuilder withNationality(String nationality) {
            this.nationality = nationality;
            return this;
        }

        public PersonDBO build() {
            return new PersonDBO(this.id, this.oldClientId, this.firstSurname, this.secondSurname, this.name, this.legalName, this.personType, this.nieNif, this.socialSecurityAffiliationNumber,
                    this.birthDate, this.civilStatus, this.study, this.addresses, this.nationality);
        }
    }
}
