package laboral.domain.person.persistence.dao;


import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.person_type.PersonType;
import laboral.domain.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

import static laboral.domain.person.persistence.dbo.PersonDBO.FIND_ALL_PERSON;

public class PersonDAO implements GenericDAO<PersonDBO, Integer> {

    private SessionFactory sessionFactory;
    private Session session;

    public PersonDAO() {
    }

    public static class PersonDAOFactory {

        private static PersonDAO personDAO;

        public static PersonDAO getInstance() {
            if(personDAO == null) {
                personDAO = new PersonDAO(HibernateUtil.retrieveGlobalSession());
            }
            return personDAO;
        }
    }

    public PersonDAO(Session session) {
        this.session = session;
    }

    @Override
    public Integer create(PersonDBO personDBO) {
        try {
            session.beginTransaction();
            session.saveOrUpdate(personDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }
        return personDBO.getId();
    }

    @Override
    public PersonDBO findById(Integer personId) {
        TypedQuery<PersonDBO> query = session.createNamedQuery(PersonDBO.FIND_PERSON_BY_ID, PersonDBO.class);
        query.setParameter("personId", personId);

        return query.getSingleResult();
    }

    @Override
    public Integer update(PersonDBO personDBO) {
        try {
            session.beginTransaction();
            session.merge(personDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }

        return personDBO.getId();
    }

    @Override
    public Boolean delete(PersonDBO personDBO) {

        return null;
    }

    @Override
    public List<PersonDBO> findAll() {

        TypedQuery<PersonDBO> query = session.createNamedQuery(PersonDBO.FIND_ALL_PERSON, PersonDBO.class);

        return query.getResultList();
    }

    public List<PersonDBO> findAllNaturalPerson() {

        TypedQuery<PersonDBO> query = session.createNamedQuery(PersonDBO.FIND_ALL_NATURAL_PERSON, PersonDBO.class);
        query.setParameter("personType", PersonType.NATURAL_PERSON);


        return query.getResultList();
    }
     
    public List<PersonDBO> findAllPersonInAlphabeticalOrder(){
        
        TypedQuery<PersonDBO> query = session.createNamedQuery(PersonDBO.FIND_ALL_PERSON_IN_ALPHABETICAL_ORDER, PersonDBO.class);

        return query.getResultList();
    }

    public List<PersonDBO> findPersonByPattern(String pattern){

        TypedQuery<PersonDBO> query = session.createNamedQuery(PersonDBO.FIND_ALL_PERSONS_BY_NAME_PATTERN, PersonDBO.class);
        query.setParameter("code", "%" + pattern.toLowerCase() + "%");

        return query.getResultList();
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public List<PersonDBO> findPersonByNieNif(String nieNif){
        TypedQuery<PersonDBO> query = session.createNamedQuery(PersonDBO.FIND_PERSON_BY_NIE_NIF, PersonDBO.class);
        query.setParameter("nieNif", nieNif);

        return query.getResultList();
    }

    public List<PersonDBO> findPersonBySSAN(String socialSecurityAffiliationNumber){
        TypedQuery<PersonDBO> query = session.createNamedQuery(PersonDBO.FIND_PERSON_BY_SSAN, PersonDBO.class);
        query.setParameter("socialSecurityAffiliationNumber", socialSecurityAffiliationNumber);

        return query.getResultList();
    }
}
