package laboral.domain.person;

import laboral.domain.address.Address;
import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.interface_pattern.NaturalPersonality;
import laboral.domain.nienif.NieNif;
import laboral.domain.person_type.PersonType;
import laboral.domain.study.StudyLevelType;

import java.time.LocalDate;
import java.util.Set;

public class NaturalPerson extends Person implements NaturalPersonality {

    private String firstSurname;
    private String secondSurname;
    private String name;
    private String socialSecurityAffiliationNumber;
    private LocalDate birthDate;
    private CivilStatusType civilStatus;
    private StudyLevelType study;
    private String nationality;

    public NaturalPerson(Integer id,
                         PersonType personType,
                         String firstSurname,
                         String secondSurname,
                         String name,
                         String socialSecurityAffiliationNumber,
                         LocalDate birthDate,
                         NieNif nieNif,
                         Set<Address> addresses,
                         CivilStatusType civilStatus,
                         StudyLevelType study,
                         String nationality) {
        super(id, personType, nieNif, addresses);

        this.firstSurname = firstSurname;
        this.secondSurname = secondSurname;
        this.name = name;
        this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
        this.birthDate = birthDate;
        this.civilStatus = civilStatus;
        this.study = study;
        this.nationality = nationality;
    }

    @Override
    public Integer getPersonId() {
        return super.getPersonId();
    }

    @Override
    public void setPersonId(Integer personId) {
        super.setPersonId(personId);
    }

    @Override
    public PersonType getPersonType() {
        return super.getPersonType();
    }

    @Override
    public void setPersonType(PersonType personType) {
        super.setPersonType(personType);
    }

    @Override
    public NieNif getNieNif() {
        return super.getNieNif();
    }

    @Override
    public void setNieNif(NieNif nieNif) {
        super.setNieNif(nieNif);
    }

    @Override
    public Set<Address> getAddresses() {
        return super.getAddresses();
    }

    @Override
    public void setAddresses(Set<Address> addresses) {
        super.setAddresses(addresses);
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    @Override
    public String getFirstSurname() {
        return firstSurname;
    }

    @Override
    public String getSecondSurname() {
        return secondSurname;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSocialSecurityAffiliationNumber() {
        return socialSecurityAffiliationNumber;
    }

    public void setSocialSecurityAffiliationNumber(String socialSecurityAffiliationNumber) {
        this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public CivilStatusType getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(CivilStatusType civilStatus) {
        this.civilStatus = civilStatus;
    }

    public StudyLevelType getStudy() {
        return study;
    }

    public void setStudy(StudyLevelType study) {
        this.study = study;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        String firstSurname = getFirstSurname() == null ? "" : getFirstSurname();
        String secondSurname = getSecondSurname() == null ? "" : getSecondSurname();

        return firstSurname.concat(" ").concat(secondSurname).concat(", ").concat(getName());
    }

    public static NaturalPersonBuilder create() {
        return new NaturalPersonBuilder();
    }

    @Override
    public String toAlphabeticalName() {
        String firstSurname = getFirstSurname() == null ? "" : getFirstSurname();
        String secondSurname = getSecondSurname() == null ? "" : getSecondSurname();

        return firstSurname.concat(" ").concat(secondSurname).concat(", ").concat(getName());    }

    @Override
    public String toNaturalName() {
        String firstSurname = getFirstSurname() == null ? "" : getFirstSurname();
        String secondSurname = getSecondSurname() == null ? "" : getSecondSurname();

        return getName().concat(firstSurname).concat(" ").concat(secondSurname);
    }

    public static class NaturalPersonBuilder {

        private Integer id;
        private String firstSurname;
        private String secondSurname;
        private String name;
        private PersonType personType;
        private NieNif nieNif;
        private Set<Address> addresses;
        private String socialSecurityAffiliationNumber;
        private LocalDate birthDate;
        private CivilStatusType civilStatus;
        private StudyLevelType study;
        private String nationality;

        public NaturalPersonBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public NaturalPersonBuilder withFirstSurname(String firstSurname) {
            this.firstSurname = firstSurname;
            return this;
        }

        public NaturalPersonBuilder withSecondSurname(String secondSurname) {
            this.secondSurname = secondSurname;
            return this;
        }

        public NaturalPersonBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public NaturalPersonBuilder withPersonType(PersonType personType) {
            this.personType = personType;
            return this;
        }

        public NaturalPersonBuilder withNieNif(NieNif nieNif) {
            this.nieNif = nieNif;
            return this;
        }

        public NaturalPersonBuilder withSocialSecurityAffiliationNumber(String socialSecurityAffiliationNumber) {
            this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
            return this;
        }

        public NaturalPersonBuilder withBirthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public NaturalPersonBuilder withCivilStatus(CivilStatusType civilStatus) {
            this.civilStatus = civilStatus;
            return this;
        }

        public NaturalPersonBuilder withStudy(StudyLevelType study) {
            this.study = study;
            return this;
        }

        public NaturalPersonBuilder withAddresses(Set<Address> addresses) {
            this.addresses = addresses;
            return this;
        }

        public NaturalPersonBuilder withNationality(String nationality) {
            this.nationality = nationality;
            return this;
        }

        public NaturalPerson build() {
            return new NaturalPerson(this.id, this.personType, this.firstSurname, this.secondSurname, this.name,  this.socialSecurityAffiliationNumber,
                    this.birthDate, this.nieNif,  this.addresses, this.civilStatus, this.study,  this.nationality);
        }
    }
}
