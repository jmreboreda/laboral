package laboral.domain.person;

import laboral.domain.address.Address;
import laboral.domain.interface_pattern.LegalPersonality;
import laboral.domain.nienif.NieNif;
import laboral.domain.person_type.PersonType;

import java.util.Set;

public class LegalPersonCreationRequest extends Person implements LegalPersonality {

    private String legalName;

    public LegalPersonCreationRequest(Integer id,
                                      String legalName,
                                      PersonType personType,
                                      NieNif nieNif,
                                      Set<Address> addresses) {
        super(id, personType, nieNif, addresses);
        this.legalName = legalName;
    }

    @Override
    public Integer getPersonId() {
        return super.getPersonId();
    }

    @Override
    public void setPersonId(Integer personId) {
        super.setPersonId(personId);
    }

    @Override
    public PersonType getPersonType() {
        return super.getPersonType();
    }

    @Override
    public void setPersonType(PersonType personType) {
        super.setPersonType(personType);
    }

    @Override
    public NieNif getNieNif() {
        return super.getNieNif();
    }

    @Override
    public void setNieNif(NieNif nieNif) {
        super.setNieNif(nieNif);
    }

    @Override
    public Set<Address> getAddresses() {
        return super.getAddresses();
    }

    @Override
    public void setAddresses(Set<Address> addresses) {
        super.setAddresses(addresses);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public String getLegalName() {
        return legalName;
    }

    @Override
    public String toAlphabeticalName() {
        return getLegalName();
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }


    public static final class LegalPersonCreationRequestBuilder {
        private String legalName;
        private Integer id;
        private PersonType personType;
        private NieNif nieNif;
        private Set<Address> addresses;

        private LegalPersonCreationRequestBuilder() {
        }

        public static LegalPersonCreationRequestBuilder create() {
            return new LegalPersonCreationRequestBuilder();
        }

        public LegalPersonCreationRequestBuilder withLegalName(String legalName) {
            this.legalName = legalName;
            return this;
        }

        public LegalPersonCreationRequestBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public LegalPersonCreationRequestBuilder withPersonType(PersonType personType) {
            this.personType = personType;
            return this;
        }

        public LegalPersonCreationRequestBuilder withNieNif(NieNif nieNif) {
            this.nieNif = nieNif;
            return this;
        }

        public LegalPersonCreationRequestBuilder withAddresses(Set<Address> addresses) {
            this.addresses = addresses;
            return this;
        }

        public LegalPersonCreationRequest build() {
            return new LegalPersonCreationRequest(id, legalName, personType, nieNif, addresses);
        }
    }
}
