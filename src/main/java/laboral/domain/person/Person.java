package laboral.domain.person;

import laboral.domain.address.Address;
import laboral.domain.nienif.NieNif;
import laboral.domain.person_type.PersonType;

import java.util.Set;

public abstract class Person {

    private Integer personId;
    private PersonType personType;
    private NieNif nieNif;
    private Set<Address> addresses;

    public Person(Integer personId, PersonType personType, NieNif nieNif, Set<Address> addresses) {
        this.personId = personId;
        this.personType = personType;
        this.nieNif = nieNif;
        this.addresses = addresses;
    }

    public Person() {
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public PersonType getPersonType() {
        return personType;
    }

    public void setPersonType(PersonType personType) {
        this.personType = personType;
    }

    public NieNif getNieNif() {
        return nieNif;
    }

    public void setNieNif(NieNif nieNif) {
        this.nieNif = nieNif;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public String toString(){
            return personType.getPersonTypeDescription();
    }
}
