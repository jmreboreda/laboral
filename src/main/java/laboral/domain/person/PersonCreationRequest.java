package laboral.domain.person;

import laboral.domain.address.Address;
import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.interface_pattern.LegalPersonality;
import laboral.domain.interface_pattern.NaturalPersonality;
import laboral.domain.nienif.NieNif;
import laboral.domain.person_type.PersonType;
import laboral.domain.study.StudyLevelType;

import java.time.LocalDate;
import java.util.Set;

public class PersonCreationRequest extends Person implements NaturalPersonality, LegalPersonality {

    private String firstSurname;
    private String secondSurname;
    private String name;
    private String legalName;
    private String socialSecurityAffiliationNumber;
    private LocalDate birthDate;
    private CivilStatusType civilStatus;
    private StudyLevelType study;
    private String nationality;

    public PersonCreationRequest(Integer id, PersonType personType, NieNif nieNif, Set<Address> addresses) {
        super(id, personType, nieNif, addresses);
    }

    public PersonCreationRequest(Integer id,
                                 String firstSurname,
                                 String secondSurname,
                                 String name,
                                 String legalName,
                                 PersonType personType,
                                 NieNif nieNif,
                                 String socialSecurityAffiliationNumber,
                                 LocalDate birthDate,
                                 CivilStatusType civilStatus,
                                 StudyLevelType study,
                                 Set<Address> addresses,
                                 String nationality) {
        super(id, personType, nieNif, addresses);
        this.firstSurname = firstSurname;
        this.secondSurname = secondSurname;
        this.name = name;
        this.legalName = legalName;
        this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
        this.birthDate = birthDate;
        this.civilStatus = civilStatus;
        this.study = study;
        this.nationality = nationality;
    }

    @Override
    public String getLegalName() {
        return this.legalName;
    }

    @Override
    public String getFirstSurname() {
        return this.firstSurname;
    }

    @Override
    public String getSecondSurname() {
        return this.secondSurname;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String toAlphabeticalName() {

        return getSecondSurname().isEmpty()
                ? getFirstSurname() + ", " + getName()
                : getFirstSurname() + " " + getSecondSurname() + ", " + getName();
    }

    @Override
    public String toNaturalName() {
        return getSecondSurname().isEmpty()
                ? getName() + " " + getFirstSurname()
                : getName() + " " + getFirstSurname() + " " + getSecondSurname();
    }

    public String getSocialSecurityAffiliationNumber() {
        return socialSecurityAffiliationNumber;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public CivilStatusType getCivilStatus() {
        return civilStatus;
    }

    public StudyLevelType getStudy() {
        return study;
    }

    public String getNationality() {
        return nationality;
    }

    public static final class PersonCreationRequestBuilder {
        private Integer id;
        private PersonType personType;
        private NieNif nieNif;
        private Set<Address> addresses;
        private String firstSurname;
        private String secondSurname;
        private String name;
        private String legalName;
        private String socialSecurityAffiliationNumber;
        private LocalDate birthDate;
        private CivilStatusType civilStatus;
        private StudyLevelType study;
        private String nationality;

        private PersonCreationRequestBuilder() {
        }

        public static PersonCreationRequestBuilder create() {
            return new PersonCreationRequestBuilder();
        }

        public PersonCreationRequestBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public PersonCreationRequestBuilder withPersonType(PersonType personType) {
            this.personType = personType;
            return this;
        }

        public PersonCreationRequestBuilder withNieNif(NieNif nieNif) {
            this.nieNif = nieNif;
            return this;
        }

        public PersonCreationRequestBuilder withAddresses(Set<Address> addresses) {
            this.addresses = addresses;
            return this;
        }

        public PersonCreationRequestBuilder withFirstSurname(String firstSurname) {
            this.firstSurname = firstSurname;
            return this;
        }

        public PersonCreationRequestBuilder withSecondSurname(String secondSurname) {
            this.secondSurname = secondSurname;
            return this;
        }

        public PersonCreationRequestBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public PersonCreationRequestBuilder withLegalName(String legalName) {
            this.legalName = legalName;
            return this;
        }

        public PersonCreationRequestBuilder withSocialSecurityAffiliationNumber(String socialSecurityAffiliationNumber) {
            this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
            return this;
        }

        public PersonCreationRequestBuilder withBirthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public PersonCreationRequestBuilder withCivilStatus(CivilStatusType civilStatus) {
            this.civilStatus = civilStatus;
            return this;
        }

        public PersonCreationRequestBuilder withStudy(StudyLevelType study) {
            this.study = study;
            return this;
        }

        public PersonCreationRequestBuilder withNationality(String nationality) {
            this.nationality = nationality;
            return this;
        }

        public PersonCreationRequest build() {
            return new PersonCreationRequest(id, firstSurname, secondSurname, name, legalName, personType, nieNif, socialSecurityAffiliationNumber, birthDate, civilStatus, study, addresses, nationality);
        }
    }
}
