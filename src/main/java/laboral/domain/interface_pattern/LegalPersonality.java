package laboral.domain.interface_pattern;

import laboral.domain.nienif.NieNif;

public interface LegalPersonality {

    String getLegalName();
    NieNif getNieNif();

    String toAlphabeticalName();

}
