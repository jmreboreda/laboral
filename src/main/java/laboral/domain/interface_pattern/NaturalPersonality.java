package laboral.domain.interface_pattern;

import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.nienif.NieNif;
import laboral.domain.study.StudyLevelType;

import java.time.LocalDate;

public interface NaturalPersonality {

    String getFirstSurname();
    String getSecondSurname();
    String getName();
    NieNif getNieNif();
    String getSocialSecurityAffiliationNumber();
    LocalDate getBirthDate();
    CivilStatusType getCivilStatus();
    StudyLevelType getStudy();
    String getNationality();

    String toAlphabeticalName();

    String toNaturalName();

}
