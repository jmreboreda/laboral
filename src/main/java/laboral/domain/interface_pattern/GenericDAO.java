package laboral.domain.interface_pattern;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO <T, ID extends Serializable> {

    ID create(T entity);
    ID update(T entity);
    Boolean delete(T entity);

    T findById(ID id);

    List<T> findAll();
}
