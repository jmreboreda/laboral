package laboral.domain.interface_pattern;

import laboral.domain.contract_type.WorkContractType;
import laboral.domain.traceability_contract_documentation.TraceabilityContractDocumentation;

import java.time.LocalDate;
import java.util.Set;

public interface Contract {

    LocalDate getDateSign();

    void setDateSign(LocalDate dateSign);

    LocalDate getStartDate();

    void setStartDate(LocalDate startDate);

    WorkContractType getContractType();

    void setContractType(WorkContractType workContractType);
}
