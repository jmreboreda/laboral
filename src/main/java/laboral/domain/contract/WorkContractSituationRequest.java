package laboral.domain.contract;

import laboral.domain.contract_json.ContractSchedule;
import laboral.domain.contract_type.WorkContractType;
import laboral.domain.employee.Employee;
import laboral.domain.employer.Employer;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.utilities.utilities.Utilities;
import laboral.domain.variation_type.VariationType;
import laboral.domain.work_center.WorkCenter;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;

public class WorkContractSituationRequest {

    private LocalDate dateSign;
    private Integer contractNumber;
    private Employer employer;
    private Employee employee;
    private WorkCenter workCenter;
    private QuoteAccountCode quoteAccountCode;
    private LocalDateTime clientNotification;
    private WorkContractType workContractType;
    private VariationType variationType;
    private LocalDate startDate;
    private LocalDate expectedEndDate;
    private LocalDate modificationDate;
    private LocalDate endDate;
    private WorkDayTypeDuration fullPartialWorkDay;
    private Set<DayOfWeek> dayOfWeekSet;
    private Duration hoursWorkWeek;
    private ContractSchedule contractSchedule;
    private String laborCategory;
    private String officialIdentificationNumber;
    private String publicNotes;
    private String privateNotes;
    private LocalDate idcReceptionDate;
    private LocalDate dateDeliveryDocumentation;
    private LocalDate endNoticeReceptionDate;

    public WorkContractSituationRequest() {
    }

    public WorkContractSituationRequest(LocalDate dateSign,
                                        Integer contractNumber,
                                        Employer employer,
                                        Employee employee,
                                        WorkCenter workCenter,
                                        QuoteAccountCode quoteAccountCode,
                                        LocalDateTime clientNotification,
                                        WorkContractType workContractType,
                                        VariationType variationType,
                                        LocalDate startDate,
                                        LocalDate expectedEndDate,
                                        LocalDate modificationDate,
                                        LocalDate endDate,
                                        WorkDayTypeDuration fullPartialWorkDay,
                                        Set<DayOfWeek> dayOfWeekSet,
                                        Duration hoursWorkWeek,
                                        ContractSchedule contractSchedule,
                                        String laborCategory,
                                        String officialIdentificationNumber,
                                        String publicNotes,
                                        String privateNotes,
                                        LocalDate idcReceptionDate,
                                        LocalDate dateDeliveryDocumentation,
                                        LocalDate endNoticeReceptionDate) {
        this.dateSign = dateSign;
        this.contractNumber = contractNumber;
        this.employer = employer;
        this.employee = employee;
        this.workCenter = workCenter;
        this.quoteAccountCode = quoteAccountCode;
        this.clientNotification = clientNotification;
        this.workContractType = workContractType;
        this.variationType = variationType;
        this.startDate = startDate;
        this.expectedEndDate = expectedEndDate;
        this.modificationDate = modificationDate;
        this.endDate = endDate;
        this.fullPartialWorkDay = fullPartialWorkDay;
        this.dayOfWeekSet = dayOfWeekSet;
        this.hoursWorkWeek = hoursWorkWeek;
        this.contractSchedule = contractSchedule;
        this.laborCategory = laborCategory;
        this.officialIdentificationNumber = officialIdentificationNumber;
        this.publicNotes = publicNotes;
        this.privateNotes = privateNotes;
        this.idcReceptionDate = idcReceptionDate;
        this.dateDeliveryDocumentation = dateDeliveryDocumentation;
        this.endNoticeReceptionDate = endNoticeReceptionDate;
    }

    public LocalDate getDateSign() {
        return dateSign;
    }

    public Integer getContractNumber() {
        return contractNumber;
    }

    public Employer getEmployer() {
        return employer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public WorkCenter getWorkCenter() {
        return workCenter;
    }

    public QuoteAccountCode getQuoteAccountCode() {
        return quoteAccountCode;
    }

    public LocalDateTime getClientNotification() {
        return clientNotification;
    }

    public WorkContractType getWorkContractType() {
        return workContractType;
    }

    public VariationType getVariationType() {
        return variationType;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getExpectedEndDate() {
        return expectedEndDate;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public WorkDayTypeDuration getFullPartialWorkDay() {
        return fullPartialWorkDay;
    }

    public Set<DayOfWeek> getDayOfWeekSet() {
        return dayOfWeekSet;
    }

    public Duration getHoursWorkWeek() {
        return hoursWorkWeek;
    }

    public ContractSchedule getContractSchedule() {
        return contractSchedule;
    }

    public String getLaborCategory() {
        return laborCategory;
    }

    public String getOfficialIdentificationNumber() {
        return officialIdentificationNumber;
    }

    public String getPublicNotes() {
        return publicNotes;
    }

    public String getPrivateNotes() {
        return privateNotes;
    }

    public LocalDate getIdcReceptionDate() {
        return idcReceptionDate;
    }

    public LocalDate getDateDeliveryDocumentation() {
        return dateDeliveryDocumentation;
    }

    public LocalDate getEndNoticeReceptionDate() {
        return endNoticeReceptionDate;
    }

    public String toFileName(){

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd_MM_yyyy");

        LocalDate fileNameDate = startDate != null ? startDate : endDate;

        return Utilities.replaceWithUnderscore(getEmployer().toAlphabeticalName())
                + "_" +
                Utilities.replaceWithUnderscore(getWorkContractType().getColloquial().toLowerCase())
                + "_" +
                fileNameDate.format(dateFormatter)
                + "_" +
                Utilities.replaceWithUnderscore(getEmployee().toAlphabeticalName());
    }

    public static final class WorkContractCreationRequestBuilder {
        private LocalDate dateSign;
        private Integer contractNumber;
        private Employer employer;
        private Employee employee;
        private WorkCenter workCenter;
        private QuoteAccountCode quoteAccountCode;
        private LocalDateTime clientNotification;
        private WorkContractType workContractType;
        private VariationType variationType;
        private LocalDate startDate;
        private LocalDate expectedEndDate;
        private LocalDate modificationDate;
        private LocalDate endDate;
        private WorkDayTypeDuration fullPartialWorkDay;
        private Set<DayOfWeek> dayOfWeekSet;
        private Duration hoursWorkWeek;
        private ContractSchedule contractSchedule;
        private String laborCategory;
        private String officialIdentificationNumber;
        private String publicNotes;
        private String privateNotes;
        private LocalDate idcReceptionDate;
        private LocalDate dateDeliveryDocumentation;
        private LocalDate endNoticeReceptionDate;

        private WorkContractCreationRequestBuilder() {
        }

        public static WorkContractCreationRequestBuilder create() {
            return new WorkContractCreationRequestBuilder();
        }

        public WorkContractCreationRequestBuilder withDateSign(LocalDate dateSign) {
            this.dateSign = dateSign;
            return this;
        }

        public WorkContractCreationRequestBuilder withContractNumber(Integer contractNumber) {
            this.contractNumber = contractNumber;
            return this;
        }

        public WorkContractCreationRequestBuilder withEmployer(Employer employer) {
            this.employer = employer;
            return this;
        }

        public WorkContractCreationRequestBuilder withEmployee(Employee employee) {
            this.employee = employee;
            return this;
        }

        public WorkContractCreationRequestBuilder withWorkCenter(WorkCenter workCenter) {
            this.workCenter = workCenter;
            return this;
        }

        public WorkContractCreationRequestBuilder withQuoteAccountCode(QuoteAccountCode quoteAccountCode) {
            this.quoteAccountCode = quoteAccountCode;
            return this;
        }

        public WorkContractCreationRequestBuilder withClientNotification(LocalDateTime clientNotification) {
            this.clientNotification = clientNotification;
            return this;
        }

        public WorkContractCreationRequestBuilder withWorkContractType(WorkContractType workContractType) {
            this.workContractType = workContractType;
            return this;
        }

        public WorkContractCreationRequestBuilder withVariationType(VariationType variationType) {
            this.variationType = variationType;
            return this;
        }

        public WorkContractCreationRequestBuilder withStartDate(LocalDate startDate) {
            this.startDate = startDate;
            return this;
        }

        public WorkContractCreationRequestBuilder withExpectedEndDate(LocalDate expectedEndDate) {
            this.expectedEndDate = expectedEndDate;
            return this;
        }

        public WorkContractCreationRequestBuilder withModificationDate(LocalDate modificationDate) {
            this.modificationDate = modificationDate;
            return this;
        }

        public WorkContractCreationRequestBuilder withEndDate(LocalDate endDate) {
            this.endDate = endDate;
            return this;
        }

        public WorkContractCreationRequestBuilder withFullPartialWorkDay(WorkDayTypeDuration fullPartialWorkDay) {
            this.fullPartialWorkDay = fullPartialWorkDay;
            return this;
        }

        public WorkContractCreationRequestBuilder withDaysOffWeek(Set<DayOfWeek> dayOfWeekSet) {
            this.dayOfWeekSet = dayOfWeekSet;
            return this;
        }

        public WorkContractCreationRequestBuilder withHoursWorkWeek(Duration hoursWorkWeek) {
            this.hoursWorkWeek = hoursWorkWeek;
            return this;
        }

        public WorkContractCreationRequestBuilder withContractSchedule(ContractSchedule contractSchedule) {
            this.contractSchedule = contractSchedule;
            return this;
        }

        public WorkContractCreationRequestBuilder withLaborCategory(String laborCategory) {
            this.laborCategory = laborCategory;
            return this;
        }

        public WorkContractCreationRequestBuilder withOfficialIdentificationNumber(String officialIdentificationNumber) {
            this.officialIdentificationNumber = officialIdentificationNumber;
            return this;
        }

        public WorkContractCreationRequestBuilder withPublicNotes(String publicNotes) {
            this.publicNotes = publicNotes;
            return this;
        }

        public WorkContractCreationRequestBuilder withPrivateNotes(String privateNotes) {
            this.privateNotes = privateNotes;
            return this;
        }

        public WorkContractCreationRequestBuilder withIdcReceptionDate(LocalDate idcReceptionDate) {
            this.idcReceptionDate = idcReceptionDate;
            return this;
        }

        public WorkContractCreationRequestBuilder withDateDeliveryDocumentation(LocalDate dateDeliveryDocumentation) {
            this.dateDeliveryDocumentation = dateDeliveryDocumentation;
            return this;
        }

        public WorkContractCreationRequestBuilder withEndNoticeReceptionDate(LocalDate enNoticeReceptionDate) {
            this.endNoticeReceptionDate = enNoticeReceptionDate;
            return this;
        }

        public WorkContractSituationRequest build() {
            return new WorkContractSituationRequest(dateSign, contractNumber, employer, employee, workCenter, quoteAccountCode, clientNotification, workContractType, variationType, startDate, expectedEndDate, modificationDate, endDate, fullPartialWorkDay, dayOfWeekSet, hoursWorkWeek, contractSchedule, laborCategory, officialIdentificationNumber, publicNotes, privateNotes, idcReceptionDate, dateDeliveryDocumentation, endNoticeReceptionDate);
        }
    }
}
