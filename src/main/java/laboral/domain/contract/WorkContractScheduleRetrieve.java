package laboral.domain.contract;

import laboral.component.work_contract.WorkContractScheduleDay;
import laboral.component.work_contract.WorkDaySchedule;
import laboral.component.work_contract.creation.components.WorkContractScheduleForm;
import laboral.component.work_contract.variation.component.WorkContractVariationSchedule;
import laboral.domain.contract_json.ContractDaySchedule;
import laboral.domain.contract_json.ContractSchedule;
import laboral.domain.utilities.utilities.Utilities;

import java.util.*;

public class WorkContractScheduleRetrieve {

    public ContractSchedule retrieveWorkContractSchedule(WorkContractScheduleForm workContractScheduleForm){

        List<List<WorkContractScheduleDay>> workContractScheduleList = new ArrayList<>();
        workContractScheduleList.add(workContractScheduleForm.getScheduleOneList());
        workContractScheduleList.add(workContractScheduleForm.getScheduleTwoList());
        workContractScheduleList.add(workContractScheduleForm.getScheduleThreeList());
        workContractScheduleList.add(workContractScheduleForm.getScheduleFourList());

        Map<String, ContractDaySchedule> contractDayScheduleSet = new HashMap<>();

        ContractSchedule schedule = new ContractSchedule();

        Integer scheduleNumber = 1;
        for(List<WorkContractScheduleDay> workContractScheduleQuarter : workContractScheduleList) {

            Integer counter = 0;

            Set<WorkDaySchedule> scheduleSet = workContractScheduleForm.retrieveScheduleWithScheduleDays(workContractScheduleQuarter);
            for (WorkDaySchedule workDaySchedule : scheduleSet) {
                if (!workDaySchedule.getDayOfWeek().isEmpty()) {
                    String dayOfWeek = workDaySchedule.getDayOfWeek() != null ? workDaySchedule.getDayOfWeek() : "";
                    String date = workDaySchedule.getDate() != null ? workDaySchedule.getDate().toString() : "";
                    String amFrom = workDaySchedule.getAmFrom() != null ? workDaySchedule.getAmFrom().toString() : "";
                    String amTo = workDaySchedule.getAmTo() != null ? workDaySchedule.getAmTo().toString() : "";
                    String pmFrom = workDaySchedule.getPmFrom() != null ? workDaySchedule.getPmFrom().toString() : "";
                    String pmTo = workDaySchedule.getPmTo() != null ? workDaySchedule.getPmTo().toString() : "";
                    String durationHours = Utilities.durationToTimeStringConverter(workDaySchedule.getDurationHours());

                    ContractDaySchedule contractDayScheduleJson = ContractDaySchedule.create()
                            .withDayOfWeek(dayOfWeek)
                            .withDate(date)
                            .withAmFrom(amFrom)
                            .withAmTo(amTo)
                            .withPmFrom(pmFrom)
                            .withPmTo(pmTo)
                            .withDurationHours(durationHours)
                            .build();

                    contractDayScheduleSet.put("Schedule" + scheduleNumber + " - workDay" + counter, contractDayScheduleJson);
                }

                counter++;

                schedule.setSchedule(contractDayScheduleSet);
            }

            scheduleNumber++;
        }

        return schedule;
    }

    public ContractSchedule retrieveWorkContractVariationSchedule(WorkContractVariationSchedule workContractVariationSchedule){

        List<List<WorkContractScheduleDay>> workContractScheduleList = new ArrayList<>();
        workContractScheduleList.add(workContractVariationSchedule.getScheduleOneList());
        workContractScheduleList.add(workContractVariationSchedule.getScheduleTwoList());
        workContractScheduleList.add(workContractVariationSchedule.getScheduleThreeList());
        workContractScheduleList.add(workContractVariationSchedule.getScheduleFourList());

        Map<String, ContractDaySchedule> contractDayScheduleSet = new HashMap<>();

        ContractSchedule schedule = new ContractSchedule();

        Integer scheduleNumber = 1;
        for(List<WorkContractScheduleDay> workContractScheduleQuarter : workContractScheduleList) {

            Integer counter = 0;

            Set<WorkDaySchedule> scheduleSet = workContractVariationSchedule.retrieveScheduleWithScheduleDays(workContractScheduleQuarter);
            for (WorkDaySchedule workDaySchedule : scheduleSet) {
                if (!workDaySchedule.getDayOfWeek().isEmpty()) {
                    String dayOfWeek = workDaySchedule.getDayOfWeek() != null ? workDaySchedule.getDayOfWeek() : "";
                    String date = workDaySchedule.getDate() != null ? workDaySchedule.getDate().toString() : "";
                    String amFrom = workDaySchedule.getAmFrom() != null ? workDaySchedule.getAmFrom().toString() : "";
                    String amTo = workDaySchedule.getAmTo() != null ? workDaySchedule.getAmTo().toString() : "";
                    String pmFrom = workDaySchedule.getPmFrom() != null ? workDaySchedule.getPmFrom().toString() : "";
                    String pmTo = workDaySchedule.getPmTo() != null ? workDaySchedule.getPmTo().toString() : "";
                    String durationHours = Utilities.durationToTimeStringConverter(workDaySchedule.getDurationHours());

                    ContractDaySchedule contractDayScheduleJson = ContractDaySchedule.create()
                            .withDayOfWeek(dayOfWeek)
                            .withDate(date)
                            .withAmFrom(amFrom)
                            .withAmTo(amTo)
                            .withPmFrom(pmFrom)
                            .withPmTo(pmTo)
                            .withDurationHours(durationHours)
                            .build();

                    contractDayScheduleSet.put("Schedule" + scheduleNumber + " - workDay" + counter, contractDayScheduleJson);
                }

                counter++;

                schedule.setSchedule(contractDayScheduleSet);
            }

            scheduleNumber++;
        }

        return schedule;
    }
}
