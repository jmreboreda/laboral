package laboral.domain.contract;

public enum WorkDayTypeDuration {

    FULL_TIME("A tiempo completo"),
    PARTIAL_TIME("A tiempo parcial");

    private String workDayTypeDescription;

    WorkDayTypeDuration(String workDayTypeDescription) {
        this.workDayTypeDescription = workDayTypeDescription;
    }

    public String getDescription() {
        return workDayTypeDescription;
    }
}
