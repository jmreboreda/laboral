package laboral.domain.contract;

import laboral.domain.contract_json.ContractSchedule;
import laboral.domain.contract_type.WorkContractType;
import laboral.domain.employee.Employee;
import laboral.domain.employer.Employer;
import laboral.domain.interface_pattern.Contract;
import laboral.domain.person.Person;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.traceability_contract_documentation.TraceabilityContractDocumentation;
import laboral.domain.variation_type.VariationType;
import laboral.domain.work_center.WorkCenter;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

public class WorkContract implements Contract {

    private Integer id;
    private LocalDate dateSign;
    private Employer employer;
    private Employee employee;
    private WorkContractType workContractType;
    private Integer contractNumber;
    private VariationType variationType;
    private LocalDate startDate;
    private LocalDate expectedEndDate;
    private LocalDate modificationDate;
    private LocalDate endDate;
    private ContractSchedule contractSchedule;
    private String laborCategory;
    private QuoteAccountCode quoteAccountCode;
    private String officialIdentificationNumber;
    private String publicNotes;
    private String privateNotes;
    private LocalDateTime clientNotification;
    private WorkCenter workCenter;
    private WorkDayTypeDuration fullPartialWorkDay;
    private LocalDate idcReceptionDate;
    private LocalDate dateDeliveryDocumentation;
    private LocalDate endNoticeReceptionDate;

    public WorkContract(Integer id,
                        LocalDate dateSign,
                        Employer employer,
                        Employee employee,
                        WorkContractType workContractType,
                        Integer contractNumber,
                        VariationType variationType,
                        LocalDate startDate,
                        LocalDate expectedEndDate,
                        LocalDate modificationDate,
                        LocalDate endDate,
                        ContractSchedule contractSchedule,
                        String laborCategory,
                        QuoteAccountCode quoteAccountCode,
                        String officialIdentificationNumber,
                        String publicNotes,
                        String privateNotes,
                        LocalDateTime clientNotification,
                        WorkCenter workCenter,
                        WorkDayTypeDuration fullPartialWorkDay,
                        LocalDate idcReceptionDate,
                        LocalDate dateDeliveryDocumentation,
                        LocalDate endNoticeReceptionDate) {

        this.id = id;
        this.dateSign = dateSign;
        this.employer = employer;
        this.employee = employee;
        this.workContractType = workContractType;
        this.contractNumber = contractNumber;
        this.variationType = variationType;
        this.startDate = startDate;
        this.expectedEndDate = expectedEndDate;
        this.modificationDate = modificationDate;
        this.endDate = endDate;
        this.contractSchedule = contractSchedule;
        this.laborCategory = laborCategory;
        this.quoteAccountCode = quoteAccountCode;
        this.officialIdentificationNumber = officialIdentificationNumber;
        this.publicNotes = publicNotes;
        this.privateNotes = privateNotes;
        this.clientNotification = clientNotification;
        this.workCenter = workCenter;
        this.fullPartialWorkDay = fullPartialWorkDay;
        this.idcReceptionDate = idcReceptionDate;
        this.dateDeliveryDocumentation = dateDeliveryDocumentation;
        this.endNoticeReceptionDate = endNoticeReceptionDate;
    }

    public WorkContract() {
    }

    @Override
    public LocalDate getDateSign() {
        return dateSign;
    }

    @Override
    public void setDateSign(LocalDate dateSign) {

    }

    @Override
    public LocalDate getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(LocalDate startDate) {

    }

    @Override
    public WorkContractType getContractType() {
        return workContractType;
    }

    @Override
    public void setContractType(WorkContractType workContractType) {

    }

    public Integer getId() {
        return id;
    }

    public Integer getContractNumber() {
        return contractNumber;
    }

    public Employer getEmployer() {
        return employer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public WorkContractType getWorkContractType() {
        return workContractType;
    }

    public VariationType getVariationType() {
        return variationType;
    }

    public LocalDate getExpectedEndDate() {
        return expectedEndDate;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public ContractSchedule getContractSchedule() {
        return contractSchedule;
    }

    public String getLaborCategory() {
        return laborCategory;
    }

    public QuoteAccountCode getQuoteAccountCode() {
        return quoteAccountCode;
    }

    public String getOfficialIdentificationNumber() {
        return officialIdentificationNumber;
    }

    public String getPublicNotes() {
        return publicNotes;
    }

    public String getPrivateNotes() {
        return privateNotes;
    }

    public LocalDateTime getClientNotification() {
        return clientNotification;
    }

    public WorkCenter getWorkCenter() {
        return workCenter;
    }

    public WorkDayTypeDuration getFullPartialWorkDay() {
        return fullPartialWorkDay;
    }

    public LocalDate getIdcReceptionDate() {
        return idcReceptionDate;
    }

    public LocalDate getDateDeliveryDocumentation() {
        return dateDeliveryDocumentation;
    }

    public LocalDate getEndNoticeReceptionDate() {
        return endNoticeReceptionDate;
    }

    public String toString(){

        return getContractNumber().toString();
    }

    public String toInitialDescription(){
        return "[" + workContractType.getContractCode() + "] " + workContractType.getColloquial() + ", " + workContractType.getContractDescription();
    }

    public String toVariationDescription(){
        return "[" + workContractType.getContractCode() + "] " + variationType.getVariationDescription();
    }
    public static WorkContractBuilder create() {
        return new WorkContractBuilder();
    }

    public static final class WorkContractBuilder {
        private Integer id;
        private LocalDate dateSign;
        private Employer employer;
        private Employee employee;
        private WorkContractType workContractType;
        private Integer contractNumber;
        private VariationType variationType;
        private LocalDate startDate;
        private LocalDate expectedEndDate;
        private LocalDate modificationDate;
        private LocalDate endDate;
        private ContractSchedule contractSchedule;
        private String laborCategory;
        private QuoteAccountCode quoteAccountCode;
        private String officialIdentificationNumber;
        private String publicNotes;
        private String privateNotes;
        private LocalDateTime clientNotification;
        private WorkCenter workCenter;
        private WorkDayTypeDuration fullPartialWorkDay;
        private LocalDate idcReceptionDate;
        private LocalDate dateDeliveryDocumentation;
        private LocalDate endNoticeReceptionDate;

        private WorkContractBuilder() {
        }

        public static WorkContractBuilder creaTE() {
            return new WorkContractBuilder();
        }

        public WorkContractBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public WorkContractBuilder withDateSign(LocalDate dateSign) {
            this.dateSign = dateSign;
            return this;
        }

        public WorkContractBuilder withEmployer(Employer employer) {
            this.employer = employer;
            return this;
        }

        public WorkContractBuilder withEmployee(Employee employee) {
            this.employee = employee;
            return this;
        }

        public WorkContractBuilder withWorkContractType(WorkContractType workContractType) {
            this.workContractType = workContractType;
            return this;
        }

        public WorkContractBuilder withContractNumber(Integer contractNumber) {
            this.contractNumber = contractNumber;
            return this;
        }

        public WorkContractBuilder withVariationType(VariationType variationType) {
            this.variationType = variationType;
            return this;
        }

        public WorkContractBuilder withStartDate(LocalDate startDate) {
            this.startDate = startDate;
            return this;
        }

        public WorkContractBuilder withExpectedEndDate(LocalDate expectedEndDate) {
            this.expectedEndDate = expectedEndDate;
            return this;
        }

        public WorkContractBuilder withModificationDate(LocalDate modificationDate) {
            this.modificationDate = modificationDate;
            return this;
        }

        public WorkContractBuilder withEndDate(LocalDate endDate) {
            this.endDate = endDate;
            return this;
        }

        public WorkContractBuilder withContractSchedule(ContractSchedule contractSchedule) {
            this.contractSchedule = contractSchedule;
            return this;
        }

        public WorkContractBuilder withLaborCategory(String laborCategory) {
            this.laborCategory = laborCategory;
            return this;
        }

        public WorkContractBuilder withQuoteAccountCode(QuoteAccountCode quoteAccountCode) {
            this.quoteAccountCode = quoteAccountCode;
            return this;
        }

        public WorkContractBuilder withOfficialIdentificationNumber(String officialIdentificationNumber) {
            this.officialIdentificationNumber = officialIdentificationNumber;
            return this;
        }

        public WorkContractBuilder withPublicNotes(String publicNotes) {
            this.publicNotes = publicNotes;
            return this;
        }

        public WorkContractBuilder withPrivateNotes(String privateNotes) {
            this.privateNotes = privateNotes;
            return this;
        }

        public WorkContractBuilder withClientNotification(LocalDateTime clientNotification) {
            this.clientNotification = clientNotification;
            return this;
        }

        public WorkContractBuilder withWorkCenter(WorkCenter workCenter) {
            this.workCenter = workCenter;
            return this;
        }

        public WorkContractBuilder withFullPartialWorkDay(WorkDayTypeDuration fullPartialWorkDay) {
            this.fullPartialWorkDay = fullPartialWorkDay;
            return this;
        }

        public WorkContractBuilder withIdcReceptionDate(LocalDate idcReceptionDate) {
            this.idcReceptionDate = idcReceptionDate;
            return this;
        }

        public WorkContractBuilder withDateDeliveryDocumentation(LocalDate dateDeliveryDocumentation) {
            this.dateDeliveryDocumentation = dateDeliveryDocumentation;
            return this;
        }

        public WorkContractBuilder withEndNoticeReceptionDate(LocalDate endNoticeReceptionDate) {
            this.endNoticeReceptionDate = endNoticeReceptionDate;
            return this;
        }

        public WorkContract build() {
            return new WorkContract(id, dateSign, employer, employee, workContractType, contractNumber, variationType, startDate, expectedEndDate, modificationDate, endDate, contractSchedule, laborCategory, quoteAccountCode, officialIdentificationNumber, publicNotes, privateNotes, clientNotification, workCenter, fullPartialWorkDay, idcReceptionDate, dateDeliveryDocumentation, endNoticeReceptionDate);
        }
    }
}
