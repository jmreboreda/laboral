package laboral.domain.contract.work_contract.controller;

import laboral.domain.contract.WorkContract;
import laboral.domain.contract.WorkContractSituationRequest;
import laboral.domain.contract.work_contract.manager.WorkContractManager;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;

import java.time.LocalDate;
import java.util.List;

public class WorkContractController {

    WorkContractManager workContractManager = new WorkContractManager();

    public Integer createWorkContract(WorkContractSituationRequest workContractSituationRequest){

        return workContractManager.createWorkContract(workContractSituationRequest);
    }

    public WorkContract findById(Integer workContractId){

        return workContractManager.findById(workContractId);
    }

    public List<WorkContract> findByEmployerId(Integer employerId){

        return workContractManager.findByEmployerId(employerId);
    }

    public List<WorkContract> findByEmployeeId(Integer employeeId){

        return workContractManager.findByEmployeeId(employeeId);
    }

    public List<WorkContract> findAllByWorkContractNumber(Integer workContractNumber){

        return workContractManager.findAllByWorkContractNumber(workContractNumber);
    }

    public List<WorkContract> findByQuoteAccountCodeId(Integer quoteAccountCodeId){

        return workContractManager.findByQuoteAccountCodeId(quoteAccountCodeId);
    }

    public Integer findLastContractNumber(){

        return workContractManager.findLastContractNumber();

    }

    public List<WorkContractDBO> findAllWorkContractDBOWithTimeRecordInMonth(LocalDate initialDate, LocalDate finalDate){

        return workContractManager.findAllWorkContractDBOWithTimeRecordInMonth(initialDate, finalDate);
    }

    public List<WorkContract> findAllWorkContractWithTimeRecordInMonth(LocalDate initialDate, LocalDate finalDate){

        return workContractManager.findAllWorkContractWithTimeRecordInMonth(initialDate, finalDate);
    }

    public List<WorkContract> findActiveWorkContractAtDate(LocalDate date ){

        return workContractManager.findActiveWorkContractAtDate(date);

    }
}
