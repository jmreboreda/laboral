package laboral.domain.contract.work_contract.persistence.dao;

import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class WorkContractDAO implements GenericDAO<WorkContractDBO, Integer> {

    private SessionFactory sessionFactory;
    private Session session;

    private static final String FIND_HIGHEST_CONTRACT_NUMBER = "SELECT MAX(contractNumber) FROM WorkContractDBO";


    public WorkContractDAO() {
    }

    public static class ContractDAOFactory {

        private static WorkContractDAO workContractDAO;

        public static WorkContractDAO getInstance() {
            if(workContractDAO == null) {
                workContractDAO = new WorkContractDAO(HibernateUtil.retrieveGlobalSession());
            }
            return workContractDAO;
        }

    }

    public WorkContractDAO(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }


    @Override
    public Integer create(WorkContractDBO workContractDBO) {
        try {
            session.beginTransaction();
            session.saveOrUpdate(workContractDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }

        return workContractDBO.getId();
    }

    @Override
    public WorkContractDBO findById(Integer contractId) {
        TypedQuery<WorkContractDBO> query = session.createNamedQuery(WorkContractDBO.FIND_CONTRACT_BY_ID, WorkContractDBO.class);
        query.setParameter("contractId", contractId);

        return query.getSingleResult();
    }

    @Override
    public Integer update(WorkContractDBO workContractDBO) {
        try {
            session.beginTransaction();
            session.merge(workContractDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }

        return workContractDBO.getId();
    }

    @Override
    public Boolean delete(WorkContractDBO workContractDBO) {

        return null;
    }

    @Override
    public List<WorkContractDBO> findAll() {
        return null;
    }

    public List<WorkContractDBO> findByQuoteAccountCodeId(Integer quoteAccountCodeId){
        TypedQuery<WorkContractDBO> query = session.createNamedQuery(WorkContractDBO.FIND_CONTRACT_BY_QUOTE_ACCOUNT_CODE_ID, WorkContractDBO.class);
        query.setParameter("quoteAccountCodeId", quoteAccountCodeId);

        return query.getResultList();
    }

    public List<WorkContractDBO> findByEmployerId(Integer employerId) {
        TypedQuery<WorkContractDBO> query = session.createNamedQuery(WorkContractDBO.FIND_ALL_CONTRACTS_BY_EMPLOYER_ID, WorkContractDBO.class);
        query.setParameter("employerId", employerId);


        return query.getResultList();
    }

    public List<WorkContractDBO> findByEmployeeId(Integer employeeId) {
        TypedQuery<WorkContractDBO> query = session.createNamedQuery(WorkContractDBO.FIND_ALL_CONTRACTS_BY_EMPLOYEE_ID, WorkContractDBO.class);
        query.setParameter("employeeId", employeeId);


        return query.getResultList();
    }

    public List<WorkContractDBO> findActiveWorkContractAtDate(LocalDate date){
        TypedQuery<WorkContractDBO> query = session.createNamedQuery(WorkContractDBO.FIND_ACTIVE_CONTRACT_AT_DATE, WorkContractDBO.class);
        query.setParameter("date", Date.valueOf(date));

        return query.getResultList();
    }

    public WorkContractDBO findInitialWorkContractByWorkContractNumber(Integer workContractNumber){
        TypedQuery<WorkContractDBO> query = session.createNamedQuery(WorkContractDBO.FIND_INITIAL_CONTRACT_BY_CONTRACT_NUMBER, WorkContractDBO.class);
        query.setParameter("contractNumber", workContractNumber);

        return query.getSingleResult();
    }

    public List<WorkContractDBO> findAllByWorkContractNumber(Integer contractNumber){
        TypedQuery<WorkContractDBO> query = session.createNamedQuery(WorkContractDBO.FIND_ALL_BY_WORK_CONTRACT_NUMBER, WorkContractDBO.class);
        query.setParameter("contractNumber", contractNumber);

        return query.getResultList();
    }

    public Integer findHighestContractNumber(){
        Query query = session.createQuery(FIND_HIGHEST_CONTRACT_NUMBER);

        return (Integer) query.getSingleResult();
    }

//    public Integer findLastContractNumber(){
//        Query query = session.createQuery("SELECT MAX(contractNumber) FROM WorkContractDBO");
//
//        return (Integer) query.getSingleResult();
//
////            Integer lastContractNumber = (Integer) session.createNativeQuery("SELECT MAX(contractNumber) FROM contract", WorkContractDBO.class).getSingleResult();
//
//    }

    public List<WorkContractDBO> findAllWorkContractWithTimeRecordInMonth(LocalDate initialDate, LocalDate finalDate){
        TypedQuery<WorkContractDBO> query = session.createNamedQuery(WorkContractDBO.FIND_ALL_WORK_CONTRACT_WITH_TIME_RECORD_IN_PERIOD, WorkContractDBO.class);

        Date initialUtilDate = Date.valueOf(initialDate);
        Date finalUtilDate = Date.valueOf(finalDate);

        query.setParameter("initialDate", initialUtilDate);
        query.setParameter("finalDate", finalUtilDate);

        return  query.getResultList();
    }

    public WorkContractDBO findWorkContractVariationDBOByVariousParameters(Integer workContractNumber, Integer variationTypeCode, LocalDate startDateVariation){

        Date startDate = Date.valueOf(startDateVariation);

        TypedQuery<WorkContractDBO> query = session.createNamedQuery(WorkContractDBO.FIND_CONTRACT_VARIATION_BY_VARIOUS_PARAMETERS, WorkContractDBO.class);
        query.setParameter("contractNumber", workContractNumber);
        query.setParameter("variationTypeCode", variationTypeCode);
        query.setParameter("startDate", startDate);


        return query.getSingleResult();
    }
}
