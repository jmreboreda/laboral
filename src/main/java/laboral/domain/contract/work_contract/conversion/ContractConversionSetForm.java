package laboral.domain.contract.work_contract.conversion;

import laboral.component.generic_component.DateHourNotificationForm;
import laboral.component.work_contract.variation.component.WorkContractVariationSelector;
import laboral.component.work_contract.variation.component.WorkContractVariationVariationForm;

public class ContractConversionSetForm {

    private final WorkContractVariationSelector workContractVariationSelector;
    private final WorkContractVariationVariationForm workContractVariationVariationForm;
    private final DateHourNotificationForm dateHourNotificationForm;

    public ContractConversionSetForm(WorkContractVariationSelector workContractVariationSelector,
                                     WorkContractVariationVariationForm workContractVariationVariationForm,
                                     DateHourNotificationForm dateHourNotificationForm) {
        this.workContractVariationSelector = workContractVariationSelector;
        this.workContractVariationVariationForm = workContractVariationVariationForm;
        this.dateHourNotificationForm = dateHourNotificationForm;
    }

    public WorkContractVariationSelector getWorkContractVariationSelector() {
        return workContractVariationSelector;
    }

    public WorkContractVariationVariationForm getWorkContractVariationVariationForm() {
        return workContractVariationVariationForm;
    }

    public DateHourNotificationForm getDateHourNotificationForm() {
        return dateHourNotificationForm;
    }

    public static final class ContractConversionSetFormBuilder {
        private WorkContractVariationSelector workContractVariationSelector;
        private WorkContractVariationVariationForm workContractVariationVariationForm;
        private DateHourNotificationForm dateHourNotificationForm;

        private ContractConversionSetFormBuilder() {
        }

        public static ContractConversionSetFormBuilder create() {
            return new ContractConversionSetFormBuilder();
        }

        public ContractConversionSetFormBuilder withWorkContractVariationSelector(WorkContractVariationSelector workContractVariationSelector) {
            this.workContractVariationSelector = workContractVariationSelector;
            return this;
        }

        public ContractConversionSetFormBuilder withWorkContractVariationVariationForm(WorkContractVariationVariationForm workContractVariationVariationForm) {
            this.workContractVariationVariationForm = workContractVariationVariationForm;
            return this;
        }

        public ContractConversionSetFormBuilder withDateHourNotificationForm(DateHourNotificationForm dateHourNotificationForm) {
            this.dateHourNotificationForm = dateHourNotificationForm;
            return this;
        }

        public ContractConversionSetForm build() {
            return new ContractConversionSetForm(workContractVariationSelector, workContractVariationVariationForm, dateHourNotificationForm);
        }
    }
}
