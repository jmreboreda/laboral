package laboral.domain.contract.work_contract.conversion;

import javafx.scene.Scene;
import javafx.stage.Stage;
import laboral.component.generic_component.DateHourNotificationForm;
import laboral.component.work_contract.variation.WorkContractVariationConstants;
import laboral.component.work_contract.variation.component.WorkContractVariationSelector;
import laboral.component.work_contract.variation.component.WorkContractVariationVariationForm;
import laboral.domain.contract.WorkContract;
import laboral.domain.contract.WorkContractSituationRequest;
import laboral.domain.contract.work_contract.WorkContractSituationCreator;
import laboral.domain.contract.work_contract.controller.WorkContractController;
import laboral.domain.contract.work_contract.persistence.dao.WorkContractDAO;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.variation_type.VariationType;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

public class ContractConversionExecutor {

    WorkContractController workContractController = new WorkContractController();

    private Scene scene;
    private WorkContractVariationSelector workContractVariationSelector;
    private WorkContractVariationVariationForm workContractVariationVariationForm;
    private DateHourNotificationForm dateHourNotificationForm;

    public Integer executeConversion(Scene scene, ContractConversionSetForm contractConversionSetForm){
        this.scene = scene;
        this.workContractVariationSelector = contractConversionSetForm.getWorkContractVariationSelector();
        this.workContractVariationVariationForm = contractConversionSetForm.getWorkContractVariationVariationForm();
        this.dateHourNotificationForm = contractConversionSetForm.getDateHourNotificationForm();

        List<WorkContract> workContractList =
                workContractController.findAllByWorkContractNumber(workContractVariationSelector.getWorkContractSelector().getSelectionModel().getSelectedItem().getContractNumber());
        for (WorkContract workContract : workContractList) {
            if (workContract.getModificationDate() == null) {
                ContractConversionSituationEntryDataContainer contractConversionDataContainer =
                        ContractConversionSituationEntryDataContainerCreator.create(dateHourNotificationForm, workContractVariationVariationForm.getWorkContractConversion());
                WorkContractConversionDataVerifier workContractConversionDataVerifier = new WorkContractConversionDataVerifier();
                Set<WorkContractConversionDataVerifier.ContractConversionSituationProblem> verificationProblems =
                        workContractConversionDataVerifier.verify(workContract, contractConversionDataContainer);
                if (!verificationProblems.isEmpty()) {
                    showErrorConversionMessage(verificationProblems);
                } else {
                    int CONTRACT_TYPE_CODE_UNDEFINED_BONUS_TO_UNDEFINED_NORMAL = 402;
                    if(contractConversionDataContainer.getVariationType().getVariationCode().equals(CONTRACT_TYPE_CODE_UNDEFINED_BONUS_TO_UNDEFINED_NORMAL)){
                        Message.informationMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_UNDEFINED_BONUS_ALERT);
                        dateHourNotificationForm.getNotificationDate().setValue(null);
                        dateHourNotificationForm.getNotificationHour().setText("");
                        return null;
                    }
                    if(Message.confirmationMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.QUESTION_CONVERSION_CONTRACT_IS_CORRECT)){
                        Integer persistedConversionId = persistContractSituationConversion(workContract, contractConversionDataContainer);
                        if(persistedConversionId != null){
                            Message.informationMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_CONVERSION_PERSISTENCE_OK);
                            updatePreviousContractSituationModificationDate(workContractList);
                            return persistedConversionId;

                        }else{
                            Message.errorMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_CONVERSION_PERSISTENCE_NOT_OK);
                            return null;
                        }
                    }else {
                        initializeConversionPublicNotes();
                    }
                }
                return null;
            }
        }
        return null;
    }

    private void initializeConversionPublicNotes() {
        workContractVariationVariationForm.getWorkContractExtension().getPublicNotes().setText("");
    }

    private void showErrorConversionMessage(Set<WorkContractConversionDataVerifier.ContractConversionSituationProblem> verificationProblems) {
        String errorsMessage = "";
        for (WorkContractConversionDataVerifier.ContractConversionSituationProblem problem : verificationProblems) {
            errorsMessage = errorsMessage.concat(problem.toString() + "\n");
        }
        Message.errorMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, errorsMessage);
    }

    private Integer persistContractSituationConversion(WorkContract workContract, ContractConversionSituationEntryDataContainer contractConversionDataContainer){

        LocalTime clientHourNotification = contractConversionDataContainer.getClientHourNotification();
        LocalDate clientDateNotification = contractConversionDataContainer.getClientDateNotification();

        VariationType variationType = contractConversionDataContainer.getVariationType();

        ContractConversionSituationEntryDataContainer contractConversionDataEntryDataContainer =
                ContractConversionSituationEntryDataContainer.ContractConversionSituationEntryDataContainerBuilder.create()
                        .withConversionDateTo(contractConversionDataContainer.getConversionDateTo())
                        .withConversionDateFrom(contractConversionDataContainer.getConversionDateFrom())
                        .withVariationType(variationType)
                        .withClientDateNotification(clientDateNotification)
                        .withClientHourNotification(clientHourNotification)
                        .withPrivateNotes(contractConversionDataContainer.getPrivateNotes())
                        .withPublicNotes(contractConversionDataContainer.getPublicNotes())
                        .build();

        WorkContractSituationCreator workContractSituationCreator = new WorkContractSituationCreator();
        WorkContractSituationRequest workContractConversionSituationRequest =
                workContractSituationCreator.workContractConversionSituationRequestGenerator(workContract, contractConversionDataEntryDataContainer);

        return workContractSituationCreator.persistWorkContract(workContractConversionSituationRequest);
    }

    private void updatePreviousContractSituationModificationDate(List<WorkContract> workContractList){
        int VARIATION_TYPE_CONVERSION_CODE = workContractVariationVariationForm.getWorkContractConversion().getConversionTypeSelector().getSelectionModel()
                .getSelectedItem().getVariationCode();

        Integer initialContractSituationId = 0;

        for(WorkContract workContract : workContractList){
            if(workContract.getModificationDate() == null && workContract.getVariationType().getVariationCode() != VARIATION_TYPE_CONVERSION_CODE){
                initialContractSituationId = workContract.getId();
            }
        }

//        Date endNoticeReceptionDate = workContractVariationVariationForm.getWorkContractConversion().getDateTo() == null
//                ? Date.valueOf(LocalDate.of(9999,12,31))
//                : null;

        WorkContractDAO workContractDAO = WorkContractDAO.ContractDAOFactory.getInstance();
        WorkContractDBO workContractDBOToUpdate = workContractDAO.getSession().load(WorkContractDBO.class, initialContractSituationId);
        workContractDBOToUpdate.setModificationDate(Date.valueOf(workContractVariationVariationForm.getWorkContractConversion().getDateFrom().getValue()));
//        workContractDBOToUpdate.setEndNoticeReceptionDate(endNoticeReceptionDate);
        workContractDAO.update(workContractDBOToUpdate);
    }
}
