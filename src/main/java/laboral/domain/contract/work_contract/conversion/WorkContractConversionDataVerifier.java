package laboral.domain.contract.work_contract.conversion;

import laboral.domain.contract.WorkContract;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class WorkContractConversionDataVerifier {

    private final Set<ContractConversionSituationProblem> problems = new HashSet<>();

    public Set<ContractConversionSituationProblem> verify(WorkContract workContract, ContractConversionSituationEntryDataContainer conversionDataContainer){

        LocalDate expectedEndDate = workContract.getExpectedEndDate() == null
                ? null
                : workContract.getExpectedEndDate();

        if(conversionDataContainer.getVariationType() == null) {
            problems.add(WorkContractConversionDataVerifier.ContractConversionSituationProblem.CONVERSION_CAUSE_ERROR);
        }

        if(conversionDataContainer.getConversionDateFrom() == null){
            problems.add(ContractConversionSituationProblem.CONVERSION_DATE_FROM_ERROR);
        }

        int CONVERSION_CODE_FROM_FORMATION_CONTRACT_TO_NORMAL_CONTRACT = 401;
        if(conversionDataContainer.getConversionDateTo() == null &&
                (conversionDataContainer.getVariationType() != null &&
            conversionDataContainer.getVariationType().getVariationCode().equals(CONVERSION_CODE_FROM_FORMATION_CONTRACT_TO_NORMAL_CONTRACT))){
            problems.add(ContractConversionSituationProblem.CONVERSION_DATE_TO_ERROR);
        }

        int CONVERSION_CODE_FROM_PREGNANCY_RISK_TO_MATERNITY = 403;
        if(conversionDataContainer.getConversionDateTo() == null &&
                (conversionDataContainer.getVariationType() != null &&
                        conversionDataContainer.getVariationType().getVariationCode().equals(CONVERSION_CODE_FROM_PREGNANCY_RISK_TO_MATERNITY))){
            problems.add(ContractConversionSituationProblem.CONVERSION_DATE_TO_ERROR);
        }

        if(conversionDataContainer.getConversionDateFrom() != null &&
                conversionDataContainer.getConversionDateTo() != null &&
                conversionDataContainer.getConversionDateFrom().isAfter(conversionDataContainer.getConversionDateTo())) {
            problems.add(ContractConversionSituationProblem.CONVERSION_DATE_ERROR);
        }

        if(expectedEndDate != null &&
                conversionDataContainer.getConversionDateFrom() != null &&
                conversionDataContainer.getConversionDateFrom().isAfter(workContract.getExpectedEndDate())) {
            problems.add(ContractConversionSituationProblem.CONVERSION_DATE_ERROR);
        }

        if(conversionDataContainer.getVariationType() != null &&
                conversionDataContainer.getVariationType().getVariationCode().equals(CONVERSION_CODE_FROM_PREGNANCY_RISK_TO_MATERNITY)) {
            if (!isCorrectConversionFromPregnancyRiskToMaternity(workContract)) {
                problems.add(ContractConversionSituationProblem.CONVERSION_RISK_PREGNANCY_TO_MATERNITY_ERROR);
            }
        }

        int CONVERSION_CODE_FROM_FORMATION_TO_NORMAL = 401;
        if(conversionDataContainer.getVariationType() != null &&
                conversionDataContainer.getVariationType().getVariationCode().equals(CONVERSION_CODE_FROM_FORMATION_TO_NORMAL)){
                if(!isCorrectConversionFormationToNormal(workContract)){
                    problems.add(ContractConversionSituationProblem.CONVERSION_TEMPORAL_FROM_FORMATION_TO_NORMAL_ERROR);
            }
        }

        return Collections.unmodifiableSet(problems);
    }

    private Boolean isCorrectConversionFromPregnancyRiskToMaternity(WorkContract workContract){
        int CONTRACT_TYPE_CODE_PREGNANCY_RISK_FULL_TIME = 4104;
        int CONTRACT_TYPE_CODE_PREGNANCY_RISK_PARTIAL_TIME = 5104;

        if(workContract.getContractType().getContractCode().equals(CONTRACT_TYPE_CODE_PREGNANCY_RISK_FULL_TIME) ||
                workContract.getContractType().getContractCode().equals(CONTRACT_TYPE_CODE_PREGNANCY_RISK_PARTIAL_TIME)){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    private Boolean isCorrectConversionFormationToNormal(WorkContract workContract){
        String FORMATION_TEXT = "formación";
        if(workContract.getContractType().getColloquial().contains(FORMATION_TEXT)){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }


    public enum ContractConversionSituationProblem {
        CONVERSION_CAUSE_ERROR("ERROR. Causa de la conversión del contrato no establecida."),
        CONVERSION_DATE_FROM_ERROR("ERROR. Fecha inicial de la conversión no establecida."),
        CONVERSION_DATE_TO_ERROR("ERROR. Fecha final de la conversión no establecida."),
        CONVERSION_DATE_ERROR("ERROR. Fecha(s) inicial/final de la conversión incorrecta(s)."),
        CONVERSION_RISK_PREGNANCY_TO_MATERNITY_ERROR("ERROR. No existe una contratación por riesgo para el embarazo previa a la conversión."),
        CONVERSION_TEMPORAL_FROM_FORMATION_TO_NORMAL_ERROR("ERROR. No existe una contratación para formación previa a la conversión.");

        private String extensionProblemDescription;

        ContractConversionSituationProblem(String conversionProblemDescription) {
            this.extensionProblemDescription = conversionProblemDescription;
        }

        public String getConversionProblemDescription() {
            return extensionProblemDescription;
        }

        public String toString(){
            return getConversionProblemDescription();
        }
    }
}
