package laboral.domain.contract.work_contract.conversion;

import laboral.domain.variation_type.VariationType;

import java.time.LocalDate;
import java.time.LocalTime;

public class ContractConversionSituationEntryDataContainer {

    private final LocalDate clientDateNotification;
    private final LocalTime clientHourNotification;
    private final VariationType variationType;
    private final LocalDate conversionDateFrom;
    private final LocalDate conversionDateTo;
    private final String publicNotes;
    private final String privateNotes;

    public ContractConversionSituationEntryDataContainer(LocalDate clientDateNotification,
                                                         LocalTime clientHourNotification,
                                                         VariationType variationType,
                                                         LocalDate conversionDateFrom,
                                                         LocalDate conversionDateTo,
                                                         String publicNotes,
                                                         String privateNotes) {
        this.clientDateNotification = clientDateNotification;
        this.clientHourNotification = clientHourNotification;
        this.variationType = variationType;
        this.conversionDateFrom = conversionDateFrom;
        this.conversionDateTo = conversionDateTo;
        this.publicNotes = publicNotes;
        this.privateNotes = privateNotes;
    }

    public LocalDate getClientDateNotification() {
        return clientDateNotification;
    }

    public LocalTime getClientHourNotification() {
        return clientHourNotification;
    }

    public VariationType getVariationType() {
        return variationType;
    }

    public LocalDate getConversionDateFrom() {
        return conversionDateFrom;
    }

    public LocalDate getConversionDateTo() {
        return conversionDateTo;
    }

    public String getPublicNotes() {
        return publicNotes;
    }

    public String getPrivateNotes() {
        return privateNotes;
    }

    public static final class ContractConversionSituationEntryDataContainerBuilder {
        private LocalDate clientDateNotification;
        private LocalTime clientHourNotification;
        private VariationType variationType;
        private LocalDate conversionDateFrom;
        private LocalDate conversionDateTo;
        private String publicNotes;
        private String privateNotes;

        private ContractConversionSituationEntryDataContainerBuilder() {
        }

        public static ContractConversionSituationEntryDataContainerBuilder create() {
            return new ContractConversionSituationEntryDataContainerBuilder();
        }

        public ContractConversionSituationEntryDataContainerBuilder withClientDateNotification(LocalDate clientDateNotification) {
            this.clientDateNotification = clientDateNotification;
            return this;
        }

        public ContractConversionSituationEntryDataContainerBuilder withClientHourNotification(LocalTime clientHourNotification) {
            this.clientHourNotification = clientHourNotification;
            return this;
        }

        public ContractConversionSituationEntryDataContainerBuilder withVariationType(VariationType variationType) {
            this.variationType = variationType;
            return this;
        }

        public ContractConversionSituationEntryDataContainerBuilder withConversionDateFrom(LocalDate extensionDateFrom) {
            this.conversionDateFrom = extensionDateFrom;
            return this;
        }

        public ContractConversionSituationEntryDataContainerBuilder withConversionDateTo(LocalDate extensionDateTo) {
            this.conversionDateTo = extensionDateTo;
            return this;
        }

        public ContractConversionSituationEntryDataContainerBuilder withPublicNotes(String publicNotes) {
            this.publicNotes = publicNotes;
            return this;
        }

        public ContractConversionSituationEntryDataContainerBuilder withPrivateNotes(String privateNotes) {
            this.privateNotes = privateNotes;
            return this;
        }

        public ContractConversionSituationEntryDataContainer build() {
            return new ContractConversionSituationEntryDataContainer(clientDateNotification, clientHourNotification, variationType, conversionDateFrom, conversionDateTo, publicNotes, privateNotes);
        }
    }
}
