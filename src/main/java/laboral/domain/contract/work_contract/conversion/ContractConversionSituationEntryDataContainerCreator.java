package laboral.domain.contract.work_contract.conversion;

import laboral.component.generic_component.DateHourNotificationForm;
import laboral.component.work_contract.variation.component.WorkContractConversionForm;
import laboral.domain.utilities.utilities.Utilities;
import laboral.domain.variation_type.VariationType;
import org.apache.commons.lang.enums.EnumUtils;

import java.time.LocalDate;
import java.time.LocalTime;

public class ContractConversionSituationEntryDataContainerCreator {

    public static ContractConversionSituationEntryDataContainer create(DateHourNotificationForm dateHourNotificationForm, WorkContractConversionForm workContractConversionForm) {

        LocalDate notificationDate = dateHourNotificationForm.getNotificationDate().getValue() == null
                ? null
                : dateHourNotificationForm.getNotificationDate().getValue();

        LocalTime notificationHour = dateHourNotificationForm.getNotificationHour().getText() == null
                ? null
                : Utilities.convertStringToLocalTime(dateHourNotificationForm.getNotificationHour().getText());

        VariationType conversionVariationType = workContractConversionForm.getConversionTypeSelector().getSelectionModel().getSelectedItem() == null
                ? null
                : workContractConversionForm.getConversionTypeSelector().getSelectionModel().getSelectedItem();

        LocalDate conversionDateFrom = workContractConversionForm.getDateFrom().getValue() == null
                ? null
                : workContractConversionForm.getDateFrom().getValue();

        LocalDate conversionDateTo = workContractConversionForm.getDateTo().getValue() == null
                ? null
                : workContractConversionForm.getDateTo().getValue();

        return ContractConversionSituationEntryDataContainer.ContractConversionSituationEntryDataContainerBuilder.create()
                .withConversionDateFrom(conversionDateFrom)
                .withConversionDateTo(conversionDateTo)
                .withVariationType(conversionVariationType)
                .withClientDateNotification(notificationDate)
                .withClientHourNotification(notificationHour)
                .withPublicNotes(workContractConversionForm.getPublicNotes().getText())
                .withPrivateNotes(workContractConversionForm.getPrivateNotes().getText())
                .build();
    }
}
