package laboral.domain.contract.work_contract.mapper;

import laboral.domain.contract.WorkContractSituationRequest;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.employee.EmployeeService;
import laboral.domain.employee.persistence.dbo.EmployeeDBO;
import laboral.domain.employer.EmployerService;
import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.quote_account_code.QuoteAccountCodeService;
import laboral.domain.quote_account_code.persistence.dbo.QuoteAccountCodeDBO;
import laboral.domain.work_center.WorkCenterService;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;

import java.sql.Date;
import java.sql.Timestamp;

public class MapperWorkContractCreationRequestToWorkContractDBO implements GenericMapper<WorkContractSituationRequest, WorkContractDBO> {

    @Override
    public WorkContractDBO map(WorkContractSituationRequest workContractSituationRequest) {

        Timestamp registrationTimeStamp = Timestamp.valueOf(workContractSituationRequest.getClientNotification());

        EmployerService employerDBOService = EmployerService.EmployerServiceFactory.getInstance();
        EmployeeService employeeService = new EmployeeService();
        WorkCenterService workCenterService = WorkCenterService.WorkCenterServiceFactory.getInstance();

        String officialIdentificationNumber = workContractSituationRequest.getOfficialIdentificationNumber() == null
                ? null
                : workContractSituationRequest.getOfficialIdentificationNumber();

        EmployerDBO employerDBO = employerDBOService.findByClientId(workContractSituationRequest.getEmployer().getClient().getClientId());

        EmployeeDBO employeeDBO = employeeService.findByPersonId(workContractSituationRequest.getEmployee().getPersonId());

        WorkCenterDBO workCenterDBO = workContractSituationRequest.getWorkCenter() == null ? null : workCenterService.findWorkCenterById(workContractSituationRequest.getWorkCenter().getId());

        QuoteAccountCodeDBO quoteAccountCodeDBO;
        if(workContractSituationRequest.getQuoteAccountCode() != null) {
            QuoteAccountCodeService quoteAccountCodeService = new QuoteAccountCodeService();
            quoteAccountCodeDBO = quoteAccountCodeService.findById(workContractSituationRequest.getQuoteAccountCode().getId());
        }else{
            quoteAccountCodeDBO = null;
        }

        Date startDate = workContractSituationRequest.getStartDate() == null ? null : Date.valueOf(workContractSituationRequest.getStartDate());
        Date expectedEndDate = workContractSituationRequest.getExpectedEndDate() == null ? null : Date.valueOf(workContractSituationRequest.getExpectedEndDate());
        Date modificationDate = workContractSituationRequest.getModificationDate() == null ? null : Date.valueOf(workContractSituationRequest.getModificationDate());
        Date endDate = workContractSituationRequest.getEndDate() == null ? null : Date.valueOf(workContractSituationRequest.getEndDate());
        Date idcReceptionDate = workContractSituationRequest.getIdcReceptionDate() == null
                ? null
                : Date.valueOf(workContractSituationRequest.getIdcReceptionDate());
        Date dateDeliveryContractDocumentation = workContractSituationRequest.getDateDeliveryDocumentation() == null
                ? null
                : Date.valueOf(workContractSituationRequest.getDateDeliveryDocumentation());
        Date endNoticeReceptionDate = workContractSituationRequest.getEndNoticeReceptionDate() == null
                ? null
                : Date.valueOf(workContractSituationRequest.getEndNoticeReceptionDate());

        return WorkContractDBO.WorkContractDBOBuilder.create()
                .withVariationTypeCode(workContractSituationRequest.getVariationType().getVariationCode())
                .withOfficialIdentificationNumber(officialIdentificationNumber)
                .withEndingDate(endDate)
                .withContractNumber(workContractSituationRequest.getContractNumber())
                .withContractTypeCode(workContractSituationRequest.getWorkContractType().getContractCode())
                .withEmployerDBO(employerDBO)
                .withContractSchedule(workContractSituationRequest.getContractSchedule())
                .withClientNotification(registrationTimeStamp)
                .withFullPartialWorkDay(workContractSituationRequest.getFullPartialWorkDay())
                .withEmployeeDBO(employeeDBO)
                .withExpectedEndDate(expectedEndDate)
                .withModificationDate(modificationDate)
                .withPublicNotes(workContractSituationRequest.getPublicNotes())
                .withWorkCenter(workCenterDBO)
                .withId(null)
                .withLaborCategory(workContractSituationRequest.getLaborCategory())
                .withPrivateNotes(workContractSituationRequest.getPrivateNotes())
                .withQuoteAccountCode(quoteAccountCodeDBO)
                .withIdcReceptionDate(idcReceptionDate)
                .withEndNoticeReceptionDate(endNoticeReceptionDate)
                .withDateDeliveryDocumentation(dateDeliveryContractDocumentation)
                .withStartDate(startDate)
                .build();
    }
}
