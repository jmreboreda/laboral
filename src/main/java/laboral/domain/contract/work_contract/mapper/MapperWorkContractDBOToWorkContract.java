package laboral.domain.contract.work_contract.mapper;

import laboral.domain.contract.WorkContract;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.contract_type.WorkContractType;
import laboral.domain.contract_type.WorkContractTypeService;
import laboral.domain.contract_type.mapper.MapperWorkContractTypeDBOToWorkContractType;
import laboral.domain.contract_type.persistence.dbo.WorkContractTypeDBO;
import laboral.domain.employee.Employee;
import laboral.domain.employee.EmployeeService;
import laboral.domain.employee.mapper.MapperEmployeeDBOToEmployee;
import laboral.domain.employee.persistence.dbo.EmployeeDBO;
import laboral.domain.employer.Employer;
import laboral.domain.employer.EmployerService;
import laboral.domain.employer.mapper.MapperEmployerDBOToEmployer;
import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.person.Person;
import laboral.domain.person.PersonService;
import laboral.domain.person.mapper.MapperPersonDBOToPerson;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.quote_account_code.mapper.MapperQuoteAccountCodeDBOToQuoteAccountCode;
import laboral.domain.variation_type.VariationType;
import laboral.domain.variation_type.VariationTypeService;
import laboral.domain.variation_type.mapper.MapperVariationTypeDBOToVariationType;
import laboral.domain.variation_type.persistence.dbo.VariationTypeDBO;
import laboral.domain.work_center.WorkCenter;
import laboral.domain.work_center.WorkCenterService;
import laboral.domain.work_center.mapper.MapperWorkCenterDBOToWorkCenter;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class MapperWorkContractDBOToWorkContract implements GenericMapper<WorkContractDBO, WorkContract> {

    @Override
    public WorkContract map(WorkContractDBO workContractDBO) {

        MapperEmployerDBOToEmployer mapperEmployerDBOToEmployer = new MapperEmployerDBOToEmployer();
        MapperEmployeeDBOToEmployee mapperEmployeeDBOToEmployee = new MapperEmployeeDBOToEmployee();
        MapperWorkContractTypeDBOToWorkContractType mapperWorkContractTypeDBOToWorkContractType = new MapperWorkContractTypeDBOToWorkContractType();
        MapperVariationTypeDBOToVariationType mapperVariationTypeDBOToVariationType = new MapperVariationTypeDBOToVariationType();
        MapperWorkCenterDBOToWorkCenter mapperWorkCenterDBOToWorkCenter = new MapperWorkCenterDBOToWorkCenter();
        MapperQuoteAccountCodeDBOToQuoteAccountCode mapperQuoteAccountCodeDBOToQuoteAccountCode = new MapperQuoteAccountCodeDBOToQuoteAccountCode();

        EmployerService employerService = EmployerService.EmployerServiceFactory.getInstance();
        EmployeeService employeeService = new EmployeeService();
        WorkContractTypeService workContractTypeService = WorkContractTypeService.WorkContractTypeServiceFactory.getInstance();
        WorkCenterService workCenterService = WorkCenterService.WorkCenterServiceFactory.getInstance();

        EmployerDBO employerDBO = employerService.findById(workContractDBO.getEmployerDBO().getId());
        Employer employer = mapperEmployerDBOToEmployer.map(employerDBO);

        EmployeeDBO employeeDBO = employeeService.findById(workContractDBO.getEmployeeDBO().getId());
        Employee employee = mapperEmployeeDBOToEmployee.map(employeeDBO);

        WorkContractTypeDBO workContractTypeDBO = workContractTypeService.findByWorkContractTypeCode(workContractDBO.getContractTypeCode());
        WorkContractType workContractType = mapperWorkContractTypeDBOToWorkContractType.map(workContractTypeDBO);

        VariationTypeService variationTypeService = VariationTypeService.VariationTypeServiceFactory.getInstance();
        VariationTypeDBO variationTypeDBO = variationTypeService.findByVariationTypeCode(workContractDBO.getVariationTypeCode());
        VariationType variationType = mapperVariationTypeDBOToVariationType.map(variationTypeDBO);

        WorkCenterDBO workCenterDBO = workContractDBO.getWorkCenter() == null ? null :
                workCenterService.findWorkCenterById(workContractDBO.getWorkCenter().getId());
        WorkCenter workCenter = workContractDBO.getWorkCenter() == null ? null : mapperWorkCenterDBOToWorkCenter.map(workCenterDBO);

        QuoteAccountCode quoteAccountCode = mapperQuoteAccountCodeDBOToQuoteAccountCode.map(workContractDBO.getQuoteAccountCode());
        LocalDate startDate = workContractDBO.getStartDate() == null ? null : workContractDBO.getStartDate().toLocalDate();
        LocalDate expectedEndDate = workContractDBO.getExpectedEndDate() == null ? null : workContractDBO.getExpectedEndDate().toLocalDate();
        LocalDate modificationDate = workContractDBO.getModificationDate() == null ? null : workContractDBO.getModificationDate().toLocalDate();
        LocalDate endDate = workContractDBO.getEndingDate() == null ? null : workContractDBO.getEndingDate().toLocalDate();

        LocalDateTime localDateTime = workContractDBO.getClientNotification() == null
                ? null
                : workContractDBO.getClientNotification().toLocalDateTime();

        return WorkContract.create()
                .withId(workContractDBO.getId())
                .withContractNumber(workContractDBO.getContractNumber())
                .withEmployer(employer)
                .withEmployee(employee)
                .withWorkContractType(workContractType)
                .withOfficialIdentificationNumber(workContractDBO.getOfficialIdentificationNumber())
                .withDateSign(null)
                .withStartDate(startDate)
                .withExpectedEndDate(expectedEndDate)
                .withModificationDate(modificationDate)
                .withEndDate(endDate)
                .withLaborCategory(workContractDBO.getLaborCategory())
                .withPublicNotes(workContractDBO.getPublicNotes())
                .withPrivateNotes(workContractDBO.getPrivateNotes())
                .withContractSchedule(workContractDBO.getContractSchedule())
                .withVariationType(variationType)
                .withWorkCenter(workCenter)
                .withQuoteAccountCode(quoteAccountCode)
                .withClientNotification(localDateTime)
                .build();
    }
}
