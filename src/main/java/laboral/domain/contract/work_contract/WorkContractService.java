package laboral.domain.contract.work_contract;

import laboral.domain.contract.work_contract.controller.WorkContractController;
import laboral.domain.contract.work_contract.persistence.dao.WorkContractDAO;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;

import java.time.LocalDate;
import java.util.List;

public class WorkContractService {

    private final WorkContractController workContractController = new WorkContractController();
    private final WorkContractDAO workContractDAO = WorkContractDAO.ContractDAOFactory.getInstance();

    private WorkContractService() {
    }

    public static class WorkContractServiceFactory {

        private static WorkContractService clientService;

        public static WorkContractService getInstance() {
            if(clientService == null) {
                clientService = new WorkContractService();
            }
            return clientService;
        }
    }
    public WorkContractDBO findById(Integer id){

        return workContractDAO.findById(id);
    }

    public List<WorkContractDBO> findAllWorkContractDBOWithTimeRecordInMonth(LocalDate initialDate, LocalDate finalDate){

        return workContractDAO.findAllWorkContractWithTimeRecordInMonth(initialDate, finalDate);
    }

    public List<WorkContractDBO> findAllWorkContractWithTimeRecordInMonth(LocalDate initialDate, LocalDate finalDate){

        return workContractDAO.findAllWorkContractWithTimeRecordInMonth(initialDate, finalDate);
    }

//
    public WorkContractDBO findWorkContractVariationDBOByVariousParameters(Integer workContractNumber, Integer variationTypeCode, LocalDate startDate){
//
//        return WorkContractService.findWorkContractVariationDBOByVariousParameters(workContractNumber, variationTypeCode, startDate);
        return null;
    }
}
