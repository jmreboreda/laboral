package laboral.domain.contract.work_contract;

import laboral.ApplicationConstants;
import laboral.component.work_contract.WorkDaySchedule;
import laboral.domain.utilities.utilities.Utilities;

import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Set;

public class ContractDataToSubfolder {

    private  Boolean isContractExtinction;
    private  Boolean isContractExtension;
    private  Boolean isContractConversion;
    private  String notificationType;
    private  String officialContractNumber;
    private  String employerFullName;
    private  String employerQuoteAccountCode;
    private  LocalDate notificationDate;
    private  Timestamp notificationHour;
    private  String employeeFullName;
    private  String employeeNif;
    private  String employeeNASS;
    private  LocalDate employeeBirthDate;
    private  String employeeCivilState;
    private  String employeeNationality;
    private  String employeeFullAddress;
    private  String employeeMaxStudyLevel;
    private  Set<DayOfWeek> dayOfWeekSet;
    private  String hoursWorkWeek;
    private  String contractTypeDescription;
    private  LocalDate dateFrom;
    private  LocalDate dateTo;
    private  String durationDays;
    private  Set<WorkDaySchedule> schedule;
    private  String additionalData;
    private  String laborCategory;
    private  String gmContractNumber;

    public ContractDataToSubfolder(Boolean isContractExtinction, Boolean isContractExtension, Boolean isContractConversion, String notificationType,
                                   String officialContractNumber, String employerFullName, String employerQuoteAccountCode, LocalDate notificationDate,
                                   Timestamp notificationHour, String employeeFullName, String employeeNif, String employeeNASS,
                                   LocalDate employeeBirthDate, String employeeCivilState, String employeeNationality, String employeeFullAddress,
                                   String employeeMaxStudyLevel, Set<DayOfWeek> dayOfWeekSet, String hoursWorkWeek, String contractTypeDescription,
                                   LocalDate dateFrom, LocalDate dateTo, String durationDays, Set<WorkDaySchedule> schedule,
                                   String additionalData, String laborCategory, String gmContractNumber) {

        this.isContractExtinction = isContractExtinction;
        this.isContractExtension = isContractExtension;
        this.isContractConversion = isContractConversion;
        this.notificationType = notificationType;
        this.officialContractNumber = officialContractNumber;
        this.employerFullName = employerFullName;
        this.employerQuoteAccountCode = employerQuoteAccountCode;
        this.notificationDate = notificationDate;
        this.notificationHour = notificationHour;
        this.employeeFullName = employeeFullName;
        this.employeeNif = employeeNif;
        this.employeeNASS = employeeNASS;
        this.employeeBirthDate = employeeBirthDate;
        this.employeeCivilState = employeeCivilState;
        this.employeeNationality = employeeNationality;
        this.employeeFullAddress = employeeFullAddress;
        this.employeeMaxStudyLevel = employeeMaxStudyLevel;
        this.dayOfWeekSet = dayOfWeekSet;
        this.hoursWorkWeek = hoursWorkWeek;
        this.contractTypeDescription = contractTypeDescription;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.durationDays = durationDays;
        this.schedule = schedule;
        this.additionalData = additionalData;
        this.laborCategory = laborCategory;
        this.gmContractNumber = gmContractNumber;
    }

    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());

    public Boolean getContractExtinction() {
        return isContractExtinction;
    }

    public Boolean getContractExtension() {
        return isContractExtension;
    }

    public Boolean getContractConversion() {
        return isContractConversion;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public String getOfficialContractNumber() {
        return officialContractNumber;
    }

    public String getEmployerFullName() {
        return employerFullName;
    }

    public String getEmployerQuoteAccountCode() {
        return employerQuoteAccountCode;
    }

    public LocalDate getNotificationDate() {
        return notificationDate;
    }

    public Timestamp getNotificationHour() {
        return notificationHour;
    }

    public String getEmployeeFullName() {
        return employeeFullName;
    }

    public String getEmployeeNif() {
        return employeeNif;
    }

    public String getEmployeeNASS() {
        return employeeNASS;
    }

    public LocalDate getEmployeeBirthDate() {
        return employeeBirthDate;
    }

    public String getEmployeeCivilState() {
        return employeeCivilState;
    }

    public String getEmployeeNationality() {
        return employeeNationality;
    }

    public String getEmployeeFullAddress() {
        return employeeFullAddress;
    }

    public String getEmployeeMaxStudyLevel() {
        return employeeMaxStudyLevel;
    }

    public Set<DayOfWeek> getDayOfWeekSet() {
        return dayOfWeekSet;
    }

    public String getHoursWorkWeek() {
        return hoursWorkWeek;
    }

    public String getContractTypeDescription() {
        return contractTypeDescription;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public String getDurationDays() {
        return durationDays;
    }

    public Set<WorkDaySchedule> getSchedule() {
        return schedule;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public String getLaborCategory() {
        return laborCategory;
    }

    public String getGmContractNumber() {
        return gmContractNumber;
    }

    public String toFileName(){

        String fileNameDate = dateFrom != null ? dateFrom.format(dateFormatter) : dateTo.format(dateFormatter);

        return Utilities.replaceWithUnderscore(employerFullName)
                + "_" +
                Utilities.replaceWithUnderscore(getNotificationType().toLowerCase())
                + "_" +
                fileNameDate
                + "_" +
                Utilities.replaceWithUnderscore(employeeFullName);
    }

    public static final class ContractDataToSubfolderBuilder {
        private Boolean isContractExtinction;
        private Boolean isContractExtension;
        private Boolean isContractConversion;
        private String notificationType;
        private String officialContractNumber;
        private String employerFullName;
        private String employerQuoteAccountCode;
        private LocalDate notificationDate;
        private Timestamp notificationHour;
        private String employeeFullName;
        private String employeeNif;
        private String employeeNASS;
        private LocalDate employeeBirthDate;
        private String employeeCivilState;
        private String employeeNationality;
        private String employeeFullAddress;
        private String employeeMaxStudyLevel;
        private Set<DayOfWeek> dayOfWeekSet;
        private String hoursWorkWeek;
        private String contractTypeDescription;
        private LocalDate dateFrom;
        private LocalDate dateTo;
        private String durationDays;
        private Set<WorkDaySchedule> schedule;
        private String additionalData;
        private String laborCategory;
        private String gmContractNumber;

        private ContractDataToSubfolderBuilder() {
        }

        public static ContractDataToSubfolderBuilder create() {
            return new ContractDataToSubfolderBuilder();
        }

        public ContractDataToSubfolderBuilder withIsContractExtinction(Boolean isContractExtinction) {
            this.isContractExtinction = isContractExtinction;
            return this;
        }

        public ContractDataToSubfolderBuilder withIsContractExtension(Boolean isContractExtension) {
            this.isContractExtension = isContractExtension;
            return this;
        }

        public ContractDataToSubfolderBuilder withIsContractConversion(Boolean isContractConversion) {
            this.isContractConversion = isContractConversion;
            return this;
        }

        public ContractDataToSubfolderBuilder withNotificationType(String notificationType) {
            this.notificationType = notificationType;
            return this;
        }

        public ContractDataToSubfolderBuilder withOfficialContractNumber(String officialContractNumber) {
            this.officialContractNumber = officialContractNumber;
            return this;
        }

        public ContractDataToSubfolderBuilder withEmployerFullName(String employerFullName) {
            this.employerFullName = employerFullName;
            return this;
        }

        public ContractDataToSubfolderBuilder withEmployerQuoteAccountCode(String employerQuoteAccountCode) {
            this.employerQuoteAccountCode = employerQuoteAccountCode;
            return this;
        }

        public ContractDataToSubfolderBuilder withNotificationDate(LocalDate notificationDate) {
            this.notificationDate = notificationDate;
            return this;
        }

        public ContractDataToSubfolderBuilder withNotificationHour(Timestamp notificationHour) {
            this.notificationHour = notificationHour;
            return this;
        }

        public ContractDataToSubfolderBuilder withEmployeeFullName(String employeeFullName) {
            this.employeeFullName = employeeFullName;
            return this;
        }

        public ContractDataToSubfolderBuilder withEmployeeNif(String employeeNif) {
            this.employeeNif = employeeNif;
            return this;
        }

        public ContractDataToSubfolderBuilder withEmployeeNASS(String employeeNASS) {
            this.employeeNASS = employeeNASS;
            return this;
        }

        public ContractDataToSubfolderBuilder withEmployeeBirthDate(LocalDate employeeBirthDate) {
            this.employeeBirthDate = employeeBirthDate;
            return this;
        }

        public ContractDataToSubfolderBuilder withEmployeeCivilState(String employeeCivilState) {
            this.employeeCivilState = employeeCivilState;
            return this;
        }

        public ContractDataToSubfolderBuilder withEmployeeNationality(String employeeNationality) {
            this.employeeNationality = employeeNationality;
            return this;
        }

        public ContractDataToSubfolderBuilder withEmployeeFullAddress(String employeeFullAddress) {
            this.employeeFullAddress = employeeFullAddress;
            return this;
        }

        public ContractDataToSubfolderBuilder withEmployeeMaxStudyLevel(String employeeMaxStudyLevel) {
            this.employeeMaxStudyLevel = employeeMaxStudyLevel;
            return this;
        }

        public ContractDataToSubfolderBuilder withDayOfWeekSet(Set<DayOfWeek> dayOfWeekSet) {
            this.dayOfWeekSet = dayOfWeekSet;
            return this;
        }

        public ContractDataToSubfolderBuilder withHoursWorkWeek(String hoursWorkWeek) {
            this.hoursWorkWeek = hoursWorkWeek;
            return this;
        }

        public ContractDataToSubfolderBuilder withContractTypeDescription(String contractTypeDescription) {
            this.contractTypeDescription = contractTypeDescription;
            return this;
        }

        public ContractDataToSubfolderBuilder withDateFrom(LocalDate dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        public ContractDataToSubfolderBuilder withDateTo(LocalDate dateTo) {
            this.dateTo = dateTo;
            return this;
        }

        public ContractDataToSubfolderBuilder withDurationDays(String durationDays) {
            this.durationDays = durationDays;
            return this;
        }

        public ContractDataToSubfolderBuilder withSchedule(Set<WorkDaySchedule> schedule) {
            this.schedule = schedule;
            return this;
        }

        public ContractDataToSubfolderBuilder withAdditionalData(String additionalData) {
            this.additionalData = additionalData;
            return this;
        }

        public ContractDataToSubfolderBuilder withLaborCategory(String laborCategory) {
            this.laborCategory = laborCategory;
            return this;
        }

        public ContractDataToSubfolderBuilder withGmContractNumber(String gmContractNumber) {
            this.gmContractNumber = gmContractNumber;
            return this;
        }

        public ContractDataToSubfolder build() {
            return new ContractDataToSubfolder(isContractExtinction, isContractExtension, isContractConversion, notificationType, officialContractNumber, employerFullName, employerQuoteAccountCode, notificationDate, notificationHour, employeeFullName, employeeNif, employeeNASS, employeeBirthDate, employeeCivilState, employeeNationality, employeeFullAddress, employeeMaxStudyLevel, dayOfWeekSet, hoursWorkWeek, contractTypeDescription, dateFrom, dateTo, durationDays, schedule, additionalData, laborCategory, gmContractNumber);
        }
    }
}
