package laboral.domain.contract.work_contract.extension;

import laboral.domain.variation_type.VariationType;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;

public class ContractExtensionSituationEntryDataContainer {

    private final LocalDate clientDateNotification;
    private final LocalTime clientHourNotification;
    private final VariationType variationType;
    private final LocalDate extensionDateFrom;
    private final LocalDate extensionDateTo;
    private final String extensionDuration;
    private final String publicNotes;
    private final String privateNotes;

    public ContractExtensionSituationEntryDataContainer(LocalDate clientDateNotification,
                                                        LocalTime clientHourNotification,
                                                        VariationType variationType,
                                                        LocalDate extensionDateFrom,
                                                        LocalDate extensionDateTo,
                                                        String extensionDuration,
                                                        String publicNotes,
                                                        String privateNotes) {
        this.clientDateNotification = clientDateNotification;
        this.clientHourNotification = clientHourNotification;
        this.variationType = variationType;
        this.extensionDateFrom = extensionDateFrom;
        this.extensionDateTo = extensionDateTo;
        this.extensionDuration = extensionDuration;
        this.publicNotes = publicNotes;
        this.privateNotes = privateNotes;
    }

    public LocalDate getClientDateNotification() {
        return clientDateNotification;
    }

    public LocalTime getClientHourNotification() {
        return clientHourNotification;
    }

    public VariationType getVariationType() {
        return variationType;
    }

    public LocalDate getExtensionDateFrom() {
        return extensionDateFrom;
    }

    public LocalDate getExtensionDateTo() {
        return extensionDateTo;
    }

    public String getExtensionDuration() {
        return extensionDuration;
    }

    public String getPublicNotes() {
        return publicNotes;
    }

    public String getPrivateNotes() {
        return privateNotes;
    }

    public static final class ContractExtensionSituationEntryDataContainerBuilder {
        private LocalDate clientDateNotification;
        private LocalTime clientHourNotification;
        private VariationType variationType;
        private LocalDate extensionDateFrom;
        private LocalDate extensionDateTo;
        private String extensionDuration;
        private String publicNotes;
        private String privateNotes;

        private ContractExtensionSituationEntryDataContainerBuilder() {
        }

        public static ContractExtensionSituationEntryDataContainerBuilder create() {
            return new ContractExtensionSituationEntryDataContainerBuilder();
        }

        public ContractExtensionSituationEntryDataContainerBuilder withClientDateNotification(LocalDate clientDateNotification) {
            this.clientDateNotification = clientDateNotification;
            return this;
        }

        public ContractExtensionSituationEntryDataContainerBuilder withClientHourNotification(LocalTime clientHourNotification) {
            this.clientHourNotification = clientHourNotification;
            return this;
        }

        public ContractExtensionSituationEntryDataContainerBuilder withVariationType(VariationType variationType) {
            this.variationType = variationType;
            return this;
        }

        public ContractExtensionSituationEntryDataContainerBuilder withExtensionDateFrom(LocalDate extensionDateFrom) {
            this.extensionDateFrom = extensionDateFrom;
            return this;
        }

        public ContractExtensionSituationEntryDataContainerBuilder withExtensionDateTo(LocalDate extensionDateTo) {
            this.extensionDateTo = extensionDateTo;
            return this;
        }

        public ContractExtensionSituationEntryDataContainerBuilder withExtensionDuration(String extensionDuration) {
            this.extensionDuration = extensionDuration;
            return this;
        }

        public ContractExtensionSituationEntryDataContainerBuilder withPublicNotes(String publicNotes) {
            this.publicNotes = publicNotes;
            return this;
        }

        public ContractExtensionSituationEntryDataContainerBuilder withPrivateNotes(String privateNotes) {
            this.privateNotes = privateNotes;
            return this;
        }

        public ContractExtensionSituationEntryDataContainer build() {
            return new ContractExtensionSituationEntryDataContainer(clientDateNotification, clientHourNotification, variationType, extensionDateFrom, extensionDateTo, extensionDuration, publicNotes, privateNotes);
        }
    }
}
