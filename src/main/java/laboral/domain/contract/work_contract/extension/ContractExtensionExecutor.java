package laboral.domain.contract.work_contract.extension;

import javafx.scene.Scene;
import javafx.stage.Stage;
import laboral.component.generic_component.DateHourNotificationForm;
import laboral.component.work_contract.variation.WorkContractVariationConstants;
import laboral.component.work_contract.variation.component.WorkContractVariationSelector;
import laboral.component.work_contract.variation.component.WorkContractVariationVariationForm;
import laboral.domain.contract.WorkContract;
import laboral.domain.contract.WorkContractSituationRequest;
import laboral.domain.contract.work_contract.WorkContractSituationCreator;
import laboral.domain.contract.work_contract.controller.WorkContractController;
import laboral.domain.contract.work_contract.persistence.dao.WorkContractDAO;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.utilities.utilities.Utilities;
import laboral.domain.variation_type.VariationType;

import java.sql.Date;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

public class ContractExtensionExecutor {

    WorkContractController workContractController = new WorkContractController();

    private Scene scene;
    private WorkContractVariationSelector workContractVariationSelector;
    private WorkContractVariationVariationForm workContractVariationVariationForm;
    private DateHourNotificationForm dateHourNotificationForm;

    public Integer executeExtension(Scene scene, ContractExtensionSetForm contractExtensionSetForm){
        this.scene = scene;
        this.workContractVariationSelector = contractExtensionSetForm.getWorkContractVariationSelector();
        this.workContractVariationVariationForm = contractExtensionSetForm.getWorkContractVariationVariationForm();
        this.dateHourNotificationForm = contractExtensionSetForm.getDateHourNotificationForm();

        List<WorkContract> workContractList =
                workContractController.findAllByWorkContractNumber(workContractVariationSelector.getWorkContractSelector().getSelectionModel().getSelectedItem().getContractNumber());
        for (WorkContract workContract : workContractList) {
            if (workContract.getModificationDate() == null) {
                ContractExtensionSituationEntryDataContainer contractExtensionDataContainer =
                        ContractExtensionSituationEntryDataContainerCreator.create(dateHourNotificationForm, workContractVariationVariationForm.getWorkContractExtension());
                WorkContractExtensionDataVerifier workContractExtensionDataVerifier = new WorkContractExtensionDataVerifier();
                Set<WorkContractExtensionDataVerifier.ContractExtensionSituationProblem> verificationProblems =
                        workContractExtensionDataVerifier.verify(workContract, contractExtensionDataContainer);
                if (verificationProblems.isEmpty()) {
                    if(Message.confirmationMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.QUESTION_EXTENSION_CONTRACT_IS_CORRECT)){
                        Integer persistedExtensionId = persistContractSituationExtension(workContract);
                        if(persistedExtensionId != null){
                            updatePreviousContractSituationModificationDate(workContractList);
                            return persistedExtensionId;

                        }else{
                            Message.errorMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTENSION_PERSISTENCE_NOT_OK);
                            return null;
                        }
                    }else {
                        initializeExtensionPublicNotes();
                    }
                }
                else{
                    showErrorExtensionMessage(verificationProblems);
                }
                return null;
            }
        }
        return null;
    }

    private void initializeExtensionPublicNotes() {
        workContractVariationVariationForm.getWorkContractExtension().getPublicNotes().setText("");
    }

    private void showErrorExtensionMessage(Set<WorkContractExtensionDataVerifier.ContractExtensionSituationProblem> verificationProblems) {
        String errorsMessage = "";
        for (WorkContractExtensionDataVerifier.ContractExtensionSituationProblem problem : verificationProblems) {
            errorsMessage = errorsMessage.concat(problem.toString() + "\n");
        }
        Message.errorMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, errorsMessage);
    }

    private Integer persistContractSituationExtension(WorkContract workContract){

        LocalTime clientHourNotification = Utilities.convertStringToLocalTime(dateHourNotificationForm.getNotificationHour().getText());
        VariationType variationType = VariationType.VariationTypeBuilder.create()
                .withVariationCode(220)
                .build();

        ContractExtensionSituationEntryDataContainer contractExtensionDataEntryDataContainer =
                ContractExtensionSituationEntryDataContainer.ContractExtensionSituationEntryDataContainerBuilder.create()
                        .withExtensionDateTo(workContractVariationVariationForm.getWorkContractExtension().getDateTo().getValue())
                        .withExtensionDateFrom(workContractVariationVariationForm.getWorkContractExtension().getDateFrom().getValue())
                        .withVariationType(variationType)
                        .withClientDateNotification(dateHourNotificationForm.getNotificationDate().getValue())
                        .withClientHourNotification(clientHourNotification)
                        .withPrivateNotes(workContractVariationVariationForm.getWorkContractExtension().getPrivateNotes().getText())
                        .withPublicNotes(workContractVariationVariationForm.getWorkContractExtension().getPublicNotes().getText())
                        .build();

        WorkContractSituationCreator workContractSituationCreator = new WorkContractSituationCreator();
        WorkContractSituationRequest workContractExtensionSituationRequest =
                workContractSituationCreator.workContractExtensionSituationRequestGenerator(workContract, contractExtensionDataEntryDataContainer);

        Integer workContractExtensionId = workContractSituationCreator.persistWorkContract(workContractExtensionSituationRequest);

        if(workContractExtensionId != null){
            Message.informationMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTENSION_PERSISTENCE_OK);
            return workContractExtensionId;

        }else{
            Message.errorMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTENSION_PERSISTENCE_NOT_OK);
        }
        return null;
    }

    private void updatePreviousContractSituationModificationDate(List<WorkContract> workContractList){
        int VARIATION_TYPE_EXTENSION_CODE = 220;
        Integer initialContractSituationId = 0;

        for(WorkContract workContract : workContractList){
            if(workContract.getModificationDate() == null && workContract.getVariationType().getVariationCode() != VARIATION_TYPE_EXTENSION_CODE){
                initialContractSituationId = workContract.getId();
            }
        }
        WorkContractDAO workContractDAO = WorkContractDAO.ContractDAOFactory.getInstance();
        WorkContractDBO workContractDBOToUpdate = workContractDAO.getSession().load(WorkContractDBO.class, initialContractSituationId);
        workContractDBOToUpdate.setModificationDate(Date.valueOf(workContractVariationVariationForm.getWorkContractExtension().getDateFrom().getValue()));
        workContractDAO.update(workContractDBOToUpdate);
    }
}
