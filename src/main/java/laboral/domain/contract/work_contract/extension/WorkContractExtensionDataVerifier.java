package laboral.domain.contract.work_contract.extension;

import laboral.component.work_contract.ContractConstants;
import laboral.domain.contract.WorkContract;

import java.time.LocalDate;
import java.time.Period;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class WorkContractExtensionDataVerifier {

    private final Set<ContractExtensionSituationProblem> problems = new HashSet<>();

    public Set<ContractExtensionSituationProblem> verify(WorkContract workContract, ContractExtensionSituationEntryDataContainer extensionDataContainer){

        LocalDate expectedEndDate = workContract.getExpectedEndDate() == null
                ? null
                : workContract.getExpectedEndDate();

        if(extensionDataContainer.getExtensionDateFrom() == null){
            problems.add(ContractExtensionSituationProblem.EXTENSION_DATE_FROM_ERROR);

        }
        if(extensionDataContainer.getExtensionDateTo() == null){
            problems.add(ContractExtensionSituationProblem.EXTENSION_DATE_TO_ERROR);

        }
        if(extensionDataContainer.getExtensionDateFrom() != null &&
                extensionDataContainer.getExtensionDateTo() != null &&
                extensionDataContainer.getExtensionDateFrom().isAfter(extensionDataContainer.getExtensionDateTo())) {
            problems.add(ContractExtensionSituationProblem.EXTENSION_DATE_ERROR);
        }

        if(expectedEndDate != null &&
                extensionDataContainer.getExtensionDateFrom() != null &&
                extensionDataContainer.getExtensionDateFrom().isAfter(workContract.getExpectedEndDate().plusDays(1L))) {
            problems.add(ContractExtensionSituationProblem.EXTENSION_DATE_ERROR);
        }
        if(extensionDataContainer.getExtensionDateTo() != null &&
                extensionDataContainer.getExtensionDateFrom().equals(expectedEndDate)){
            problems.add(ContractExtensionSituationProblem.EXTENSION_DATE_ERROR);
        }
        if(extensionDataContainer.getExtensionDateTo() != null &&
                extensionDataContainer.getExtensionDateTo().equals(expectedEndDate)){
            problems.add(ContractExtensionSituationProblem.EXTENSION_DATE_ERROR);
        }

        int yearsPeriod = 0;
        int monthsPeriod = 0;
        int daysPeriod = 0;
        double totalDaysPeriod = 0;
        double totalDaysPeriodWholePart = 0;

        if(extensionDataContainer.getExtensionDateTo() != null) {
            yearsPeriod = Period.between(workContract.getStartDate(), extensionDataContainer.getExtensionDateTo()).getYears();
            monthsPeriod = Period.between(workContract.getStartDate(), extensionDataContainer.getExtensionDateTo()).getMonths();
            daysPeriod = Period.between(workContract.getStartDate(), extensionDataContainer.getExtensionDateTo()).getDays();
            totalDaysPeriod = yearsPeriod * 365.25 + monthsPeriod * 30.4375 + daysPeriod + 1;
            totalDaysPeriodWholePart = totalDaysPeriod - totalDaysPeriod % 1;
            if (totalDaysPeriodWholePart > ContractConstants.MAXIMUM_LEGAL_EXTENSION_DURATION_DAYS) {
                problems.add(ContractExtensionSituationProblem.EXTENSION_EXCESSIVE_DURATION_ERROR);
            }
        }
        return Collections.unmodifiableSet(problems);
    }

    public enum ContractExtensionSituationProblem {
        EXTENSION_DATE_FROM_ERROR("Error. Fecha inicial de la prórroga no establecida."),
        EXTENSION_DATE_TO_ERROR("Error. Fecha final de la prórroga no establecida."),
        EXTENSION_DATE_ERROR("Error. Fecha(s) inicial/final de la prórroga incorrecta(s)."),
        EXTENSION_EXCESSIVE_DURATION_ERROR("Error. La duración de la prórroga es excesiva");

        private String extensionProblemDescription;

        ContractExtensionSituationProblem(String extensionProblemDescription) {
            this.extensionProblemDescription = extensionProblemDescription;
        }

        public String getExtensionProblemDescription() {
            return extensionProblemDescription;
        }

        public String toString(){
            return getExtensionProblemDescription();
        }
    }
}
