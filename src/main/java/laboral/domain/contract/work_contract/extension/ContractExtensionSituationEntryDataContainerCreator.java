package laboral.domain.contract.work_contract.extension;

import laboral.component.generic_component.DateHourNotificationForm;
import laboral.component.work_contract.variation.component.WorkContractExtensionForm;
import laboral.domain.utilities.utilities.Utilities;
import laboral.domain.variation_type.VariationType;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;

public class ContractExtensionSituationEntryDataContainerCreator {

    public static ContractExtensionSituationEntryDataContainer create(DateHourNotificationForm dateHourNotificationForm, WorkContractExtensionForm workContractExtensionForm) {

        LocalDate notificationDate = dateHourNotificationForm.getNotificationDate().getValue() == null
                ? null
                : dateHourNotificationForm.getNotificationDate().getValue();

        LocalTime notificationHour = dateHourNotificationForm.getNotificationHour().getText() == null
                ? null
                : Utilities.convertStringToLocalTime(dateHourNotificationForm.getNotificationHour().getText());

        VariationType extensionVariationType = VariationType.VariationTypeBuilder.create()
                .withVariationCode(220)
                .build();


        LocalDate extensionDateFrom = workContractExtensionForm.getDateFrom().getValue() == null
                ? null
                : workContractExtensionForm.getDateFrom().getValue();

        LocalDate extensionDateTo = workContractExtensionForm.getDateTo().getValue() == null
                ? null
                : workContractExtensionForm.getDateTo().getValue();

        String extensionDuration = workContractExtensionForm.getExtensionDurationDays().getText();

        return ContractExtensionSituationEntryDataContainer.ContractExtensionSituationEntryDataContainerBuilder.create()
                .withExtensionDateFrom(extensionDateFrom)
                .withExtensionDateTo(extensionDateTo)
                .withExtensionDuration(extensionDuration)
                .withVariationType(extensionVariationType)
                .withClientDateNotification(notificationDate)
                .withClientHourNotification(notificationHour)
                .withPublicNotes(workContractExtensionForm.getPublicNotes().getText())
                .withPrivateNotes(workContractExtensionForm.getPrivateNotes().getText())
                .build();
    }
}
