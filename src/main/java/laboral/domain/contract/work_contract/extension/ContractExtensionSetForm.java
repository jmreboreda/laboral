package laboral.domain.contract.work_contract.extension;

import laboral.component.generic_component.DateHourNotificationForm;
import laboral.component.work_contract.variation.component.WorkContractVariationSelector;
import laboral.component.work_contract.variation.component.WorkContractVariationVariationForm;

public class ContractExtensionSetForm {

    private final WorkContractVariationSelector workContractVariationSelector;
    private final WorkContractVariationVariationForm workContractVariationVariationForm;
    private final DateHourNotificationForm dateHourNotificationForm;

    public ContractExtensionSetForm(WorkContractVariationSelector workContractVariationSelector,
                                    WorkContractVariationVariationForm workContractVariationVariationForm,
                                    DateHourNotificationForm dateHourNotificationForm) {
        this.workContractVariationSelector = workContractVariationSelector;
        this.workContractVariationVariationForm = workContractVariationVariationForm;
        this.dateHourNotificationForm = dateHourNotificationForm;
    }

    public WorkContractVariationSelector getWorkContractVariationSelector() {
        return workContractVariationSelector;
    }

    public WorkContractVariationVariationForm getWorkContractVariationVariationForm() {
        return workContractVariationVariationForm;
    }

    public DateHourNotificationForm getDateHourNotificationForm() {
        return dateHourNotificationForm;
    }

    public static final class ContractExtensionSetFormBuilder {
        private WorkContractVariationSelector workContractVariationSelector;
        private WorkContractVariationVariationForm workContractVariationVariationForm;
        private DateHourNotificationForm dateHourNotificationForm;

        private ContractExtensionSetFormBuilder() {
        }

        public static ContractExtensionSetFormBuilder create() {
            return new ContractExtensionSetFormBuilder();
        }

        public ContractExtensionSetFormBuilder withWorkContractVariationSelector(WorkContractVariationSelector workContractVariationSelector) {
            this.workContractVariationSelector = workContractVariationSelector;
            return this;
        }

        public ContractExtensionSetFormBuilder withWorkContractVariationVariationForm(WorkContractVariationVariationForm workContractVariationVariationForm) {
            this.workContractVariationVariationForm = workContractVariationVariationForm;
            return this;
        }

        public ContractExtensionSetFormBuilder withDateHourNotificationForm(DateHourNotificationForm dateHourNotificationForm) {
            this.dateHourNotificationForm = dateHourNotificationForm;
            return this;
        }

        public ContractExtensionSetForm build() {
            return new ContractExtensionSetForm(workContractVariationSelector, workContractVariationVariationForm, dateHourNotificationForm);
        }
    }
}
