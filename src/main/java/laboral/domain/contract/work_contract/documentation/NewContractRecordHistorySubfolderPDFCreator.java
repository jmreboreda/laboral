package laboral.domain.contract.work_contract.documentation;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import laboral.ApplicationConstants;
import laboral.component.work_contract.ContractConstants;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.contract.WorkContractSituationRequest;
import laboral.domain.contract.WorkDayTypeDuration;
import laboral.domain.contract_json.ContractSchedule;
import laboral.domain.person.PersonService;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.utilities.utilities.Utilities;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class NewContractRecordHistorySubfolderPDFCreator {

    private static final String PATH_TO_PDF_TEMPLATE = "/pdf_forms/DGM_006_Portada_Expediente_Contrato_Trabajo.pdf";

    public NewContractRecordHistorySubfolderPDFCreator() {
    }

    public static Path createContractRecordHistorySubfolderPDF(WorkContractSituationRequest workContractSituationRequest, Path pathOut) throws IOException, DocumentException {

        PdfReader reader = new PdfReader(PATH_TO_PDF_TEMPLATE);
        PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(pathOut.toString()));
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());

        PersonService personService = PersonService.PersonServiceFactory.getInstance();
        PersonDBO personEmployee = personService.findPersonById(workContractSituationRequest.getEmployee().getPersonId());
        AddressDBO selectedAddress = null;
        for(AddressDBO address : personEmployee.getAddresses()){
            if (address.getDefaultAddress()){
                selectedAddress = address;
            }
        }

        String completeAddress = "";
        if(selectedAddress.getLocation().equals(selectedAddress.getMunicipality())){
            completeAddress = selectedAddress.getStreetType().getStreetTypeDescription() + " " + selectedAddress.getStreetName() + ", " + selectedAddress.getStreetExtended() +
                    "  " + selectedAddress.getPostalCode() + "  " + selectedAddress.getMunicipality();
        }else {
            completeAddress = selectedAddress.getStreetType().getStreetTypeDescription() + " " + selectedAddress.getStreetName() + ", " + selectedAddress.getStreetExtended() +
                    "  " + selectedAddress.getLocation() + "  " + selectedAddress.getPostalCode() + "  " + selectedAddress.getMunicipality();
        }

        String workDays = "";

        Long durationDays = workContractSituationRequest.getExpectedEndDate() != null
                ? ChronoUnit.DAYS.between(workContractSituationRequest.getStartDate(), workContractSituationRequest.getExpectedEndDate()) + 1
                : null;

        AcroFields contractDataSubfolderPDFFields = stamp.getAcroFields();
        contractDataSubfolderPDFFields.setField("employerFullName",workContractSituationRequest.getEmployer().toAlphabeticalName());
        contractDataSubfolderPDFFields.setField("quoteAccountCode",workContractSituationRequest.getQuoteAccountCode().toCompleteNumber());
        contractDataSubfolderPDFFields.setField("employeeFullName",workContractSituationRequest.getEmployee().toAlphabeticalName());
        contractDataSubfolderPDFFields.setField("employeeNIF",workContractSituationRequest.getEmployee().getNieNif().getNif());
        contractDataSubfolderPDFFields.setField("employeeNASS",workContractSituationRequest.getEmployee().getSocialSecurityAffiliationNumber());
        contractDataSubfolderPDFFields.setField("employeeBirthDate",workContractSituationRequest.getEmployee().getBirthDate().format(dateFormatter));
        contractDataSubfolderPDFFields.setField("employeeCivilState",workContractSituationRequest.getEmployee().getCivilStatus().getCivilStatusDescription());
        contractDataSubfolderPDFFields.setField("employeeNationality",workContractSituationRequest.getEmployee().getNationality());
        contractDataSubfolderPDFFields.setField("employeeAddress",completeAddress);
        contractDataSubfolderPDFFields.setField("employeeMaximumStudyLevel",workContractSituationRequest.getEmployee().getStudy().getStudyLevelDescription());
        contractDataSubfolderPDFFields.setField("contractNumber", workContractSituationRequest.getContractNumber().toString());

        if(workContractSituationRequest.getDayOfWeekSet().contains(DayOfWeek.MONDAY)) {
            workDays = workDays + "l";
        }
        if(workContractSituationRequest.getDayOfWeekSet().contains(DayOfWeek.TUESDAY)) {
            workDays = workDays + " m";
        }
        if(workContractSituationRequest.getDayOfWeekSet().contains(DayOfWeek.WEDNESDAY)) {
            workDays = workDays + " x";
        }
        if(workContractSituationRequest.getDayOfWeekSet().contains(DayOfWeek.THURSDAY)) {
            workDays = workDays + " j";
        }
        if(workContractSituationRequest.getDayOfWeekSet().contains(DayOfWeek.FRIDAY)) {
            workDays = workDays + " v";
        }
        if(workContractSituationRequest.getDayOfWeekSet().contains(DayOfWeek.SATURDAY)) {
            workDays = workDays + " s";
        }
        if(workContractSituationRequest.getDayOfWeekSet().contains(DayOfWeek.SUNDAY)) {
            workDays = workDays + " d";
        }
        contractDataSubfolderPDFFields.setField("dateFrom", workContractSituationRequest.getStartDate().format(dateFormatter));
        if(workContractSituationRequest.getExpectedEndDate() != null) {
            contractDataSubfolderPDFFields.setField("dateTo", workContractSituationRequest.getExpectedEndDate().format(dateFormatter));
        }else{
            contractDataSubfolderPDFFields.setField("dateTo", null);
        }
        if(durationDays != null) {
            contractDataSubfolderPDFFields.setField("durationDays", durationDays + " días");
        }else{
            contractDataSubfolderPDFFields.setField("durationDays", ContractConstants.UNDEFINED_DURATION_TEXT);
        }
        contractDataSubfolderPDFFields.setField("contractTypeDescription",workContractSituationRequest.getWorkContractType().getColloquial() + ", "
                + workContractSituationRequest.getWorkContractType().getContractDescription());

        contractDataSubfolderPDFFields.setField("laborCategory",workContractSituationRequest.getLaborCategory());

        Duration hoursWorkWeek = workContractSituationRequest.getFullPartialWorkDay().equals(WorkDayTypeDuration.FULL_TIME)
                ? Duration.ofHours(40)
                : ContractSchedule.retrieveHoursWorkWeek(workContractSituationRequest.getContractSchedule());
        contractDataSubfolderPDFFields.setField("hoursWorkWeek", Utilities.durationToTimeStringConverter(hoursWorkWeek));

        contractDataSubfolderPDFFields.setField("workDays",workDays);

        stamp.setFormFlattening(true);
        stamp.close();

        return pathOut;
    }
}
