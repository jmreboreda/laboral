package laboral.domain.contract.work_contract.documentation;

import com.lowagie.text.DocumentException;
import laboral.ApplicationConstants;
import laboral.component.work_contract.ContractConstants;
import laboral.domain.contract.WorkContractSituationRequest;
import laboral.domain.utilities.OSUtils;
import laboral.domain.utilities.utilities.Utilities;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class NewContractDocumentationCreator {


    public NewContractDocumentationCreator() {
    }

    public Path retrievePathToContractDataSubFolderPDF(WorkContractSituationRequest workContractSituationRequest){
        Path pathOut = null;

        final Optional<Path> maybePath = OSUtils.TemporalFolderUtils.tempFolder();
        String temporalDir = maybePath.get().toString();

        Path pathToContractDataSubFolder = Paths.get(ApplicationConstants.USER_HOME.toString(), temporalDir, workContractSituationRequest.toFileName().concat(ApplicationConstants.PDF_EXTENSION.toString()));
        try {
            Files.createDirectories(pathToContractDataSubFolder.getParent());
            pathOut = NewContractDataSubFolderPDFCreator.createContractDataSubFolderPDF(workContractSituationRequest, ContractConstants.STANDARD_NEW_CONTRACT_TEXT, pathToContractDataSubFolder);
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        }

        return pathOut;
    }

    public Path retrievePathToContractDataSubFolderHistoryPDF(WorkContractSituationRequest workContractSituationRequest){

        Path pathOut = null;

        final Optional<Path> maybePath = OSUtils.TemporalFolderUtils.tempFolder();
        String temporalDir = maybePath.get().toString();

        String fileName = Utilities.replaceWithUnderscore(ContractConstants.STANDARD_CONTRACT_SUB_FOLDER_RECORD_HISTORY_TEXT.concat("_")
                .concat(workContractSituationRequest.getEmployee().toAlphabeticalName()));

        Path pathToContractRecordHistorySubfolder = Paths.get(ApplicationConstants.USER_HOME.getValue(), temporalDir, fileName.concat(ApplicationConstants.PDF_EXTENSION.getValue()));
        try {
            Files.createDirectories(pathToContractRecordHistorySubfolder.getParent());
            pathOut = NewContractRecordHistorySubfolderPDFCreator.createContractRecordHistorySubfolderPDF(workContractSituationRequest, pathToContractRecordHistorySubfolder);
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        }

        return pathOut;
    }

    public Path retrievePathToContractDataToContractAgentPDF(WorkContractSituationRequest workContractSituationRequest){
        Path pathOut = null;

        final Optional<Path> maybePath = OSUtils.TemporalFolderUtils.tempFolder();
        String temporalDir = maybePath.get().toString();

        Path pathToContractDataToContractAgent = Paths.get(ApplicationConstants.USER_HOME.getValue(), temporalDir, workContractSituationRequest.toFileName().concat("_gst.pdf"));
        try {
            Files.createDirectories(pathToContractDataToContractAgent.getParent());
            pathOut = NewContractDataSubFolderPDFCreator.createContractDataToContractAgentPDF(workContractSituationRequest, ContractConstants.STANDARD_NEW_CONTRACT_TEXT, pathToContractDataToContractAgent);
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        }

        return pathOut;
    }




}
