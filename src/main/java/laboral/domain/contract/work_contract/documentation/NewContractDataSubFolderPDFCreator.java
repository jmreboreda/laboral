package laboral.domain.contract.work_contract.documentation;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import laboral.ApplicationConstants;
import laboral.component.work_contract.ContractConstants;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.contract.WorkContractSituationRequest;
import laboral.domain.contract.WorkDayTypeDuration;
import laboral.domain.contract_json.ContractDaySchedule;
import laboral.domain.person.PersonService;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.utilities.utilities.Utilities;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import static java.time.temporal.ChronoUnit.DAYS;

public class NewContractDataSubFolderPDFCreator {

    private static final String PATH_TO_PDF_TEMPLATE = "/pdf_forms/DGM_003_Data_Of_Registration_Of_Change_Or_Of_New_Employment_Contract.pdf";
    private static final String PATH_TO_PDF_TEMPLATE_AGENT = "/pdf_forms/DGM_XXX_Data_Of_Registration_Of_Change_Or_Of_New_Employment_Contract_to_Agent.pdf";

    public NewContractDataSubFolderPDFCreator() {
    }

    public static Path createContractDataSubFolderPDF(WorkContractSituationRequest workContractSituationRequest, String notificationType, Path pathOut) throws IOException, DocumentException {

        PdfReader reader = new PdfReader(PATH_TO_PDF_TEMPLATE);
        PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(pathOut.toString()));
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.toString());
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_TIME_FORMAT.toString());

        String startDate = workContractSituationRequest.getStartDate() == null ? "" : workContractSituationRequest.getStartDate().format(dateFormatter);
        String expectedEndDate = workContractSituationRequest.getExpectedEndDate() == null ? "" : workContractSituationRequest.getExpectedEndDate().format(dateFormatter);
        Long durationDays = null;
        if(!expectedEndDate.equals("")){
            durationDays = DAYS.between(workContractSituationRequest.getStartDate(), workContractSituationRequest.getExpectedEndDate()) + 1;
        }

        PersonService personService = PersonService.PersonServiceFactory.getInstance();
        PersonDBO personEmployee = personService.findPersonById(workContractSituationRequest.getEmployee().getPersonId());
        AddressDBO selectedAddress = null;
        for(AddressDBO address : personEmployee.getAddresses()){
            if (address.getDefaultAddress()){
                selectedAddress = address;
            }
        }

        String completeAddress = "";
        if(selectedAddress.getLocation().equals(selectedAddress.getMunicipality())){
            completeAddress = selectedAddress.getStreetType().getStreetTypeDescription() + " " + selectedAddress.getStreetName() + ", " + selectedAddress.getStreetExtended() +
                    "  " + selectedAddress.getPostalCode() + "  " + selectedAddress.getMunicipality();
        }else {
            completeAddress = selectedAddress.getStreetType().getStreetTypeDescription() + " " + selectedAddress.getStreetName() + ", " + selectedAddress.getStreetExtended() +
                    "  " + selectedAddress.getLocation() + "  " + selectedAddress.getPostalCode() + "  " + selectedAddress.getMunicipality();
        }

        String hoursWorkWeek = Utilities.durationToTimeStringConverter(workContractSituationRequest.getHoursWorkWeek());
        String hoursWorkWeekText = "[" + hoursWorkWeek + " " + ContractConstants.HOURS_WORK_WEEK_TEXT + "]";

        String contractDescription = workContractSituationRequest.getWorkContractType().getColloquial() + ", "
                + workContractSituationRequest.getWorkContractType().getContractDescription() + " " + hoursWorkWeekText;

        AcroFields contractDataSubFolderPDFFields = stamp.getAcroFields();
        contractDataSubFolderPDFFields.setField("notificationType",notificationType);
        contractDataSubFolderPDFFields.setField("officialContractNumber", workContractSituationRequest.getOfficialIdentificationNumber());
        contractDataSubFolderPDFFields.setField("employerFullName", workContractSituationRequest.getEmployer().toAlphabeticalName());
        contractDataSubFolderPDFFields.setField("quoteAccountCode", workContractSituationRequest.getQuoteAccountCode().toCompleteNumber());
        contractDataSubFolderPDFFields.setField("notificationDate", workContractSituationRequest.getClientNotification().format(dateFormatter));
        contractDataSubFolderPDFFields.setField("notificationHour", workContractSituationRequest.getClientNotification().format(timeFormatter));
        contractDataSubFolderPDFFields.setField("employeeFullName", workContractSituationRequest.getEmployee().toAlphabeticalName());
        contractDataSubFolderPDFFields.setField("employeeNIF", workContractSituationRequest.getEmployee().getNieNif().formattedAsNIF());
        contractDataSubFolderPDFFields.setField("employeeNASS",personEmployee.getSocialSecurityAffiliationNumber());
        contractDataSubFolderPDFFields.setField("employeeBirthDate",personEmployee.getBirthDate().toLocalDate().format(dateFormatter));
        contractDataSubFolderPDFFields.setField("employeeCivilStatus",personEmployee.getCivilStatus().getCivilStatusDescription());
        contractDataSubFolderPDFFields.setField("employeeNationality",personEmployee.getNationality());
        contractDataSubFolderPDFFields.setField("employeeFullAddress",completeAddress);
        contractDataSubFolderPDFFields.setField("employeeMaxStudyLevel",personEmployee.getStudy().getStudyLevelDescription());

        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.MONDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("monday", "Yes");
        }
        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.TUESDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("tuesday", "Yes");
        }
        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("wednesday", "Yes");
        }
        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.THURSDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("thursday", "Yes");
        }
        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.FRIDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("friday", "Yes");
        }
        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.SATURDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("saturday", "Yes");
        }
        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.SUNDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("sunday", "Yes");
        }
        contractDataSubFolderPDFFields.setField("contractDescription", contractDescription);
        contractDataSubFolderPDFFields.setField("startDate", startDate);
        contractDataSubFolderPDFFields.setField("expectedEndDate", expectedEndDate);

        if(durationDays != null) {
            contractDataSubFolderPDFFields.setField("durationDays", durationDays + " días");
        }else{
            contractDataSubFolderPDFFields.setField("durationDays", ContractConstants.UNDEFINED_DURATION_TEXT);
        }

        contractDataSubFolderPDFFields.setField("workCenter", workContractSituationRequest.getWorkCenter().toString());

        Duration totalHoursSchedule1 = Duration.ZERO;
        Duration totalHoursSchedule2 = Duration.ZERO;
        Duration totalHoursSchedule3 = Duration.ZERO;
        Duration totalHoursSchedule4 = Duration.ZERO;

        /* Start of the form fill loop*/
        Map<String, ContractDaySchedule> schedule = workContractSituationRequest.getContractSchedule().getSchedule();
        for (Map.Entry<String, ContractDaySchedule> entry : schedule.entrySet()) {
            String scheduleNumber = entry.getKey().substring(8,9);
            if (entry.getValue().getDayOfWeek().contains("lunes")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 1, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 1, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 1 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 1 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 1 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 1 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 1 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("mondayH"+ scheduleNumber, "Yes");
                }
            } else if (entry.getValue().getDayOfWeek().contains("martes")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 2, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 2, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 2 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 2 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 2 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 2 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 2 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("tuesdayH"+ scheduleNumber, "Yes");
                }
            } else if (entry.getValue().getDayOfWeek().contains("miércoles")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 3, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 3, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 3 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 3 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 3 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 3 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 3 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("wednesdayH"+ scheduleNumber, "Yes");
                }
            } else if (entry.getValue().getDayOfWeek().contains("jueves")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 4, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 4, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 4 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 4 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 4 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 4 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 4 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("thursdayH"+ scheduleNumber, "Yes");
                }
            } else if (entry.getValue().getDayOfWeek().contains("viernes")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 5, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 5, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 5 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 5 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 5 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 5 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 5 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("fridayH"+ scheduleNumber, "Yes");
                }
            }else if (entry.getValue().getDayOfWeek().contains("sábado")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 6, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 6, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 6 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 6 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 6 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 6 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 6 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("saturdayH"+ scheduleNumber, "Yes");
                }
            }else if (entry.getValue().getDayOfWeek().contains("domingo")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 7, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 7, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 7 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 7 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 7 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 7 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 7 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("sundayH"+ scheduleNumber, "Yes");
                }
            }

            if(entry.getKey().contains("Schedule1")){
                totalHoursSchedule1 = totalHoursSchedule1.plus(Objects.requireNonNull(Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours())));
            }
            if(entry.getKey().contains("Schedule2")){
                totalHoursSchedule2 = totalHoursSchedule2.plus(Objects.requireNonNull(Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours())));
            }
            if(entry.getKey().contains("Schedule3")){
                totalHoursSchedule3 = totalHoursSchedule3.plus(Objects.requireNonNull(Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours())));
            }
            if(entry.getKey().contains("Schedule4")){
                totalHoursSchedule4 = totalHoursSchedule4.plus(Objects.requireNonNull(Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours())));
            }
        }

        contractDataSubFolderPDFFields.setField("TOTALH1", Utilities.durationToTimeStringConverter(totalHoursSchedule1));
        contractDataSubFolderPDFFields.setField("TOTALH2", Utilities.durationToTimeStringConverter(totalHoursSchedule2));
        contractDataSubFolderPDFFields.setField("TOTALH3", Utilities.durationToTimeStringConverter(totalHoursSchedule3));
        contractDataSubFolderPDFFields.setField("TOTALH4", Utilities.durationToTimeStringConverter(totalHoursSchedule4));

        contractDataSubFolderPDFFields.setField("publicNotes", workContractSituationRequest.getPublicNotes());
        contractDataSubFolderPDFFields.setField("laborCategory", workContractSituationRequest.getLaborCategory());
        contractDataSubFolderPDFFields.setField("contractNumber", workContractSituationRequest.getContractNumber().toString());

        stamp.setFormFlattening(true);
        stamp.close();

        return pathOut;
    }

    public static Path createContractDataToContractAgentPDF(WorkContractSituationRequest workContractSituationRequest, String notificationType,  Path pathOut) throws IOException, DocumentException{

        PdfReader reader = new PdfReader(PATH_TO_PDF_TEMPLATE_AGENT);
        PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(pathOut.toString()));
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.toString());
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_TIME_FORMAT.toString());

        String startDate = workContractSituationRequest.getStartDate() == null ? "" : workContractSituationRequest.getStartDate().format(dateFormatter);
        String expectedEndDate = workContractSituationRequest.getExpectedEndDate() == null ? "" : workContractSituationRequest.getExpectedEndDate().format(dateFormatter);
        Long durationDays = null;
        if(!expectedEndDate.equals("")){
            durationDays = DAYS.between(workContractSituationRequest.getStartDate(), workContractSituationRequest.getExpectedEndDate()) + 1;
        }

        PersonService personService = PersonService.PersonServiceFactory.getInstance();
        PersonDBO personEmployee = personService.findPersonById(workContractSituationRequest.getEmployee().getPersonId());
        AddressDBO selectedAddress = null;
        for(AddressDBO address : personEmployee.getAddresses()){
            if (address.getDefaultAddress()){
                selectedAddress = address;
            }
        }

        String completeAddress = "";
        if(selectedAddress.getLocation().equals(selectedAddress.getMunicipality())){
            completeAddress = selectedAddress.getStreetType().getStreetTypeDescription() + " " + selectedAddress.getStreetName() + ", " + selectedAddress.getStreetExtended() +
                    "  " + selectedAddress.getPostalCode() + "  " + selectedAddress.getMunicipality();
        }else {
            completeAddress = selectedAddress.getStreetType().getStreetTypeDescription() + " " + selectedAddress.getStreetName() + ", " + selectedAddress.getStreetExtended() +
                    "  " + selectedAddress.getLocation() + "  " + selectedAddress.getPostalCode() + "  " + selectedAddress.getMunicipality();
        }

        String hoursWorkWeek = Utilities.durationToTimeStringConverter(workContractSituationRequest.getHoursWorkWeek());
        String hoursWorkWeekText = "[" + hoursWorkWeek + " " + ContractConstants.HOURS_WORK_WEEK_TEXT + "]";

        String contractDescription = workContractSituationRequest.getWorkContractType().getColloquial() + ", "
                + workContractSituationRequest.getWorkContractType().getContractDescription() + " " + hoursWorkWeekText;

        AcroFields contractDataSubFolderPDFFields = stamp.getAcroFields();
        contractDataSubFolderPDFFields.setField("notificationType",notificationType);
        contractDataSubFolderPDFFields.setField("officialContractNumber", workContractSituationRequest.getOfficialIdentificationNumber());
        contractDataSubFolderPDFFields.setField("employerFullName", workContractSituationRequest.getEmployer().toAlphabeticalName());
        contractDataSubFolderPDFFields.setField("quoteAccountCode", workContractSituationRequest.getQuoteAccountCode().toCompleteNumber());
        contractDataSubFolderPDFFields.setField("notificationDate", workContractSituationRequest.getClientNotification().format(dateFormatter));
        contractDataSubFolderPDFFields.setField("notificationHour", workContractSituationRequest.getClientNotification().format(timeFormatter));
        contractDataSubFolderPDFFields.setField("employeeFullName", workContractSituationRequest.getEmployee().toAlphabeticalName());
        contractDataSubFolderPDFFields.setField("employeeNIF", workContractSituationRequest.getEmployee().getNieNif().formattedAsNIF());
        contractDataSubFolderPDFFields.setField("employeeNASS",personEmployee.getSocialSecurityAffiliationNumber());
        contractDataSubFolderPDFFields.setField("employeeBirthDate",personEmployee.getBirthDate().toLocalDate().format(dateFormatter));
        contractDataSubFolderPDFFields.setField("employeeCivilStatus",personEmployee.getCivilStatus().getCivilStatusDescription());
        contractDataSubFolderPDFFields.setField("employeeNationality",personEmployee.getNationality());
        contractDataSubFolderPDFFields.setField("employeeFullAddress",completeAddress);
        contractDataSubFolderPDFFields.setField("employeeMaxStudyLevel",personEmployee.getStudy().getStudyLevelDescription());

        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.MONDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("monday", "Yes");
        }
        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.TUESDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("tuesday", "Yes");
        }
        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("wednesday", "Yes");
        }
        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.THURSDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("thursday", "Yes");
        }
        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.FRIDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("friday", "Yes");
        }
        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.SATURDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("saturday", "Yes");
        }
        if(workContractSituationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.SUNDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("sunday", "Yes");
        }
        contractDataSubFolderPDFFields.setField("contractDescription", contractDescription);
        contractDataSubFolderPDFFields.setField("startDate", startDate);
        contractDataSubFolderPDFFields.setField("expectedEndDate", expectedEndDate);

        if(durationDays != null) {
            contractDataSubFolderPDFFields.setField("durationDays", durationDays + " días");
        }else{
            contractDataSubFolderPDFFields.setField("durationDays", ContractConstants.UNDEFINED_DURATION_TEXT);
        }

        contractDataSubFolderPDFFields.setField("workCenter", workContractSituationRequest.getWorkCenter().toString());

        Duration totalHoursSchedule1 = Duration.ZERO;
        Duration totalHoursSchedule2 = Duration.ZERO;
        Duration totalHoursSchedule3 = Duration.ZERO;
        Duration totalHoursSchedule4 = Duration.ZERO;

        /* Start of the form fill loop*/
        Map<String, ContractDaySchedule> schedule = workContractSituationRequest.getContractSchedule().getSchedule();
        for (Map.Entry<String, ContractDaySchedule> entry : schedule.entrySet()) {
            String scheduleNumber = entry.getKey().substring(8,9);
            if (entry.getValue().getDayOfWeek().contains("lunes")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 1, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 1, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 1 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 1 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 1 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 1 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 1 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("mondayH"+ scheduleNumber, "Yes");
                }
            } else if (entry.getValue().getDayOfWeek().contains("martes")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 2, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 2, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 2 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 2 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 2 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 2 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 2 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("tuesdayH"+ scheduleNumber, "Yes");
                }
            } else if (entry.getValue().getDayOfWeek().contains("miércoles")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 3, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 3, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 3 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 3 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 3 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 3 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 3 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("wednesdayH"+ scheduleNumber, "Yes");
                }
            } else if (entry.getValue().getDayOfWeek().contains("jueves")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 4, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 4, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 4 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 4 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 4 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 4 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 4 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("thursdayH"+ scheduleNumber, "Yes");
                }
            } else if (entry.getValue().getDayOfWeek().contains("viernes")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 5, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 5, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 5 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 5 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 5 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 5 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 5 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("fridayH"+ scheduleNumber, "Yes");
                }
            }else if (entry.getValue().getDayOfWeek().contains("sábado")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 6, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 6, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 6 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 6 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 6 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 6 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 6 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("saturdayH"+ scheduleNumber, "Yes");
                }
            }else if (entry.getValue().getDayOfWeek().contains("domingo")) {
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "WD" + 7, entry.getValue().getDayOfWeek());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "DATE" + 7, entry.getValue().getDate());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 7 + "AMF", entry.getValue().getAmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 7 + "AMT", entry.getValue().getAmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 7 + "PMF", entry.getValue().getPmFrom());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 7 + "PMT", entry.getValue().getPmTo());
                contractDataSubFolderPDFFields.setField("H" + scheduleNumber + "D" + 7 + "TOTH", entry.getValue().getDurationHours());
                if(entry.getValue().getDurationHours() != null){
                    contractDataSubFolderPDFFields.setField("sundayH"+ scheduleNumber, "Yes");
                }
            }

            if(entry.getKey().contains("Schedule1")){
                totalHoursSchedule1 = totalHoursSchedule1.plus(Objects.requireNonNull(Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours())));
            }
            if(entry.getKey().contains("Schedule2")){
                totalHoursSchedule2 = totalHoursSchedule2.plus(Objects.requireNonNull(Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours())));
            }
            if(entry.getKey().contains("Schedule3")){
                totalHoursSchedule3 = totalHoursSchedule3.plus(Objects.requireNonNull(Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours())));
            }
            if(entry.getKey().contains("Schedule4")){
                totalHoursSchedule4 = totalHoursSchedule4.plus(Objects.requireNonNull(Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours())));
            }
        }

        contractDataSubFolderPDFFields.setField("TOTALH1", Utilities.durationToTimeStringConverter(totalHoursSchedule1));
        contractDataSubFolderPDFFields.setField("TOTALH2", Utilities.durationToTimeStringConverter(totalHoursSchedule2));
        contractDataSubFolderPDFFields.setField("TOTALH3", Utilities.durationToTimeStringConverter(totalHoursSchedule3));
        contractDataSubFolderPDFFields.setField("TOTALH4", Utilities.durationToTimeStringConverter(totalHoursSchedule4));

        contractDataSubFolderPDFFields.setField("publicNotes", workContractSituationRequest.getPublicNotes());
        contractDataSubFolderPDFFields.setField("laborCategory", workContractSituationRequest.getLaborCategory());
        contractDataSubFolderPDFFields.setField("contractNumber", null);

        stamp.setFormFlattening(true);
        stamp.close();

        return pathOut;
    }
}
