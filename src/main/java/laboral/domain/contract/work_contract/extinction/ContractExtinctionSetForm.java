package laboral.domain.contract.work_contract.extinction;

import laboral.component.generic_component.DateHourNotificationForm;
import laboral.component.work_contract.variation.component.WorkContractVariationSelector;
import laboral.component.work_contract.variation.component.WorkContractVariationVariationForm;

public class ContractExtinctionSetForm {

    private final WorkContractVariationSelector workContractVariationSelector;
    private final WorkContractVariationVariationForm workContractVariationVariationForm;
    private final DateHourNotificationForm dateHourNotificationForm;

    public ContractExtinctionSetForm(WorkContractVariationSelector workContractVariationSelector,
                                     WorkContractVariationVariationForm workContractVariationVariationForm,
                                     DateHourNotificationForm dateHourNotificationForm) {
        this.workContractVariationSelector = workContractVariationSelector;
        this.workContractVariationVariationForm = workContractVariationVariationForm;
        this.dateHourNotificationForm = dateHourNotificationForm;
    }

    public WorkContractVariationSelector getWorkContractVariationSelector() {
        return workContractVariationSelector;
    }

    public WorkContractVariationVariationForm getWorkContractVariationVariationForm() {
        return workContractVariationVariationForm;
    }

    public DateHourNotificationForm getDateHourNotificationForm() {
        return dateHourNotificationForm;
    }

    public static final class ContractExtinctionSetFormBuilder {
        private WorkContractVariationSelector workContractVariationSelector;
        private WorkContractVariationVariationForm workContractVariationVariationForm;
        private DateHourNotificationForm dateHourNotificationForm;

        private ContractExtinctionSetFormBuilder() {
        }

        public static ContractExtinctionSetFormBuilder create() {
            return new ContractExtinctionSetFormBuilder();
        }

        public ContractExtinctionSetFormBuilder withWorkContractVariationSelector(WorkContractVariationSelector workContractVariationSelector) {
            this.workContractVariationSelector = workContractVariationSelector;
            return this;
        }

        public ContractExtinctionSetFormBuilder withWorkContractVariationVariationForm(WorkContractVariationVariationForm workContractVariationVariationForm) {
            this.workContractVariationVariationForm = workContractVariationVariationForm;
            return this;
        }

        public ContractExtinctionSetFormBuilder withDateHourNotificationForm(DateHourNotificationForm dateHourNotificationForm) {
            this.dateHourNotificationForm = dateHourNotificationForm;
            return this;
        }

        public ContractExtinctionSetForm build() {
            return new ContractExtinctionSetForm(workContractVariationSelector, workContractVariationVariationForm, dateHourNotificationForm);
        }
    }
}
