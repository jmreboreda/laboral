package laboral.domain.contract.work_contract.extinction;

import javafx.scene.Scene;
import javafx.stage.Stage;
import laboral.component.generic_component.DateHourNotificationForm;
import laboral.component.work_contract.variation.WorkContractVariationConstants;
import laboral.component.work_contract.variation.component.WorkContractVariationSelector;
import laboral.component.work_contract.variation.component.WorkContractVariationVariationForm;
import laboral.domain.contract.WorkContract;
import laboral.domain.contract.WorkContractSituationRequest;
import laboral.domain.contract.work_contract.WorkContractSituationCreator;
import laboral.domain.contract.work_contract.controller.WorkContractController;
import laboral.domain.contract.work_contract.persistence.dao.WorkContractDAO;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.utilities.utilities.Utilities;

import java.sql.Date;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

public class ContractExtinctionExecutor {

    WorkContractController workContractController = new WorkContractController();

    private Scene scene;
    private WorkContractVariationSelector workContractVariationSelector;
    private WorkContractVariationVariationForm workContractVariationVariationForm;
    private DateHourNotificationForm dateHourNotificationForm;

    public Integer executeExtinction(Scene scene, ContractExtinctionSetForm contractExtinctionSetForm){
        this.scene = scene;
        this.workContractVariationSelector = contractExtinctionSetForm.getWorkContractVariationSelector();
        this.workContractVariationVariationForm = contractExtinctionSetForm.getWorkContractVariationVariationForm();
        this.dateHourNotificationForm = contractExtinctionSetForm.getDateHourNotificationForm();

        List<WorkContract> workContractList =
                workContractController.findAllByWorkContractNumber(workContractVariationSelector.getWorkContractSelector().getSelectionModel().getSelectedItem().getContractNumber());
        for (WorkContract workContract : workContractList) {
            if (workContract.getModificationDate() == null) {
                ContractExtinctionSituationEntryDataContainer contractExtinctionDataContainer =
                        ContractExtinctionSituationEntryDataContainerCreator.create(dateHourNotificationForm, workContractVariationVariationForm.getWorkContractExtinction());
                WorkContractExtinctionDataVerifier workContractExtinctionDataVerifier = new WorkContractExtinctionDataVerifier();
                Set<WorkContractExtinctionDataVerifier.ContractExtinctionSituationProblem> verificationProblems =
                        workContractExtinctionDataVerifier.verify(workContract, contractExtinctionDataContainer);
                if (verificationProblems.isEmpty()) {
                    if(Message.confirmationMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.QUESTION_EXTINCT_CONTRACT_IS_CORRECT)){
                        Integer persistedExtinctionId = persistContractSituationExtinction(workContract);
                        if(persistedExtinctionId != null){
                            updateInitialContractEndDate(workContractList);
                            return persistedExtinctionId;

                        }else{
                            Message.errorMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTINCTION_PERSISTENCE_NOT_OK);
                            return null;
                        }
                    }else {
                        initializeExtinctionHolidaysAndPublicNotes();
                    }
                }
                else{
                    showErrorExtinctionMessage(verificationProblems);
                }
                return null;
            }
        }
        Message.errorMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.WORK_CONTRACT_SITUATION_TO_EXTINGUISH_NOT_FOUND);
        return null;
    }

    private void initializeExtinctionHolidaysAndPublicNotes() {
        workContractVariationVariationForm.getWorkContractExtinction().getPublicNotes().setText("");
        workContractVariationVariationForm.getWorkContractExtinction().getRbHolidaysYes().setSelected(Boolean.FALSE);
        workContractVariationVariationForm.getWorkContractExtinction().getRbHolidaysNo().setSelected(Boolean.FALSE);
        workContractVariationVariationForm.getWorkContractExtinction().getRbHolidaysCalculate().setSelected(Boolean.FALSE);
    }

    private void showErrorExtinctionMessage(Set<WorkContractExtinctionDataVerifier.ContractExtinctionSituationProblem> verificationProblems) {
        String errorsMessage = "";
        for (WorkContractExtinctionDataVerifier.ContractExtinctionSituationProblem problem : verificationProblems) {
            errorsMessage = errorsMessage.concat(problem.toString() + "\n");
        }
        Message.errorMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, errorsMessage);
    }

    private Integer persistContractSituationExtinction(WorkContract workContract){

        LocalTime clientHourNotification = Utilities.convertStringToLocalTime(dateHourNotificationForm.getNotificationHour().getText());

        ContractExtinctionSituationEntryDataContainer contractExtinctionDataEntryDataContainer =
                ContractExtinctionSituationEntryDataContainer.ContractExtinctionSituationEntryDataContainerBuilder.create()
                        .withExtinctionDate(workContractVariationVariationForm.getWorkContractExtinction().getDateFrom().getValue())
                        .withVariationType(workContractVariationVariationForm.getWorkContractExtinction().getExtinctionCauseSelector().getSelectionModel().getSelectedItem())
                        .withClientDateNotification(dateHourNotificationForm.getNotificationDate().getValue())
                        .withClientHourNotification(clientHourNotification)
                        .withHolidaysCalculate(workContractVariationVariationForm.getWorkContractExtinction().getRbHolidaysCalculate().isSelected())
                        .withHolidaysNo(workContractVariationVariationForm.getWorkContractExtinction().getRbHolidaysNo().isSelected())
                        .withHolidaysYes(workContractVariationVariationForm.getWorkContractExtinction().getRbHolidaysYes().isSelected())
                        .withPrivateNotes(workContractVariationVariationForm.getWorkContractExtinction().getPrivateNotes().getText())
                        .withPublicNotes(workContractVariationVariationForm.getWorkContractExtinction().getPublicNotes().getText())
                        .build();

        WorkContractSituationCreator workContractSituationCreator = new WorkContractSituationCreator();
        WorkContractSituationRequest workContractExtinctionSituationRequest =
                workContractSituationCreator.workContractExtinctionSituationRequestGenerator(workContract, contractExtinctionDataEntryDataContainer);

        Integer workContractExtinctionId = workContractSituationCreator.persistWorkContract(workContractExtinctionSituationRequest);

        if(workContractExtinctionId != null){
            Message.informationMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTINCTION_PERSISTENCE_OK);
            return workContractExtinctionId;

        }else{
            Message.errorMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTINCTION_PERSISTENCE_NOT_OK);
        }
        return null;
    }

    private void updateInitialContractEndDate(List<WorkContract> workContractList){
        int VARIATION_TYPE_INITIAL_CONTRACT_CODE = 100;
        Integer initialContractSituationId = 0;

        for(WorkContract workContract : workContractList){
            if(workContract.getVariationType().getVariationCode() == VARIATION_TYPE_INITIAL_CONTRACT_CODE){
                initialContractSituationId = workContract.getId();
            }
        }
        WorkContractDAO workContractDAO = WorkContractDAO.ContractDAOFactory.getInstance();
        WorkContractDBO workContractDBOToUpdate = workContractDAO.getSession().load(WorkContractDBO.class, initialContractSituationId);
        workContractDBOToUpdate.setEndingDate(Date.valueOf(workContractVariationVariationForm.getWorkContractExtinction().getDateFrom().getValue()));
        workContractDAO.update(workContractDBOToUpdate);
    }
}
