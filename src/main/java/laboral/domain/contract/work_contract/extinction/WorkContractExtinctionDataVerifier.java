package laboral.domain.contract.work_contract.extinction;

import laboral.domain.contract.WorkContract;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class WorkContractExtinctionDataVerifier {

    private final Set<ContractExtinctionSituationProblem> problems = new HashSet<>();

    public Set<ContractExtinctionSituationProblem> verify(WorkContract workContract, ContractExtinctionSituationEntryDataContainer extinctionDataContainer){

        LocalDate expectedEndDate = workContract.getExpectedEndDate() == null
                ? null
                : workContract.getExpectedEndDate();

        if(extinctionDataContainer.getVariationType() == null) {
            problems.add(ContractExtinctionSituationProblem.EXTINCTION_CAUSE_ERROR);
        }
        if(extinctionDataContainer.getExtinctionDate() == null) {
            problems.add(ContractExtinctionSituationProblem.EXTINCTION_DATE_ERROR);
        }
        if(expectedEndDate != null &&
                extinctionDataContainer.getExtinctionDate() != null &&
                extinctionDataContainer.getExtinctionDate().isAfter(workContract.getExpectedEndDate())) {
            problems.add(ContractExtinctionSituationProblem.EXTINCTION_DATE_ERROR);
        }
        if(extinctionDataContainer.getHolidaysYes().equals(Boolean.FALSE) &&
                extinctionDataContainer.getHolidaysNo().equals(Boolean.FALSE) &&
                extinctionDataContainer.getHolidaysCalculate().equals(Boolean.FALSE)){
            problems.add(ContractExtinctionSituationProblem.HOLIDAYS_SITUATION_ERROR);
        }

        return Collections.unmodifiableSet(problems);
    }

    public enum ContractExtinctionSituationProblem {
        EXTINCTION_CAUSE_ERROR("Error. Causa de la extinción del contrato no establecida."),
        EXTINCTION_DATE_ERROR("Error. Fecha de extinción del contrato no establecida."),
        HOLIDAYS_SITUATION_ERROR("Error. Situación de las vacaciones no establecida.");

        private String extinctionProblemDescription;

        ContractExtinctionSituationProblem(String extinctionProblemDescription) {
            this.extinctionProblemDescription = extinctionProblemDescription;
        }

        public String getExtinctionProblemDescription() {
            return extinctionProblemDescription;
        }

        public String toString(){
            return getExtinctionProblemDescription();
        }
    }
}
