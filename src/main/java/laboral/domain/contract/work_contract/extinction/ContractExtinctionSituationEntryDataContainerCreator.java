package laboral.domain.contract.work_contract.extinction;

import laboral.component.generic_component.DateHourNotificationForm;
import laboral.component.work_contract.variation.component.WorkContractExtinctionForm;
import laboral.domain.utilities.utilities.Utilities;
import laboral.domain.variation_type.VariationType;

import java.time.LocalDate;
import java.time.LocalTime;

public class ContractExtinctionSituationEntryDataContainerCreator {

    public static ContractExtinctionSituationEntryDataContainer create(DateHourNotificationForm dateHourNotificationForm, WorkContractExtinctionForm workContractExtinctionForm) {

        LocalDate notificationDate = dateHourNotificationForm.getNotificationDate().getValue() == null
                ? null
                : dateHourNotificationForm.getNotificationDate().getValue();

        LocalTime notificationHour = dateHourNotificationForm.getNotificationHour().getText() == null
                ? null
                : Utilities.convertStringToLocalTime(dateHourNotificationForm.getNotificationHour().getText());

        VariationType extinctionCause =
                workContractExtinctionForm.getExtinctionCauseSelector().getSelectionModel().getSelectedItem() == null
                        ? null
                        : workContractExtinctionForm.getExtinctionCauseSelector().getSelectionModel().getSelectedItem();

        LocalDate extinctionDate = workContractExtinctionForm.getDateFrom().getValue() == null
                ? null
                : workContractExtinctionForm.getDateFrom().getValue();

        return ContractExtinctionSituationEntryDataContainer.ContractExtinctionSituationEntryDataContainerBuilder.create()
                .withExtinctionDate(extinctionDate)
                .withVariationType(extinctionCause)
                .withClientDateNotification(notificationDate)
                .withClientHourNotification(notificationHour)
                .withHolidaysNo(workContractExtinctionForm.getRbHolidaysNo().isSelected())
                .withHolidaysYes(workContractExtinctionForm.getRbHolidaysYes().isSelected())
                .withHolidaysCalculate(workContractExtinctionForm.getRbHolidaysCalculate().isSelected())
                .withPublicNotes(workContractExtinctionForm.getPublicNotes().getText())
                .withPrivateNotes(workContractExtinctionForm.getPrivateNotes().getText())
                .build();
    }
}
