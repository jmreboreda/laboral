package laboral.domain.contract.work_contract.extinction;

import laboral.domain.variation_type.VariationType;

import java.time.LocalDate;
import java.time.LocalTime;

public class ContractExtinctionSituationEntryDataContainer {

    private final LocalDate clientDateNotification;
    private final LocalTime clientHourNotification;
    private final VariationType variationType;
    private final LocalDate extinctionDate;
    private final Boolean holidaysYes;
    private final Boolean holidaysNo;
    private final Boolean holidaysCalculate;
    private final String publicNotes;
    private final String privateNotes;

    public ContractExtinctionSituationEntryDataContainer(LocalDate clientDateNotification,
                                                         LocalTime clientHourNotification,
                                                         VariationType variationType,
                                                         LocalDate extinctionDate,
                                                         Boolean holidaysYes,
                                                         Boolean holidaysNo,
                                                         Boolean holidaysCalculate,
                                                         String publicNotes,
                                                         String privateNotes) {
        this.clientDateNotification = clientDateNotification;
        this.clientHourNotification = clientHourNotification;
        this.variationType = variationType;
        this.extinctionDate = extinctionDate;
        this.holidaysYes = holidaysYes;
        this.holidaysNo = holidaysNo;
        this.holidaysCalculate = holidaysCalculate;
        this.publicNotes = publicNotes;
        this.privateNotes = privateNotes;
    }

    public LocalDate getClientDateNotification() {
        return clientDateNotification;
    }

    public LocalTime getClientHourNotification() {
        return clientHourNotification;
    }

    public VariationType getVariationType() {
        return variationType;
    }

    public LocalDate getExtinctionDate() {
        return extinctionDate;
    }

    public Boolean getHolidaysYes() {
        return holidaysYes;
    }

    public Boolean getHolidaysNo() {
        return holidaysNo;
    }

    public Boolean getHolidaysCalculate() {
        return holidaysCalculate;
    }

    public String getPublicNotes() {
        return publicNotes;
    }

    public String getPrivateNotes() {
        return privateNotes;
    }

    public static final class ContractExtinctionSituationEntryDataContainerBuilder {
        private LocalDate clientDateNotification;
        private LocalTime clientHourNotification;
        private VariationType variationType;
        private LocalDate extinctionDate;
        private Boolean holidaysYes;
        private Boolean holidaysNo;
        private Boolean holidaysCalculate;
        private String publicNotes;
        private String privateNotes;

        private ContractExtinctionSituationEntryDataContainerBuilder() {
        }

        public static ContractExtinctionSituationEntryDataContainerBuilder create() {
            return new ContractExtinctionSituationEntryDataContainerBuilder();
        }

        public ContractExtinctionSituationEntryDataContainerBuilder withClientDateNotification(LocalDate clientDateNotification) {
            this.clientDateNotification = clientDateNotification;
            return this;
        }

        public ContractExtinctionSituationEntryDataContainerBuilder withClientHourNotification(LocalTime clientHourNotification) {
            this.clientHourNotification = clientHourNotification;
            return this;
        }

        public ContractExtinctionSituationEntryDataContainerBuilder withVariationType(VariationType variationType) {
            this.variationType = variationType;
            return this;
        }

        public ContractExtinctionSituationEntryDataContainerBuilder withExtinctionDate(LocalDate extinctionDate) {
            this.extinctionDate = extinctionDate;
            return this;
        }

        public ContractExtinctionSituationEntryDataContainerBuilder withHolidaysYes(Boolean holidaysYes) {
            this.holidaysYes = holidaysYes;
            return this;
        }

        public ContractExtinctionSituationEntryDataContainerBuilder withHolidaysNo(Boolean holidaysNo) {
            this.holidaysNo = holidaysNo;
            return this;
        }

        public ContractExtinctionSituationEntryDataContainerBuilder withHolidaysCalculate(Boolean holidaysCalculate) {
            this.holidaysCalculate = holidaysCalculate;
            return this;
        }

        public ContractExtinctionSituationEntryDataContainerBuilder withPublicNotes(String publicNotes) {
            this.publicNotes = publicNotes;
            return this;
        }

        public ContractExtinctionSituationEntryDataContainerBuilder withPrivateNotes(String privateNotes) {
            this.privateNotes = privateNotes;
            return this;
        }

        public ContractExtinctionSituationEntryDataContainer build() {
            return new ContractExtinctionSituationEntryDataContainer(clientDateNotification, clientHourNotification, variationType, extinctionDate, holidaysYes, holidaysNo, holidaysCalculate, publicNotes, privateNotes);
        }
    }
}
