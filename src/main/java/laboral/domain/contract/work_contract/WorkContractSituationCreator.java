package laboral.domain.contract.work_contract;

import laboral.domain.contract.WorkContract;
import laboral.domain.contract.WorkContractSituationRequest;
import laboral.domain.contract.WorkDayTypeDuration;
import laboral.domain.contract.work_contract.controller.WorkContractController;
import laboral.domain.contract.work_contract.conversion.ContractConversionSituationEntryDataContainer;
import laboral.domain.contract.work_contract.creation.InitialContractSituationEntryDataContainer;
import laboral.domain.contract.work_contract.extension.ContractExtensionSituationEntryDataContainer;
import laboral.domain.contract.work_contract.extinction.ContractExtinctionSituationEntryDataContainer;
import laboral.domain.contract_json.ContractSchedule;
import laboral.domain.contract_type.WorkContractType;
import laboral.domain.utilities.utilities.Utilities;
import laboral.domain.variation_type.VariationType;

import java.sql.Date;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class WorkContractSituationCreator {

    private final WorkContractController workContractController = new WorkContractController();

    public WorkContractSituationRequest newWorkContractSituationRequestGenerator(InitialContractSituationEntryDataContainer initialContractSituationEntryDataContainer){

        Integer variationTypeCode = 100;
        VariationType variationType = VariationType.VariationTypeBuilder.create()
                .withVariationCode(variationTypeCode)
                .build();

        LocalDate notificationDate = initialContractSituationEntryDataContainer.getClientDateNotification();
        LocalTime notificationTime = Utilities.convertStringToLocalTime(initialContractSituationEntryDataContainer.getClientHourNotification());
        LocalDate startDate = initialContractSituationEntryDataContainer.getDateFrom();
        LocalDate expectedEndDate = initialContractSituationEntryDataContainer.getDateTo() == null ? null : initialContractSituationEntryDataContainer.getDateTo();
        LocalDate endNoticeReceptionDate = expectedEndDate == null ? LocalDate.of(9999,12,31) : null;

        ContractSchedule completeSchedule = initialContractSituationEntryDataContainer.getContractSchedule();

        WorkDayTypeDuration fullPartialWorkDay = initialContractSituationEntryDataContainer.getPartialTime() == Boolean.TRUE ? WorkDayTypeDuration.PARTIAL_TIME: WorkDayTypeDuration.FULL_TIME;
        Integer contractNumber = workContractController.findLastContractNumber() + 1;

        Duration hoursWorkWeek = Utilities.timeStringToDurationConverter(initialContractSituationEntryDataContainer.getWeeklyWorkHours());

        return WorkContractSituationRequest.WorkContractCreationRequestBuilder.create()
                .withDateSign(null)
                .withContractNumber(contractNumber)
                .withEmployer(initialContractSituationEntryDataContainer.getEmployer())
                .withEmployee(initialContractSituationEntryDataContainer.getEmployee())
                .withWorkContractType(initialContractSituationEntryDataContainer.getWorkContractType())
                .withWorkCenter(initialContractSituationEntryDataContainer.getWorkCenter())
                .withVariationType(variationType)
                .withStartDate(startDate)
                .withExpectedEndDate(expectedEndDate)
                .withModificationDate(null)
                .withEndDate(null)
                .withFullPartialWorkDay(fullPartialWorkDay)
                .withDaysOffWeek(initialContractSituationEntryDataContainer.getDaysOfWeekSelector().getDaysOfWeek())
                .withHoursWorkWeek(hoursWorkWeek)
                .withContractSchedule(completeSchedule)
                .withLaborCategory(initialContractSituationEntryDataContainer.getLaborCategory())
                .withQuoteAccountCode(initialContractSituationEntryDataContainer.getQuoteAccountCode())
                .withOfficialIdentificationNumber(null)
                .withPublicNotes(initialContractSituationEntryDataContainer.getPublicNotes())
                .withPrivateNotes(initialContractSituationEntryDataContainer.getPrivateNotes())
                .withClientNotification(LocalDateTime.of(notificationDate, notificationTime))
                .withIdcReceptionDate(null)
                .withEndNoticeReceptionDate(endNoticeReceptionDate)
                .withDateDeliveryDocumentation(null)
                .build();
    }

    public WorkContractSituationRequest workContractExtinctionSituationRequestGenerator(WorkContract workContract, ContractExtinctionSituationEntryDataContainer contractExtinctionEntryDataContainer){

        VariationType variationType = VariationType.VariationTypeBuilder.create()
                .withVariationCode(contractExtinctionEntryDataContainer.getVariationType().getVariationCode())
                .build();

        LocalDate notificationDate = contractExtinctionEntryDataContainer.getClientDateNotification();
        LocalTime notificationTime = contractExtinctionEntryDataContainer.getClientHourNotification();
        LocalDate extinctionDate = contractExtinctionEntryDataContainer.getExtinctionDate();

        WorkDayTypeDuration fullPartialWorkDay = workContract.getContractType().getPartialTime() == Boolean.TRUE
                ? WorkDayTypeDuration.PARTIAL_TIME
                : WorkDayTypeDuration.FULL_TIME;

        return WorkContractSituationRequest.WorkContractCreationRequestBuilder.create()
                .withDateSign(workContract.getDateSign())
                .withContractNumber(workContract.getContractNumber())
                .withEmployer(workContract.getEmployer())
                .withEmployee(workContract.getEmployee())
                .withWorkContractType(workContract.getWorkContractType())
                .withWorkCenter(workContract.getWorkCenter())
                .withVariationType(variationType)
                .withStartDate(extinctionDate)
                .withExpectedEndDate(extinctionDate)
                .withModificationDate(extinctionDate)
                .withEndDate(extinctionDate)
                .withFullPartialWorkDay(fullPartialWorkDay)
                .withDaysOffWeek(null)
                .withHoursWorkWeek(null)
                .withContractSchedule(workContract.getContractSchedule())
                .withLaborCategory(workContract.getLaborCategory())
                .withQuoteAccountCode(workContract.getQuoteAccountCode())
                .withOfficialIdentificationNumber(workContract.getOfficialIdentificationNumber())
                .withPublicNotes(contractExtinctionEntryDataContainer.getPublicNotes())
                .withPrivateNotes(contractExtinctionEntryDataContainer.getPrivateNotes())
                .withClientNotification(LocalDateTime.of(notificationDate, notificationTime))
                .withIdcReceptionDate(null)
                .withEndNoticeReceptionDate(LocalDate.of(9999,12,31))
                .withDateDeliveryDocumentation(null)
                .build();
    }

    public WorkContractSituationRequest workContractExtensionSituationRequestGenerator(WorkContract workContract, ContractExtensionSituationEntryDataContainer contractExtensionEntryDataContainer){

        VariationType variationType = VariationType.VariationTypeBuilder.create()
                .withVariationCode(contractExtensionEntryDataContainer.getVariationType().getVariationCode())
                .build();

        LocalDate notificationDate = contractExtensionEntryDataContainer.getClientDateNotification();
        LocalTime notificationTime = contractExtensionEntryDataContainer.getClientHourNotification();
        LocalDate extensionDateFrom = contractExtensionEntryDataContainer.getExtensionDateFrom();
        LocalDate extensionDateTo = contractExtensionEntryDataContainer.getExtensionDateTo();

        WorkDayTypeDuration fullPartialWorkDay = workContract.getContractType().getPartialTime() == Boolean.TRUE
                ? WorkDayTypeDuration.PARTIAL_TIME
                : WorkDayTypeDuration.FULL_TIME;

        return WorkContractSituationRequest.WorkContractCreationRequestBuilder.create()
                .withDateSign(workContract.getDateSign())
                .withContractNumber(workContract.getContractNumber())
                .withEmployer(workContract.getEmployer())
                .withEmployee(workContract.getEmployee())
                .withWorkContractType(workContract.getWorkContractType())
                .withWorkCenter(workContract.getWorkCenter())
                .withVariationType(variationType)
                .withStartDate(extensionDateFrom)
                .withExpectedEndDate(extensionDateTo)
                .withModificationDate(null)
                .withEndDate(null)
                .withFullPartialWorkDay(fullPartialWorkDay)
                .withDaysOffWeek(null)
                .withHoursWorkWeek(null)
                .withContractSchedule(workContract.getContractSchedule())
                .withLaborCategory(workContract.getLaborCategory())
                .withQuoteAccountCode(workContract.getQuoteAccountCode())
                .withOfficialIdentificationNumber(workContract.getOfficialIdentificationNumber())
                .withPublicNotes(contractExtensionEntryDataContainer.getPublicNotes())
                .withPrivateNotes(contractExtensionEntryDataContainer.getPrivateNotes())
                .withClientNotification(LocalDateTime.of(notificationDate, notificationTime))
                .withIdcReceptionDate(LocalDate.of(9999,12,31))
                .withEndNoticeReceptionDate(null)
                .withDateDeliveryDocumentation(null)
                .build();
    }

    public WorkContractSituationRequest workContractConversionSituationRequestGenerator(WorkContract workContract, ContractConversionSituationEntryDataContainer contractConversionEntryDataContainer){

        WorkContractType workContractType = workContract.getWorkContractType();

        int VARIATION_CODE_OF_CONVERSION_FROM_TEMPORAL_TO_UNDEFINED = 400;
        if(contractConversionEntryDataContainer.getVariationType().getVariationCode() == VARIATION_CODE_OF_CONVERSION_FROM_TEMPORAL_TO_UNDEFINED){
            if(workContract.getFullPartialWorkDay().equals(WorkDayTypeDuration.FULL_TIME)){
                int CONTRACT_CODE_CONVERSION_FROM_TEMPORAL_TO_UNDEFINED_FULL_TIME = 189;
                workContractType.setContractCode(CONTRACT_CODE_CONVERSION_FROM_TEMPORAL_TO_UNDEFINED_FULL_TIME);
            }else{
                int CONTRACT_CODE_CONVERSION_FROM_TEMPORAL_TO_UNDEFINED_PARTIAL_TIME = 289;
                workContractType.setContractCode(CONTRACT_CODE_CONVERSION_FROM_TEMPORAL_TO_UNDEFINED_PARTIAL_TIME);
            }
        }

        int CONVERSION_CODE_FROM_FORMATION_CONTRACT_TO_NORMAL_CONTRACT = 401;
        if(contractConversionEntryDataContainer.getVariationType().getVariationCode() == CONVERSION_CODE_FROM_FORMATION_CONTRACT_TO_NORMAL_CONTRACT){
            if(workContract.getFullPartialWorkDay().equals(WorkDayTypeDuration.FULL_TIME)) {
                int CONTRACT_CODE_FROM_FORMATION_TO_NORMAL_FULL_TIME = 109;
                workContractType.setContractCode(CONTRACT_CODE_FROM_FORMATION_TO_NORMAL_FULL_TIME);
            }else{
                int CONTRACT_CODE_FROM_FORMATION_TO_NORMAL_PARTIAL_TIME = 209;
                workContractType.setContractCode(CONTRACT_CODE_FROM_FORMATION_TO_NORMAL_PARTIAL_TIME);
            }
        }

        int CONVERSION_CODE_FROM_PREGNANCY_RISK_TO_MATERNITY = 403;
        if(contractConversionEntryDataContainer.getVariationType().getVariationCode() == CONVERSION_CODE_FROM_PREGNANCY_RISK_TO_MATERNITY){
            if(workContract.getFullPartialWorkDay().equals(WorkDayTypeDuration.FULL_TIME)) {
                int CONTRACT_CODE_MATERNITY_SUBSTITUTION_FULL_TIME = 4103;
                workContractType.setContractCode(CONTRACT_CODE_MATERNITY_SUBSTITUTION_FULL_TIME);
            }else{
                int CONTRACT_CODE_MATERNITY_SUBSTITUTION_PARTIAL_TIME = 5103;
                workContractType.setContractCode(CONTRACT_CODE_MATERNITY_SUBSTITUTION_PARTIAL_TIME);

            }
        }

        VariationType variationType = VariationType.VariationTypeBuilder.create()
                .withVariationCode(contractConversionEntryDataContainer.getVariationType().getVariationCode())
                .build();

        LocalDate notificationDate = contractConversionEntryDataContainer.getClientDateNotification();
        LocalTime notificationTime = contractConversionEntryDataContainer.getClientHourNotification();
        LocalDate extensionDateFrom = contractConversionEntryDataContainer.getConversionDateFrom();
        LocalDate extensionDateTo = contractConversionEntryDataContainer.getConversionDateTo();

        LocalDate endNoticeReceptionDate = contractConversionEntryDataContainer.getConversionDateTo() == null
                ? LocalDate.of(9999,12,31)
                : null;

        WorkDayTypeDuration fullPartialWorkDay = workContract.getContractType().getPartialTime() == Boolean.TRUE
                ? WorkDayTypeDuration.PARTIAL_TIME
                : WorkDayTypeDuration.FULL_TIME;

        return WorkContractSituationRequest.WorkContractCreationRequestBuilder.create()
                .withDateSign(workContract.getDateSign())
                .withContractNumber(workContract.getContractNumber())
                .withEmployer(workContract.getEmployer())
                .withEmployee(workContract.getEmployee())
                .withWorkContractType(workContractType)
                .withWorkCenter(workContract.getWorkCenter())
                .withVariationType(variationType)
                .withStartDate(extensionDateFrom)
                .withExpectedEndDate(extensionDateTo)
                .withModificationDate(null)
                .withEndDate(null)
                .withFullPartialWorkDay(fullPartialWorkDay)
                .withDaysOffWeek(null)
                .withHoursWorkWeek(null)
                .withContractSchedule(workContract.getContractSchedule())
                .withLaborCategory(workContract.getLaborCategory())
                .withQuoteAccountCode(workContract.getQuoteAccountCode())
                .withOfficialIdentificationNumber(workContract.getOfficialIdentificationNumber())
                .withPublicNotes(contractConversionEntryDataContainer.getPublicNotes())
                .withPrivateNotes(contractConversionEntryDataContainer.getPrivateNotes())
                .withClientNotification(LocalDateTime.of(notificationDate, notificationTime))
                .withIdcReceptionDate(null)
                .withEndNoticeReceptionDate(endNoticeReceptionDate)
                .withDateDeliveryDocumentation(null)
                .build();
    }

    public Integer persistWorkContract(WorkContractSituationRequest workContractSituationRequest) {

        return workContractController.createWorkContract(workContractSituationRequest);
    }
}
