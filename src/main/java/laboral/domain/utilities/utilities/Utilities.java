package laboral.domain.utilities.utilities;

import laboral.ApplicationConstants;
import laboral.domain.utilities.Parameters;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.regex.Pattern;

import static laboral.domain.utilities.utilities.UtilitiesErrors.FORMAT_TEXT_TO_CONVERT_INCORRECT;
import static laboral.domain.utilities.utilities.UtilitiesErrors.TEXT_TO_CONVERT_IS_NULL;

public class Utilities {

    public static String formatDateAsStringDate(LocalDate date){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());

        return formatter.format(date);

    }

    public static LocalDate convertStringToLocalDate(String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate localDate = null;

        try{
            localDate = LocalDate.parse(date, formatter);
        }
        catch (DateTimeParseException e){
            System.out.println(FORMAT_TEXT_TO_CONVERT_INCORRECT);
        }
        catch(NullPointerException e){
            System.out.println(TEXT_TO_CONVERT_IS_NULL);
        }

        return localDate;
    }

    public static LocalTime convertStringToLocalTime(String time) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime localTime = null;

        try {
            localTime = LocalTime.parse(time, formatter);
        }
        catch (DateTimeParseException e) {
            System.out.println(FORMAT_TEXT_TO_CONVERT_INCORRECT);
        }
        catch (NullPointerException e) {
            System.out.println(TEXT_TO_CONVERT_IS_NULL);
        }

        return localTime;
    }

    public static Date localDateToSqlDateConverter(LocalDate localDate){

        return localDate == null ? null : Date.valueOf(localDate);
    }

    public static String localTimeToStringConverter(Date date){

        return date == null ? null : new SimpleDateFormat("HH:mm:sss.SSS").format(date);
    }

    public static Duration timeStringToDurationConverter(String timeAsString){
        String time = "";
        String hours = "";
        String minutes = "";

        Pattern timeCompletePattern = Pattern.compile("\\d{2}:\\d{2}");
        Pattern timeFourDigitPattern = Pattern.compile("\\d{4}");
        Pattern timeOnlyOneDigitPattern = Pattern.compile("\\d");
        Pattern timeOnlyTwoDigitPattern = Pattern.compile("\\d{2}");
        Pattern timeOnlyMinutesPattern = Pattern.compile(":\\d{2}");

        if(timeCompletePattern.matcher(timeAsString).matches()){
            hours = timeAsString.substring(0,2);
            minutes = timeAsString.substring(3,5);
        } else

        if(timeFourDigitPattern.matcher(timeAsString).matches()){
            hours = timeAsString.substring(0,2);
            minutes = timeAsString.substring(2,4);
        } else

        if(timeOnlyOneDigitPattern.matcher(timeAsString).matches()){
            hours = "0".concat(timeAsString);
            minutes = "00";
        } else

        if(timeOnlyTwoDigitPattern.matcher(timeAsString).matches()){
            hours = timeAsString;
            minutes = "00";
        } else

        if(timeOnlyMinutesPattern.matcher(timeAsString).matches()){
            hours = "00";
            minutes = timeAsString.substring(1, 3);
        }
        else {
            return null;
        }

        if(Integer.parseInt(hours) > Parameters.INTEGER_LEGAL_MAXIMUM_HOURS_OF_WORK_PER_WEEK ||
        Integer.parseInt(minutes) > Parameters.INTEGER_MAXIMUM_VALUE_MINUTES_IN_HOUR) {

            return null;
        }

        String stringDuration = "PT" + hours + "H" + minutes + "M";

        return Duration.parse(stringDuration);
    }

    public static  String durationToTimeStringConverter(Duration duration){
        if(duration == Duration.ZERO){
            return "00:00";
        }

        if(duration == null){
            return null;
        }

        String hours = Long.toString(duration.toHours());
        String minutes = Long.toString(duration.toMinutes() - duration.toHours() * 60);

        hours = hours.length() == 1 ? "0" + hours : hours;
        minutes = minutes.length() == 1 ? "0" + minutes : minutes;

        return hours + ":" + minutes;
    }

    public static Duration convertIntegerToDuration(Integer intergerToConvert){

        return Duration.parse("P" + intergerToConvert + "D");
    }

    public static DayOfWeek converterStringToDayOfWeek(String dayOfWeekString){
        DayOfWeek dayOfWeek = null;
        if (dayOfWeekString.equals(DayOfWeek.MONDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            dayOfWeek = DayOfWeek.MONDAY;
        }
        else if (dayOfWeekString.equals(DayOfWeek.TUESDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            dayOfWeek = DayOfWeek.TUESDAY;
        }
        else if (dayOfWeekString.equals(DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            dayOfWeek = DayOfWeek.WEDNESDAY;
        }
        else if (dayOfWeekString.equals(DayOfWeek.THURSDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            dayOfWeek = DayOfWeek.THURSDAY;
        }
        else if (dayOfWeekString.equals(DayOfWeek.FRIDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            dayOfWeek = DayOfWeek.FRIDAY;
        }
        else if (dayOfWeekString.equals(DayOfWeek.SATURDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            dayOfWeek = DayOfWeek.SATURDAY;
        }
        else if (dayOfWeekString.equals(DayOfWeek.SUNDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            dayOfWeek = DayOfWeek.SUNDAY;
        }

        return dayOfWeek;
    }

    public static java.util.Date validateStringAsTime(String time){
        java.util.Date hour;
        DateFormat hourFormatter = new SimpleDateFormat(String.valueOf(ApplicationConstants.DEFAULT_TIME_FORMAT));
        hourFormatter.setLenient(false);
        try{
            hour = hourFormatter.parse(time);
        }catch(ParseException e){
            return null;
        }
        return hour;
    }

    public static String replaceWithUnderscore(String aString){

        return aString.replace(". ", "")
                .replace(".", "")
                .replace(",", "")
                .replace(" ", "_");
    }

    public static void deleteFileFromPath(String pathToFile) throws IOException {
        Path path = FileSystems.getDefault().getPath(pathToFile);
        Files.delete(path);
    }
}
