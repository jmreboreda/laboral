package laboral.domain.utilities.events;

import javafx.event.Event;
import javafx.event.EventType;
import laboral.domain.employer.Employer;

public class EmployerToManageEvent extends Event {

    public static final EventType<EmployerToManageEvent> EMPLOYER_SELECTED_EVENT_TYPE = new EventType<>("EMPLOYER_SELECTED_EVENT_TYPE");
        private final Employer employerSelected;

    public EmployerToManageEvent(Employer employerSelected) {
        super(EMPLOYER_SELECTED_EVENT_TYPE);
        this.employerSelected = employerSelected;

    }

    public Employer getEmployerSelected() {
        return employerSelected;
    }
}
