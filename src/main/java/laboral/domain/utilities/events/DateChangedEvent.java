package laboral.domain.utilities.events;

import javafx.event.Event;
import javafx.event.EventType;

import java.time.LocalDate;

public class DateChangedEvent extends Event {

    public static final EventType<DateChangedEvent> DATE_CHANGE_EVENT_TYPE = new EventType<>("DATE_CHANGE_EVENT_TYPE");
    private final LocalDate date;

    public DateChangedEvent(LocalDate date) {
        super(DATE_CHANGE_EVENT_TYPE);
        this.date = date;

    }

    public LocalDate getDate() {
        return date;
    }
}
