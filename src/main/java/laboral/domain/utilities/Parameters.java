package laboral.domain.utilities;

public class Parameters {

    public static final String CANNOT_FIND_PROPERTIES_FILE = "Archivo de propiedades no encontrado: ";


    /** Printer */
    public static final String PRINTER_TRAY_OF_A3 = "Bandeja 3";
    public static final String NO_PRINTER_FOR_THESE_ATTRIBUTES = "No hay impresora para imprimir con los atributos indicados.";

    /** Colors */
    public static final String FOREGROUND_TEXT_BLUE_COLOR = "-fx-text-fill: #000FFF;";
    public static final String FOREGROUND_TEXT_GREEN_COLOR = "-fx-text-fill: #006400;";
    public static final String FOREGROUND_TEXT_RED_COLOR ="-fx-text-fill: #800000;";

    /** Messages */
    public static final String SYSTEM_INFORMATION_TEXT = "Información del sistema";
    public static final String DATE_LABEL_TEXT = "Fecha";
    public static final String DATE_FROM_TEXT = "Desde";
    public static final String DATE_TO_TEXT = "Hasta";

    /** Various */
    public static final Long MINIMUM_NUMBER_DAYS_CONTRACT_DURATION_TO_SEND_NOTICE_END_CONTRACT = 15L;
    public static final  Integer INTEGER_MAXIMUM_HOURS_IN_A_DAY = 24;
    public static final  Integer INTEGER_LEGAL_MAXIMUM_HOURS_OF_WORK_PER_WEEK = 40;
    public static final Integer INTEGER_MAXIMUM_VALUE_MINUTES_IN_HOUR = 59;
}
