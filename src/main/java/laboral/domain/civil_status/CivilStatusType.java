package laboral.domain.civil_status;

public enum CivilStatusType {

    UNKNOWN("Desconocido"),
    SINGLE("Soltero(a)"),
    MARRIED("Casado(a)"),
    LEGALLY_SEPARATED("Separado(a) legalmente"),
    DIVORCED("Divorciado(a)"),
    WIDOWER_WIDOW("Viudo(a)"),
    IN_FACT_PARTNER("Pareja de hecho informal"),
    LEGAL_PARTNER("Pareja de hecho registrada");

    private String civilStatusDescription;

    CivilStatusType(String civilStatusDescription) {
        this.civilStatusDescription = civilStatusDescription;
    }

    public String getCivilStatusDescription() {
        return civilStatusDescription;
    }

    public String toString(){
        return getCivilStatusDescription();
    }
}
