package laboral.domain.civil_status;

public class CivilStatus {

    private Integer id;
    private CivilStatusType civilStatus;

    public CivilStatus() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CivilStatusType getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(CivilStatusType civilStatus) {
        this.civilStatus = civilStatus;
    }
}
