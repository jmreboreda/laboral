package laboral.domain.email.dto;

import laboral.domain.employee.Employee;
import laboral.domain.employer.Employer;

import java.nio.file.Path;

public class EmailDataDTO {

    private Path path;
    private String fileName;
    private Employer employer;
    private Employee employee;
    private String variationTypeText;

    public EmailDataDTO(Path path, String fileName, Employer employer, Employee employee, String variationTypeText) {
        this.path = path;
        this.fileName = fileName;
        this.employer = employer;
        this.employee = employee;
        this.variationTypeText = variationTypeText;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getVariationTypeText() {
        return variationTypeText;
    }

    public void setVariationTypeText(String variationTypeText) {
        this.variationTypeText = variationTypeText;
    }

    public static EmailDataDTOBuilder create() {
        return new EmailDataDTOBuilder();
    }

    public static class EmailDataDTOBuilder {

        private Path path;
        private String fileName;
        private Employer employer;
        private Employee employee;
        private String variationTypeText;

        public EmailDataDTOBuilder withPath(Path path) {
            this.path = path;
            return this;
        }

        public EmailDataDTOBuilder withFileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public EmailDataDTOBuilder withEmployer(Employer employer) {
            this.employer = employer;
            return this;
        }

        public EmailDataDTOBuilder withEmployee(Employee employee) {
            this.employee = employee;
            return this;
        }

        public EmailDataDTOBuilder withVariationTypeText(String variationTypeText) {
            this.variationTypeText = variationTypeText;
            return this;
        }

        public EmailDataDTO build() {
            return new EmailDataDTO(this.path, this.fileName, this.employer, this.employee, this.variationTypeText);
        }

    }
}
