package laboral.domain.validator;

import javafx.stage.Stage;
import laboral.component.person_management.PersonManagementConstants;
import laboral.domain.address.Address;
import laboral.domain.nienif.NieNif;
import laboral.domain.nienif.NieNifType;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.social_security_affiliation_number.SocialSecurityAffiliationNumber;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.utilities.validations.PostalCodeValidator;

import java.util.Set;

public class PersonDataValidator {

    Stage stage;

    public PersonDataValidator(Stage stage) {
        this.stage = stage;
    }

    public Boolean naturalPersonDataValidator(PersonCreationRequest personCreationRequest) {

        // Validate no missing data
        if (personCreationRequest.getBirthDate() == null ||
                personCreationRequest.getCivilStatus() == null ||
                personCreationRequest.getFirstSurname().isEmpty() ||
                personCreationRequest.getName().isEmpty() ||
                personCreationRequest.getNationality().isEmpty() ||
                personCreationRequest.getNieNif().getNif().isEmpty() ||
                personCreationRequest.getSecondSurname().isEmpty() ||
                personCreationRequest.getSocialSecurityAffiliationNumber().isEmpty() ||
                personCreationRequest.getStudy() == null) {
            Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.INCOMPLETE_PERSONAL_DATA_ENTRY);
            return Boolean.FALSE;
        }

        // Validate no missing data in addresses
        if(!validateNoMissingAddressesData(personCreationRequest.getAddresses())){
            Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.SOME_ADDRESS_OR_ADDRESSES_ARE_INCOMPLETE);
            return Boolean.FALSE;
        }

        // Validate NieNif
        NieNif nieNif = personCreationRequest.getNieNif();
        if(nieNif.validateNieNif() != NieNifType.DOCUMENT_NIF_DNI &&
                nieNif.validateNieNif() != NieNifType.DOCUMENT_NIF_NIE){
            Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.NIE_NIF_IS_NOT_VALID);
            return Boolean.FALSE;
        }

        // Validate Social Security Affiliation Number
        if(personCreationRequest.getSocialSecurityAffiliationNumber() != null){
            SocialSecurityAffiliationNumber socialSecurityAffiliationNumber = new SocialSecurityAffiliationNumber(personCreationRequest.getSocialSecurityAffiliationNumber());
            if(!socialSecurityAffiliationNumber.isValid()){
                Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.SSAN_IS_NOT_VALID);
                return Boolean.FALSE;
            }
        }

        // Validate default address is set
        Boolean isDefaultAddressSet = Boolean.FALSE;
        for(Address address : personCreationRequest.getAddresses()){
            if(address.getDefaultAddress()){
                isDefaultAddressSet = Boolean.TRUE;
            }
        }

        if(!isDefaultAddressSet){
            Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.DEFAULT_ADDRESS_IS_NOT_SET);
            return Boolean.FALSE;
        }


        // Validate Postal Code
        PostalCodeValidator postalCodeValidator = new PostalCodeValidator();
        for(Address address : personCreationRequest.getAddresses()){
            if(!postalCodeValidator.validatePostalCode(address.getPostalCode())){
                Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.POSTAL_CODE_IS_NOT_VALID);
                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    public Boolean legalPersonDataValidator(PersonCreationRequest legalNaturalPersonCreationRequest){

        // Validate no missing data
        if (legalNaturalPersonCreationRequest.getLegalName().isEmpty() ||
                legalNaturalPersonCreationRequest.getNieNif().getNif().isEmpty()) {
            Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.INCOMPLETE_DATA_ENTRY);
            return Boolean.FALSE;
        }

        // Validate no missing data in addresses
        if(!validateNoMissingAddressesData(legalNaturalPersonCreationRequest.getAddresses())){
            Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.SOME_ADDRESS_OR_ADDRESSES_ARE_INCOMPLETE);
            return Boolean.FALSE;
        }

        // Validate NieNif
        NieNif nieNif = legalNaturalPersonCreationRequest.getNieNif();
        if(nieNif.validateNieNif() == NieNifType.DOCUMENT_ERROR ||
                nieNif.validateNieNif() == NieNifType.DOCUMENT_NIF_DNI ||
                nieNif.validateNieNif() == NieNifType.DOCUMENT_NIF_NIE ||
                nieNif.validateNieNif() == NieNifType.DOCUMENT_NIF_EWLP){
            Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.NIE_NIF_IS_NOT_VALID);
            return Boolean.FALSE;

        }

        // Validate default address is set
        Boolean isDefaultAddressSet = Boolean.FALSE;
        for(Address address : legalNaturalPersonCreationRequest.getAddresses()){
            if(address.getDefaultAddress()){
                isDefaultAddressSet = Boolean.TRUE;
            }
        }

        if(!isDefaultAddressSet){
            Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.DEFAULT_ADDRESS_IS_NOT_SET);
            return Boolean.FALSE;
        }

        // Validate Postal Code
        PostalCodeValidator postalCodeValidator = new PostalCodeValidator();
        for(Address address : legalNaturalPersonCreationRequest.getAddresses()){
            if(!postalCodeValidator.validatePostalCode(address.getPostalCode())){
                Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.POSTAL_CODE_IS_NOT_VALID);

                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    public Boolean entityWithoutLegalPersonalityDataValidator(PersonCreationRequest legalPersonCreationRequest){

        // Validate no missing data
        if (legalPersonCreationRequest.getLegalName().isEmpty() ||
                legalPersonCreationRequest.getNieNif().getNif().isEmpty()) {
            Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.INCOMPLETE_DATA_ENTRY);
            return Boolean.FALSE;
        }

        // Validate no missing data in addresses
        if(!validateNoMissingAddressesData(legalPersonCreationRequest.getAddresses())){
            Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.SOME_ADDRESS_OR_ADDRESSES_ARE_INCOMPLETE);
            return Boolean.FALSE;
        }

        // Validate NieNif
        NieNif nieNif = legalPersonCreationRequest.getNieNif();
        if(nieNif.validateNieNif() != NieNifType.DOCUMENT_NIF_EWLP){
            Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.NIE_NIF_IS_NOT_VALID);
            return Boolean.FALSE;

        }

        // Validate default address is set
        Boolean isDefaultAddressSet = Boolean.FALSE;
        for(Address address : legalPersonCreationRequest.getAddresses()){
            if(address.getDefaultAddress()){
                isDefaultAddressSet = Boolean.TRUE;
            }
        }

        if(!isDefaultAddressSet){
            Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.DEFAULT_ADDRESS_IS_NOT_SET);
            return Boolean.FALSE;
        }

        // Validate Postal Code
        PostalCodeValidator postalCodeValidator = new PostalCodeValidator();
        for(Address address : legalPersonCreationRequest.getAddresses()){
            if(!postalCodeValidator.validatePostalCode(address.getPostalCode())){
                Message.errorMessage(stage, Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.POSTAL_CODE_IS_NOT_VALID);

                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    private Boolean validateNoMissingAddressesData(Set<Address> addresses){
        for(Address address : addresses){
            if(address.getStreetName().isEmpty() ||
            address.getStreetExtended().isEmpty() ||
            address.getLocation().isEmpty() ||
            address.getMunicipality().isEmpty() ||
            address.getPostalCode().isEmpty()) {
                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }
}
