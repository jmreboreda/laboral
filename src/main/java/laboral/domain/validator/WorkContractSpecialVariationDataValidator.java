package laboral.domain.validator;

import javafx.stage.Stage;
import laboral.component.work_contract.variation.WorkContractVariationConstants;
import laboral.component.work_contract.variation.component.WorkContractSpecial;
import laboral.domain.contract.WorkContract;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import java.time.LocalDate;
import java.util.List;

public class WorkContractSpecialVariationDataValidator {

    public WorkContractSpecialVariationDataValidator() {
    }

    public Boolean verifySpecialInitialVariationData(WorkContractSpecial workContractSpecial, List<WorkContract> workContractToSpecialList){

        LocalDate startDate = workContractSpecial.getStartDate().getValue();
        LocalDate expectedEndDate = workContractSpecial.getExpectedEndDate().getValue();

        if (startDate == null && expectedEndDate == null) {
            Message.errorMessage((Stage) workContractSpecial.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.ALL_DATE_CONTRACT_SPECIAL_IS_MISSING);
            return Boolean.FALSE;
        }

        if(startDate == null){
            Message.errorMessage((Stage) workContractSpecial.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.START_SPECIAL_DATE_IS_INCORRECT);
            return Boolean.FALSE;
        }

        if(expectedEndDate != null && expectedEndDate.isBefore(startDate) ){
            Message.errorMessage((Stage) workContractSpecial.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.SPECIAL_VARIATION_INCOHERENT_DATES);
            return Boolean.FALSE;
        }

        for(WorkContract workContract : workContractToSpecialList){
            if(workContract.getModificationDate() == null){
                if(workContract.getVariationType().getSpecialInitial() || workContract.getVariationType().getSpecialFinal()){
                    Message.errorMessage((Stage) workContractSpecial.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.ALREADY_SPECIAL_VARIATION_IN_PROGRESS +
                            workContract.getVariationType().getVariationDescription());
                    return Boolean.FALSE;
                }

                if(startDate.isBefore(workContract.getStartDate())){
                    Message.errorMessage((Stage) workContractSpecial.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.START_SPECIAL_DATE_IS_INCORRECT);
                    return Boolean.FALSE;
                }

                if(workContract.getExpectedEndDate() != null && startDate.isAfter(workContract.getExpectedEndDate())){
                    Message.errorMessage((Stage) workContractSpecial.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.START_SPECIAL_DATE_IS_INCORRECT);
                    return Boolean.FALSE;
                }
            }
        }

        if(expectedEndDate == null){
            if(!Message.confirmationMessage((Stage) workContractSpecial.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.QUESTION_EXPECTED_END_DATE_IS_EMPTY)){
                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    public Boolean verifySpecialFinalVariationData(WorkContractSpecial workContractSpecial, List<WorkContract> workContractToSpecialList) {

        LocalDate endDate = workContractSpecial.getEndDate().getValue();
        Boolean isInSpecialInitial = Boolean.FALSE;


        if (endDate == null) {
            Message.errorMessage((Stage) workContractSpecial.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.END_SPECIAL_DATE_IS_INCORRECT);
            return Boolean.FALSE;
        }

        for (WorkContract workContract : workContractToSpecialList) {
            if (workContract.getModificationDate() == null) {
                if (endDate.isBefore(workContract.getStartDate())) {
                    Message.errorMessage((Stage) workContractSpecial.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.END_SPECIAL_DATE_IS_INCORRECT);
                    return Boolean.FALSE;
                }

                if (workContract.getExpectedEndDate() != null && endDate.isAfter(workContract.getExpectedEndDate())) {
                    Message.errorMessage((Stage) workContractSpecial.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.END_SPECIAL_DATE_IS_INCORRECT);
                    return Boolean.FALSE;
                }
            }

            if (workContract.getVariationType().getSpecialInitial() && workContract.getModificationDate() == null) {
                isInSpecialInitial = Boolean.TRUE;
            }
       }

        if(!isInSpecialInitial){
            Message.errorMessage((Stage) workContractSpecial.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.SPECIAL_FINALIZATION_WITHOUT_SPECIAL_INITIAL);
            return Boolean.FALSE;
        }

         return Boolean.TRUE;
    }

    public Boolean verifySpecialReincorporationVariationData(WorkContractSpecial workContractSpecial, List<WorkContract> workContractToSpecialList){

        LocalDate reincorporationDate = workContractSpecial.getRestartDate().getValue();
        Boolean isInSpecialFinal = Boolean.FALSE;

        if (reincorporationDate == null) {
            Message.errorMessage((Stage) workContractSpecial.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.REINCORPORATION_SPECIAL_DATE_IS_INCORRECT);
            return Boolean.FALSE;
        }

        for(WorkContract workContract : workContractToSpecialList){
            if(workContract.getModificationDate() == null){
                if(reincorporationDate.isBefore(workContract.getStartDate())){
                    Message.errorMessage((Stage) workContractSpecial.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.REINCORPORATION_SPECIAL_DATE_IS_INCORRECT);
                    return Boolean.FALSE;
                }

                if(workContract.getExpectedEndDate() != null && reincorporationDate.isAfter(workContract.getExpectedEndDate())){
                    Message.errorMessage((Stage) workContractSpecial.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.REINCORPORATION_SPECIAL_DATE_IS_INCORRECT);
                    return Boolean.FALSE;
                }
            }

            if (workContract.getVariationType().getSpecialFinal() && workContract.getModificationDate() == null) {
                isInSpecialFinal = Boolean.TRUE;
            }
        }

        if(!isInSpecialFinal){
            Message.errorMessage((Stage) workContractSpecial.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.SPECIAL_REINCORPORATION_WITHOUT_SPECIAL_FINAL);
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }
}
