package laboral.domain.validator;

import javafx.stage.Stage;
import laboral.component.client_management.ClientManagementConstants;
import laboral.component.employer_management.EmployerManagementConstants;
import laboral.component.employer_management.employer_creation.controllers.EmployerCreationMainController;
import laboral.domain.address.Address;
import laboral.domain.address.StreetType;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.utilities.validations.PostalCodeValidator;

import java.util.*;
import java.util.regex.Pattern;

public class EmployerCreationDataValidator {

    EmployerCreationMainController employerCreationMainController;

    public EmployerCreationDataValidator(EmployerCreationMainController employerCreationMainController) {
        this.employerCreationMainController = employerCreationMainController;
    }

    public Boolean validateWorkCenterDataEntry(){
        List<Address> workCenterTableList = employerCreationMainController.getEmployerWorkCenterTableView().getAddresses().getItems();

        if(!validateNoMissingWorkCenterData(workCenterTableList)){
            Message.errorMessage((Stage) this.employerCreationMainController.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, EmployerManagementConstants.SOME_ADDRESS_OR_ADDRESSES_ARE_INCOMPLETE);

            return Boolean.FALSE;
        }

        Boolean isDefaultWorkCenterSet = Boolean.FALSE;
        for(Address address : workCenterTableList){
            if(address.getDefaultAddress()){
                isDefaultWorkCenterSet = Boolean.TRUE;
            }
        }

        if(!isDefaultWorkCenterSet){
            Message.informationMessage((Stage) this.employerCreationMainController.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.DEFAULT_WORK_CENTER_IS_NOT_SET);

//            return Boolean.FALSE;
        }

        PostalCodeValidator postalCodeValidator = new PostalCodeValidator();
        for(Address address : workCenterTableList){
            if(!postalCodeValidator.validatePostalCode(address.getPostalCode())){
                Message.errorMessage((Stage) this.employerCreationMainController.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.POSTAL_CODE_IS_NOT_VALID);

                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    public Boolean validateQuoteAccountCodeDataEntry(){
        List<QuoteAccountCode> quoteAccountCodeList = employerCreationMainController.getEmployerQuoteAccountCodeTableView().getQuoteAccountCodeTableView().getItems();
        if(!validateNoMissingQuoteAccountCodeData(quoteAccountCodeList)){
            Message.errorMessage((Stage) this.employerCreationMainController.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, EmployerManagementConstants.SOME_QAC_ARE_INCORRECT);

            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

    private Boolean validateNoMissingWorkCenterData(List<Address> workCenterTableList) {

        String pattern = "\\W*";

        for (Address address : workCenterTableList) {
            if (address.getStreetName().isEmpty() ||
                    address.getStreetName().matches(pattern) ||
                    address.getStreetType().equals(StreetType.UNKN) ||
                    address.getStreetExtended().isEmpty() ||
                    address.getStreetExtended().matches(pattern) ||
                    address.getLocation().isEmpty() ||
                    address.getLocation().matches(pattern) ||
                    address.getMunicipality().isEmpty() ||
                    address.getMunicipality().matches(pattern) ||
                    address.getPostalCode().isEmpty()) {

                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    private Boolean validateNoMissingQuoteAccountCodeData(List<QuoteAccountCode> quoteAccountCodeList) {

        Pattern provinceCodePattern = Pattern.compile("\\d{2}");
        Pattern numberPattern = Pattern.compile("\\d{7}");
        Pattern controlDigitPattern = Pattern.compile("\\d{2}");
        Pattern activityEpigraphPattern = Pattern.compile("^[0-9]{3}([.][0-9]{1,2})?$");

        for (QuoteAccountCode quoteAccountCode : quoteAccountCodeList) {
            if (!provinceCodePattern.matcher(quoteAccountCode.getProvince()).matches() ||
                    !numberPattern.matcher(quoteAccountCode.getQuoteAccountCodeNumber()).matches() ||
                    !controlDigitPattern.matcher(quoteAccountCode.getControlDigit()).matches() ||
                    !activityEpigraphPattern.matcher(quoteAccountCode.getActivityEpigraphNumber()).matches()||
                    quoteAccountCode.getActivityDescription().length() < 5)
            {

                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }
}
