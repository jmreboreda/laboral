package laboral.domain.service_gm;

public enum ServiceGMEnum {

    SERVICE_NOT_ESTABLISHED("Servicio sin establecer", false,0),
    RENTS("Alquileres", false,1),
    ADVISORY_WITH_INVOICES_TO_NATURAL_PERSON("Asesoría - Estimación", true,10),
    ADVISORY_WITH_INVOICES_TO_LEGAL_PERSON("Asesoría - Contabilidad", true,11),
    ADVISORY_MODULES_WITH_INVOICES("Asesoría - Módulos - con IVA", false, 20),
    ADVISORY_MODULES_WITHOUT_INVOICES("Asesoría - Módulos - sin IVA", false, 30),
    ADVISORY_WITHOUT_INVOICES("Asesoría sin facturas", false,40),
    LABOR_SERVICE_CUSTOMER("Asesoría y gestión laboral exclusivamente", false, 50);


    private String serviceGMDescription;
    private Boolean claimInvoices;
    private Integer serviceGMCode;

    ServiceGMEnum(String serviceGMDescription, Boolean claimInvoices, Integer serviceGMCode) {
        this.serviceGMDescription = serviceGMDescription;
        this.claimInvoices = claimInvoices;
        this.serviceGMCode = serviceGMCode;
    }

    public String getServiceGMDescription() {
        return serviceGMDescription;
    }

    public Boolean getClaimInvoices() {
        return claimInvoices;
    }

    public Integer getServiceGMCode() {
        return serviceGMCode;
    }

    public String toString(){
        return getServiceGMDescription();
    }
}
