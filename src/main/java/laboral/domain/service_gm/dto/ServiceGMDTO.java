package laboral.domain.service_gm.dto;

import laboral.domain.client.ClientDTO;
import laboral.domain.service_gm.ServiceGMEnum;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ServiceGMDTO {

    private Integer id;
    private ServiceGMEnum serviceGM;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private Boolean claimInvoices;
    private ClientDTO clientDTO;

    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public ServiceGMDTO() {
    }

    public ServiceGMDTO(Integer id,
                        ServiceGMEnum serviceGM,
                        LocalDate dateFrom,
                        LocalDate dateTo,
                        Boolean claimInvoices,
                        ClientDTO clientDTO) {
        this.id = id;
        this.serviceGM = serviceGM;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.claimInvoices= claimInvoices;
        this.clientDTO = clientDTO;
    }

    public Integer getId() {
        return id;
    }

    public ServiceGMEnum getServiceGM() {
        return serviceGM;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public Boolean getClaimInvoices() {
        return claimInvoices;
    }

    public ClientDTO getClientDTO() {
        return clientDTO;
    }

    public Boolean isActive(){
        if(dateTo == null){

            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    public String toString(){
        return getServiceGM().getServiceGMDescription().concat(" desde ").concat(getDateFrom().format(dateTimeFormatter) + " :: " + getClaimInvoices());
    }

    public static final class ServiceGMDTOBuilder {
        private Integer id;
        private ServiceGMEnum serviceGM;
        private LocalDate dateFrom;
        private LocalDate dateTo;
        private Boolean claimInvoices;
        private ClientDTO clientDTO;

        private ServiceGMDTOBuilder() {
        }

        public static ServiceGMDTOBuilder create() {
            return new ServiceGMDTOBuilder();
        }

        public ServiceGMDTOBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public ServiceGMDTOBuilder withServiceGM(ServiceGMEnum serviceGM) {
            this.serviceGM = serviceGM;
            return this;
        }

        public ServiceGMDTOBuilder withDateFrom(LocalDate dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        public ServiceGMDTOBuilder withDateTo(LocalDate dateTo) {
            this.dateTo = dateTo;
            return this;
        }

        public ServiceGMDTOBuilder withClaimInvoices(Boolean claimInvoices) {
            this.claimInvoices = claimInvoices;
            return this;
        }

        public ServiceGMDTOBuilder withClientDTO(ClientDTO clientDTO) {
            this.clientDTO = clientDTO;
            return this;
        }

        public ServiceGMDTO build() {
            return new ServiceGMDTO(id, serviceGM, dateFrom, dateTo, claimInvoices, clientDTO);
        }
    }
}
