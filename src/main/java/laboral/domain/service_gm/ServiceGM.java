package laboral.domain.service_gm;

import laboral.domain.client.Client;
import laboral.domain.client.persistence.dbo.ClientDBO;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ServiceGM {

    private Integer id;
    private ServiceGMEnum serviceGM;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private Boolean claimInvoices;
    private ClientDBO client;

    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    public ServiceGM() {
    }

    public ServiceGM(Integer id,
                     ServiceGMEnum serviceGM,
                     LocalDate dateFrom,
                     LocalDate dateTo,
                     Boolean claimInvoices,
                     ClientDBO client) {
        this.id = id;
        this.serviceGM = serviceGM;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.claimInvoices= claimInvoices;
        this.client = client;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ServiceGMEnum getServiceGM() {
        return serviceGM;
    }

    public void setServiceGM(ServiceGMEnum serviceGM) {
        this.serviceGM = serviceGM;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public Boolean getClaimInvoices() {
        return claimInvoices;
    }

    public void setClaimInvoices(Boolean claimInvoices) {
        this.claimInvoices = claimInvoices;
    }

    public ClientDBO getClient() {
        return client;
    }

    public void setClient(ClientDBO client) {
        this.client = client;
    }

    public Boolean isActive(){
        if(dateTo == null){

            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    public String toString(){
        return getServiceGM().getServiceGMDescription().concat(" desde ").concat(getDateFrom().format(dateTimeFormatter) + " :: " + getClaimInvoices());
    }
}
