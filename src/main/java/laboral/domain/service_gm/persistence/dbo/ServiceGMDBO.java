package laboral.domain.service_gm.persistence.dbo;

import laboral.domain.client.persistence.dbo.ClientDBO;
import laboral.domain.service_gm.ServiceGMEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

@Entity
@Table(name = "service_gm")
@NamedQueries(value = {
        @NamedQuery(
                name = ServiceGMDBO.FIND_SERVICE_GM_BY_CLIENT_ID,
                query = "select p from ServiceGMDBO as p where clientId = :clientId"
        ),
        @NamedQuery(
                name = ServiceGMDBO.FIND_ALL_CLIENT_WITH_INVOICES_TO_CLAIM_IN_PERIOD,
                query = "select p from ServiceGMDBO as p where (p.dateFrom is null or p.dateFrom <= :periodFinalDate) " +
                        "and (p.dateTo is null or p.dateTo >= :periodFinalDate) and claimInvoices = true"
        )
})

public class ServiceGMDBO implements Serializable{

    public static final String FIND_SERVICE_GM_BY_CLIENT_ID = "ServiceGMDBO.FIND_SERVICE_GM_BY_CLIENT_ID";
    public static final String FIND_ALL_CLIENT_WITH_INVOICES_TO_CLAIM_IN_PERIOD = "ServiceGMDBO.FIND_ALL_CLIENT_WITH_INVOICES_TO_CLAIM_IN_PERIOD";

    @Id
    @SequenceGenerator(name = "service_gm_id_seq", sequenceName = "service_gm_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "service_gm_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;

    private Date dateFrom;

    private Date dateTo;

    @Enumerated(EnumType.STRING)
    private ServiceGMEnum service;

    private Boolean claimInvoices;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "clientId")
    private ClientDBO clientDBO;

    public ServiceGMDBO() {
    }

    public ServiceGMDBO(Integer id, Date dateFrom, Date dateTo, ServiceGMEnum service, Boolean claimInvoices, ClientDBO clientDBO) {
        this.id = id;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.service = service;
        this.claimInvoices = claimInvoices;
        this.clientDBO = clientDBO;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public ServiceGMEnum getService() {
        return service;
    }

    public void setService(ServiceGMEnum service) {
        this.service = service;
    }

    public Boolean getClaimInvoices() {
        return claimInvoices;
    }

    public void setClaimInvoices(Boolean claimInvoices) {
        this.claimInvoices = claimInvoices;
    }

    public ClientDBO getClientDBO() {
        return clientDBO;
    }

    public void setClientDBO(ClientDBO clientDBO) {
        this.clientDBO = clientDBO;
    }

    public Boolean isActiveAtDate(LocalDate date){
        if(dateTo == null ||
        dateTo.toLocalDate().isAfter(date)){

            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }
}
