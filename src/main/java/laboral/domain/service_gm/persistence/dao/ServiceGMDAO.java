package laboral.domain.service_gm.persistence.dao;


import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.service_gm.persistence.dbo.ServiceGMDBO;
import laboral.domain.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import javax.persistence.TypedQuery;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class ServiceGMDAO implements GenericDAO<ServiceGMDBO, Integer> {

    private SessionFactory sessionFactory;
    private Session session;

    public ServiceGMDAO() {
    }

    public static class ServiceGMDAOFactory {

        private static ServiceGMDAO clientDAO;

        public static ServiceGMDAO getInstance() {
            if(clientDAO == null) {
                clientDAO = new ServiceGMDAO(HibernateUtil.retrieveGlobalSession());
            }
            return clientDAO;
        }

    }

    public ServiceGMDAO(Session session) {
        this.session = session;
    }

    @Override
    public Integer create(ServiceGMDBO serviceGMDBO) {
        try {
            session.beginTransaction();
            session.saveOrUpdate(serviceGMDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }
        return serviceGMDBO.getId();    }

    @Override
    public ServiceGMDBO findById(Integer integer) {
        return null;
    }

    @Override
    public Integer update(ServiceGMDBO entity) {

        return null;
    }

    @Override
    public Boolean delete(ServiceGMDBO serviceGMDBO) {

        return null;
    }

    @Override
    public List<ServiceGMDBO> findAll() {
        return null;
    }

    public List<ServiceGMDBO> findAllClientGMWithInvoicesToClaimInPeriod(LocalDate periodInitialDate, LocalDate periodFinalDate){

        TypedQuery<ServiceGMDBO> query = session.createNamedQuery(ServiceGMDBO.FIND_ALL_CLIENT_WITH_INVOICES_TO_CLAIM_IN_PERIOD, ServiceGMDBO.class);
        query.setParameter("periodFinalDate", Date.valueOf(periodFinalDate));

        return query.getResultList();

    }

    public List<ServiceGMDBO> findServiceGMByClientId(Integer clientId){

        TypedQuery<ServiceGMDBO> query = session.createNamedQuery(ServiceGMDBO.FIND_SERVICE_GM_BY_CLIENT_ID, ServiceGMDBO.class);
        query.setParameter("clientId", clientId);

        return query.getResultList();
    }
}
