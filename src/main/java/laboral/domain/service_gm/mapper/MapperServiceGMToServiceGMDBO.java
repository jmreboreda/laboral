package laboral.domain.service_gm.mapper;

import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.service_gm.ServiceGM;
import laboral.domain.service_gm.persistence.dbo.ServiceGMDBO;

import java.sql.Date;

public class MapperServiceGMToServiceGMDBO implements GenericMapper<ServiceGM, ServiceGMDBO> {

    @Override
    public ServiceGMDBO map(ServiceGM serviceGM) {

        Date dateTo = serviceGM.getDateTo() == null ? null : Date.valueOf(serviceGM.getDateTo());

        ServiceGMDBO serviceGMDBO = new ServiceGMDBO();
        serviceGMDBO.setId(serviceGM.getId());
        serviceGMDBO.setDateFrom(Date.valueOf(serviceGM.getDateFrom()));
        serviceGMDBO.setDateTo(dateTo);
        serviceGMDBO.setService(serviceGM.getServiceGM());
        serviceGMDBO.setClaimInvoices(serviceGM.getClaimInvoices());

        return serviceGMDBO;
    }
}
