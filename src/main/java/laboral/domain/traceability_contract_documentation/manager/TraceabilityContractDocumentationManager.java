package laboral.domain.traceability_contract_documentation.manager;


import laboral.domain.traceability_contract_documentation.TraceabilityContractDocumentation;
import laboral.domain.traceability_contract_documentation.mapper.MapperTraceabilityContractDocumentationToTraceabilityContractDocumentationDBO;
import laboral.domain.traceability_contract_documentation.persistence.dao.TraceabilityContractDocumentationDAO;

public class TraceabilityContractDocumentationManager {

    private final TraceabilityContractDocumentationDAO traceabilityContractDocumentationDAO = TraceabilityContractDocumentationDAO.TraceabilityContractDocumentationDAOFactory.getInstance();


    public TraceabilityContractDocumentationManager() {
    }

    public Integer saveContractTraceability(TraceabilityContractDocumentation traceabilityContractDocumentation){

        MapperTraceabilityContractDocumentationToTraceabilityContractDocumentationDBO mapperTraceabilityContractDocumentationToTraceabilityContractDocumentationDBO =
                new MapperTraceabilityContractDocumentationToTraceabilityContractDocumentationDBO();

        return traceabilityContractDocumentationDAO.create(mapperTraceabilityContractDocumentationToTraceabilityContractDocumentationDBO.map(traceabilityContractDocumentation));
    }
}
