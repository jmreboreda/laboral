package laboral.domain.traceability_contract_documentation.mapper;

import laboral.domain.contract.work_contract.WorkContractService;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.traceability_contract_documentation.TraceabilityContractDocumentation;
import laboral.domain.traceability_contract_documentation.persistence.dbo.TraceabilityContractDocumentationDBO;

import java.sql.Date;

public class MapperTraceabilityContractDocumentationToTraceabilityContractDocumentationDBO  implements GenericMapper<TraceabilityContractDocumentation, TraceabilityContractDocumentationDBO> {

    @Override
    public TraceabilityContractDocumentationDBO map(TraceabilityContractDocumentation traceabilityContractDocumentation) {

        WorkContractService workContractService = WorkContractService.WorkContractServiceFactory.getInstance();

        WorkContractDBO workContractVariationDBO =
                workContractService.findWorkContractVariationDBOByVariousParameters(traceabilityContractDocumentation.getContractNumber(),
                        traceabilityContractDocumentation.getVariationType().getVariationCode(), traceabilityContractDocumentation.getStartDate());

        Date expectedEndDate = traceabilityContractDocumentation.getExpectedEndDate() == null ? null : Date.valueOf(traceabilityContractDocumentation.getExpectedEndDate());
        Date idcReceptionDate = traceabilityContractDocumentation.getIdcReceptionDate() == null ? null : Date.valueOf(traceabilityContractDocumentation.getIdcReceptionDate());
        Date dateDeliveryContractDocumentationToClient = traceabilityContractDocumentation.getDateDeliveryContractDocumentationToClient() == null ? null : Date.valueOf(traceabilityContractDocumentation.getDateDeliveryContractDocumentationToClient());
        Date endNoticeReceptionDate = traceabilityContractDocumentation.getContractEndNoticeReceptionDate() == null ? null : Date.valueOf(traceabilityContractDocumentation.getContractEndNoticeReceptionDate());

        TraceabilityContractDocumentationDBO traceabilityContractDocumentationDBO = new TraceabilityContractDocumentationDBO();
        traceabilityContractDocumentationDBO.setId(null);
        traceabilityContractDocumentationDBO.setContractNumber(traceabilityContractDocumentation.getContractNumber());
        traceabilityContractDocumentationDBO.setVariationType(traceabilityContractDocumentation.getVariationType().getVariationCode());
        traceabilityContractDocumentationDBO.setContract(workContractVariationDBO);
        traceabilityContractDocumentationDBO.setStartDate(Date.valueOf(traceabilityContractDocumentation.getStartDate()));
        traceabilityContractDocumentationDBO.setExpectedEndDate(expectedEndDate);
        traceabilityContractDocumentationDBO.setIDCReceptionDate(idcReceptionDate);
        traceabilityContractDocumentationDBO.setDateDeliveryContractDocumentationToClient(dateDeliveryContractDocumentationToClient);
        traceabilityContractDocumentationDBO.setContractEndNoticeReceptionDate(endNoticeReceptionDate);

        return traceabilityContractDocumentationDBO;
    }
}
