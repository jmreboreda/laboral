package laboral.domain.services.printer;

public class PrinterConstants {

    public static final String TIME_RECORD_CREATED_IN = "Registro horario creado en:";
    public static final String TIME_RECORD_SEND_TO_PRINTER = "Registro horario enviado a la impresora.";
    public static final String NO_PRINTER_EXISTS_TO_PRINT_THIS_TIME_RECORD = "No existe impresora para imprimir el registro horario con los atributos indicados.";
}
