package laboral.domain.services;

import laboral.domain.email.EmailConstants;
import laboral.domain.email.EmailData;
import laboral.domain.email.dto.EmailDataDTO;
import laboral.domain.services.email.EmailParameters;
import laboral.domain.services.email.EmailSender;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class AgentNotificator {

    public AgentNotificator() {
    }

    public Boolean sendEmailToContractsAgent(EmailDataDTO emailDataDTO) throws AddressException {

        Boolean isSendOk = false;

        EmailData emailData = EmailData.create()
                .withEmailFrom(new InternetAddress(EmailParameters.EMAIL_FROM_TO_SEND_CONTRACT))
                .withEmailTo(new InternetAddress(EmailParameters.EMAIL_TO_SEND_CONTRACT))
                .withEmailDeliveryNotification(new InternetAddress(EmailParameters.EMAIL_DELIVERY_NOTIFICATION))
                .withEmailSubject(EmailConstants.STANDARD_TEXT_GESTORIAGM + emailDataDTO.getVariationTypeText() + emailDataDTO.getEmployee() +
                        " [" + emailDataDTO.getEmployer() + "]")
                .withEmailMessageText(EmailConstants .STANDARD_TEXT_SEND_CONTRACT_DATA + EmailConstants.STANDARD_LOPD_TEXT_SEND_MAIL)
                .withAttachedPath(emailDataDTO.getPath())
                .withAttachedName(emailDataDTO.getFileName())
                .build();
        EmailSender emailSender = new EmailSender();
        try {
            isSendOk = emailSender.sendEmail(emailData);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return isSendOk;
    }
}
