package laboral.domain.employee.manager;

import laboral.component.work_contract.HoursWorkWeekScheduleRetrieve;
import laboral.component.work_contract.WorkContractParameters;
import laboral.component.work_contract.creation.components.WorkContractConstants;
import laboral.domain.contract.work_contract.manager.WorkContractManager;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.contract_json.ContractDaySchedule;
import laboral.domain.contract_type.manager.ContractTypeManager;
import laboral.domain.contract_type.persistence.dbo.WorkContractTypeDBO;
import laboral.domain.time_record.dto.EmployeeDataForTimeRecord;
import laboral.domain.utilities.utilities.Utilities;

import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class EmployeeFinderByEmployerForTimeRecord {

    public List<EmployeeDataForTimeRecord> findEmployeeByEmployerForTimeRecord(Integer employerId, LocalDate startDate, LocalDate endDate) {

        WorkContractManager workContractManager = new WorkContractManager();
        ContractTypeManager contractTypeManager = new ContractTypeManager();

        List<EmployeeDataForTimeRecord> employeeDataForTimeRecordList = new ArrayList<>();

        List<WorkContractDBO> workContractDBOList = workContractManager.findAllWorkContractDBOWithTimeRecordInMonth(startDate, endDate);
        for(WorkContractDBO workContractDBO : workContractDBOList){
            if(workContractDBO.getEmployerDBO().getId().equals(employerId)){
                String dateFrom = workContractDBO.getStartDate() == null ? null : Utilities.formatDateAsStringDate(workContractDBO.getStartDate().toLocalDate());
                String dateTo = workContractDBO.getExpectedEndDate() == null ? null : Utilities.formatDateAsStringDate(workContractDBO.getExpectedEndDate().toLocalDate());
                WorkContractTypeDBO workContractTypeDBO = contractTypeManager.findByWorkContractTypeCode(workContractDBO.getContractTypeCode());
                String workContractDayType = workContractTypeDBO.getIsFullTime() ? WorkContractConstants.FULL_WORKDAY : WorkContractConstants.PARTIAL_WORKDAY;

                String hoursWorkWeek = Utilities.durationToTimeStringConverter(HoursWorkWeekScheduleRetrieve.retrieveHours(workContractDBO.getContractSchedule()));

                String workContractDescription =  workContractTypeDBO.getColloquial() + ", " + workContractTypeDBO.getContractDescription();

                String nieNif = workContractDBO.getEmployeeDBO().getNieNif().formattedAsNIF();

                String quoteAccountCode = workContractDBO.getQuoteAccountCode().getQuoteAccountCodeExtended() == null ? null : workContractDBO.getQuoteAccountCode().getQuoteAccountCodeExtended();

                EmployeeDataForTimeRecord employeeDataForTimeRecord = EmployeeDataForTimeRecord.EmployeeDataForTimeRecordBuilder.create()
                        .withDateFrom(dateFrom)
                        .withDateTo(dateTo)
                        .withEmployeeFullName(workContractDBO.getEmployeeDBO().toAlphabeticalName())
                        .withEmployeeNif(nieNif)
                        .withHoursByWeek(hoursWorkWeek)
                        .withWorkDayType(workContractDayType)
                        .withContractType(workContractDescription)
                        .withQuoteAccountCode(quoteAccountCode)
                        .build();
                employeeDataForTimeRecordList.add(employeeDataForTimeRecord);
            }
        }

        return employeeDataForTimeRecordList;
    }
}
