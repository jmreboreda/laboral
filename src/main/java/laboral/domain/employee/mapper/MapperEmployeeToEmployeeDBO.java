package laboral.domain.employee.mapper;

import laboral.domain.employee.Employee;
import laboral.domain.employee.persistence.dbo.EmployeeDBO;
import laboral.domain.interface_pattern.GenericMapper;

public class MapperEmployeeToEmployeeDBO implements GenericMapper<Employee, EmployeeDBO> {

    @Override
    public EmployeeDBO map(Employee employee) {


        EmployeeDBO employeeDBO = new EmployeeDBO();
        employeeDBO.setId(employee.getId());

        return employeeDBO;
    }
}
