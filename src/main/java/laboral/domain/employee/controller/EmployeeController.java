package laboral.domain.employee.controller;

import laboral.domain.employee.Employee;
import laboral.domain.employee.EmployeeCreationRequest;
import laboral.domain.employee.manager.EmployeeManager;

import java.util.List;

public class EmployeeController {

    public EmployeeController() {
    }

    EmployeeManager employeeManager = new EmployeeManager();

    public Integer createEmployee(EmployeeCreationRequest employeeCreationRequest){

        return employeeManager.createEmployee(employeeCreationRequest);
    }

    public Employee findEmployeeByPersonId(Integer personId){

        return employeeManager.findEmployeeByPersonId(personId);
    }

    public List<Employee> findEmployeeByNamePattern(String pattern){

        return employeeManager.findEmployeeByNamePattern(pattern);
    }

    public Employee findById(Integer id){

        return employeeManager.findById(id);
    }

    public List<Employee> findAll(){

        return employeeManager.findAll();
    }
}
