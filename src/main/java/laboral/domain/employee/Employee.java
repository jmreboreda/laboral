package laboral.domain.employee;

import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.interface_pattern.NaturalPersonality;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.study.StudyLevelType;

import java.time.LocalDate;
import java.util.Objects;

public class Employee implements NaturalPersonality {

    private Integer id;
    private PersonDBO person;

    public Employee() {
    }

    public Employee(Integer id, PersonDBO person) {
        this.id = id;
        this.person = person;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Employee employee = (Employee) o;
//        return id.equals(employee.id);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(id);
//    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private PersonDBO getPerson() {
        return person;
    }

    public void setPerson(PersonDBO person) {
        this.person = person;
    }

    public String toString(){
        String firstSurname = person.getFirstSurname() == null ? person.getFirstSurname() + ", " : person.getFirstSurname() + " ";
        String secondSurname = person.getSecondSurname() == null ? "" : person.getSecondSurname() + ", ";

        return firstSurname
                .concat(secondSurname)
                .concat(person.getName());
    }

    public Integer getPersonId(){
        return getPerson().getId();
    }

    @Override
    public String getFirstSurname() {
        return getPerson().getFirstSurname();
    }

    @Override
    public String getSecondSurname() {
        return getPerson().getSecondSurname();
    }

    @Override
    public String getName() {
        return getPerson().getName();
    }

    @Override
    public String getSocialSecurityAffiliationNumber() {
        return getPerson().getSocialSecurityAffiliationNumber();
    }

    @Override
    public LocalDate getBirthDate() {
        return getPerson().getBirthDate().toLocalDate();
    }

    @Override
    public CivilStatusType getCivilStatus() {
        return getPerson().getCivilStatus();
    }

    @Override
    public StudyLevelType getStudy() {
        return getPerson().getStudy();
    }

    @Override
    public String getNationality() {
        return getPerson().getNationality();
    }

    @Override
    public String toAlphabeticalName() {
        return getPerson().toAlphabeticalName();
    }

    @Override
    public String toNaturalName() {
        return getPerson().toNaturalName();
    }

    @Override
    public NieNif getNieNif() {
        return new NieNif(getPerson().getNieNif());
    }

    public static final class EmployeeBuilder {
        private Integer id;
        private PersonDBO person;

        private EmployeeBuilder() {
        }

        public static EmployeeBuilder create() {
            return new EmployeeBuilder();
        }

        public EmployeeBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public EmployeeBuilder withPerson(PersonDBO person) {
            this.person = person;
            return this;
        }

        public Employee build() {
            Employee employee = new Employee();
            employee.setId(id);
            employee.setPerson(person);
            return employee;
        }
    }
}
