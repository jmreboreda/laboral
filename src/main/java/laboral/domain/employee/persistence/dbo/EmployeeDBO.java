package laboral.domain.employee.persistence.dbo;

import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.interface_pattern.NaturalPersonality;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.study.StudyLevelType;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

@NamedQueries(value = {
        @NamedQuery(
                name = EmployeeDBO.FIND_EMPLOYEE_BY_ID,
                query = "select p from EmployeeDBO p where p.id = :employeeId"
        ),
        @NamedQuery(
                name = EmployeeDBO.FIND_EMPLOYEE_BY_PERSON_ID,
                query = "select p from EmployeeDBO p where p.personDBO.id = :personId"
        ),
        @NamedQuery(
                name = EmployeeDBO.FIND_ALL_EMPLOYEE,
                query = "select p from EmployeeDBO p"
        ),
        @NamedQuery(
                name = EmployeeDBO.FIND_EMPLOYEE_BY_NAME_PATTERN,
                query = "select p from EmployeeDBO as p where concat(lower(p.personDBO.firstSurname), ' ', lower(p.personDBO.secondSurname), ', ', lower(p.personDBO.name)) like :pattern or " +
                        "lower(p.personDBO.legalName) like :pattern"
        )
})

@Entity
@Table(name = "employee")
public class EmployeeDBO implements Serializable, NaturalPersonality {

    public static final String FIND_EMPLOYEE_BY_ID = "EmployeeDBO.FIND_EMPLOYEE_BY_ID";
    public static final String FIND_ALL_EMPLOYEE = "EmployeeDBO.FIND_ALL_EMPLOYEE";
    public static final String FIND_EMPLOYEE_BY_PERSON_ID = "EmployeeDBO.FIND_EMPLOYEE_BY_PERSON_ID";
    public static final String FIND_EMPLOYEE_BY_NAME_PATTERN = "EmployeeDBO.FIND_EMPLOYEE_BY_NAME_PATTERN";

    @Id
    @SequenceGenerator(name = "employee_id_seq", sequenceName = "employee_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;
    @OneToOne(fetch = FetchType.EAGER, cascade= CascadeType.ALL)
    @JoinColumn(name = "personId")
    private PersonDBO personDBO;
    @OneToMany(fetch = FetchType.EAGER)//, cascade = CascadeType.ALL)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "employeeId")
    private Set<WorkContractDBO> contracts;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PersonDBO getPersonDBO() {
        return personDBO;
    }

    public void setPersonDBO(PersonDBO personDBO) {
        this.personDBO = personDBO;
    }

    public Set<WorkContractDBO> getContracts() {
        return contracts;
    }

    public void setContracts(Set<WorkContractDBO> contracts) {
        this.contracts = contracts;
    }

    @Override
    public String getFirstSurname() {
        return getPersonDBO().getFirstSurname();
    }

    @Override
    public String getSecondSurname() {
        return getPersonDBO().getSecondSurname();
    }

    @Override
    public String getName() {
        return getPersonDBO().getName();
    }

    @Override
    public String getSocialSecurityAffiliationNumber() {
        return getSocialSecurityAffiliationNumber();
    }

    @Override
    public LocalDate getBirthDate() {
        return getBirthDate();
    }

    @Override
    public CivilStatusType getCivilStatus() {
        return getCivilStatus();
    }

    @Override
    public StudyLevelType getStudy() {
        return getStudy();
    }

    @Override
    public String getNationality() {
        return getNationality();
    }

    @Override
    public String toAlphabeticalName() {
        return getPersonDBO().toAlphabeticalName();
    }

    @Override
    public String toNaturalName() {
        return getPersonDBO().toNaturalName();
    }

    @Override
    public NieNif getNieNif() {
        return new NieNif(getPersonDBO().getNieNif());
    }
}
