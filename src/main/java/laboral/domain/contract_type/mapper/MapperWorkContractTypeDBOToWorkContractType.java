package laboral.domain.contract_type.mapper;

import laboral.domain.contract_type.WorkContractType;
import laboral.domain.contract_type.persistence.dbo.WorkContractTypeDBO;
import laboral.domain.interface_pattern.GenericMapper;

public class MapperWorkContractTypeDBOToWorkContractType implements GenericMapper<WorkContractTypeDBO, WorkContractType> {

    @Override
    public WorkContractType map(WorkContractTypeDBO workContractTypeDBO) {

        WorkContractType workContractType = new WorkContractType();
        workContractType.setId(workContractTypeDBO.getId());
        workContractType.setContractCode(workContractTypeDBO.getContractTypeCode());
        workContractType.setContractDescription(workContractTypeDBO.getContractDescription());
        workContractType.setColloquial(workContractTypeDBO.getColloquial());
        workContractType.setInitialContract(workContractTypeDBO.getIsInitialContract());
        workContractType.setTemporal(workContractTypeDBO.getIsTemporal());
        workContractType.setUndefined(workContractTypeDBO.getIsUndefined());
        workContractType.setPartialTime(workContractTypeDBO.getIsPartialTime());
        workContractType.setFullTime(workContractTypeDBO.getIsFullTime());
        workContractType.setMenuSelectable(workContractTypeDBO.getIsMenuSelectable());
        workContractType.setDeterminedDuration(workContractTypeDBO.getIsDeterminedDuration());
        workContractType.setSurrogate(workContractTypeDBO.getSurrogate());
        workContractType.setAdminPartnerSimilar(workContractTypeDBO.getAdminPartnerSimilar());
        workContractType.setInterim(workContractTypeDBO.getInterim());

        return workContractType;
    }
}
