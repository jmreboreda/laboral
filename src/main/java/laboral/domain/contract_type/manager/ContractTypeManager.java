package laboral.domain.contract_type.manager;

import laboral.domain.contract_type.WorkContractType;
import laboral.domain.contract_type.mapper.MapperWorkContractTypeDBOToWorkContractType;
import laboral.domain.contract_type.persistence.dao.WorkContractTypeDAO;
import laboral.domain.contract_type.persistence.dbo.WorkContractTypeDBO;

import java.util.ArrayList;
import java.util.List;

public class ContractTypeManager {

    public ContractTypeManager() {
    }

    WorkContractTypeDAO workContractTypeDAO = WorkContractTypeDAO.WorkContractTypeDAOFactory.getInstance();

    public List<WorkContractType> findAll(){

        List<WorkContractType> workContractTypeList = new ArrayList<>();
        MapperWorkContractTypeDBOToWorkContractType mapperWorkContractTypeDBOToWorkContractType = new MapperWorkContractTypeDBOToWorkContractType();

        List<WorkContractTypeDBO> workContractTypeDBOList = workContractTypeDAO.findAll();
        for(WorkContractTypeDBO workContractTypeDBO : workContractTypeDBOList){
            WorkContractType workContractType = mapperWorkContractTypeDBOToWorkContractType.map(workContractTypeDBO);
            workContractTypeList.add(workContractType);
        }

        return workContractTypeList;
    }

    public WorkContractTypeDBO findByWorkContractTypeCode(Integer workContractTypeCode){

        return workContractTypeDAO.findByWorkContractTypeCode(workContractTypeCode);
    }
}
