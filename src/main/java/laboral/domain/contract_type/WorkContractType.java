package laboral.domain.contract_type;

public class WorkContractType {

    private Integer id;
    private Integer contractCode;
    private String contractDescription;
    private String colloquial;
    private Boolean isInitialContract;
    private Boolean isTemporal;
    private Boolean isUndefined;
    private Boolean isPartialTime;
    private Boolean isFullTime;
    private Boolean isMenuSelectable;
    private Boolean isDeterminedDuration;
    private Boolean isSurrogate;
    private Boolean isAdminPartnerSimilar;
    private Boolean isInterim;

    public WorkContractType() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getContractCode() {
        return contractCode;
    }

    public void setContractCode(Integer contractCode) {
        this.contractCode = contractCode;
    }

    public String getContractDescription() {
        return contractDescription;
    }

    public void setContractDescription(String contractDescription) {
        this.contractDescription = contractDescription;
    }

    public String getColloquial() {
        return colloquial;
    }

    public void setColloquial(String colloquial) {
        this.colloquial = colloquial;
    }

    public Boolean getInitialContract() {
        return isInitialContract;
    }

    public void setInitialContract(Boolean initialContract) {
        isInitialContract = initialContract;
    }

    public Boolean getTemporal() {
        return isTemporal;
    }

    public void setTemporal(Boolean temporal) {
        isTemporal = temporal;
    }

    public Boolean getUndefined() {
        return isUndefined;
    }

    public void setUndefined(Boolean undefined) {
        isUndefined = undefined;
    }

    public Boolean getPartialTime() {
        return isPartialTime;
    }

    public void setPartialTime(Boolean partialTime) {
        isPartialTime = partialTime;
    }

    public Boolean getFullTime() {
        return isFullTime;
    }

    public void setFullTime(Boolean fullTime) {
        isFullTime = fullTime;
    }

    public Boolean getMenuSelectable() {
        return isMenuSelectable;
    }

    public void setMenuSelectable(Boolean menuSelectable) {
        isMenuSelectable = menuSelectable;
    }

    public Boolean getDeterminedDuration() {
        return isDeterminedDuration;
    }

    public void setDeterminedDuration(Boolean determinedDuration) {
        isDeterminedDuration = determinedDuration;
    }

    public Boolean getSurrogate() {
        return isSurrogate;
    }

    public void setSurrogate(Boolean surrogate) {
        isSurrogate = surrogate;
    }

    public Boolean getAdminPartnerSimilar() {
        return isAdminPartnerSimilar;
    }

    public void setAdminPartnerSimilar(Boolean adminPartnerSimilar) {
        isAdminPartnerSimilar = adminPartnerSimilar;
    }

    public Boolean getInterim() {
        return isInterim;
    }

    public void setInterim(Boolean interim) {
        isInterim = interim;
    }

    public String toString(){
            return  "[" + getContractCode() + "] " + getColloquial() + " [ " + getContractDescription() + " ] ";
    }

    public String getFullPartialTimeText(){
        if(getFullTime()){
            return "A tiempo completo";
        }

        return "A tiempo parcial";
    }
}
