package laboral.domain.contract_type.persistence.dbo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "contract_type")
@NamedQueries(value = {
        @NamedQuery(
                name = WorkContractTypeDBO.FIND_ALL_SELECTABLE_CONTRACT_TYPES,
                query = "select p from WorkContractTypeDBO p where isMenuSelectable = true and isInitialContract = true order by contractDescription"
        ),
        @NamedQuery(
                name = WorkContractTypeDBO.FIND_CONTRACT_TYPE_BY_ID,
                query = "select p from WorkContractTypeDBO p where p.id = :contractTypeId"
        ),
        @NamedQuery(
                name = WorkContractTypeDBO.FIND_WORK_CONTRACT_TYPE_BY_WORK_CONTRACT_TYPE_CODE,
                query = "select p from WorkContractTypeDBO p where p.contractTypeCode = :workContractTypeParameter"
        )
})

public class WorkContractTypeDBO implements Serializable{

    public static final String FIND_ALL_SELECTABLE_CONTRACT_TYPES = "WorkContractTypeDBO.FIND_ALL_SELECTABLE_CONTRACT_TYPES";
    public static final String FIND_CONTRACT_TYPE_BY_ID = "WorkContractTypeDBO.FIND_CONTRACT_TYPE_BY_ID";
    public static final String FIND_WORK_CONTRACT_TYPE_BY_WORK_CONTRACT_TYPE_CODE = "WorkContractTypeDBO.FIND_WORK_CONTRACT_TYPE_BY_WORK_CONTRACT_TYPE_CODE";

    @Id
    @SequenceGenerator(name = "contract_type_id_seq", sequenceName = "contract_type_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contract_type_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;
    private Integer contractTypeCode;
    private String contractDescription;
    private String colloquial;
    private Boolean isInitialContract;
    private Boolean isTemporal;
    private Boolean isUndefined;
    private Boolean isPartialTime;
    private Boolean isFullTime;
    private Boolean isMenuSelectable;
    private Boolean isDeterminedDuration;
    private Boolean isSurrogate;
    private Boolean isAdminPartnerSimilar;
    private Boolean isInterim;

    public WorkContractTypeDBO() {
    }

    public WorkContractTypeDBO(Integer id, Integer contractTypeCode, String contractDescription, String colloquial, Boolean isInitialContract, Boolean isTemporal, Boolean isUndefined, Boolean isPartialTime, Boolean isFullTime, Boolean isMenuSelectable, Boolean isDeterminedDuration, Boolean isSurrogate, Boolean isAdminPartnerSimilar, Boolean isInterim) {
        this.id = id;
        this.contractTypeCode = contractTypeCode;
        this.contractDescription = contractDescription;
        this.colloquial = colloquial;
        this.isInitialContract = isInitialContract;
        this.isTemporal = isTemporal;
        this.isUndefined = isUndefined;
        this.isPartialTime = isPartialTime;
        this.isFullTime = isFullTime;
        this.isMenuSelectable = isMenuSelectable;
        this.isDeterminedDuration = isDeterminedDuration;
        this.isSurrogate = isSurrogate;
        this.isAdminPartnerSimilar = isAdminPartnerSimilar;
        this.isInterim = isInterim;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getContractTypeCode() {
        return contractTypeCode;
    }

    public void setContractTypeCode(Integer contractTypeCode) {
        this.contractTypeCode = contractTypeCode;
    }

    public String getContractDescription() {
        return contractDescription;
    }

    public void setContractDescription(String contractDescription) {
        this.contractDescription = contractDescription;
    }

    public String getColloquial() {
        return colloquial;
    }

    public void setColloquial(String colloquial) {
        this.colloquial = colloquial;
    }

    public Boolean getIsInitialContract() {
        return isInitialContract;
    }

    public void setIsInitialContract(Boolean isInitialContract) {
        this.isInitialContract = isInitialContract;
    }

    public Boolean getIsTemporal() {
        return isTemporal;
    }

    public void setIsTemporal(Boolean isTemporal) {
        this.isTemporal = isTemporal;
    }

      public Boolean getIsUndefined() {
        return isUndefined;
    }

    public void setIsUndefined(Boolean isundefined) {
        this.isUndefined = isundefined;
    }

    public Boolean getIsPartialTime() {
        return isPartialTime;
    }

    public void setIsPartialTime(Boolean isPartialTime) {
        this.isPartialTime = isPartialTime;
    }

    public Boolean getIsFullTime() {
        return isFullTime;
    }

    public void setIsFullTime(Boolean isFullTime) {
        this.isFullTime = isFullTime;
    }

    public Boolean getIsMenuSelectable() {
        return isMenuSelectable;
    }

    public void setIsMenuSelectable(Boolean isMenuSelectable) {
        this.isMenuSelectable = isMenuSelectable;
    }

    public Boolean getIsDeterminedDuration() {
        return isDeterminedDuration;
    }

    public void setIsDeterminedDuration(Boolean isDeterminedDuration) {
        this.isDeterminedDuration = isDeterminedDuration;
    }

    public Boolean getSurrogate() {
        return isSurrogate;
    }

    public void setSurrogated(Boolean surrogate) {
        isSurrogate = surrogate;
    }

    public Boolean getAdminPartnerSimilar() {
        return isAdminPartnerSimilar;
    }

    public void setAdminPartnerSimilar(Boolean adminPartnerSimilar) {
        isAdminPartnerSimilar = adminPartnerSimilar;
    }

    public Boolean getInterim() {
        return isInterim;
    }

    public void setInterim(Boolean interim) {
        isInterim = interim;
    }
}
