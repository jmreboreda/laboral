package laboral.domain.contract_type;

import laboral.domain.contract_type.manager.ContractTypeManager;
import laboral.domain.contract_type.persistence.dbo.WorkContractTypeDBO;

public class WorkContractTypeService {

    private final ContractTypeManager contractTypeManager = new ContractTypeManager();

    private WorkContractTypeService() {
    }

    public static class WorkContractTypeServiceFactory {

        private static WorkContractTypeService workContractTypeService;

        public static WorkContractTypeService getInstance() {
            if(workContractTypeService == null) {
                workContractTypeService = new WorkContractTypeService();
            }
            return workContractTypeService;
        }
    }

    public WorkContractTypeDBO findByWorkContractTypeCode(Integer workContractTypeCode){

        return contractTypeManager.findByWorkContractTypeCode(workContractTypeCode);
    }
}
