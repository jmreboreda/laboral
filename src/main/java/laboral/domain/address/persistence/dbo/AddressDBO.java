package laboral.domain.address.persistence.dbo;

import laboral.domain.address.Province;
import laboral.domain.address.StreetType;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "address")
@NamedQueries(value = {
        @NamedQuery(
                name = AddressDBO.FIND_ADDRESS_BY_ID,
                query = "select p from AddressDBO p where p.id = :addressId"
        )
})

public class AddressDBO implements Serializable{

    public static final String FIND_ADDRESS_BY_ID = "AddressDBO.FIND_ADDRESS_BY_ID";

    @Id
    @SequenceGenerator(name = "address_id_seq", sequenceName = "address_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private StreetType streetType;

    private String streetName;

    private String streetExtended;

    private String location;

    private String municipality;
    @Enumerated(EnumType.STRING)

    private Province province;

    private String postalCode;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="personId")
    private PersonDBO personDBO;

    @OneToOne(mappedBy = "addressDBO", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private WorkCenterDBO workCenterDBO;

    private Boolean defaultAddress;

    public AddressDBO() {
    }

    public AddressDBO(Integer id,
                      StreetType streetType,
                      String streetName,
                      String streetExtended,
                      String location,
                      String municipality,
                      Province province,
                      String postalCode,
                      Boolean defaultAddress) {
        this.id = id;
        this.streetType = streetType;
        this.streetName = streetName;
        this.streetExtended = streetExtended;
        this.location = location;
        this.municipality = municipality;
        this.province = province;
        this.postalCode = postalCode;
        this.defaultAddress = defaultAddress;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StreetType getStreetType() {
        return streetType;
    }

    public void setStreetType(StreetType streetType) {
        this.streetType = streetType;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetExtended() {
        return streetExtended;
    }

    public void setStreetExtended(String streetExtended) {
        this.streetExtended = streetExtended;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public PersonDBO getPersonDBO() {
        return personDBO;
    }

    public void setPersonDBO(PersonDBO personDBO) {
        this.personDBO = personDBO;
    }

    public WorkCenterDBO getWorkCenterDBO() {
        return workCenterDBO;
    }

    public void setWorkCenterDBO(WorkCenterDBO workCenterDBO) {
        this.workCenterDBO = workCenterDBO;
    }

    public Boolean getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(Boolean defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public String toString(){

        String streetType = getStreetType().getStreetTypeDescription() != null ? getStreetType().getStreetTypeDescription() : "";
        String streetName = getStreetName() != null ? getStreetName() : "";
        String streetExtended = getStreetExtended() != null ? getStreetExtended() : "";
        String location = getLocation() != null ? getLocation() : "";
        String postalCode = getPostalCode() != null ? getPostalCode() : "";
        String municipality = getMunicipality() != null ? getMunicipality() : "";
        String province = getProvince().getName() != null ? getProvince().getName() : "";

        return streetType.concat(" ").concat(streetName).concat(" ").concat(streetExtended).concat(" ")
                .concat(location).concat(" ").concat(postalCode).concat(" ").concat(municipality).concat(" ")
                .concat(province);
    }

    public static final class AddressDBOBuilder {
        private Integer id;
        private StreetType streetType;
        private String streetName;
        private String streetExtended;
        private String location;
        private String municipality;
        private Province province;
        private String postalCode;
        private Boolean defaultAddress;

        private AddressDBOBuilder() {
        }

        public static AddressDBOBuilder create() {
            return new AddressDBOBuilder();
        }

        public AddressDBOBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public AddressDBOBuilder withStreetType(StreetType streetType) {
            this.streetType = streetType;
            return this;
        }

        public AddressDBOBuilder withStreetName(String streetName) {
            this.streetName = streetName;
            return this;
        }

        public AddressDBOBuilder withStreetExtended(String streetExtended) {
            this.streetExtended = streetExtended;
            return this;
        }

        public AddressDBOBuilder withLocation(String location) {
            this.location = location;
            return this;
        }

        public AddressDBOBuilder withMunicipality(String municipality) {
            this.municipality = municipality;
            return this;
        }

        public AddressDBOBuilder withProvince(Province province) {
            this.province = province;
            return this;
        }

        public AddressDBOBuilder withPostalCode(String postalCode) {
            this.postalCode = postalCode;
            return this;
        }

        public AddressDBOBuilder withDefaultAddress(Boolean defaultAddress) {
            this.defaultAddress = defaultAddress;
            return this;
        }

        public AddressDBO build() {
            return new AddressDBO(id, streetType, streetName, streetExtended, location, municipality, province, postalCode, defaultAddress);
        }
    }
}
