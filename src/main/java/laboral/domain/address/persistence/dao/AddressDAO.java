package laboral.domain.address.persistence.dao;


import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.service_gm.persistence.dbo.ServiceGMDBO;
import laboral.domain.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.TypedQuery;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class AddressDAO implements GenericDAO<AddressDBO, Integer> {

    private SessionFactory sessionFactory;
    private Session session;

    public AddressDAO() {
    }

    public static class AddressDAOFactory {

        private static AddressDAO addressDAO;

        public static AddressDAO getInstance() {
            if(addressDAO == null) {
                addressDAO = new AddressDAO(HibernateUtil.retrieveGlobalSession());
            }
            return addressDAO;
        }

    }

    public AddressDAO(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    @Override
    public Integer create(AddressDBO addressDBO) {
        try {
            session.beginTransaction();
            session.saveOrUpdate(addressDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }
        return addressDBO.getId();    }

    @Override
    public AddressDBO findById(Integer addressId) {
        TypedQuery<AddressDBO> query = session.createNamedQuery(AddressDBO.FIND_ADDRESS_BY_ID, AddressDBO.class);
        query.setParameter("addressId", addressId);

        return query.getSingleResult();    }

    @Override
    public Integer update(AddressDBO addressDBO) {
        try {
            session.beginTransaction();
            session.merge(addressDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }

        return addressDBO.getId();
    }

    @Override
    public Boolean delete(AddressDBO addressDBO) {
        try {
            session.beginTransaction();
            session.delete(addressDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }

        return Boolean.TRUE;
    }

    @Override
    public List<AddressDBO> findAll() {
        return null;
    }

    public List<ServiceGMDBO> findAllClientGMWithInvoicesToClaimInPeriod(LocalDate periodInitialDate, LocalDate periodFinalDate){

        TypedQuery<ServiceGMDBO> query = session.createNamedQuery(ServiceGMDBO.FIND_ALL_CLIENT_WITH_INVOICES_TO_CLAIM_IN_PERIOD, ServiceGMDBO.class);
        query.setParameter("periodFinalDate", Date.valueOf(periodFinalDate));

        return query.getResultList();

    }

    public ServiceGMDBO findServiceGMByClientId(Integer clientId){

        TypedQuery<ServiceGMDBO> query = session.createNamedQuery(ServiceGMDBO.FIND_SERVICE_GM_BY_CLIENT_ID, ServiceGMDBO.class);
        query.setParameter("clientId", clientId);

        return query.getSingleResult();
    }
}
