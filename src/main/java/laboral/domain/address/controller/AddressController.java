package laboral.domain.address.controller;

import laboral.domain.address.Address;
import laboral.domain.address.manager.AddressManager;

public class AddressController {

    AddressManager addressManager = new AddressManager();

    public Integer createAddress(Address address, Integer personId){

        return addressManager.createAddress(address, personId);
    }

    public Boolean deleteAddress(Address address){
        return addressManager.deleteAddress(address);
    }

    public Address findAddressById(Integer id){

        return addressManager.findAddressById(id);

    }
}
