package laboral.domain.address;

public enum Province {

    ZZZZZ("", "00"),
    ALAVA("Álava","01"),
    ALBACETE("Albacete","02"),
    ALICANTE("Alicante","03"),
    ALMERIA ("Almería","04"),
    AVILA ("Ávila","05"),
    BADAJOZ ("Badajoz","06"),
    BALEARES ("Baleares","07"),
    BARCELONA ("Barcelona","08"),
    BURGOS ("Burgos","09"),
    CACERES ("Cáceres","10"),
    CADIZ ("Cádiz","11"),
    CASTELLON ("Castellón","12"),
    CIUDAD_REAL ("Ciudad Real","13"),
    CORDOBA ("Córdoba","14"),
    CORUNA ("Coruña","15"),
    CUENCA ("Cuenca","16"),
    GIRONA ("Girona","17"),
    GRANADA ("Granada","18"),
    GUADALAJARA ("Guadalajara","19"),
    GUIPUZCOA ("Guipúzcoa","20"),
    HUELVA ("Huelva","21"),
    HUESCA ("Huesca","22"),
    JAEN ("Jaén","23"),
    LEON ("León","24"),
    LLEIDA ("Lleida","25"),
    RIOJA_LA ("La Rioja","26"),
    LUGO ("Lugo","27"),
    MADRID ("Madrid","28"),
    MALAGA ("Málaga","29"),
    MURCIA ("Murcia","30"),
    NAVARRA ("Navarra","31"),
    OURENSE ("Ourense","32"),
    ASTURIAS ("Asturias","33"),
    PALENCIA ("Palencia","34"),
    PALMAS_LAS ("Las Palmas","35"),
    PONTEVEDRA ("Pontevedra","36"),
    SALAMANCA ("Salamanca","37"),
    TENERIFE ("Santa Cruz de Tenerife","38"),
    CANTABRIA_SANTANDER ("Cantabria (Santander)","39"),
    SEGOVIA ("Segovia","40"),
    SEVILLA ("Sevilla","41"),
    SORIA ("Soria","42"),
    TARRAGONA ("Tarragona","43"),
    TERUEL ("Teruel","44"),
    TOLEDO ("Toledo","45"),
    VALENCIA ("Valencia","46"),
    VALLADOLID ("Valladolid","47"),
    VIZCAYA_BILBAO ("Vizcaya (Bilbao)","48"),
    ZAMORA ("Zamora","49"),
    ZARAGOZA ("Zaragoza","50"),
    CEUTA ("Ceuta","51"),
    MELILLA ("Melilla","52");


    private String name;
    private String code;

    Province(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String toString(){
        return getName();
    }
}
