package laboral.domain.person_type;

public enum PersonType {

    NATURAL_PERSON("Persona física", 10),
    LEGAL_PERSON("Persona jurídica", 20),
    ENTITY_WITHOUT_LEGAL_PERSONALITY("Entidad sin personalidad jurídica", 30);

    private String personTypeDescription;
    private Integer personTypeCode;

    PersonType(String personTypeDescription, Integer personTypeCode) {
        this.personTypeDescription = personTypeDescription;
        this.personTypeCode = personTypeCode;
    }

    public String getPersonTypeDescription() {
        return personTypeDescription;
    }

    public Integer getPersonTypeCode() {
        return personTypeCode;
    }

    public String toString(){
        return getPersonTypeDescription();
    }
}
