package laboral.domain.contract_json;

import laboral.domain.utilities.utilities.Utilities;

import java.time.Duration;
import java.util.Map;
import java.util.Objects;

public class ContractSchedule {

    private Map<String, ContractDaySchedule> schedule;

    public ContractSchedule() {
    }

    public ContractSchedule(Map<String, ContractDaySchedule> schedule) {
        this.schedule = schedule;
    }


    public Map<String, ContractDaySchedule> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, ContractDaySchedule> schedule) {
        this.schedule = schedule;
    }


    public static Duration retrieveHoursWorkWeek(ContractSchedule contractSchedule){
        Duration schedule1TotalHours = Duration.ZERO;
        Duration schedule2TotalHours = Duration.ZERO;
        Duration schedule3TotalHours = Duration.ZERO;
        Duration schedule4TotalHours = Duration.ZERO;

        Duration hoursWorkWeek = Duration.ZERO;
        Map<String, ContractDaySchedule> schedule = contractSchedule.getSchedule();
        for (Map.Entry<String, ContractDaySchedule> entry : schedule.entrySet()) {
            if(entry.getKey() == null) continue;
            if(entry.getKey().contains("Schedule1")) schedule1TotalHours = schedule1TotalHours.plus(Objects.requireNonNull((Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours()))));
            if(entry.getKey().contains("Schedule2")) schedule2TotalHours = schedule2TotalHours.plus(Objects.requireNonNull((Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours()))));
            if(entry.getKey().contains("Schedule3")) schedule3TotalHours = schedule3TotalHours.plus(Objects.requireNonNull((Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours()))));
            if(entry.getKey().contains("Schedule4")) schedule4TotalHours = schedule4TotalHours.plus(Objects.requireNonNull((Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours()))));
            hoursWorkWeek = hoursWorkWeek.plus(Objects.requireNonNull((Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours()))));
        }

        Integer schedulesWithDuration = 0;
        if(schedule1TotalHours != Duration.ZERO)
            schedulesWithDuration++;
        if(schedule2TotalHours != Duration.ZERO)
            schedulesWithDuration++;
        if(schedule3TotalHours != Duration.ZERO)
            schedulesWithDuration++;
        if(schedule4TotalHours != Duration.ZERO)
            schedulesWithDuration++;

        Long averageWeeklyHours = schedulesWithDuration == 0 ? 0 : (schedule1TotalHours.toSeconds() + schedule2TotalHours.toSeconds() +
                schedule3TotalHours.toSeconds() + schedule4TotalHours.toSeconds()) / schedulesWithDuration;

        return Duration.ofSeconds((averageWeeklyHours));
    }
}
