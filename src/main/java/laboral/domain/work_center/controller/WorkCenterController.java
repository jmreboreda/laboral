package laboral.domain.work_center.controller;

import laboral.domain.work_center.WorkCenter;
import laboral.domain.work_center.manager.WorkCenterManager;

public class WorkCenterController {

    WorkCenterManager workCenterManager = new WorkCenterManager();

    public Boolean deleteWorkCenter(WorkCenter workCenter){
        return workCenterManager.deleteWorkCenter(workCenter);
    }

    public WorkCenter findWorkCenterById(Integer id){

        return workCenterManager.findWorkCenterById(id);

    }
}
