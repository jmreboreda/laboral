package laboral.domain.work_center.persistence.dbo;

import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.client.persistence.dbo.ClientDBO;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "work_center")
@NamedQueries(value = {
        @NamedQuery(
                name = WorkCenterDBO.FIND_WORK_CENTER_BY_ID,
                query = "select p from WorkCenterDBO p where p.id = :workCenterId"
        ),
        @NamedQuery(
                name = WorkCenterDBO.FIND_WORK_CENTER_BY_ADDRESS_ID,
                query = "select p from WorkCenterDBO p where p.addressDBO.id = :addressId and p.clientDBO.id = :clientId"
        ),
        @NamedQuery(
                name = WorkCenterDBO.FIND_WORK_CENTER_BY_CLIENT_ID,
                query = "select p from WorkCenterDBO p where p.clientDBO.id = :clientId"
        )
    }
)

public class WorkCenterDBO implements Serializable {

    public static final String FIND_WORK_CENTER_BY_ID = "WorkCenterDBO.FIND_WORK_CENTER_BY_ID";
    public static final String FIND_WORK_CENTER_BY_ADDRESS_ID = "WorkCenterDBO.FIND_WORK_CENTER_BY_ADDRESS_ID";
    public static final String FIND_WORK_CENTER_BY_CLIENT_ID = "WorkCenterDBO.FIND_WORK_CENTER_BY_CLIENT_ID";

    @Id
    @SequenceGenerator(name = "work_center_id_seq", sequenceName = "work_center_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "work_center_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "addressId", nullable = false, updatable = false)
    private AddressDBO addressDBO;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="clientId")
    private ClientDBO clientDBO;

    @OneToMany(fetch = FetchType.EAGER)//, cascade = CascadeType.ALL)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name="id")
    private Set<WorkContractDBO> contracts;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public AddressDBO getAddressDBO() {
        return addressDBO;
    }

    public void setAddressDBO(AddressDBO addressDBO) {
        this.addressDBO = addressDBO;
    }

    public ClientDBO getClientDBO() {
        return clientDBO;
    }

    public void setClientDBO(ClientDBO clientDBO) {
        this.clientDBO = clientDBO;
    }

    public Set<WorkContractDBO> getContracts() {
        return contracts;
    }

    public void setContracts(Set<WorkContractDBO> contracts) {
        this.contracts = contracts;
    }
}
