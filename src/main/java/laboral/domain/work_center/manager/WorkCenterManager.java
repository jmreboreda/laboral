package laboral.domain.work_center.manager;

import laboral.domain.work_center.WorkCenter;
import laboral.domain.work_center.mapper.MapperWorkCenterDBOToPersistedWorkCenter;
import laboral.domain.work_center.persistence.dao.WorkCenterDAO;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;

public class WorkCenterManager {

    WorkCenterDAO workCenterDAO = WorkCenterDAO.WorkCenterDAOFactory.getInstance();

    public Boolean deleteWorkCenter(WorkCenter workCenter){
        WorkCenterDBO persistedWorkCenterDBO = workCenterDAO.getSession().load(WorkCenterDBO.class, workCenter.getId());

        return workCenterDAO.delete(persistedWorkCenterDBO);
    }

    public WorkCenter findWorkCenterById(Integer id){
        MapperWorkCenterDBOToPersistedWorkCenter mapperWorkCenterDBOToPersistedWorkCenter = new MapperWorkCenterDBOToPersistedWorkCenter();

        return mapperWorkCenterDBOToPersistedWorkCenter.map(workCenterDAO.findById(id));
    }
}
