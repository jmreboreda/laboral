package laboral.domain.work_center.mapper;

import laboral.domain.address.mapper.MapperAddressDBOToAddress;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.work_center.WorkCenter;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;

public class MapperWorkCenterDBOToWorkCenter implements GenericMapper<WorkCenterDBO, WorkCenter> {

    @Override
    public WorkCenter map(WorkCenterDBO workCenterDBO) {

        MapperAddressDBOToAddress mapperAddressDBOToAddress = new MapperAddressDBOToAddress();

        return new WorkCenter(workCenterDBO.getId(), mapperAddressDBOToAddress.map(workCenterDBO.getAddressDBO()));
    }
}
