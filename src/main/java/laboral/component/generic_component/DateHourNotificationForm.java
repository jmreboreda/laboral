package laboral.component.generic_component;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.DatePicker;
import javafx.scene.layout.AnchorPane;
import javafx.util.converter.LocalDateStringConverter;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;

import java.time.format.DateTimeFormatter;

public class DateHourNotificationForm extends AnchorPane {

    private static final String DATE_HOUR_NOTIFICATION_FXML = "/fxml/generic/date_hour_notification.fxml";

    Parent parent;

    @FXML
    private DatePicker notificationDate;
    @FXML
    private TimeInput24HoursClock notificationHour;


    public DateHourNotificationForm() {
        this.parent = ViewLoader.load(this, DATE_HOUR_NOTIFICATION_FXML);
    }

    public void initialize(){

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());
        notificationDate.setConverter(new LocalDateStringConverter(dateFormatter, null));
    }

    public void interfaceInitialState(){
        notificationDate.getEditor().clear();
        notificationHour.setText("");
        notificationDate.setMouseTransparent(false);
        notificationHour.setMouseTransparent(false);
    }

    public DatePicker getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(DatePicker notificationDate) {
        this.notificationDate = notificationDate;
    }

    public TimeInput24HoursClock getNotificationHour() {
        return notificationHour;
    }

    public void setNotificationHour(TimeInput24HoursClock notificationHour) {
        this.notificationHour = notificationHour;
    }
}
