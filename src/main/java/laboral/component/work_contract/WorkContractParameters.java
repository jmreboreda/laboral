package laboral.component.work_contract;

import java.time.Duration;

public class WorkContractParameters {

    public static final Duration LEGAL_MAXIMUM_HOURS_OF_WORK_PER_WEEK = Duration.parse("PT40H"); // 40 hours of work per week.
    public static final Long MINIMUM_NUMBER_DAYS_CONTRACT_DURATION_TO_SEND_NOTICE_END_CONTRACT = 15L;
    public static final Integer MAXIMUM_VALUE_MINUTES_IN_HOUR = 59;    public static final String THERE_ARE_INCOHERENT_DATES_IN_CONTRACT = "Las fechas de inicio y finalización del contrato no son coherentes.";

}
