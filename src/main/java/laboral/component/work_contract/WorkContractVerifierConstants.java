package laboral.component.work_contract;

public class WorkContractVerifierConstants {

    public static final String EMPLOYER_IS_NOT_SELECTED = "Pestaña \"Partes\"\nNo se ha seleccionado el cliente\\empleador.";
    public static final String WORK_CENTER_IS_NOT_SELECTED = "No se ha seleccionado un centro de trabajo";
    public static final String EMPLOYEE_IS_NOT_SELECTED = "Pestaña \"Partes\"\nNo se ha seleccionado el empleado.";
    public static final String QUESTION_NULL_CCC_CODE_IS_CORRECT = "Pestaña \"Partes\"\nEl CCC no está disponible o no ha sido seleccionado. ¿Es correcto?";
    public static final String HOUR_NOTIFICATION_IS_NOT_ESTABLISHED = "Pestaña \"Contrato\"\nRevise los datos de notificación.\nHora no establecida.";
    public static final String DIFFERENT_NUMBER_HOURS_CONTRACT_DATA_AND_CONTRACT_SCHEDULE = "Pestañas \"Contrato\" y \"Horario\"\nLos números de horas de trabajo por semana son distintos " +
            "en la pestaña \"Contrato\" y en la pestaña \"Horario\".";
    public static final String DAYS_TO_WORK_ARE_NOT_SELECTED = "Pestaña \"Contrato\"\nNo se han indicado los días de trabajo de la semana.";
    public static final String LABOR_CATEGORY_IS_NOT_ESTABLISHED = "Pestaña \"Contrato\"\nNo se ha establecido la categoría laboral.";
    public static final String WORKING_DAYS_ARE_DIFFERENT_IN_CONTRACTDATA_AND_CONTRACTSCHEDULE = "Los días de semana a trabajar en la pestaña \"Contrato\" " +
            "son distintos de los de la pestaña \"Horario\".";
    public static final String QUESTION_SAVE_NEW_CONTRACT = "El contrato no tiene errores ni datos pendientes.\n¿Desea guardar el contrato en la base de datos?";
    public static final String INVALID_CONTRACT_DURATION = "El número de días de duración del contrato es erróneo.";
    public static final String CONTRACT_TYPE_NOT_SELECTED = "No se ha seleccionado el tipo de contrato.";
    public static final String PARTIAL_WORK_DAY_WITHOUT_HOURS_WORK_WEEK = "No se han establecido las horas de trabajo por semana para la jornada parcial.";
    public static final String DURATION_CONTRACT_SELECTED_DOES_NOT_MATCH = "Hay discrepancia entre los datos seleccionados de duración del contrato.";
    public static final String WORKDAY_TYPE_SELECTED_DOES_NOT_MATCH = "Hay discrepancia entre los datos seleccionados de duración de la jornada de trabajo.";
    public static final String NO_SCHEDULE_HAS_BEEN_ESTABLISHED_FOR_THE_PART_TIME = "No se ha establecido ningún horario para la jornada parcial.";
    public static final String HOURS_SCHEDULE_NOT_COINCIDE_WITH_ESTABLISHED_PART_TIME  = "El promedio de las horas en el(los) horario(s) establecido(s) no coincide con la duración de la jornada parcial declarada.\n";

}
