package laboral.component.work_contract;

import laboral.component.client_management.client_modification.generic_components.DaysOfWeekSelector;
import laboral.domain.contract_json.ContractSchedule;
import laboral.domain.contract_type.WorkContractType;
import laboral.domain.employee.Employee;
import laboral.domain.employer.Employer;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.utilities.utilities.Utilities;
import laboral.domain.variation_type.VariationType;
import laboral.domain.work_center.WorkCenter;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;

public class ContractSituationEntryDataContainer {

    private final Employer employer;
    private final Employee employee;
    private final WorkCenter workCenter;
    private final QuoteAccountCode quoteAccountCode;
    private final LocalDate clientDateNotification;
    private final String clientHourNotification;
    private final WorkContractType workContractType;
    private final VariationType variationType;
    private final Boolean undefinedDuration;
    private final Boolean temporalDuration;
    private final LocalDate dateFrom;
    private final LocalDate dateTo;
    private final String durationDays;
    private final Boolean fullTime;
    private final Boolean partialTime;
    private final String weeklyWorkHours;
    private final DaysOfWeekSelector daysOfWeekSelector;
    private final String laborCategory;
    private final ContractSchedule contractSchedule;
    private final String publicNotes;
    private final String privateNotes;
    private final List<WorkContractScheduleDay> scheduleOneList;
    private final List<WorkContractScheduleDay> scheduleTwoList;
    private final List<WorkContractScheduleDay> scheduleThreeList;
    private final List<WorkContractScheduleDay> scheduleFourList;
    private final Set<DayOfWeek> tableColumnDayOfWeekData;


    public ContractSituationEntryDataContainer(Employer employer,
                                               Employee employee,
                                               WorkCenter workCenter,
                                               QuoteAccountCode quoteAccountCode,
                                               LocalDate clientDateNotification,
                                               String clientHourNotification,
                                               WorkContractType workContractType,
                                               VariationType variationType,
                                               Boolean undefinedDuration,
                                               Boolean temporalDuration,
                                               LocalDate dateFrom,
                                               LocalDate dateTo,
                                               String durationDays,
                                               Boolean fullTime,
                                               Boolean partialTime,
                                               String weeklyWorkHours,
                                               DaysOfWeekSelector daysOfWeekSelector,
                                               String laborCategory,
                                               ContractSchedule contractSchedule,
                                               String publicNotes,
                                               String privateNotes,
                                               List<WorkContractScheduleDay> scheduleOneList,
                                               List<WorkContractScheduleDay> scheduleTwoList,
                                               List<WorkContractScheduleDay> scheduleThreeList,
                                               List<WorkContractScheduleDay> scheduleFourList,
                                               Set<DayOfWeek> tableColumnDayOfWeekData
    ) {
        this.employer = employer;
        this.employee = employee;
        this.workCenter = workCenter;
        this.quoteAccountCode = quoteAccountCode;
        this.clientDateNotification = clientDateNotification;
        this.clientHourNotification = clientHourNotification;
        this.workContractType = workContractType;
        this.variationType = variationType;
        this.undefinedDuration = undefinedDuration;
        this.temporalDuration = temporalDuration;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.durationDays = durationDays;
        this.fullTime = fullTime;
        this.partialTime = partialTime;
        this.weeklyWorkHours = weeklyWorkHours;
        this.daysOfWeekSelector = daysOfWeekSelector;
        this.laborCategory = laborCategory;
        this.contractSchedule = contractSchedule;
        this.publicNotes = publicNotes;
        this.privateNotes = privateNotes;
        this.scheduleOneList = scheduleOneList;
        this.scheduleTwoList = scheduleTwoList;
        this.scheduleThreeList = scheduleThreeList;
        this.scheduleFourList = scheduleFourList;
        this.tableColumnDayOfWeekData = tableColumnDayOfWeekData;
    }

    public Employer getEmployer() {
        return employer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public WorkCenter getWorkCenter() {
        return workCenter;
    }

    public QuoteAccountCode getQuoteAccountCode() {
        return quoteAccountCode;
    }

    public LocalDate getClientDateNotification() {
        return clientDateNotification;
    }

    public String getClientHourNotification() {
        return clientHourNotification;
    }

    public WorkContractType getWorkContractType() {
        return workContractType;
    }

    public VariationType getVariationType() {
        return variationType;
    }

    public Boolean getUndefinedDuration() {
        return undefinedDuration;
    }

    public Boolean getTemporalDuration() {
        return temporalDuration;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public String getDurationDays() {
        return durationDays;
    }

    public Boolean getFullTime() {
        return fullTime;
    }

    public Boolean getPartialTime() {
        return partialTime;
    }

    public String getWeeklyWorkHours() {
        return weeklyWorkHours;
    }

    public DaysOfWeekSelector getDaysOfWeekSelector() {
        return daysOfWeekSelector;
    }

    public String getLaborCategory() {
        return laborCategory;
    }

    public ContractSchedule getContractSchedule() {
        return contractSchedule;
    }

    public String getPublicNotes() {
        return publicNotes;
    }

    public String getPrivateNotes() {
        return privateNotes;
    }

    public List<WorkContractScheduleDay> getScheduleOneList() {
        return scheduleOneList;
    }

    public List<WorkContractScheduleDay> getScheduleTwoList() {
        return scheduleTwoList;
    }

    public List<WorkContractScheduleDay> getScheduleThreeList() {
        return scheduleThreeList;
    }

    public List<WorkContractScheduleDay> getScheduleFourList() {
        return scheduleFourList;
    }

    public Set<DayOfWeek> getTableColumnDayOfWeekData() {
        return tableColumnDayOfWeekData;
    }

    public String toFileName(){

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd_MM_yyyy");

        LocalDate fileNameDate = getDateFrom() != null ? getDateFrom() : getDateTo();

        return Utilities.replaceWithUnderscore(getEmployer().toAlphabeticalName())
                + "_" +
                Utilities.replaceWithUnderscore(getWorkContractType().getColloquial().toLowerCase())
                + "_" +
                fileNameDate.format(dateFormatter)
                + "_" +
                Utilities.replaceWithUnderscore(getEmployee().toAlphabeticalName());
    }

    public static final class ContractSituationEntryDataContainerBuilder {
        private Employer employer;
        private Employee employee;
        private WorkCenter workCenter;
        private QuoteAccountCode quoteAccountCode;
        private LocalDate clientDateNotification;
        private String clientHourNotification;
        private WorkContractType workContractType;
        private VariationType variationType;
        private Boolean undefinedDuration;
        private Boolean temporalDuration;
        private LocalDate dateFrom;
        private LocalDate dateTo;
        private String durationDays;
        private Boolean fullTime;
        private Boolean partialTime;
        private String weeklyWorkHours;
        private DaysOfWeekSelector daysOfWeekSelector;
        private String laborCategory;
        private ContractSchedule contractSchedule;
        private String publicNotes;
        private String privateNotes;
        private List<WorkContractScheduleDay> scheduleOneList;
        private List<WorkContractScheduleDay> scheduleTwoList;
        private List<WorkContractScheduleDay> scheduleThreeList;
        private List<WorkContractScheduleDay> scheduleFourList;
        private Set<DayOfWeek> tableColumnDayOfWeekData;

        private ContractSituationEntryDataContainerBuilder() {
        }

        public static ContractSituationEntryDataContainerBuilder create() {
            return new ContractSituationEntryDataContainerBuilder();
        }

        public ContractSituationEntryDataContainerBuilder withEmployer(Employer employer) {
            this.employer = employer;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withEmployee(Employee employee) {
            this.employee = employee;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withWorkCenter(WorkCenter workCenter) {
            this.workCenter = workCenter;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withQuoteAccountCode(QuoteAccountCode quoteAccountCode) {
            this.quoteAccountCode = quoteAccountCode;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withClientDateNotification(LocalDate clientDateNotification) {
            this.clientDateNotification = clientDateNotification;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withClientHourNotification(String clientHourNotification) {
            this.clientHourNotification = clientHourNotification;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withWorkContractType(WorkContractType workContractType) {
            this.workContractType = workContractType;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withVariationType(VariationType variationType) {
            this.variationType = variationType;
            return this;
        }


        public ContractSituationEntryDataContainerBuilder withUndefinedDuration(Boolean undefinedDuration) {
            this.undefinedDuration = undefinedDuration;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withTemporalDuration(Boolean temporalDuration) {
            this.temporalDuration = temporalDuration;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withDateFrom(LocalDate dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withDateTo(LocalDate dateTo) {
            this.dateTo = dateTo;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withDurationDays(String durationDays) {
            this.durationDays = durationDays;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withFullTime(Boolean fullTime) {
            this.fullTime = fullTime;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withPartialTime(Boolean partialTime) {
            this.partialTime = partialTime;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withWeeklyWorkHours(String weeklyWorkHours) {
            this.weeklyWorkHours = weeklyWorkHours;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withDaysOfWeekSelector(DaysOfWeekSelector daysOfWeekSelector) {
            this.daysOfWeekSelector = daysOfWeekSelector;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withLaborCategory(String laborCategory) {
            this.laborCategory = laborCategory;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withContractSchedule(ContractSchedule contractSchedule) {
            this.contractSchedule = contractSchedule;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withPublicNotes(String publicNotes) {
            this.publicNotes = publicNotes;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withPrivateNotes(String privateNotes) {
            this.privateNotes = privateNotes;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withScheduleOneList(List<WorkContractScheduleDay> scheduleOneList) {
            this.scheduleOneList = scheduleOneList;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withScheduleTwoList(List<WorkContractScheduleDay> scheduleTwoList) {
            this.scheduleTwoList = scheduleTwoList;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withScheduleThreeList(List<WorkContractScheduleDay> scheduleThreeList) {
            this.scheduleThreeList = scheduleThreeList;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withScheduleFourList(List<WorkContractScheduleDay> scheduleFourList) {
            this.scheduleFourList = scheduleFourList;
            return this;
        }

        public ContractSituationEntryDataContainerBuilder withTableColumnDayOfWeekData(Set<DayOfWeek> tableColumnDayOfWeekData) {
            this.tableColumnDayOfWeekData = tableColumnDayOfWeekData;
            return this;
        }

        public ContractSituationEntryDataContainer build() {
            return new ContractSituationEntryDataContainer(employer, employee, workCenter, quoteAccountCode, clientDateNotification, clientHourNotification, workContractType, variationType, undefinedDuration, temporalDuration, dateFrom, dateTo, durationDays, fullTime, partialTime, weeklyWorkHours, daysOfWeekSelector, laborCategory, contractSchedule, publicNotes, privateNotes, scheduleOneList, scheduleTwoList, scheduleThreeList, scheduleFourList, tableColumnDayOfWeekData);
        }
    }
}
