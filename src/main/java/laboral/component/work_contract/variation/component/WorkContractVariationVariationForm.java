package laboral.component.work_contract.variation.component;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import laboral.component.ViewLoader;

public class WorkContractVariationVariationForm extends AnchorPane {

    private static final String WORK_CONTRACT_VARIATION_VARIATION_FXML = "/fxml/work_contract/variation/work_contract_variation_variation.fxml";

    Parent parent;

    private final Font normalFont = Font.font("Noto Sans", FontWeight.NORMAL, FontPosture.REGULAR, 13);
    private final Font boldFont = Font.font("Noto Sans", FontWeight.BOLD, FontPosture.REGULAR, 13);

    private final Integer extinctionWorkContractOption = 0;
    private final Integer extensionWorkContractOption = 1;
    private final Integer variationHoursWorkWeekOption = 2;
    private final Integer conversionWorkContractOption = 3;

    @FXML
    private Accordion variationOption;

    @FXML
    private WorkContractExtinctionForm workContractExtinctionForm;
    @FXML
    private WorkContractExtensionForm workContractExtensionForm;
    @FXML
    private WorkContractVariationSchedule workContractVariationSchedule;
    @FXML
    private WorkContractConversionForm workContractConversionForm;
    @FXML
    private WorkContractSpecial workContractSpecial;

    public WorkContractVariationVariationForm() {
        this.parent = ViewLoader.load(this, WORK_CONTRACT_VARIATION_VARIATION_FXML);
    }

    @FXML
    public void initialize() {

        Double widthMin = workContractVariationSchedule.getAmFrom().getWidth() * 1.25;
        workContractVariationSchedule.getAmFrom().setMinWidth(widthMin);
        workContractVariationSchedule.getAmTo().setMinWidth(widthMin);
        workContractVariationSchedule.getPmFrom().setMinWidth(widthMin);
        workContractVariationSchedule.getPmTo().setMinWidth(widthMin);

        variationOption.expandedPaneProperty().addListener((ov, old_val, new_val) -> {
            for(TitledPane titledPane :  variationOption.getPanes()){
                if (titledPane == variationOption.getExpandedPane()){
                    titledPane.setStyle("-fx-text-fill: #000FFF");
                    titledPane.setFont(boldFont);
                }else{
                    titledPane.setStyle("-fx-text-fill: #000FFF");
                    titledPane.setFont(normalFont);
                }
            }

            initializePanes();

        });
    }

    public void interfaceInitialState(){
        variationOption.setExpandedPane(null);
    }


    public Accordion getVariationOption() {
        return variationOption;
    }

    public void setVariationOption(Accordion variationOption) {
        this.variationOption = variationOption;
    }

    public WorkContractExtinctionForm getWorkContractExtinction() {
        return workContractExtinctionForm;
    }

    public void setWorkContractExtinction(WorkContractExtinctionForm workContractExtinctionForm) {
        this.workContractExtinctionForm = workContractExtinctionForm;
    }

    public WorkContractExtensionForm getWorkContractExtension() {
        return workContractExtensionForm;
    }

    public void setWorkContractExtension(WorkContractExtensionForm workContractExtensionForm) {
        this.workContractExtensionForm = workContractExtensionForm;
    }

    public WorkContractVariationSchedule getWorkContractVariationSchedule() {
        return workContractVariationSchedule;
    }

    public void setWorkContractVariationSchedule(WorkContractVariationSchedule workContractVariationSchedule) {
        this.workContractVariationSchedule = workContractVariationSchedule;
    }

    public WorkContractConversionForm getWorkContractConversion() {
        return workContractConversionForm;
    }

    public void setWorkContractConversion(WorkContractConversionForm workContractConversionForm) {
        this.workContractConversionForm = workContractConversionForm;
    }

    public WorkContractSpecial getWorkContractSpecial() {
        return workContractSpecial;
    }

    public void setWorkContractSpecial(WorkContractSpecial workContractSpecial) {
        this.workContractSpecial = workContractSpecial;
    }

    private void initializePanes(){
        workContractExtinctionForm.cleanComponents();
        workContractExtensionForm.cleanComponents();
        workContractVariationSchedule.cleanComponents();
        workContractConversionForm.cleanComponents();
        workContractSpecial.cleanComponents();
    }

    public void setWorkContractNotModifiable(Boolean isExtinct){
        variationOption.setDisable(isExtinct);
    }

    public void setWorkContractNotConvertible(Boolean isNotConvertible){
        variationOption.getPanes().get(conversionWorkContractOption).setDisable(isNotConvertible);
    }

    public void setWorkContractNotExtendable(Boolean isNotExtendable){
        variationOption.getPanes().get(extensionWorkContractOption).setDisable(isNotExtendable);
    }
}
