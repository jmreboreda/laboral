package laboral.component.work_contract.variation.component;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.LocalDateStringConverter;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.work_contract.variation.WorkContractVariationConstants;
import laboral.domain.contract.WorkContract;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.variation_type.VariationType;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class WorkContractConversionForm extends AnchorPane {

    private static final String CONTRACT_VARIATION_CONTRACT_CONVERSION_FXML = "/fxml/work_contract/variation/work_contract_conversion.fxml";

    private Parent parent;

    @FXML
    private ComboBox<VariationType> conversionTypeSelector;
    @FXML
    private DatePicker dateFrom;
    @FXML
    private DatePicker dateTo;
    @FXML
    private TextArea publicNotes;
    @FXML
    private TextArea privateNotes;

    public WorkContractConversionForm() {
        this.parent = ViewLoader.load(this, CONTRACT_VARIATION_CONTRACT_CONVERSION_FXML);

    }

    @FXML
    public void initialize(){

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());
        dateFrom.setConverter(new LocalDateStringConverter(dateFormatter, null));
        dateTo.setConverter(new LocalDateStringConverter(dateFormatter, null));


        conversionTypeSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 14));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.CENTER_LEFT);
                    setText(item.toString());
                }
            }

        });

        conversionTypeSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<VariationType> call(ListView<VariationType> param) {
                        final ListCell<VariationType> cell = new ListCell<VariationType>() {
                            @Override
                            public void updateItem(VariationType item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }
                });

        dateFrom.disableProperty().bind(this.conversionTypeSelector.valueProperty().isNull());
        publicNotes.disableProperty().bind(this.conversionTypeSelector.valueProperty().isNull());
        privateNotes.disableProperty().bind(this.conversionTypeSelector.valueProperty().isNull());

        publicNotes.setStyle("-fx-text-fill: #000FFF");
        privateNotes.setStyle("-fx-text-fill: #640000");

        dateTo.setDisable(true);
        conversionTypeSelector.setOnAction(this::onConversionSelectorChange);
    }

    public ComboBox<VariationType> getConversionTypeSelector() {
        return conversionTypeSelector;
    }

    public void setConversionTypeSelector(ComboBox<VariationType> conversionTypeSelector) {
        this.conversionTypeSelector = conversionTypeSelector;
    }

    public DatePicker getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(DatePicker dateFrom) {
        this.dateFrom = dateFrom;
    }

    public DatePicker getDateTo() {
        return dateTo;
    }

    public void setDateTo(DatePicker dateTo) {
        this.dateTo = dateTo;
    }

    public TextArea getPublicNotes() {
        return publicNotes;
    }

    public void setPublicNotes(TextArea publicNotes) {
        this.publicNotes = publicNotes;
    }

    public TextArea getPrivateNotes() {
        return privateNotes;
    }

    public void setPrivateNotes(TextArea privateNotes) {
        this.privateNotes = privateNotes;
    }

    private void onConversionSelectorChange(ActionEvent event){
        int CONVERSION_CODE_FROM_FORMATION_CONTRACT_TO_NORMAL_CONTRACT = 401;
        int CONVERSION_CODE_FROM_PREGNANCY_RISK_TO_MATERNITY = 403;

        dateFrom.setValue(null);
        dateTo.setValue(null);

        if(conversionTypeSelector.getSelectionModel().getSelectedItem().getVariationCode() == CONVERSION_CODE_FROM_FORMATION_CONTRACT_TO_NORMAL_CONTRACT ||
                conversionTypeSelector.getSelectionModel().getSelectedItem().getVariationCode() == CONVERSION_CODE_FROM_PREGNANCY_RISK_TO_MATERNITY){
            dateTo.setDisable(false);
        }else{
            dateTo.setDisable(true);
        }
    }

    public void cleanComponents(){
        conversionTypeSelector.getSelectionModel().clearSelection();
        dateFrom.setValue(null);
        publicNotes.clear();
        privateNotes.clear();
    }

    public void refreshConversionTypeSelector(ObservableList<VariationType> variationTypeExtinctionObservableList){
        conversionTypeSelector.getSelectionModel().clearSelection();
        conversionTypeSelector.getItems().clear();
        conversionTypeSelector.setItems(variationTypeExtinctionObservableList);
    }
}
