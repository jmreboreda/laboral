package laboral.component.work_contract.variation.component;

import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class WorkContractVariationHeader extends AnchorPane {

    private static final String WORK_CONTRACT_HEADER_FXML = "/fxml/work_contract/variation/work_contract_variation_header.fxml";

    Parent parent;


    public WorkContractVariationHeader() {
        this.parent = ViewLoader.load(this, WORK_CONTRACT_HEADER_FXML);
    }
}
