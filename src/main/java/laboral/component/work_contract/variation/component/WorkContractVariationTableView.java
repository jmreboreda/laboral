package laboral.component.work_contract.variation.component;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;
import laboral.domain.contract_variation_for_table_view.WorkContractVariationDataForTableView;
import laboral.domain.utilities.cells.WorkContractVariationDateCell;
import laboral.domain.utilities.cells.WorkContractVariationDurationCell;
import laboral.domain.utilities.cells.WorkContractVariationStringCell;

import java.time.Duration;
import java.time.LocalDate;

public class WorkContractVariationTableView extends AnchorPane {

    private static final String WORK_CONTRACT_VARIATION_TABLE_VIEW_FXML = "/fxml/work_contract/variation/work_contract_variation_table_view.fxml";

    Parent parent;

    private final ContextMenu contextMenu = new ContextMenu();

    @FXML
    private TableView<WorkContractVariationDataForTableView> workContractTableView;
    @FXML
    private TableColumn<WorkContractVariationDataForTableView, Integer> code;
    @FXML
    private TableColumn<WorkContractVariationDataForTableView, String> description;
    @FXML
    private TableColumn<WorkContractVariationDataForTableView, LocalDate> startDate;
    @FXML
    private TableColumn<WorkContractVariationDataForTableView, LocalDate> endingDate;
    @FXML
    private TableColumn<WorkContractVariationDataForTableView, LocalDate> modificationDate;
    @FXML
    private TableColumn<WorkContractVariationDataForTableView, LocalDate> extinctionDate;
    @FXML
    private TableColumn<WorkContractVariationDataForTableView, Duration> weeklyWorkHours;


    public WorkContractVariationTableView() {
        this.parent = ViewLoader.load(this, WORK_CONTRACT_VARIATION_TABLE_VIEW_FXML);
    }

    public void initialize(){
        workContractTableView.setEditable(false);

        MenuItem mi1 = new MenuItem("Mostrar el horario de esta variación de contrato");
        contextMenu.getItems().addAll(mi1);
        mi1.setStyle("-fx-font-size: 11.5");


        workContractTableView.addEventHandler(MouseEvent.MOUSE_CLICKED, t -> {
            if(t.getButton() == MouseButton.SECONDARY) {
                contextMenu.show(workContractTableView, t.getScreenX(), t.getScreenY());
            }
        });

        contextMenu.getItems().get(0).setOnAction(this::onContextMenuAction);

        description.setCellFactory(param -> new WorkContractVariationStringCell());
        startDate.setCellFactory(param-> new WorkContractVariationDateCell());
        endingDate.setCellFactory(param-> new WorkContractVariationDateCell());
        modificationDate.setCellFactory(param-> new WorkContractVariationDateCell());
        extinctionDate.setCellFactory(param-> new WorkContractVariationDateCell());
        weeklyWorkHours.setCellFactory(param -> new WorkContractVariationDurationCell());


        code.setCellValueFactory(new PropertyValueFactory<>("variationTypeCode"));
        description.setCellValueFactory(new PropertyValueFactory<>("variationDescription"));
        startDate.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        endingDate.setCellValueFactory(new PropertyValueFactory<>("expectedEndDate"));
        modificationDate.setCellValueFactory(new PropertyValueFactory<>("modificationDate"));
        extinctionDate.setCellValueFactory(new PropertyValueFactory<>("endingDate"));
        weeklyWorkHours.setCellValueFactory(new PropertyValueFactory<>("hoursWorkWeek"));


        code.getStyleClass().add("center");
        startDate.getStyleClass().add("center");
        endingDate.getStyleClass().add("center");
        modificationDate.getStyleClass().add("center");
        extinctionDate.getStyleClass().add("center");
        weeklyWorkHours.getStyleClass().add("center");
    }

    public TableView<WorkContractVariationDataForTableView> getWorkContractTableView() {
        return workContractTableView;
    }

    public void setWorkContractTableView(TableView<WorkContractVariationDataForTableView> workContractTableView) {
        this.workContractTableView = workContractTableView;
    }

    public TableColumn<WorkContractVariationDataForTableView, Integer> getCode() {
        return code;
    }

    public void setCode(TableColumn<WorkContractVariationDataForTableView, Integer> code) {
        this.code = code;
    }

    public TableColumn<WorkContractVariationDataForTableView, String> getDescription() {
        return description;
    }

    public void setDescription(TableColumn<WorkContractVariationDataForTableView, String> description) {
        this.description = description;
    }

    public TableColumn<WorkContractVariationDataForTableView, LocalDate> getStartDate() {
        return startDate;
    }

    public void setStartDate(TableColumn<WorkContractVariationDataForTableView, LocalDate> startDate) {
        this.startDate = startDate;
    }

    public TableColumn<WorkContractVariationDataForTableView, LocalDate> getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(TableColumn<WorkContractVariationDataForTableView, LocalDate> endingDate) {
        this.endingDate = endingDate;
    }

    public TableColumn<WorkContractVariationDataForTableView, LocalDate> getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(TableColumn<WorkContractVariationDataForTableView, LocalDate> modificationDate) {
        this.modificationDate = modificationDate;
    }

    public TableColumn<WorkContractVariationDataForTableView, LocalDate> getExtinctionDate() {
        return extinctionDate;
    }

    public void setExtinctionDate(TableColumn<WorkContractVariationDataForTableView, LocalDate> extinctionDate) {
        this.extinctionDate = extinctionDate;
    }

    public TableColumn<WorkContractVariationDataForTableView, Duration> getWeeklyWorkHours() {
        return weeklyWorkHours;
    }

    public void setWeeklyWorkHours(TableColumn<WorkContractVariationDataForTableView, Duration> weeklyWorkHours) {
        this.weeklyWorkHours = weeklyWorkHours;
    }

    public void refreshWorkContractTable(ObservableList<WorkContractVariationDataForTableView> workContractObservableList){
        workContractTableView.setItems(FXCollections.observableArrayList(workContractObservableList));
        workContractTableView.refresh();
    }

    private void onContextMenuAction(ActionEvent event){
        System.out.println("Se ha seleccionado item en context menu: " + ((MenuItem) event.getTarget()).getText());
    }

    //    public WorkContractVariationTableView getWorkContractVariationTableView() {
//        return workContractVariationTableView;
//    }
//
//    public void setWorkContractVariationTableView(WorkContractVariationTableView workContractVariationTableView) {
//        this.workContractVariationTableView = workContractVariationTableView;
//    }
}
