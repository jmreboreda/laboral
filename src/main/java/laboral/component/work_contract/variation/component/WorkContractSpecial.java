package laboral.component.work_contract.variation.component;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import javafx.util.converter.LocalDateStringConverter;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.domain.contract.WorkContract;
import laboral.domain.validator.WorkContractSpecialVariationDataValidator;
import laboral.domain.variation_type.VariationType;

import java.sql.Date;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class WorkContractSpecial extends AnchorPane {

    private static final String CONTRACT_VARIATION_CONTRACT_SPECIAL_FXML = "/fxml/work_contract/variation/work_contract_special.fxml";

    private Parent parent;

    @FXML
    private ComboBox<VariationType> specialVariationSelector;
    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker expectedEndDate;
    @FXML
    private DatePicker endDate;
    @FXML
    private DatePicker restartDate;
    @FXML
    private TextArea publicNotes;
    @FXML
    private TextArea privateNotes;

    public WorkContractSpecial() {
        this.parent = ViewLoader.load(this, CONTRACT_VARIATION_CONTRACT_SPECIAL_FXML);

    }

    @FXML
    public void initialize(){

        startDate.disableProperty().bind(this.specialVariationSelector.valueProperty().isNull());
        expectedEndDate.disableProperty().bind(this.specialVariationSelector.valueProperty().isNull());
        endDate.disableProperty().bind(this.specialVariationSelector.valueProperty().isNull());
        restartDate.disableProperty().bind(this.specialVariationSelector.valueProperty().isNull());
        publicNotes.disableProperty().bind(this.specialVariationSelector.valueProperty().isNull());
        privateNotes.disableProperty().bind(this.specialVariationSelector.valueProperty().isNull());


        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());
        startDate.setConverter(new LocalDateStringConverter(dateFormatter, null));
        expectedEndDate.setConverter(new LocalDateStringConverter(dateFormatter, null));
        endDate.setConverter(new LocalDateStringConverter(dateFormatter, null));
        restartDate.setConverter(new LocalDateStringConverter(dateFormatter, null));

        specialVariationSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 14));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.CENTER_LEFT);
                    setText(item.toString());
                }
            }

        });

        specialVariationSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<VariationType> call(ListView<VariationType> param) {
                        final ListCell<VariationType> cell = new ListCell<>() {
                            @Override
                            public void updateItem(VariationType item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }
                });

//        startDate.valueProperty().addListener((ov, oldValue, newValue) -> {
////           if(newValue != null && expectedEndDate.getValue() != null){
////               long daysBetween = DAYS.between(newValue, expectedEndDate.getValue()) + 1L;
////               extensionDurationDays.setText(String.valueOf(daysBetween));
////           }
//        });
//
//        expectedEndDate.valueProperty().addListener((ov, oldValue, newValue) -> {
////            if(newValue != null && expectedEndDate.getValue() != null){
////                long daysBetween = DAYS.between( startDate.getValue(), newValue) + 1L;
////                extensionDurationDays.setText(String.valueOf(daysBetween));
////            }
//        });


        publicNotes.setStyle("-fx-text-fill: #000FFF");
        privateNotes.setStyle("-fx-text-fill: #640000");

        specialVariationSelector.setOnAction(this::onSpecialVariationSelectorChange);
    }

    public DatePicker getStartDate() {
        return startDate;
    }

    public void setStartDate(DatePicker startDate) {
        this.startDate = startDate;
    }

    public DatePicker getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(DatePicker expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public DatePicker getEndDate() {
        return endDate;
    }

    public void setEndDate(DatePicker endDate) {
        this.endDate = endDate;
    }

    public DatePicker getRestartDate() {
        return restartDate;
    }

    public void setRestartDate(DatePicker restartDate) {
        this.restartDate = restartDate;
    }

    public ComboBox<VariationType> getSpecialVariationSelector() {
        return specialVariationSelector;
    }

    public void setSpecialVariationSelector(ComboBox<VariationType> specialVariationSelector) {
        this.specialVariationSelector = specialVariationSelector;
    }

    public TextArea getPublicNotes() {
        return publicNotes;
    }

    public void setPublicNotes(TextArea publicNotes) {
        this.publicNotes = publicNotes;
    }

    public TextArea getPrivateNotes() {
        return privateNotes;
    }

    public void setPrivateNotes(TextArea privateNotes) {
        this.privateNotes = privateNotes;
    }

    public void cleanComponents(){
        specialVariationSelector.getSelectionModel().clearSelection();
        partialClean();
    }

    private void partialClean(){
        startDate.setValue(null);
        expectedEndDate.setValue(null);
        endDate.setValue(null);
        restartDate.setValue(null);
        publicNotes.clear();
        privateNotes.clear();
    }

    public void refreshSpecialVariationSelector(ObservableList<VariationType> variationTypeObservableList){
        specialVariationSelector.getSelectionModel().clearSelection();
        specialVariationSelector.getItems().clear();
        specialVariationSelector.setItems(variationTypeObservableList);
    }

    private void onSpecialVariationSelectorChange(ActionEvent event){

        partialClean();

        startDate.setVisible(false);
        expectedEndDate.setVisible(false);
        endDate.setVisible(false);
        restartDate.setVisible(false);

        VariationType variationTypeSelected = specialVariationSelector.getSelectionModel().getSelectedItem();

        if(variationTypeSelected == null){
            return;
        }

        if(variationTypeSelected.getSpecialInitial() && !variationTypeSelected.getReincorporation()){
            startDate.setVisible(true);
            expectedEndDate.setVisible(true);
            startDate.requestFocus();
            return;
        }

        if(variationTypeSelected.getSpecialFinal()){
            endDate.setVisible(true);
            endDate.requestFocus();
            return;
        }

        if(variationTypeSelected.getReincorporation()){
            restartDate.setVisible(true);
            restartDate.requestFocus();
            return;
        }

        if(variationTypeSelected.getSpecialInitial() == Boolean.FALSE && variationTypeSelected.getSpecialFinal() == Boolean.FALSE){
            startDate.setVisible(true);
            startDate.requestFocus();
        }
    }

    public Boolean verifyCorrectSpecialData(WorkContractVariationVariationForm workContractVariationVariationForm, List<WorkContract> workContractToSpecialList) {

        WorkContractSpecialVariationDataValidator workContractSpecialVariationDataValidator = new WorkContractSpecialVariationDataValidator();

        VariationType specialVariationTypeSelected = workContractVariationVariationForm.getWorkContractSpecial().getSpecialVariationSelector().getSelectionModel().getSelectedItem();

    if(specialVariationTypeSelected.getSpecialInitial() && !specialVariationTypeSelected.getReincorporation()){
            if(!workContractSpecialVariationDataValidator.verifySpecialInitialVariationData(workContractVariationVariationForm.getWorkContractSpecial(), workContractToSpecialList)){
                return Boolean.FALSE;
            }
        }

        if(specialVariationTypeSelected.getSpecialFinal()){
            if(!workContractSpecialVariationDataValidator.verifySpecialFinalVariationData(workContractVariationVariationForm.getWorkContractSpecial(), workContractToSpecialList)){
                return Boolean.FALSE;
            }
        }

        if(specialVariationTypeSelected.getReincorporation()){
            if(!workContractSpecialVariationDataValidator.verifySpecialReincorporationVariationData(workContractVariationVariationForm.getWorkContractSpecial(), workContractToSpecialList)){
                return Boolean.FALSE;
            }
        }

        //TODO

        return Boolean.TRUE;
    }

    public void updateModificationDateInAllWorkContractVariation(WorkContract workContractToVariation, Date modificationDate){
//        WorkContractService workContractService = WorkContractService.WorkContractServiceFactory.getInstance();
//        WorkContractDBO workContractDBO = workContractService.findById(workContractToVariation.getId());
//        workContractDBO.setModificationDate(modificationDate);
//        workContractService.update(workContractDBO);
    }
}
