package laboral.component.work_contract.variation.component;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.converter.LocalDateStringConverter;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.work_contract.variation.WorkContractVariationConstants;
import laboral.domain.contract.WorkContract;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;

public class WorkContractExtensionForm extends AnchorPane {

    private static final String CONTRACT_VARIATION_CONTRACT_EXTENSION_FXML = "/fxml/work_contract/variation/work_contract_extension.fxml";

    private Parent parent;

    @FXML
    private DatePicker dateFrom;
    @FXML
    private DatePicker dateTo;
    @FXML
    private TextField extensionDurationDays;
    @FXML
    private TextArea publicNotes;
    @FXML
    private TextArea privateNotes;

    public WorkContractExtensionForm() {
        this.parent = ViewLoader.load(this, CONTRACT_VARIATION_CONTRACT_EXTENSION_FXML);

    }

    @FXML
    private void initialize(){

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());
        dateFrom.setConverter(new LocalDateStringConverter(dateFormatter, null));
        dateTo.setConverter(new LocalDateStringConverter(dateFormatter, null));

        dateFrom.valueProperty().addListener((ov, oldValue, newValue) -> {
           if(newValue != null && dateTo.getValue() != null){
               long daysBetween = DAYS.between(newValue, dateTo.getValue()) + 1L;
               extensionDurationDays.setText(String.valueOf(daysBetween));
           }
        });

        dateTo.valueProperty().addListener((ov, oldValue, newValue) -> {
            if(newValue != null && dateTo.getValue() != null){
                long daysBetween = DAYS.between( dateFrom.getValue(), newValue) + 1L;
                extensionDurationDays.setText(String.valueOf(daysBetween));
            }
        });


        publicNotes.setStyle("-fx-text-fill: #000FFF");
        privateNotes.setStyle("-fx-text-fill: #640000");
    }

    public DatePicker getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(DatePicker dateFrom) {
        this.dateFrom = dateFrom;
    }

    public DatePicker getDateTo() {
        return dateTo;
    }

    public void setDateTo(DatePicker dateTo) {
        this.dateTo = dateTo;
    }

    public TextField getExtensionDurationDays() {
        return extensionDurationDays;
    }

    public void setExtensionDurationDays(TextField extensionDurationDays) {
        this.extensionDurationDays = extensionDurationDays;
    }

    public TextArea getPublicNotes() {
        return publicNotes;
    }

    public void setPublicNotes(TextArea publicNotes) {
        this.publicNotes = publicNotes;
    }

    public TextArea getPrivateNotes() {
        return privateNotes;
    }

    public void setPrivateNotes(TextArea privateNotes) {
        this.privateNotes = privateNotes;
    }

    public void cleanComponents(){
        dateFrom.setValue(null);
        dateTo.setValue(null);

        publicNotes.clear();
        privateNotes.clear();
    }

    public Boolean verifyCorrectExtensionData(List<WorkContract> workContractToExtendList, LocalDate dateFrom, LocalDate dateTo) {

        if (dateFrom == null || dateTo == null) {
            Message.errorMessage((Stage) this.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.SOME_DATE_CONTRACT_EXTENSION_IS_MISSING);
            return Boolean.FALSE;
        }

        if (dateTo.isBefore(dateFrom) || dateTo == dateFrom) {
            Message.errorMessage((Stage) this.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.ERROR_EXTENSION_CONTRACT_INCOHERENT_DATES);
            return Boolean.FALSE;
        }

        for (WorkContract workContract : workContractToExtendList) {
            if (workContract.getModificationDate() == null && (dateFrom.isBefore(workContract.getStartDate()) || dateTo.isBefore(workContract.getStartDate()))) {
                Message.errorMessage((Stage) this.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.START_OR_END_EXTENSION_DATES_ARE_INCORRECT);
                return Boolean.FALSE;
            }

            if(workContract.getModificationDate() == null && workContract.getExpectedEndDate() != null && DAYS.between(workContract.getExpectedEndDate(), dateFrom) > 1){
                Message.errorMessage((Stage) this.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.START_EXTENSION_DATE_IS_INCORRECT);
                return Boolean.FALSE;
            }

            if (workContract.getModificationDate() != null && dateFrom.isBefore(workContract.getModificationDate())) {
                Message.errorMessage((Stage) this.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.START_EXTENSION_DATE_IS_INCORRECT);
                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    public void updateModificationDateInAllWorkContractVariation(Integer contractNumber, Date endingDate){
//        WorkContractService workContractService = WorkContractService.WorkContractServiceFactory.getInstance();
//        VariationTypeService variationTypeService = new VariationTypeService();
//        List<WorkContractDBO> workContractDBOList = workContractService.findAllWorkContractVariationByContractNumber(contractNumber);
//        for(WorkContractDBO workContractDBO : workContractDBOList){
//            VariationTypeDBO variationTypeDBO = variationTypeService.findByVariationTypeCode(workContractDBO.getVariationTypeCode());
//            if(workContractDBO.getModificationDate() == null && !variationTypeDBO.getExtension()){
//                workContractDBO.setModificationDate(endingDate);
//                workContractService.update(workContractDBO);
//            }
//        }
    }
}
