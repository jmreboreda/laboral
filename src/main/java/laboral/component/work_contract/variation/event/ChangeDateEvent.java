package laboral.component.work_contract.variation.event;

import javafx.event.Event;
import javafx.event.EventType;

import java.time.Duration;
import java.time.LocalDate;

public class ChangeDateEvent extends Event {

	public static final EventType<ChangeDateEvent> CHANGE_DATE_EVENT = new EventType<>("CHANGE_DATE_EVENT");
	private final LocalDate date;


	public ChangeDateEvent(LocalDate date) {
		super(CHANGE_DATE_EVENT);
		this.date = date;
	}

	public LocalDate getDate() {
		return date;
	}
}
