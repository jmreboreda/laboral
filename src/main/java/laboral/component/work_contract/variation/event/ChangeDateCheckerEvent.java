package laboral.component.work_contract.variation.event;

import javafx.event.Event;
import javafx.event.EventType;

import java.time.LocalDate;

public class ChangeDateCheckerEvent extends Event {

	public static final EventType<ChangeDateCheckerEvent> CHANGE_DATE_CHECKER_EVENT = new EventType<>("CHANGE_DATE_CHECKER_EVENT");
	private final Boolean checked;


	public ChangeDateCheckerEvent(Boolean checked) {
		super(CHANGE_DATE_CHECKER_EVENT);
		this.checked = checked;
	}

	public Boolean getChecked() {
		return checked;
	}
}
