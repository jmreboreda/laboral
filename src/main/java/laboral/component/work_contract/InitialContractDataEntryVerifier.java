package laboral.component.work_contract;

import javafx.scene.Scene;
import javafx.stage.Stage;
import laboral.domain.contract.work_contract.creation.InitialContractSituationEntryDataContainer;
import laboral.domain.contract_json.ContractDaySchedule;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.utilities.utilities.Utilities;

import java.time.DayOfWeek;
import java.time.Duration;
import java.util.*;

public class InitialContractDataEntryVerifier {

    public Boolean verify(InitialContractSituationEntryDataContainer contractSituationEntryDataContainer, Scene scene) {

       if(!verifyContractParts(contractSituationEntryDataContainer, scene)){
           return Boolean.FALSE;
       }

       if(!verifyContractData(contractSituationEntryDataContainer, scene)){
           return Boolean.FALSE;
       }

        if(!verifyContractSchedule(contractSituationEntryDataContainer, scene)){
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

    private Boolean verifyContractParts(InitialContractSituationEntryDataContainer initialContractSituationEntryDataContainer, Scene scene){

        if(initialContractSituationEntryDataContainer.getEmployer() == null){
            Message.errorMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.EMPLOYER_IS_NOT_SELECTED);
            return false;
        }

        if(initialContractSituationEntryDataContainer.getEmployee() == null){
            Message.errorMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.EMPLOYEE_IS_NOT_SELECTED);
            return false;
        }

        if(initialContractSituationEntryDataContainer.getWorkCenter() == null){
            Message.errorMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.WORK_CENTER_IS_NOT_SELECTED);
            return false;
        }

        if(initialContractSituationEntryDataContainer.getQuoteAccountCode() == null){
            if(!Message.confirmationMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVerifierConstants.QUESTION_NULL_CCC_CODE_IS_CORRECT)){
                return false;
            }
        }

        return true;
    }

    private Boolean verifyContractData(InitialContractSituationEntryDataContainer initialContractSituationEntryDataContainer, Scene scene){

        if(initialContractSituationEntryDataContainer.getClientHourNotification() == null){
            Message.errorMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.HOUR_NOTIFICATION_IS_NOT_ESTABLISHED);
            return false;
        }

        if(initialContractSituationEntryDataContainer.getWorkContractType() == null){
            Message.errorMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.CONTRACT_TYPE_NOT_SELECTED);
            return false;
        }

        if(initialContractSituationEntryDataContainer.getTemporalDuration() != initialContractSituationEntryDataContainer.getWorkContractType().getTemporal()){
            Message.errorMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.DURATION_CONTRACT_SELECTED_DOES_NOT_MATCH);
            return false;
        }

        if(initialContractSituationEntryDataContainer.getFullTime() != initialContractSituationEntryDataContainer.getWorkContractType().getFullTime()){
            Message.errorMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.WORKDAY_TYPE_SELECTED_DOES_NOT_MATCH);
            return false;
        }

        if(initialContractSituationEntryDataContainer.getTemporalDuration() && initialContractSituationEntryDataContainer.getDurationDays().isEmpty()) {
                Message.errorMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                        WorkContractVerifierConstants.INVALID_CONTRACT_DURATION);
                return false;
        }


        if(!initialContractSituationEntryDataContainer.getDurationDays().isEmpty()) {
            Integer contractDurationDays = Integer.parseInt(initialContractSituationEntryDataContainer.getDurationDays().replace(".",""));
            if (contractDurationDays <= 0) {
                Message.errorMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                        WorkContractVerifierConstants.INVALID_CONTRACT_DURATION);
                return false;
            }
        }

        if(initialContractSituationEntryDataContainer.getPartialTime() && initialContractSituationEntryDataContainer.getWeeklyWorkHours().equals("00:00")){
            Message.errorMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.PARTIAL_WORK_DAY_WITHOUT_HOURS_WORK_WEEK);
            return false;
        }

        Integer contractDataWeeklyWorkHours = Integer.parseInt(initialContractSituationEntryDataContainer.getWeeklyWorkHours().replace(":", ""));
        Integer scheduleHoursWorkWeek = Integer.parseInt(initialContractSituationEntryDataContainer.getWeeklyWorkHours().replace(":", ""));
        if(initialContractSituationEntryDataContainer.getPartialTime() &&
                !contractDataWeeklyWorkHours.equals(scheduleHoursWorkWeek)){
            Message.errorMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.DIFFERENT_NUMBER_HOURS_CONTRACT_DATA_AND_CONTRACT_SCHEDULE);
            return false;
        }

        if(initialContractSituationEntryDataContainer.getDaysOfWeekSelector().getDaysOfWeek().size() == 0){
            Message.errorMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.DAYS_TO_WORK_ARE_NOT_SELECTED);
            return false;
        }
        if(initialContractSituationEntryDataContainer.getLaborCategory().length() == 0){
            Message.errorMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.LABOR_CATEGORY_IS_NOT_ESTABLISHED);
            return false;
        }

        return true;
    }

    private Boolean verifyContractSchedule(InitialContractSituationEntryDataContainer initialContractSituationEntryDataContainer, Scene scene){

        Duration schedule1TotalHours = Duration.ZERO;
        Duration schedule2TotalHours = Duration.ZERO;
        Duration schedule3TotalHours = Duration.ZERO;
        Duration schedule4TotalHours = Duration.ZERO;

        Duration hoursWorkWeek = Duration.ZERO;
        Map<String, ContractDaySchedule> schedule = initialContractSituationEntryDataContainer.getContractSchedule().getSchedule();
        for (Map.Entry<String, ContractDaySchedule> entry : schedule.entrySet()) {
            if(entry.getKey() == null) continue;
            if(entry.getKey().contains("Schedule1")) schedule1TotalHours = schedule1TotalHours.plus(Objects.requireNonNull((Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours()))));
            if(entry.getKey().contains("Schedule2")) schedule2TotalHours = schedule2TotalHours.plus(Objects.requireNonNull((Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours()))));
            if(entry.getKey().contains("Schedule3")) schedule3TotalHours = schedule3TotalHours.plus(Objects.requireNonNull((Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours()))));
            if(entry.getKey().contains("Schedule4")) schedule4TotalHours = schedule4TotalHours.plus(Objects.requireNonNull((Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours()))));
            hoursWorkWeek = hoursWorkWeek.plus(Objects.requireNonNull((Utilities.timeStringToDurationConverter(entry.getValue().getDurationHours()))));
        }

        if(initialContractSituationEntryDataContainer.getFullTime() &&
                schedule1TotalHours == Duration.ZERO &&
                schedule2TotalHours == Duration.ZERO &&
                schedule3TotalHours == Duration.ZERO &&
                schedule4TotalHours == Duration.ZERO){

            return true;
        }

        if(initialContractSituationEntryDataContainer.getPartialTime() &&
                schedule1TotalHours == Duration.ZERO &&
                schedule2TotalHours == Duration.ZERO &&
                schedule3TotalHours == Duration.ZERO &&
                schedule4TotalHours == Duration.ZERO){

            Message.errorMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.NO_SCHEDULE_HAS_BEEN_ESTABLISHED_FOR_THE_PART_TIME);

            return false;
        }

//        System.out.println("--------------------");
//        System.out.println("Horario 1: " + String.format("%02d:%02d", schedule1TotalHours.toSeconds() / 3600, (schedule1TotalHours.toSeconds() % 3600) / 60));
//        System.out.println("Horario 2: " + String.format("%02d:%02d", schedule2TotalHours.toSeconds() / 3600, (schedule2TotalHours.toSeconds() % 3600) / 60));
//        System.out.println("Horario 3: " + String.format("%02d:%02d", schedule3TotalHours.toSeconds() / 3600, (schedule3TotalHours.toSeconds() % 3600) / 60));
//        System.out.println("Horario 4: " + String.format("%02d:%02d", schedule4TotalHours.toSeconds() / 3600, (schedule4TotalHours.toSeconds() % 3600) / 60));

        /** The average weekly hours resulting from the different established working hours tables
            must coincide with the number of weekly working hours declared. **/

        Integer schedulesWithDuration = 0;
        if(schedule1TotalHours != Duration.ZERO)
            schedulesWithDuration++;
        if(schedule2TotalHours != Duration.ZERO)
            schedulesWithDuration++;
        if(schedule3TotalHours != Duration.ZERO)
            schedulesWithDuration++;
        if(schedule4TotalHours != Duration.ZERO)
            schedulesWithDuration++;

        Long averageWeeklyHours = schedulesWithDuration == 0 ? 0 : (schedule1TotalHours.toSeconds() + schedule2TotalHours.toSeconds() +
                schedule3TotalHours.toSeconds() + schedule4TotalHours.toSeconds()) / schedulesWithDuration;
        String averageHours = String.format("%02d:%02d", averageWeeklyHours / 3600, (averageWeeklyHours % 3600) / 60);
//        System.out.println("Promedio horarios: " + averageHours);

        Long weeklyWorkHoursDeclared = Utilities.timeStringToDurationConverter(initialContractSituationEntryDataContainer.getWeeklyWorkHours()).toSeconds();
        String declaredHours = String.format("%02d:%02d", weeklyWorkHoursDeclared / 3600, (weeklyWorkHoursDeclared % 3600) / 60);
        if(!averageWeeklyHours.equals(weeklyWorkHoursDeclared)){
            Message.errorMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.HOURS_SCHEDULE_NOT_COINCIDE_WITH_ESTABLISHED_PART_TIME
                            .concat("[Declarada: " + declaredHours + " :: Promedio: " + averageHours + "]"));

            return false;
        }

        /*************************************************************************************************/

        Set<DayOfWeek> contractDataDaysOfWeekToWork = initialContractSituationEntryDataContainer.getDaysOfWeekSelector().getDaysOfWeek();
        Set<DayOfWeek> contractScheduleDayOfWeekToWork = initialContractSituationEntryDataContainer.getTableColumnDayOfWeekData();

       if(!contractDataDaysOfWeekToWork.equals(contractScheduleDayOfWeekToWork)){
           Message.warningMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                   WorkContractVerifierConstants.WORKING_DAYS_ARE_DIFFERENT_IN_CONTRACTDATA_AND_CONTRACTSCHEDULE);
           return false;
       }

        return true;
    }
}
