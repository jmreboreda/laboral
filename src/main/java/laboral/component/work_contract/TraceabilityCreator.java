package laboral.component.work_contract;

import laboral.domain.contract.WorkContract;
import laboral.domain.contract.work_contract.controller.WorkContractController;
import laboral.domain.traceability_contract_documentation.TraceabilityContractDocumentation;
import laboral.domain.traceability_contract_documentation.manager.TraceabilityContractDocumentationManager;

import java.time.LocalDate;
import java.util.logging.Logger;

public class TraceabilityCreator {

    private static final Logger logger = Logger.getLogger(TraceabilityCreator.class.getSimpleName());

    public Integer persistTraceabilityOfContractVariation(Integer workContractVariationId) {

        WorkContractController workContractController = new WorkContractController();

        WorkContract workContractVariation = workContractController.findById(workContractVariationId);
        Integer workContractNumber = workContractVariation.getContractNumber();
        LocalDate contractEndNoticeReceptionDate = workContractVariation.getExpectedEndDate() == null ? LocalDate.of(9999, 12, 31) : null;

        TraceabilityContractDocumentation traceability = new TraceabilityContractDocumentation();
        traceability.setContractNumber(workContractNumber);
        traceability.setVariationType(workContractVariation.getVariationType());
        traceability.setStartDate(workContractVariation.getStartDate());
        traceability.setExpectedEndDate(workContractVariation.getExpectedEndDate());
        traceability.setIdcReceptionDate(null);
        traceability.setDateDeliveryContractDocumentationToClient(null);
        traceability.setContractEndNoticeReceptionDate(contractEndNoticeReceptionDate);

        TraceabilityContractDocumentationManager traceabilityManager = new TraceabilityContractDocumentationManager();
        Integer traceabilityId = traceabilityManager.saveContractTraceability(traceability);
        if(traceabilityId != null) {
            logger.info(">>>>> The traceability of the variation of the workContract number " + workContractNumber + " has been registered.");
            return traceabilityId;
        }

        return null;
    }

    public Integer updateTraceabilityInContractVariation(Integer traceabilityId, Integer workContractVariationId){

//        TraceabilityContractDocumentationService traceabilityContractDocumentationService = TraceabilityContractDocumentationService.TraceabilityContractDocumentationServiceFactory.getInstance();
//        TraceabilityContractDocumentationDBO traceabilityContractDocumentationDBO = traceabilityContractDocumentationService.findById(traceabilityId);
//
//        WorkContractService workContractService = WorkContractService.WorkContractServiceFactory.getInstance();
//        WorkContract workContract = workContractService.findById(workContractVariationId);
//        Integer workContractNumber = workContract.getContractNumber();
//        workContract.setTraceability(traceabilityContractDocumentationDBO);
//
//        Integer workContractId = workContractService.update(workContract);
//        if (workContractId != null) {
//            logger.info(">>>>> The variation of the work contract number " + workContractNumber + " has been updated with its corresponding traceability.");
//            return workContractId;
//        }
//
//        return null;
        return null;
    }
}
