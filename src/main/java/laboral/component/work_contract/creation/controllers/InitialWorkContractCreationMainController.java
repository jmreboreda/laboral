package laboral.component.work_contract.creation.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.person_management.events.NamePatternChangedEvent;
import laboral.component.work_contract.*;
import laboral.component.work_contract.creation.components.*;
import laboral.component.work_contract.creation.events.*;
import laboral.domain.client.Client;
import laboral.domain.client.controller.ClientController;
import laboral.domain.contract.WorkContract;
import laboral.domain.contract.WorkContractSituationRequest;
import laboral.domain.contract.work_contract.creation.InitialContractSituationEntryDataContainer;
import laboral.domain.contract.work_contract.WorkContractSituationCreator;
import laboral.domain.contract.work_contract.controller.WorkContractController;
import laboral.domain.contract.work_contract.documentation.NewContractDocumentationCreator;
import laboral.domain.contract_type.WorkContractType;
import laboral.domain.contract_type.controllers.ContractTypeController;
import laboral.domain.email.dto.EmailDataDTO;
import laboral.domain.employee.Employee;
import laboral.domain.employee.controller.EmployeeController;
import laboral.domain.employer.Employer;
import laboral.domain.employer.controller.EmployerController;
import laboral.domain.person.controller.PersonController;
import laboral.domain.person.persistence.dao.PersonDAO;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.service_gm.ServiceGMController;
import laboral.domain.services.AgentNotificator;
import laboral.domain.email.EmailConstants;
import laboral.domain.services.printer.Printer;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.utilities.events.DateChangedEvent;
import laboral.domain.utilities.utilities.Utilities;
import laboral.domain.work_center.WorkCenter;

import javax.mail.internet.AddressException;
import java.awt.print.PrinterException;
import java.io.IOException;
import java.nio.file.Path;
import java.text.Collator;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

public class InitialWorkContractCreationMainController extends AnchorPane {

    private static final Logger logger = Logger.getLogger(InitialWorkContractCreationMainController.class.getSimpleName());

    private static final String WORK_CONTRACT_MAIN_CONTROLLER_FXML = "/fxml/work_contract/creation/work_contract_main.fxml";
    private static final String COLOR_RED = "-fx-text-fill: #640000";
    private static final String COLOR_BLUE = "-fx-text-inner-color: #000FFF;";
    private static final String COLOR_GREEN = "-fx-text-fill: #185E03";
    private static final String EMPLOYEE_OF_NEW_CLIENT = "Empleado de nuevo cliente";
    private static final String ADVISORY_TEXT = "Asesoría";
    private static final String UNDEFINED_TEXT  = "Indefinida";

    Parent parent;

    private Boolean contractHasBeenSavedInDatabase = Boolean.FALSE;
    private Boolean contractHasBeenSentToContractAgent = Boolean.FALSE;

    PersonDAO personDAO = PersonDAO.PersonDAOFactory.getInstance();
    EmployerController employerController = new EmployerController();
    EmployeeController employeeController = new EmployeeController();
    ClientController clientController = new ClientController();
    PersonController personController = new PersonController();
    ServiceGMController serviceGMController = new ServiceGMController();
    WorkContractController workContractController = new WorkContractController();
    ContractTypeController contractTypeController = new ContractTypeController();

    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());

    @FXML
    private WorkContractHeader workContractHeader;
    @FXML
    private WorkContractPartsForm workContractPartsForm;
    @FXML
    private WorkContractDataForm workContractDataForm;
    @FXML
    private WorkContractScheduleForm workContractScheduleForm;
    @FXML
    private WorkContractPublicNotesForm workContractPublicNotesForm;
    @FXML
    private WorkContractPrivateNotesForm workContractPrivateNotesForm;
    @FXML
    private WorkContractCreationActionComponents workContractCreationActionComponents;
    @FXML
    private WorkContractProvisionalData workContractProvisionalData;

    public InitialWorkContractCreationMainController() {
        this.parent = ViewLoader.load(this, WORK_CONTRACT_MAIN_CONTROLLER_FXML);
    }

    @FXML
    public void initialize(){
        initWorkContractPartsForm();

        workContractDataForm.setOnWeeklyWorkHoursChange(this::onWeeklyWorkHoursChange);
        workContractDataForm.setOnContractTypeSelectorChange(this::onContractTypeSelectorChange);
        workContractDataForm.getUndefinedDuration().setOnAction(this::onUndefinedDurationChange);
        workContractDataForm.getTemporalDuration().setOnAction(this::onTemporalDurationChange);
        workContractDataForm.getFullTime().setOnAction(this::onFullTimeChange);
        workContractDataForm.getPartialTime().setOnAction(this::onPartialTimeChange);
        workContractDataForm.setOnDateFromChange(this::onDateFromChange);
        workContractDataForm.setOnDateToChange(this::onDateToChange);
        workContractDataForm.getDaysOfWeekSelector().getMondayCheck().setOnAction(this::onMondayChange);
        workContractDataForm.getDaysOfWeekSelector().getTuesdayCheck().setOnAction(this::onTuesdayChange);
        workContractDataForm.getDaysOfWeekSelector().getWednesdayCheck().setOnAction(this::onWednesdayChange);
        workContractDataForm.getDaysOfWeekSelector().getThursdayCheck().setOnAction(this::onThursdayChange);
        workContractDataForm.getDaysOfWeekSelector().getFridayCheck().setOnAction(this::onFridayChange);
        workContractDataForm.getDaysOfWeekSelector().getSaturdayCheck().setOnAction(this::onSaturdayChange);
        workContractDataForm.getDaysOfWeekSelector().getSundayCheck().setOnAction(this::onSundayChange);
        workContractDataForm.setOnLaborCategoryChange(this::onLaborCategoryChange);

        workContractScheduleForm.setOnChangeScheduleDuration(this::onChangeScheduleDuration);

        workContractCreationActionComponents.setOnOkButton(this::onOkButton);
        workContractCreationActionComponents.setOnSendMailButton(this::onSendMailButton);

        workContractCreationActionComponents.setOnExitButton(this::onExitButton);
        
        loadContractTypeSelector();
    }

    private void initWorkContractPartsForm() {
        workContractPartsForm.setOnEmployerPatternChanged(this::onEmployerNamePatternChange);
        workContractPartsForm.setOnEmployerSelectorChange(this::onEmployerSelectorChange);
        workContractPartsForm.setOnEmployeePatternChanged(this::onEmployeeNamePatternChange);
        workContractPartsForm.setOnEmployeeSelectorChange(this::onEmployeeSelectorChange);
        workContractPartsForm.setOnWorkCenterSelectorChange(this::onWorkCenterSelectorChange);
        workContractPartsForm.setOnQuoteAccountCodeSelectorChange(this::onQuoteAccountCodeSelectorChange);
    }

    public WorkContractPartsForm getWorkContractParts() {
        return workContractPartsForm;
    }

    public void setWorkContractParts(WorkContractPartsForm workContractPartsForm) {
        this.workContractPartsForm = workContractPartsForm;
    }

    public WorkContractDataForm getWorkContractData() {
        return workContractDataForm;
    }

    public void setWorkContractData(WorkContractDataForm workContractDataForm) {
        this.workContractDataForm = workContractDataForm;
    }

    public WorkContractProvisionalData getWorkContractProvisionalData() {
        return workContractProvisionalData;
    }

    public void setWorkContractProvisionalData(WorkContractProvisionalData workContractProvisionalData) {
        this.workContractProvisionalData = workContractProvisionalData;
    }

    public WorkContractScheduleForm getWorkContractSchedule() {
        return workContractScheduleForm;
    }

    public void setWorkContractSchedule(WorkContractScheduleForm workContractScheduleForm) {
        this.workContractScheduleForm = workContractScheduleForm;
    }

    public WorkContractPublicNotesForm getWorkContractPublicNotes() {
        return workContractPublicNotesForm;
    }

    public void setWorkContractPublicNotes(WorkContractPublicNotesForm workContractPublicNotesForm) {
        this.workContractPublicNotesForm = workContractPublicNotesForm;
    }

    public WorkContractPrivateNotesForm getWorkContractPrivateNotes() {
        return workContractPrivateNotesForm;
    }

    public void setWorkContractPrivateNotes(WorkContractPrivateNotesForm workContractPrivateNotesForm) {
        this.workContractPrivateNotesForm = workContractPrivateNotesForm;
    }

    public WorkContractCreationActionComponents getWorkContractActionComponents() {
        return workContractCreationActionComponents;
    }

    public void setWorkContractActionComponents(WorkContractCreationActionComponents workContractCreationActionComponents) {
        this.workContractCreationActionComponents = workContractCreationActionComponents;
    }

    private void loadContractTypeSelector(){
        List<WorkContractType> workContractTypeList = contractTypeController.findAll();

        Collator primaryCollator = Collator.getInstance(new Locale("es","ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        List<WorkContractType> orderedAndFilteredWorkContractTypeList = workContractTypeList
                .stream()
                .sorted(Comparator.comparing(WorkContractType::getColloquial, primaryCollator)).collect(Collectors.toList());

        ObservableList<WorkContractType> workContractTypeObservableList = FXCollections.observableList(orderedAndFilteredWorkContractTypeList);
        workContractDataForm.contractTypeSelectorRefresh(workContractTypeObservableList);
    }

    private void onEmployerNamePatternChange(NamePatternChangedEvent namePatternChangedEvent){
        String pattern = namePatternChangedEvent.getPattern();

        workContractPartsForm.getEmployerSelector().getSelectionModel().clearSelection();
        workContractPartsForm.getEmployerSelector().getItems().clear();
        workContractPartsForm.getEmployerSelector().setPromptText("");
        workContractPartsForm.getQuoteAccountCodeSelector().getSelectionModel().clearSelection();
        workContractPartsForm.getQuoteAccountCodeSelector().getItems().clear();

        if(pattern.isEmpty() || pattern.length() < 3){
            return;
        }

        List<Employer> employerList = employerController.findActiveEmployerByNamePattern(pattern);

        if (employerList.size() == 0) {
            return;
        }

        Collator primaryCollator = Collator.getInstance(new Locale("es","ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        List<Employer> orderedEmployerList = employerList
                .stream()
                .sorted(Comparator.comparing(Employer::toString, primaryCollator)).collect(Collectors.toList());

        ObservableList<Employer> employerObservableList = FXCollections.observableArrayList(orderedEmployerList);
        workContractPartsForm.employerSelectorRefresh(employerObservableList);
    }

    private void onEmployerSelectorChange(EmployerSelectorChangedEvent event){

        if(event.getSelectedEmployer() != null) {
            workContractProvisionalData.getEmployerLabel().setText(event.getSelectedEmployer().toAlphabeticalName());
            ObservableList<QuoteAccountCode> quoteAccountCodeObservableList = FXCollections.observableList(new ArrayList<>(event.getSelectedEmployer().getQuoteAccountCodes()));
            workContractPartsForm.quoteAccountCodeSelectorRefresh(quoteAccountCodeObservableList);

            Client client = clientController.findClientById(event.getSelectedEmployer().getClient().getClientId());
            List<WorkCenter> workCenterList = new ArrayList<>(client.getWorkCenters());

            workContractPartsForm.workCenterSelectorRefresh(FXCollections.observableList(workCenterList));
        }else{
            workContractPartsForm.getWorkCenterSelector().getSelectionModel().clearSelection();
            workContractPartsForm.getWorkCenterSelector().getItems().clear();
            workContractProvisionalData.getEmployerLabel().setText("");
            workContractProvisionalData.getQacLabel().setText("");
            workContractProvisionalData.getWorkCenterLabel().setText("");
        }
    }

    private void onWorkCenterSelectorChange(WorkCenterSelectorChangedEvent event){
        String workCenterAddress = event.getSelectedWorkCenter() == null ? "" : event.getSelectedWorkCenter().toString();
        workContractProvisionalData.getWorkCenterLabel().setText(workCenterAddress);
    }

    private void onQuoteAccountCodeSelectorChange(QuoteAccountCodeSelectorChangedEvent quoteAccountCodeSelectorChangedEvent){
        if(workContractPartsForm.getQuoteAccountCodeSelector().getSelectionModel().getSelectedItem() != null) {
            String qacText = quoteAccountCodeSelectorChangedEvent.getSelectedQuoteAccountCode().toCompleteNumber();
            Tooltip tooltip = new Tooltip(workContractPartsForm.getQuoteAccountCodeSelector().getSelectionModel().getSelectedItem().toString());
            tooltip.setStyle("-fx-background-color: blue; -fx-text-fill: white;");
            tooltip.setFont(Font.font("Noto Sans", 12));
            workContractPartsForm.getQuoteAccountCodeSelector().setTooltip(tooltip);
            workContractProvisionalData.getQacLabel().setText(qacText);
        }
    }

    private void onEmployeeNamePatternChange(NamePatternChangedEvent namePatternChangedEvent){
        String pattern = namePatternChangedEvent.getPattern();

        if(pattern.isEmpty()){
            workContractPartsForm.getEmployeeSelector().getSelectionModel().clearSelection();
            workContractPartsForm.getEmployeeSelector().getItems().clear();
            workContractPartsForm.getEmployeeSelector().setPromptText("");

            return;
        }

        List<Employee> employeeList = employeeController.findEmployeeByNamePattern(pattern);

        if (employeeList.size() == 0) {
            workContractPartsForm.getEmployeeSelector().getSelectionModel().clearSelection();
            workContractPartsForm.getEmployeeSelector().getItems().clear();
            workContractPartsForm.getEmployeeSelector().setPromptText("");

            return;
        }

        Collator primaryCollator = Collator.getInstance(new Locale("es","ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        List<Employee> orderedEmployeeList = employeeList
                .stream()
                .sorted(Comparator.comparing(Employee::toString, primaryCollator)).collect(Collectors.toList());

        ObservableList<Employee> employeeObservableList = FXCollections.observableArrayList(orderedEmployeeList);
        workContractPartsForm.employeeSelectorRefresh(employeeObservableList);
    }

    private void onEmployeeSelectorChange(EmployeeSelectorChangedEvent event) {
        if (event.getSelectedEmployee() != null) {
            workContractProvisionalData.getEmployeeLabel().setText(event.getSelectedEmployee().toString());
            if (event.getSelectedEmployee().getNieNif() != null) {
                List<PersonDBO> personDBOList = personDAO.findPersonByNieNif(event.getSelectedEmployee().getNieNif().getNif());
                workContractPartsForm.refreshEmployeeData(personDBOList.get(0));
            }
        }else {
            workContractProvisionalData.getEmployeeLabel().setText("");
            workContractPartsForm.clearEmployeeData();
        }
    }

    private void onContractTypeSelectorChange(ContractTypeSelectorChangedEvent event){
        workContractProvisionalData.getContractTypeLabel().setText(event.getSelectedContractType().toString());
        workContractDataForm.getDateFrom().setValue(LocalDate.now().minusDays(10L));
        workContractDataForm.getDateTo().setValue(LocalDate.now().minusDays(10L));

        workContractDataForm.getUndefinedDuration().setSelected(event.getSelectedContractType().getUndefined());
        workContractDataForm.getTemporalDuration().setSelected(event.getSelectedContractType().getTemporal());

        workContractDataForm.getFullTime().setSelected(event.getSelectedContractType().getFullTime());
        workContractDataForm.getPartialTime().setSelected(event.getSelectedContractType().getPartialTime());

        if(event.getSelectedContractType().getUndefined()){
            workContractDataForm.getDateTo().setValue(null);
            workContractDataForm.getDurationDays().setText("");
            workContractProvisionalData.getProvisionalDateTo().setText("");
            workContractProvisionalData.getProvisionalContractDurationDays().setText(UNDEFINED_TEXT);
        }else{
            workContractProvisionalData.getProvisionalContractDurationDays().setText("");
        }

        if(workContractDataForm.getFullTime().isSelected()){
            workContractProvisionalData.getProvisionalFullPartialTime().setText(event.getSelectedContractType().getFullPartialTimeText());
            workContractProvisionalData.getProvisionalWeeklyWorkHours().setText(String.valueOf(ApplicationConstants.DEFAULT_HOURS_FOR_FULL_TIME));
            workContractDataForm.getWeeklyWorkHours().setText(String.valueOf(ApplicationConstants.DEFAULT_HOURS_FOR_FULL_TIME));
        }

        if(workContractDataForm.getPartialTime().isSelected()){
            workContractProvisionalData.getProvisionalFullPartialTime().setText(event.getSelectedContractType().getFullPartialTimeText());
            workContractProvisionalData.getProvisionalWeeklyWorkHours().setText("00:00");
            workContractDataForm.getWeeklyWorkHours().setText("00:00");
        }
    }

    private void onUndefinedDurationChange(ActionEvent event){
        workContractDataForm.getUndefinedDuration().setSelected(workContractDataForm.getContractTypeSelector().getSelectionModel().getSelectedItem().getUndefined());
        workContractDataForm.getTemporalDuration().setSelected(workContractDataForm.getContractTypeSelector().getSelectionModel().getSelectedItem().getTemporal());
    }

    private void onTemporalDurationChange(ActionEvent event) {
        workContractDataForm.getTemporalDuration().setSelected(workContractDataForm.getContractTypeSelector().getSelectionModel().getSelectedItem().getTemporal());
        workContractDataForm.getUndefinedDuration().setSelected(workContractDataForm.getContractTypeSelector().getSelectionModel().getSelectedItem().getUndefined());
    }

    private void onDateFromChange(DateChangedEvent event){
        if(event.getDate() == null){
            workContractDataForm.getDurationDays().setText("");
            workContractProvisionalData.getProvisionalDateFrom().setText("");
            workContractDataForm.getDateFrom().requestFocus();

            return;
        }

//        if(event.getDate().isBefore(LocalDate.now().minusDays(4L))){
//            workContractData.getDateFrom().setStyle(COLOR_RED);
//        }else{
//            workContractData.getDateFrom().setStyle(COLOR_BLUE);
//        }

        workContractProvisionalData.getProvisionalDateFrom().setText(event.getDate().format(dateFormatter));
        calculateContractDurationDays();
    }

    private void onDateToChange(DateChangedEvent event){
        if(event.getDate() == null){
            workContractDataForm.getDurationDays().setText("");
            workContractProvisionalData.getProvisionalDateTo().setText("");
            return;
        }

        if(event.getDate().isBefore(LocalDate.now().minusDays(4L))){
            workContractDataForm.getDateTo().setStyle(COLOR_RED);
        }else{
            workContractDataForm.getDateTo().setStyle(COLOR_BLUE);
        }

        workContractProvisionalData.getProvisionalDateTo().setText(event.getDate().format(dateFormatter));
        calculateContractDurationDays();
    }

    private void onFullTimeChange(ActionEvent event){
        workContractDataForm.getFullTime().setSelected(workContractDataForm.getContractTypeSelector().getSelectionModel().getSelectedItem().getFullTime());
        workContractDataForm.getPartialTime().setSelected(workContractDataForm.getContractTypeSelector().getSelectionModel().getSelectedItem().getPartialTime());
    }

    private void onPartialTimeChange(ActionEvent event){
        workContractDataForm.getPartialTime().setSelected(workContractDataForm.getContractTypeSelector().getSelectionModel().getSelectedItem().getPartialTime());
        workContractDataForm.getFullTime().setSelected(workContractDataForm.getContractTypeSelector().getSelectionModel().getSelectedItem().getFullTime());
    }

    private void onWeeklyWorkHoursChange(WeeklyWorkHoursChangedEvent event){
        Duration duration = Utilities.timeStringToDurationConverter(event.getWeeklyWorkHours());
        if(duration == null ||  duration.compareTo(WorkContractParameters.LEGAL_MAXIMUM_HOURS_OF_WORK_PER_WEEK) >= 0L){
            workContractDataForm.getWeeklyWorkHours().setText("");
            workContractProvisionalData.getProvisionalWeeklyWorkHours().setText("");

            return;
        }

        if(duration != null){
            String weeklyWorkHours = Utilities.durationToTimeStringConverter(duration);
            workContractDataForm.getWeeklyWorkHours().setText(weeklyWorkHours);
            workContractProvisionalData.getProvisionalWeeklyWorkHours().setText(weeklyWorkHours);
        }else{
            workContractDataForm.getWeeklyWorkHours().setText("");
            workContractProvisionalData.getProvisionalWeeklyWorkHours().setText("");
        }
    }

    private void onMondayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalMonday().setSelected(workContractDataForm.getDaysOfWeekSelector().getMondayCheck().isSelected());
    }

    private void onTuesdayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalTuesday().setSelected(workContractDataForm.getDaysOfWeekSelector().getTuesdayCheck().isSelected());
    }

    private void onWednesdayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalWednesday().setSelected(workContractDataForm.getDaysOfWeekSelector().getWednesdayCheck().isSelected());
    }

    private void onThursdayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalThursday().setSelected(workContractDataForm.getDaysOfWeekSelector().getThursdayCheck().isSelected());
    }

    private void onFridayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalFriday().setSelected(workContractDataForm.getDaysOfWeekSelector().getFridayCheck().isSelected());
    }

    private void onSaturdayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalSaturday().setSelected(workContractDataForm.getDaysOfWeekSelector().getSaturdayCheck().isSelected());
    }

    private void onSundayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalSunday().setSelected(workContractDataForm.getDaysOfWeekSelector().getSundayCheck().isSelected());
    }

    private void onLaborCategoryChange(LaborCategoryChangedEvent event){
        workContractProvisionalData.getProvisionalLaborCategory().setText(event.getLaborCategory());
    }

    private void calculateContractDurationDays() {
        if (workContractDataForm.getUndefinedDuration().isSelected()) {
            return;
        }
        if (areDateToOrDateFromNull()) {
            return;
        }
        dateToIsBeforeDateFrom();
    }

    private void dateToIsBeforeDateFrom() {
        if (workContractDataForm.getDateTo().getValue().isBefore(workContractDataForm.getDateFrom().getValue())) {
            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractParameters.THERE_ARE_INCOHERENT_DATES_IN_CONTRACT);
            workContractDataForm.getDateFrom().setValue(null);
            workContractDataForm.getDateTo().setValue(null);
            workContractDataForm.getDurationDays().setText("");
            workContractProvisionalData.getProvisionalContractDurationDays().setText("");
        } else {
            long daysBetween = DAYS.between(workContractDataForm.getDateFrom().getValue(), workContractDataForm.getDateTo().getValue()) + 1L;
            String durationContractDays = String.format("%,d", daysBetween);
            workContractDataForm.getDurationDays().setText(durationContractDays);
            workContractProvisionalData.getProvisionalContractDurationDays().setText(durationContractDays);
        }
    }

    private boolean areDateToOrDateFromNull() {
        return workContractDataForm.getDateFrom().getValue() == null || workContractDataForm.getDateTo().getValue() == null;
    }

    private void onChangeScheduleDuration(ChangeScheduleDurationEvent event) {
        if (exceededLegalMaximumHours(event))
            return;

        exceededOnDeclaredHours(event);
    }

    private void exceededOnDeclaredHours(ChangeScheduleDurationEvent event) {
        Duration weeklyWorkHoursDuration = Utilities.timeStringToDurationConverter(workContractDataForm.getWeeklyWorkHours().getText()) == null ? Duration.ZERO
                : Utilities.timeStringToDurationConverter(workContractDataForm.getWeeklyWorkHours().getText());

        if (event.getContractScheduleTotalHoursDuration().compareTo(weeklyWorkHoursDuration) > 0) {
            Message.warningMessage((Stage) getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                    ContractMainControllerConstants.SCHEDULE_HOURS_GREATER_THAN_CONTRACT_HOURS);
        }
    }

    private boolean exceededLegalMaximumHours(ChangeScheduleDurationEvent event) {
        if (event.getContractScheduleTotalHoursDuration().compareTo(ContractConstants.LEGAL_MAXIMUM_HOURS_OF_WORK_PER_WEEK) > 0) {
            Message.warningMessage((Stage) getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                    ContractMainControllerConstants.EXCEEDED_MAXIMUM_LEGAL_WEEKLY_HOURS + " [" + Parameters.INTEGER_LEGAL_MAXIMUM_HOURS_OF_WORK_PER_WEEK + "]");
            return true;
        }
        return false;
    }

    private void lockInterfaceForChanges(){
        workContractPartsForm.setMouseTransparent(true);
        workContractDataForm.setMouseTransparent(true);
        workContractScheduleForm.setMouseTransparent(true);
        workContractPublicNotesForm.setMouseTransparent(true);
        workContractPrivateNotesForm.setMouseTransparent(true);
    }

    /*
    PRUEBA DE GUILLE
    private static final Map<ContractPartProblem, Consumer<Window>> problemsActuator = new HashMap<>();
    static {
        problemsActuator.put(ContractPartProblem.EMPLOYEE_NOT_DEFINED, window -> onEmployeeNotDefined((Stage) window));
    }

    private static void onEmployeeNotDefined(Stage window) {
        Message.errorMessage(window, Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVerifierConstants.EMPLOYER_IS_NOT_SELECTED);
    }
     */

    private void onOkButton(MouseEvent event){
        workContractProvisionalData.getStatus().setStyle(COLOR_RED);

        InitialContractSituationEntryDataContainer initialContractSituationEntryDataContainer = InitialContractSituationEntryDataContainer.NewContractSituationEntryDataContainerBuilder.create()
                .withContractSchedule(this.getWorkContractSchedule().retrieveWorkContractSchedule())
                .withEmployee(this.getWorkContractParts().getEmployeeSelector().getSelectionModel().getSelectedItem())
                .withWorkCenter(this.getWorkContractParts().getWorkCenterSelector().getSelectionModel().getSelectedItem())
                .withClientDateNotification(this.getWorkContractData().getNotificationDate().getValue())
                .withClientHourNotification(this.getWorkContractData().getNotificationHour().getText())
                .withDateFrom(this.getWorkContractData().getDateFrom().getValue())
                .withDateTo(this.getWorkContractData().getDateTo().getValue())
                .withEmployer(this.getWorkContractParts().getEmployerSelector().getSelectionModel().getSelectedItem())
                .withPrivateNotes(this.getWorkContractPrivateNotes().getPrivateNotes().getText())
                .withQuoteAccountCode(this.getWorkContractParts().getQuoteAccountCodeSelector().getSelectionModel().getSelectedItem())
                .withPublicNotes(this.getWorkContractPublicNotes().getPublicNotes().getText())
                .withWorkContractType(this.getWorkContractData().getContractTypeSelector().getSelectionModel().getSelectedItem())
                .withVariationType(null)
                .withLaborCategory(this.getWorkContractData().getLaborCategory().getText())
                .withScheduleOneList(this.workContractScheduleForm.getScheduleOneList())
                .withScheduleTwoList(this.workContractScheduleForm.getScheduleTwoList())
                .withScheduleThreeList(this.workContractScheduleForm.getScheduleThreeList())
                .withScheduleFourList(this.workContractScheduleForm.getScheduleFourList())
                .withTableColumnDayOfWeekData(this.workContractScheduleForm.getTableColumnDayOfWeekData())
                .withDurationDays(this.getWorkContractData().getDurationDays().getText())
                .withFullTime(this.getWorkContractData().getFullTime().isSelected())
                .withPartialTime(this.getWorkContractData().getPartialTime().isSelected())
                .withTemporalDuration(this.getWorkContractData().getTemporalDuration().isSelected())
                .withUndefinedDuration(this.getWorkContractData().getUndefinedDuration().isSelected())
                .withDaysOfWeekSelector(this.getWorkContractData().getDaysOfWeekSelector())
                .withWeeklyWorkHours(this.getWorkContractData().getWeeklyWorkHours().getText())
                .build();

        InitialContractDataEntryVerifier initialContractDataEntryVerifier = new InitialContractDataEntryVerifier();
        Boolean isCorrectDataEntry = initialContractDataEntryVerifier.verify(initialContractSituationEntryDataContainer, this.getScene());

/*
        PRUEBA DE GUILLE
        Set<ContractPartProblem> problems = initialContractSituationEntryDataContainer.hasProblems();
        if(!problems.isEmpty()) {
            problems.forEach(p -> problemsActuator.get(p).accept(this.getScene().getWindow()));
        }
*/

        if(!isCorrectDataEntry){
            workContractProvisionalData.getStatus().setText(ContractConstants.REVISION_WITH_ERRORS);
            return;
        }

        workContractProvisionalData.getStatus().setStyle(COLOR_GREEN);
        workContractProvisionalData.getStatus().setText(ContractConstants.REVISION_WITHOUT_ERRORS);

        establishPersistence(initialContractSituationEntryDataContainer);
    }

    private void onExitButton(MouseEvent event){
        if(contractHasBeenSavedInDatabase){
            if(!contractHasBeenSentToContractAgent){
                if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ContractMainControllerConstants.CONTRACT_SAVED_BUT_NOT_SENDED_TO_CONTRACT_AGENT)){
                    return;
                }
            }
        }
        Stage stage = (Stage) this.getScene().getWindow();
        stage.close();
    }

    private void onSendMailButton(MouseEvent event){
        Boolean isSendOk = false;
        Path pathOut;

        if (Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ContractMainControllerConstants.QUESTION_SEND_MAIL_TO_CONTRACT_AGENT)) {
            workContractCreationActionComponents.getSendMailButton().setDisable(true);
            
            InitialContractSituationEntryDataContainer initialContractSituationEntryDataContainer = InitialContractSituationEntryDataContainer.NewContractSituationEntryDataContainerBuilder.create()
                    .withContractSchedule(this.getWorkContractSchedule().retrieveWorkContractSchedule())
                    .withEmployee(this.getWorkContractParts().getEmployeeSelector().getSelectionModel().getSelectedItem())
                    .withWorkCenter(this.getWorkContractParts().getWorkCenterSelector().getSelectionModel().getSelectedItem())
                    .withClientDateNotification(this.getWorkContractData().getNotificationDate().getValue())
                    .withClientHourNotification(this.getWorkContractData().getNotificationHour().getText())
                    .withDateFrom(this.getWorkContractData().getDateFrom().getValue())
                    .withDateTo(this.getWorkContractData().getDateTo().getValue())
                    .withEmployer(this.getWorkContractParts().getEmployerSelector().getSelectionModel().getSelectedItem())
                    .withPrivateNotes(this.getWorkContractPrivateNotes().getPrivateNotes().getText())
                    .withQuoteAccountCode(this.getWorkContractParts().getQuoteAccountCodeSelector().getSelectionModel().getSelectedItem())
                    .withPublicNotes(this.getWorkContractPublicNotes().getPublicNotes().getText())
                    .withWorkContractType(this.getWorkContractData().getContractTypeSelector().getSelectionModel().getSelectedItem())
                    .withVariationType(null)
                    .withLaborCategory(this.getWorkContractData().getLaborCategory().getText())
                    .withScheduleOneList(this.workContractScheduleForm.getScheduleOneList())
                    .withScheduleTwoList(this.workContractScheduleForm.getScheduleTwoList())
                    .withScheduleThreeList(this.workContractScheduleForm.getScheduleThreeList())
                    .withScheduleFourList(this.workContractScheduleForm.getScheduleFourList())
                    .withTableColumnDayOfWeekData(this.workContractScheduleForm.getTableColumnDayOfWeekData())
                    .withDurationDays(this.getWorkContractData().getDurationDays().getText())
                    .withFullTime(this.getWorkContractData().getFullTime().isSelected())
                    .withPartialTime(this.getWorkContractData().getPartialTime().isSelected())
                    .withTemporalDuration(this.getWorkContractData().getTemporalDuration().isSelected())
                    .withUndefinedDuration(this.getWorkContractData().getUndefinedDuration().isSelected())
                    .withDaysOfWeekSelector(this.getWorkContractData().getDaysOfWeekSelector())
                    .withWeeklyWorkHours(this.getWorkContractData().getWeeklyWorkHours().getText())
                    .build();

            WorkContractSituationCreator workContractSituationCreator = new WorkContractSituationCreator();
            WorkContractSituationRequest workContractSituationRequest = workContractSituationCreator.newWorkContractSituationRequestGenerator(initialContractSituationEntryDataContainer);

            pathOut =  contractDataSubfolderAgentCommunicationCreate(workContractSituationRequest);

            String attachedFileName = workContractSituationRequest.toFileName().concat(ApplicationConstants.PDF_EXTENSION.getValue());

            AgentNotificator agentNotificator = new AgentNotificator();

            EmailDataDTO emailDataDTO = retrieveDateForEmailCreation(pathOut, attachedFileName);

            try {
                isSendOk = agentNotificator.sendEmailToContractsAgent(emailDataDTO);
            } catch (AddressException e) {
                e.printStackTrace();
            }
            if(isSendOk){

                Message.warningMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, EmailConstants.MAIL_SEND_OK);
                contractHasBeenSentToContractAgent = true;
                getWorkContractActionComponents().getSendMailButton().setDisable(Boolean.TRUE);

            }else{
                Message.warningMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, EmailConstants.MAIL_NOT_SEND_OK);
            }
        }
    }

    private void establishPersistence(InitialContractSituationEntryDataContainer initialContractSituationEntryDataContainer) {
        if (Message.confirmationMessage((Stage) this.getScene().getWindow(),
                Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVerifierConstants.QUESTION_SAVE_NEW_CONTRACT)) {

            workContractCreationActionComponents.getSendMailButton().setDisable(true);
//            workContractCreationActionComponents.getViewPDFButton().setDisable(true);
            workContractCreationActionComponents.getOkButton().setDisable(true);

            persistNewInitialWorkContract(initialContractSituationEntryDataContainer);
        }
    }

    private void persistNewInitialWorkContract(InitialContractSituationEntryDataContainer initialContractSituationEntryDataContainer){
        WorkContractSituationCreator workContractSituationCreator = new WorkContractSituationCreator();
        WorkContractSituationRequest workContractSituationRequest = workContractSituationCreator.newWorkContractSituationRequestGenerator(initialContractSituationEntryDataContainer);

        Integer newWorkContractId = workContractSituationCreator.persistWorkContract(workContractSituationRequest);
        if(newWorkContractId != null){
            WorkContract workContract = workContractController.findById(newWorkContractId);
            Integer workContractNumber = workContract.getContractNumber();
            Message.informationMessage((Stage) getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                    ContractMainControllerConstants.CONTRACT_SAVED_OK + workContractNumber);
            workContractProvisionalData.getWorkContractSavedOkText().setText(ContractMainControllerConstants.CONTRACT_SAVED_OK + workContractNumber);
            contractHasBeenSavedInDatabase = Boolean.TRUE;
            lockInterfaceForChanges();

            contractDataSubfolderCreate(workContractSituationRequest);
            contractDataSubFolderHistoryCreate(workContractSituationRequest);

            workContractCreationActionComponents.getSendMailButton().setDisable(false);
            workContractCreationActionComponents.getViewPDFButton().setDisable(false);
            workContractCreationActionComponents.getOkButton().setDisable(true);
        }else{
            Message.errorMessage((Stage) getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                    ContractMainControllerConstants.CONTRACT_NOT_SAVED_OK);
            lockInterfaceForChanges();
        }
    }

    private void contractDataSubfolderCreate(WorkContractSituationRequest workContractSituationRequest) {
        NewContractDocumentationCreator newContractDocumentationCreator = new NewContractDocumentationCreator();
        Path path = newContractDocumentationCreator.retrievePathToContractDataSubFolderPDF(workContractSituationRequest);
        printSubFolderOfTheInitialContract(path);
    }

    private void contractDataSubFolderHistoryCreate(WorkContractSituationRequest workContractSituationRequest){
        NewContractDocumentationCreator newContractDocumentationCreator = new NewContractDocumentationCreator();
        Path path = newContractDocumentationCreator.retrievePathToContractDataSubFolderHistoryPDF(workContractSituationRequest);
        printSubFolderHistoryContract(path);

    }

    private Path contractDataSubfolderAgentCommunicationCreate(WorkContractSituationRequest workContractSituationRequest) {
        NewContractDocumentationCreator newContractDocumentationCreator = new NewContractDocumentationCreator();
        return newContractDocumentationCreator.retrievePathToContractDataToContractAgentPDF(workContractSituationRequest);
    }

    public void printSubFolderOfTheInitialContract(Path path) {

        Map<String, String> attributes = new HashMap<>();
        attributes.put("papersize", "A3");
        attributes.put("sides", "ONE_SIDED");
        attributes.put("chromacity", "MONOCHROME");
        attributes.put("orientation", "LANDSCAPE");

        try {
            String printOk = Printer.printPDF(path.toString(), attributes);
            Message.informationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ContractConstants.CONTRACT_DATA_SUBFOLFER_TO_PRINTER_OK);
            if (!printOk.equals("ok")) {
                Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, Parameters.NO_PRINTER_FOR_THESE_ATTRIBUTES);
            }
        } catch (IOException | PrinterException e) {
            e.printStackTrace();
        }
    }

    public void printSubFolderHistoryContract(Path path){

        Map<String, String> attributes = new HashMap<>();
        attributes.put("papersize", "A3");
        attributes.put("sides", "ONE_SIDED");
        attributes.put("chromacity", "MONOCHROME");
        attributes.put("orientation", "LANDSCAPE");

        try {
            String printOk = Printer.printPDF(path.toString(), attributes);
            Message.informationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ContractConstants.SUBFOLFER_RECORD_OF_CONTRACT_HISTORY_TO_PRINTER_OK);
            if(!printOk.equals("ok")){
                Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, Parameters.NO_PRINTER_FOR_THESE_ATTRIBUTES);
            }
        } catch (IOException | PrinterException e) {
            e.printStackTrace();
        }
    }

    private EmailDataDTO retrieveDateForEmailCreation(Path path, String attachedFileName){

        Employer employer = getWorkContractParts().getEmployerSelector().getSelectionModel().getSelectedItem();
        Employee employee = getWorkContractParts().getEmployeeSelector().getSelectionModel().getSelectedItem();
        String variationTypeText = EmailConstants.STANDARD_NEW_CONTRACT_TEXT;
        return EmailDataDTO.create()
                .withPath(path)
                .withFileName(attachedFileName)
                .withEmployer(employer)
                .withEmployee(employee)
                .withVariationTypeText(variationTypeText)
                .build();
    }
}
