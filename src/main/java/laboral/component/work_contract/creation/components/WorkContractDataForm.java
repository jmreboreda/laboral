package laboral.component.work_contract.creation.components;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import javafx.util.converter.LocalDateStringConverter;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.client_management.client_modification.generic_components.DaysOfWeekSelector;
import laboral.component.generic_component.TimeInput24HoursClock;
import laboral.component.work_contract.creation.events.ContractTypeSelectorChangedEvent;
import laboral.component.work_contract.creation.events.LaborCategoryChangedEvent;
import laboral.component.work_contract.creation.events.WeeklyWorkHoursChangedEvent;
import laboral.domain.contract_type.WorkContractType;
import laboral.domain.utilities.events.DateChangedEvent;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Set;

public class WorkContractDataForm extends AnchorPane {

    private static final String WORK_CONTRACT_DATA_FXML = "/fxml/work_contract/creation/work_contract_data.fxml";

    Parent parent;

    private EventHandler<ContractTypeSelectorChangedEvent> contractTypeSelectorChangedEventEventHandler;
    private EventHandler<DateChangedEvent> localDateFromEventHandler;
    private EventHandler<DateChangedEvent> localDateToEventHandler;
    private EventHandler<WeeklyWorkHoursChangedEvent> weeklyWorkHoursChangedEventEventHandler;
    private EventHandler<LaborCategoryChangedEvent> laborCategoryChangedEventEventHandler;

    @FXML
    private Tab partsTab;

    @FXML
    private Tab contractTab;
    @FXML
    private ComboBox<WorkContractType> contractTypeSelector;
    @FXML
    private DatePicker notificationDate;
    @FXML
    private TimeInput24HoursClock notificationHour;
    @FXML
    private RadioButton undefinedDuration;
    @FXML
    private RadioButton temporalDuration;
    @FXML
    private DatePicker dateFrom;
    @FXML
    private DatePicker dateTo;
    @FXML
    private TextField durationDays;
    @FXML
    private RadioButton fullTime;
    @FXML
    private RadioButton partialTime;
    @FXML
    private TextField weeklyWorkHours;
    @FXML
    private DaysOfWeekSelector daysOfWeekSelector;
    @FXML
    private CheckBox mondayCheck;
    @FXML
    private CheckBox tuesdayCheck;
    @FXML
    private CheckBox wednesdayCheck;
    @FXML
    private CheckBox thursdayCheck;
    @FXML
    private CheckBox fridayCheck;
    @FXML
    private CheckBox saturdayCheck;
    @FXML
    private CheckBox sundayCheck;

    @FXML
    private TextField laborCategory;

    public WorkContractDataForm() {
        this.parent = ViewLoader.load(this, WORK_CONTRACT_DATA_FXML);
    }

    @FXML
    public void initialize(){

        undefinedDuration.disableProperty().bind(contractTypeSelector.getSelectionModel().selectedItemProperty().isNull());
        temporalDuration.disableProperty().bind(contractTypeSelector.getSelectionModel().selectedItemProperty().isNull());
        dateFrom.disableProperty().bind(contractTypeSelector.getSelectionModel().selectedItemProperty().isNull());
        dateTo.disableProperty().bind(contractTypeSelector.getSelectionModel().selectedItemProperty().isNull());
        durationDays.disableProperty().bind(getUndefinedDuration().selectedProperty());
        durationDays.disableProperty().bind(contractTypeSelector.getSelectionModel().selectedItemProperty().isNull());

        fullTime.disableProperty().bind(contractTypeSelector.getSelectionModel().selectedItemProperty().isNull());
        partialTime.disableProperty().bind(contractTypeSelector.getSelectionModel().selectedItemProperty().isNull());
        weeklyWorkHours.disableProperty().bind(contractTypeSelector.getSelectionModel().selectedItemProperty().isNull());
        weeklyWorkHours.editableProperty().bind(partialTime.selectedProperty());

        dateTo.disableProperty().bind(temporalDuration.selectedProperty().not());
        weeklyWorkHours.editableProperty().bind(partialTime.selectedProperty());

        laborCategory.disableProperty().bind(contractTypeSelector.getSelectionModel().selectedItemProperty().isNull());

        contractTypeSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item == null){
                    setFont(Font.font ("Noto Sans", 13));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 12));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.CENTER);
                    setText(item.toString());
                }
            }

        });

        contractTypeSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<WorkContractType> call(ListView<WorkContractType> param) {
                        final ListCell<WorkContractType> cell = new ListCell<WorkContractType>() {
                            @Override
                            public void updateItem(WorkContractType item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 13));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }
                });

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());
        dateFrom.setConverter(new LocalDateStringConverter(dateFormatter, null));
        dateFrom.valueProperty().addListener((ov, oldValue, newValue) -> {
            if(newValue != null) {
                onDateFromChange();
            }
        });

        dateTo.setConverter(new LocalDateStringConverter(dateFormatter, null));
        dateTo.valueProperty().addListener((ov, oldValue, newValue) -> {
            if(newValue != null) {
                onDateToChange();
            }
        });

        notificationDate.setConverter(new LocalDateStringConverter(dateFormatter, null));
        notificationDate.setValue(LocalDate.now());

        contractTypeSelector.setOnAction(this::onContractTypeChange);

        weeklyWorkHours.setOnAction(this::onWeeklyWorkHoursChangeByEnterKey);
        weeklyWorkHours.setOnKeyPressed(this::onWeeklyWorkHoursChangeByTabKey);
        laborCategory.setOnKeyReleased(this::onLaborCategoryChange);
    }

    public Tab getPartsTab() {
        return partsTab;
    }

    public void setPartsTab(Tab partsTab) {
        this.partsTab = partsTab;
    }

    public DatePicker getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(DatePicker notificationDate) {
        this.notificationDate = notificationDate;
    }

    public TimeInput24HoursClock getNotificationHour() {
        return notificationHour;
    }

    public void setNotificationHour(TimeInput24HoursClock notificationHour) {
        this.notificationHour = notificationHour;
    }

    public DatePicker getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(DatePicker dateFrom) {
        this.dateFrom = dateFrom;
    }

    public DatePicker getDateTo() {
        return dateTo;
    }

    public void setDateTo(DatePicker dateTo) {
        this.dateTo = dateTo;
    }

    public ComboBox<WorkContractType> getContractTypeSelector() {
        return contractTypeSelector;
    }

    public void setContractTypeSelector(ComboBox<WorkContractType> contractTypeSelector) {
        this.contractTypeSelector = contractTypeSelector;
    }

     public RadioButton getUndefinedDuration() {
        return undefinedDuration;
    }

    public void setUndefinedDuration(RadioButton undefinedDuration) {
        this.undefinedDuration = undefinedDuration;
    }

    public RadioButton getTemporalDuration() {
        return temporalDuration;
    }

    public void setTemporalDuration(RadioButton temporalDuration) {
        this.temporalDuration = temporalDuration;
    }

    public TextField getDurationDays() {
        return durationDays;
    }

    public void setDurationDays(TextField durationDays) {
        this.durationDays = durationDays;
    }

    public RadioButton getFullTime() {
        return fullTime;
    }

    public void setFullTime(RadioButton fullTime) {
        this.fullTime = fullTime;
    }

    public RadioButton getPartialTime() {
        return partialTime;
    }

    public void setPartialTime(RadioButton partialTime) {
        this.partialTime = partialTime;
    }

    public TextField getWeeklyWorkHours() {
        return weeklyWorkHours;
    }

    public void setWeeklyWorkHours(TextField weeklyWorkHours) {
        this.weeklyWorkHours = weeklyWorkHours;
    }

    public DaysOfWeekSelector getDaysOfWeekSelector() {
        return daysOfWeekSelector;
    }

    public void setDaysOfWeekSelector(DaysOfWeekSelector daysOfWeekSelector) {
        this.daysOfWeekSelector = daysOfWeekSelector;
    }

    public Set<DayOfWeek> getDaysOfWeekToWork(){
        return this.getDaysOfWeekSelector().getDaysOfWeek();
    }

    public CheckBox getMondayCheck() {
        return mondayCheck;
    }

    public CheckBox getTuesdayCheck() {
        return tuesdayCheck;
    }

    public CheckBox getWednesdayCheck() {
        return wednesdayCheck;
    }

    public CheckBox getThursdayCheck() {
        return thursdayCheck;
    }

    public CheckBox getFridayCheck() {
        return fridayCheck;
    }

    public CheckBox getSaturdayCheck() {
        return saturdayCheck;
    }

    public CheckBox getSundayCheck() {
        return sundayCheck;
    }

    public TextField getLaborCategory() {
        return laborCategory;
    }

    public void setLaborCategory(TextField laborCategory) {
        this.laborCategory = laborCategory;
    }

    public Tab getContractTab() {
        return contractTab;
    }

    public void setContractTab(Tab contractTab) {
        this.contractTab = contractTab;
    }

    public void contractTypeSelectorRefresh(ObservableList<WorkContractType> workContractTypeObservableList){
        contractTypeSelector.getItems().addAll(workContractTypeObservableList);
    }

    private void onContractTypeChange(ActionEvent event){
        contractTypeSelectorChangedEventEventHandler.handle(new ContractTypeSelectorChangedEvent((contractTypeSelector.getSelectionModel().getSelectedItem())));
    }

    private void onDateFromChange(){
        if(dateFrom.getValue() != null) {
            localDateFromEventHandler.handle(new DateChangedEvent(dateFrom.getValue()));
        }
    }

    private void onDateToChange(){
        localDateToEventHandler.handle(new DateChangedEvent(dateTo.getValue()));
    }

    private void onWeeklyWorkHoursChangeByEnterKey(ActionEvent event){
        weeklyWorkHoursChangedEventEventHandler.handle(new WeeklyWorkHoursChangedEvent(getWeeklyWorkHours().getText()));
    }

    private void onWeeklyWorkHoursChangeByTabKey(KeyEvent event){
        if(event.getCode() == KeyCode.TAB){
            onWeeklyWorkHoursChangeByEnterKey(new ActionEvent());
        }
    }

    private void onLaborCategoryChange(KeyEvent event){
        laborCategoryChangedEventEventHandler.handle(new LaborCategoryChangedEvent(getLaborCategory().getText()));
    }

    public void setOnContractTypeSelectorChange(EventHandler<ContractTypeSelectorChangedEvent> contractTypeSelectorChangedEventEventHandler){
        this.contractTypeSelectorChangedEventEventHandler = contractTypeSelectorChangedEventEventHandler;
    }

    public void setOnDateFromChange(EventHandler<DateChangedEvent> localDateFromEventHandler){
        this.localDateFromEventHandler = localDateFromEventHandler;
    }

    public void setOnDateToChange(EventHandler<DateChangedEvent> localDateToEventHandler){
        this.localDateToEventHandler = localDateToEventHandler;
    }

    public void setOnWeeklyWorkHoursChange(EventHandler<WeeklyWorkHoursChangedEvent> weeklyWorkHoursChangedEventEventHandler) {
        this.weeklyWorkHoursChangedEventEventHandler = weeklyWorkHoursChangedEventEventHandler;
    }

    public void setOnLaborCategoryChange(EventHandler<LaborCategoryChangedEvent> laborCategoryChangedEventEventHandler){
        this.laborCategoryChangedEventEventHandler = laborCategoryChangedEventEventHandler;
    }
}
