package laboral.component.work_contract.creation.components;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;


public class WorkContractProvisionalData extends AnchorPane {

    private static final String WORK_CONTRACT_PROVISIONAL_DATA_FXML = "/fxml/work_contract/creation/work_contract_provisional_data.fxml";

    Parent parent;

    @FXML
    private Label employerLabel;
    @FXML
    private Label qacLabel;
    @FXML
    private Label workCenterLabel;
    @FXML
    private Label employeeLabel;
    @FXML
    private Label contractTypeLabel;
    @FXML
    private Label provisionalDateFrom;
    @FXML
    Label provisionalDateTo;
    @FXML
    private CheckBox provisionalMonday;
    @FXML
    private CheckBox provisionalTuesday;
    @FXML
    private CheckBox provisionalWednesday;
    @FXML
    private CheckBox provisionalThursday;
    @FXML
    private CheckBox provisionalFriday;
    @FXML
    private CheckBox provisionalSaturday;
    @FXML
    private CheckBox provisionalSunday;
    @FXML
    private Label provisionalFullPartialTime;
    @FXML
    private Label provisionalWeeklyWorkHours;
    @FXML
    private Label provisionalContractDurationDays;
    @FXML
    private Label provisionalLaborCategory;
    @FXML
    private Label status;
    @FXML
    private Label workContractSavedOkText;

    public WorkContractProvisionalData() {
        this.parent = ViewLoader.load(this, WORK_CONTRACT_PROVISIONAL_DATA_FXML);
    }

    @FXML
    public void initialize(){

    }

    public Label getEmployerLabel() {
        return employerLabel;
    }

    public void setEmployerLabel(Label employerLabel) {
        this.employerLabel = employerLabel;
    }

    public Label getQacLabel() {
        return qacLabel;
    }

    public void setQacLabel(Label qacLabel) {
        this.qacLabel = qacLabel;
    }

    public Label getWorkCenterLabel() {
        return workCenterLabel;
    }

    public void setWorkCenterLabel(Label workCenterLabel) {
        this.workCenterLabel = workCenterLabel;
    }

    public Label getEmployeeLabel() {
        return employeeLabel;
    }

    public void setEmployeeLabel(Label employeeLabel) {
        this.employeeLabel = employeeLabel;
    }

    public Label getContractTypeLabel() {
        return contractTypeLabel;
    }

    public void setContractTypeLabel(Label contractTypeLabel) {
        this.contractTypeLabel = contractTypeLabel;
    }

    public Label getProvisionalDateFrom() {
        return provisionalDateFrom;
    }

    public void setProvisionalDateFrom(Label provisionalDateFrom) {
        this.provisionalDateFrom = provisionalDateFrom;
    }

    public Label getProvisionalDateTo() {
        return provisionalDateTo;
    }

    public void setProvisionalDateTo(Label provisionalDateTo) {
        this.provisionalDateTo = provisionalDateTo;
    }

    public CheckBox getProvisionalMonday() {
        return provisionalMonday;
    }

    public void setProvisionalMonday(CheckBox provisionalMonday) {
        this.provisionalMonday = provisionalMonday;
    }

    public CheckBox getProvisionalTuesday() {
        return provisionalTuesday;
    }

    public void setProvisionalTuesday(CheckBox provisionalTuesday) {
        this.provisionalTuesday = provisionalTuesday;
    }

    public CheckBox getProvisionalWednesday() {
        return provisionalWednesday;
    }

    public void setProvisionalWednesday(CheckBox provisionalWednesday) {
        this.provisionalWednesday = provisionalWednesday;
    }

    public CheckBox getProvisionalThursday() {
        return provisionalThursday;
    }

    public void setProvisionalThursday(CheckBox provisionalThursday) {
        this.provisionalThursday = provisionalThursday;
    }

    public CheckBox getProvisionalFriday() {
        return provisionalFriday;
    }

    public void setProvisionalFriday(CheckBox provisionalFriday) {
        this.provisionalFriday = provisionalFriday;
    }

    public CheckBox getProvisionalSaturday() {
        return provisionalSaturday;
    }

    public void setProvisionalSaturday(CheckBox provisionalSaturday) {
        this.provisionalSaturday = provisionalSaturday;
    }

    public CheckBox getProvisionalSunday() {
        return provisionalSunday;
    }

    public void setProvisionalSunday(CheckBox provisionalSunday) {
        this.provisionalSunday = provisionalSunday;
    }

    public Label getProvisionalFullPartialTime() {
        return provisionalFullPartialTime;
    }

    public void setProvisionalFullPartialTime(Label provisionalFullPartialTime) {
        this.provisionalFullPartialTime = provisionalFullPartialTime;
    }

    public Label getProvisionalContractDurationDays() {
        return provisionalContractDurationDays;
    }

    public void setProvisionalContractDurationDays(Label provisionalContractDurationDays) {
        this.provisionalContractDurationDays = provisionalContractDurationDays;
    }

    public Label getProvisionalLaborCategory() {
        return provisionalLaborCategory;
    }

    public void setProvisionalLaborCategory(Label provisionalLaborCategory) {
        this.provisionalLaborCategory = provisionalLaborCategory;
    }

    public Label getProvisionalWeeklyWorkHours() {
        return provisionalWeeklyWorkHours;
    }

    public void setProvisionalWeeklyWorkHours(Label provisionalWeeklyWorkHours) {
        this.provisionalWeeklyWorkHours = provisionalWeeklyWorkHours;
    }

    public Label getStatus() {
        return status;
    }

    public void setStatus(Label status) {
        this.status = status;
    }

    public Label getWorkContractSavedOkText() {
        return workContractSavedOkText;
    }

    public void setWorkContractSavedOkText(Label workContractSavedOkText) {
        this.workContractSavedOkText = workContractSavedOkText;
    }
}
