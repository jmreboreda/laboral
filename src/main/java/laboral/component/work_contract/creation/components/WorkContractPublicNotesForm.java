package laboral.component.work_contract.creation.components;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class WorkContractPublicNotesForm extends AnchorPane {

    private static final String PUBLIC_NOTES_FXML = "/fxml/work_contract/creation/work_contract_public_notes.fxml";

    private static final String STANDARD_TEXT_OF_APPORTIONMENT_EXTRAORDINARY_PAYMENTS = "Pagas extraordinarias prorrateadas.";

    private Parent parent;

    @FXML
    private TextArea taPublicNotes;

    public WorkContractPublicNotesForm() {
        this.parent = ViewLoader.load(this, PUBLIC_NOTES_FXML);
    }

    @FXML
    public void initialize(){

        taPublicNotes.setStyle("-fx-text-fill: #000FFF"); //#1807ac
        taPublicNotes.setText(STANDARD_TEXT_OF_APPORTIONMENT_EXTRAORDINARY_PAYMENTS);
    }

    public TextArea getPublicNotes(){
        return taPublicNotes;
    }
}

