package laboral.component.work_contract.creation.components;

import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class WorkContractHeader extends AnchorPane {

    private static final String WORK_CONTRACT_HEADER_FXML = "/fxml/work_contract/creation/work_contract_header.fxml";

    Parent parent;


    public WorkContractHeader() {
        this.parent = ViewLoader.load(this, WORK_CONTRACT_HEADER_FXML);
    }
}
