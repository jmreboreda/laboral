package laboral.component.work_contract.creation.components;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import laboral.component.ViewLoader;
import laboral.component.person_management.events.NamePatternChangedEvent;
import laboral.component.work_contract.creation.events.EmployeeSelectorChangedEvent;
import laboral.component.work_contract.creation.events.EmployerSelectorChangedEvent;
import laboral.component.work_contract.creation.events.QuoteAccountCodeSelectorChangedEvent;
import laboral.component.work_contract.creation.events.WorkCenterSelectorChangedEvent;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.employee.Employee;
import laboral.domain.employer.Employer;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.work_center.WorkCenter;

import java.time.format.DateTimeFormatter;

public class WorkContractPartsForm extends AnchorPane {

    private static final String CONTRACT_PARTS_FXML = "/fxml/work_contract/creation/work_contract_parts.fxml";

    private EventHandler<MouseEvent> onNewPersonEventHandler;
    private EventHandler<NamePatternChangedEvent> employerNamePatternEventEventHandler;
    private EventHandler<EmployerSelectorChangedEvent> employerSelectorChangedEventEventHandler;
    private EventHandler<WorkCenterSelectorChangedEvent> workCenterSelectorChangedEventEventHandler;
    private EventHandler<NamePatternChangedEvent> employeeNamePatternEventEventHandler;
    private EventHandler<QuoteAccountCodeSelectorChangedEvent> quoteAccountCodeSelectorChangeEvent;
    private EventHandler<EmployeeSelectorChangedEvent> employeeSelectorChangedEventEventHandler;


    private Parent parent;

    @FXML
    private Tab partsTab;

    @FXML
    private TextField employerPattern;
    @FXML
    private ComboBox<Employer> employerSelector;

    @FXML
    private ComboBox<WorkCenter> workCenterSelector;

    @FXML
    private TextField employeePattern;
    @FXML
    private ComboBox<Employee> employeeSelector;

    @FXML
    private ComboBox<QuoteAccountCode> quoteAccountCodeSelector;
    @FXML
    private TextField nifField;
    @FXML
    private TextField nassField;
    @FXML
    private TextField birthDateField;
    @FXML
    private TextField nationalityField;
    @FXML
    private TextField studyLevelField;
    @FXML
    private TextField civilStatusField;
    @FXML
    private TextField addressField;


    public WorkContractPartsForm() {

        this.parent = ViewLoader.load(this, CONTRACT_PARTS_FXML);
    }

    @FXML
    public void initialize() {

        employerSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item == null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 14));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.CENTER);
                    setText(item.toString());
                }
            }

        });

        employerSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<Employer> call(ListView<Employer> param) {
                        final ListCell<Employer> cell = new ListCell<>() {
                            @Override
                            public void updateItem(Employer item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }
                });

        workCenterSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item == null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 14));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.CENTER);
                    setText(item.toString());
                }
            }

        });

        workCenterSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<WorkCenter> call(ListView<WorkCenter> param) {
                        final ListCell<WorkCenter> cell = new ListCell<>() {
                            @Override
                            public void updateItem(WorkCenter item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }
                });

        quoteAccountCodeSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item == null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 14));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.CENTER);
                    setText(item.toString());
                }
            }

        });

        quoteAccountCodeSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<QuoteAccountCode> call(ListView<QuoteAccountCode> param) {
                        final ListCell<QuoteAccountCode> cell = new ListCell<>() {
                            @Override
                            public void updateItem(QuoteAccountCode item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }
                });

        employeeSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item == null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 14));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.CENTER);
                    setText(item.toString());
                }
            }

        });

        employeeSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<Employee> call(ListView<Employee> param) {
                        final ListCell<Employee> cell = new ListCell<Employee>() {
                            @Override
                            public void updateItem(Employee item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }
                });

        employerPattern.setOnKeyReleased(this::onEmployerPatternChange);
        employerSelector.setOnAction(this::onEmployerSelectorChange);
        workCenterSelector.setOnAction(this::onWorkCenterSelectorChange);
        quoteAccountCodeSelector.setOnAction(this::onQuoteAccountCodeSelectorChange);
        employeePattern.setOnKeyReleased(this::onEmployeePatternChange);
        employeeSelector.setOnAction(this::onEmployeeSelectorChange);
    }

    public TextField getEmployerPattern() {
        return employerPattern;
    }

    public void setEmployerPattern(TextField employerPattern) {
        this.employerPattern = employerPattern;
    }

    public ComboBox<Employer> getEmployerSelector() {
        return employerSelector;
    }

    public void setEmployerSelector(ComboBox<Employer> employerSelector) {
        this.employerSelector = employerSelector;
    }

    public ComboBox<QuoteAccountCode> getQuoteAccountCodeSelector() {
        return quoteAccountCodeSelector;
    }

    public ComboBox<WorkCenter> getWorkCenterSelector() {
        return workCenterSelector;
    }

    public void setWorkCenterSelector(ComboBox<WorkCenter> workCenterSelector) {
        this.workCenterSelector = workCenterSelector;
    }

    public TextField getEmployeePattern() {
        return employeePattern;
    }

    public void setEmployeePattern(TextField employeePattern) {
        this.employeePattern = employeePattern;
    }

    public ComboBox<Employee> getEmployeeSelector() {
        return employeeSelector;
    }

    public void setEmployeeSelector(ComboBox<Employee> employeeSelector) {
        this.employeeSelector = employeeSelector;
    }

    public TextField getNifField() {
        return nifField;
    }

    public void setNifField(TextField nifField) {
        this.nifField = nifField;
    }

    public TextField getNassField() {
        return nassField;
    }

    public void setNassField(TextField nassField) {
        this.nassField = nassField;
    }

    public TextField getBirthDateField() {
        return birthDateField;
    }

    public void setBirthDateField(TextField birthDateField) {
        this.birthDateField = birthDateField;
    }

    public TextField getNationalityField() {
        return nationalityField;
    }

    public void setNationalityField(TextField nationalityField) {
        this.nationalityField = nationalityField;
    }

    public TextField getStudyLevelField() {
        return studyLevelField;
    }

    public void setStudyLevelField(TextField studyLevelField) {
        this.studyLevelField = studyLevelField;
    }

    public TextField getCivilStatusField() {
        return civilStatusField;
    }

    public void setCivilStatusField(TextField civilStatusField) {
        this.civilStatusField = civilStatusField;
    }

    public TextField getAddressField() {
        return addressField;
    }

    public void setAddressField(TextField addressField) {
        this.addressField = addressField;
    }

    public void employerSelectorRefresh(ObservableList<Employer> employerObservableList){
        getEmployerSelector().getSelectionModel().clearSelection();
        getEmployerSelector().getItems().clear();
        getEmployerSelector().getItems().addAll(employerObservableList);
        getEmployerSelector().setPromptText("Se han encontrado empleadores");
    }

    public void workCenterSelectorRefresh(ObservableList<WorkCenter> workCenterObservableList){
        if(workCenterSelector.getItems().size() > 0){
            workCenterSelector.getItems().clear();
        }
        getWorkCenterSelector().getItems().addAll(workCenterObservableList);
        if(workCenterObservableList.size() == 1){
            getWorkCenterSelector().getSelectionModel().select(0);
        }
    }

    private void onEmployerPatternChange(KeyEvent event){
        employerNamePatternEventEventHandler.handle(new NamePatternChangedEvent(employerPattern.getText()));

    }

    private void onEmployerSelectorChange(ActionEvent event){
        employerSelectorChangedEventEventHandler.handle(new EmployerSelectorChangedEvent(employerSelector.getSelectionModel().getSelectedItem()));
    }

    private void onWorkCenterSelectorChange(ActionEvent event){
        workCenterSelectorChangedEventEventHandler.handle(new WorkCenterSelectorChangedEvent(workCenterSelector.getSelectionModel().getSelectedItem()));
    }

    private void onQuoteAccountCodeSelectorChange(ActionEvent event){
        QuoteAccountCodeSelectorChangedEvent quoteAccountCodeSelectorChangedEvent =
                new QuoteAccountCodeSelectorChangedEvent(getQuoteAccountCodeSelector().getSelectionModel().getSelectedItem());
        quoteAccountCodeSelectorChangeEvent.handle(quoteAccountCodeSelectorChangedEvent);
    }

    private void onEmployeePatternChange(KeyEvent event){
        employeeNamePatternEventEventHandler.handle(new NamePatternChangedEvent(employeePattern.getText()));
    }

    private void onEmployeeSelectorChange(ActionEvent event){
        employeeSelectorChangedEventEventHandler.handle(new EmployeeSelectorChangedEvent(employeeSelector.getSelectionModel().getSelectedItem()));
    }

    public void quoteAccountCodeSelectorRefresh(ObservableList<QuoteAccountCode> quoteAccountCodeObservableList){
        quoteAccountCodeSelector.getItems().addAll(quoteAccountCodeObservableList);
    }

    public void employeeSelectorRefresh(ObservableList<Employee> employeeObservableList){
        getEmployeeSelector().getSelectionModel().clearSelection();
        getEmployeeSelector().getItems().clear();
        getEmployeeSelector().getItems().addAll(employeeObservableList);
        getEmployeeSelector().setPromptText("Se han encontrado empleados");    }

    public void refreshEmployeeData(PersonDBO selectedEmployee){
        clearEmployeeData();

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        String birthDate = selectedEmployee.getBirthDate() == null ? "" : selectedEmployee.getBirthDate().toLocalDate().format(dateFormatter);
        String nieNif = selectedEmployee.getNieNif() == null ? "" : new NieNif(selectedEmployee.getNieNif()).formattedAsNIF();
        String study = selectedEmployee.getStudy() == null ? "" : selectedEmployee.getStudy().getStudyLevelDescription();
        String civilStatus = selectedEmployee.getCivilStatus() == null ? "" : selectedEmployee.getCivilStatus().getCivilStatusDescription();

        getNifField().setText(nieNif);
        getNassField().setText(selectedEmployee.getSocialSecurityAffiliationNumber());
        getBirthDateField().setText(birthDate);
        getNationalityField().setText(selectedEmployee.getNationality());
        getStudyLevelField().setText(study);
        getCivilStatusField().setText(civilStatus);
        for(AddressDBO addressDBO : selectedEmployee.getAddresses()){
            if(addressDBO.getDefaultAddress()){
                getAddressField().setText(addressDBO.toString());
            }
        }
    }

    public void clearEmployeeData(){
        getNifField().setText("");
        getNassField().setText("");
        getBirthDateField().setText("");
        getNationalityField().setText("");
        getStudyLevelField().setText("");
        getCivilStatusField().setText("");
        getAddressField().setText("");
    }

//    public WorkContractProvisionalData getAllData(){
//
//        String selectedEmployerName = "";
//        if(this.employerSelector.getSelectionModel().getSelectedItem() != null){
//            selectedEmployerName = this.employerSelector.getSelectionModel().getSelectedItem().toString();
//        }
//        String selectedEmployeeName = "";
//        if(this.employeeSelector.getSelectionModel().getSelectedItem() != null){
//            selectedEmployeeName = this.employeeSelector.getSelectionModel().getSelectedItem().toString();
//        }
//        String CCC = "";
//        if(this.getQuoteAccountCodeSelector().getSelectionModel().getSelectedItem() != null){
//            CCC = this.quoteAccountCodeSelector.getSelectionModel().getSelectedItem().toString();
//        }
//
//        return WorkContractProvisionalData.create()
//                .withEmployerFullName(selectedEmployerName)
//                .withQuoteAccountCode(CCC)
//                .withEmployeeFullName(selectedEmployeeName)
//                .build();
//    }

    public void setOnEmployerSelectorChange(EventHandler<EmployerSelectorChangedEvent> employerSelectorChangedEventEventHandler){
        this.employerSelectorChangedEventEventHandler = employerSelectorChangedEventEventHandler;
    }

    public void setOnWorkCenterSelectorChange(EventHandler<WorkCenterSelectorChangedEvent> workCenterSelectorChangedEventEventHandler){
        this.workCenterSelectorChangedEventEventHandler = workCenterSelectorChangedEventEventHandler;
    }

    public void setOnEmployeeSelectorChange(EventHandler<EmployeeSelectorChangedEvent> employeeSelectorChangedEventEventHandler){
        this.employeeSelectorChangedEventEventHandler = employeeSelectorChangedEventEventHandler;
    }

    public void setOnEmployerPatternChanged(EventHandler<NamePatternChangedEvent> employerNamePatternEventEventHandler){
        this.employerNamePatternEventEventHandler = employerNamePatternEventEventHandler;
    }

    public void setOnQuoteAccountCodeSelectorChange(EventHandler<QuoteAccountCodeSelectorChangedEvent> quoteAccountCodeSelectorChangeEvent){
        this.quoteAccountCodeSelectorChangeEvent = quoteAccountCodeSelectorChangeEvent;

    }

    public void setOnEmployeePatternChanged(EventHandler<NamePatternChangedEvent> employeeNamePatternEventEventHandler){
        this.employeeNamePatternEventEventHandler = employeeNamePatternEventEventHandler;
    }
}
