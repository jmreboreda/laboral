package laboral.component.work_contract.creation.components;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.StringConverter;
import laboral.component.ViewLoader;
import laboral.component.work_contract.ContractConstants;
import laboral.component.work_contract.WorkContractScheduleDay;
import laboral.component.work_contract.WorkDaySchedule;
import laboral.component.work_contract.creation.events.ChangeScheduleDurationEvent;
import laboral.domain.contract_json.ContractDaySchedule;
import laboral.domain.contract_json.ContractSchedule;
import laboral.domain.utilities.cells.ContractScheduleDayDateCell;
import laboral.domain.utilities.cells.DurationCell;
import laboral.domain.utilities.cells.TimeCell;
import laboral.domain.utilities.utilities.Utilities;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.util.*;

public class WorkContractScheduleForm extends AnchorPane {

    private static final String SCHEDULE_FXML = "/fxml/work_contract/creation/work_contract_schedule_table.fxml";
    private static final Integer DAY_OF_WEEK_COLUMN = 0;
    private static final Integer DATE_COLUMN = 1;
    private static final Integer AM_FROM_COLUMN = 2;
    private static final Integer AM_TO_COLUMN = 3;
    private static final Integer PM_FROM_COLUMN = 4;
    private static final Integer PM_TO_COLUMN = 5;
    private static final KeyCode REQUEST_DELETION_ALL_DATA = KeyCode.F8;
    private static final KeyCode REQUEST_FOR_DATA_DUPLICATION = KeyCode.F10;

    private EventHandler<ChangeScheduleDurationEvent> onChangeScheduleDurationEventHandler;

    private Parent parent;

    private List<WorkContractScheduleDay> scheduleOneList = new ArrayList<>();
    private List<WorkContractScheduleDay> scheduleTwoList =  new ArrayList<>();
    private List<WorkContractScheduleDay> scheduleThreeList =  new ArrayList<>();
    private List<WorkContractScheduleDay> scheduleFourList =  new ArrayList<>();

    @FXML
    private TableView <WorkContractScheduleDay> workContractScheduleTable;
    @FXML
    private TableColumn<WorkContractScheduleDay, DayOfWeek> dayOfWeek;
    @FXML
    private TableColumn<WorkContractScheduleDay, LocalDate> date;
    @FXML
    private TableColumn<WorkContractScheduleDay, LocalTime> amFrom;
    @FXML
    private TableColumn<WorkContractScheduleDay, LocalTime> amTo;
    @FXML
    private TableColumn<WorkContractScheduleDay, LocalTime> pmFrom;
    @FXML
    private TableColumn<WorkContractScheduleDay, LocalTime> pmTo;
    @FXML
    private TableColumn<WorkContractScheduleDay, Duration> totalDayHours;
    @FXML
    private Label hoursWorkWeek;
//    @FXML
//    private Tab scheduleTab;
    @FXML
    private ToggleGroup scheduleGroup;
    @FXML
    private RadioButton scheduleOne;
    @FXML
    private RadioButton scheduleTwo;
    @FXML
    private RadioButton scheduleThree;
    @FXML
    private RadioButton scheduleFour;
//    @FXML
//    private Tab managerNotesTab;
//    @FXML
//    private Tab privateNotesTab;


    public WorkContractScheduleForm() {
        this.parent = ViewLoader.load(this, SCHEDULE_FXML);
    }

    @FXML
    public void initialize() {

        workContractScheduleTable.setEditable(true);

        final ObservableList<DayOfWeek> daysOfWeek = FXCollections.observableArrayList();
        daysOfWeek.add(DayOfWeek.MONDAY);
        daysOfWeek.add(DayOfWeek.TUESDAY);
        daysOfWeek.add(DayOfWeek.WEDNESDAY);
        daysOfWeek.add(DayOfWeek.THURSDAY);
        daysOfWeek.add(DayOfWeek.FRIDAY);
        daysOfWeek.add(DayOfWeek.SATURDAY);
        daysOfWeek.add(DayOfWeek.SUNDAY);

        dayOfWeek.setCellFactory(param -> {
            ComboBoxTableCell<WorkContractScheduleDay, DayOfWeek> comboBoxTableCell = new ComboBoxTableCell<>(daysOfWeek);
            comboBoxTableCell.setPickOnBounds(true);
            comboBoxTableCell.updateSelected(true);
            comboBoxTableCell.setConverter(new StringConverter<>() {
                @Override
                public String toString(DayOfWeek object) {
                    if (object != null) {
                        dayOfWeek.setStyle("-fx-text-fill: #000FFF;");
                        return object.getDisplayName(TextStyle.FULL, Locale.getDefault());
                    }
                    return null;
                }

                @Override
                public DayOfWeek fromString(String string) {
                    return DayOfWeek.valueOf(string);
                }
            });

            return comboBoxTableCell;
        });

        scheduleGroup.selectedToggleProperty().addListener((ov, old_toggle, new_toggle) -> {
                onScheduleNumberChange(old_toggle, new_toggle);
        });

        date.setCellFactory(param -> new ContractScheduleDayDateCell());
        amFrom.setCellFactory(param -> new TimeCell());
        amTo.setCellFactory(param -> new TimeCell());
        pmFrom.setCellFactory(param -> new TimeCell());
        pmTo.setCellFactory(param -> new TimeCell());
        totalDayHours.setCellFactory(param -> new DurationCell());

        dayOfWeek.setCellValueFactory(new PropertyValueFactory<>("dayOfWeek"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        amFrom.setCellValueFactory(new PropertyValueFactory<>("amFrom"));
        amTo.setCellValueFactory(new PropertyValueFactory<>("amTo"));
        pmFrom.setCellValueFactory(new PropertyValueFactory<>("pmFrom"));
        pmTo.setCellValueFactory(new PropertyValueFactory<>("pmTo"));
        totalDayHours.setCellValueFactory(new PropertyValueFactory<>("totalDayHours"));

        date.getStyleClass().add("tableDateStyle");
        amFrom.getStyleClass().add("tableTimeStyle");
        amTo.getStyleClass().add("tableTimeStyle");
        pmFrom.getStyleClass().add("tableTimeStyle");
        pmTo.getStyleClass().add("tableTimeStyle");
        totalDayHours.getStyleClass().add("tableTotalDayHoursStyle");

        amFrom.setOnEditCommit(this::updateTotalWeekHours);
        amTo.setOnEditCommit(this::updateTotalWeekHours);
        pmFrom.setOnEditCommit(this::updateTotalWeekHours);
        pmTo.setOnEditCommit(this::updateTotalWeekHours);

        workContractScheduleTable.setOnKeyPressed(this::verifyRequestWithSpecialKeyCode);

        List<WorkContractScheduleDay> workContractScheduleDayList = new ArrayList<>();
        for(int i = 0; i <= ContractConstants.LAST_ROW_SCHEDULE_TABLE; i++){
            workContractScheduleDayList.add(
                    WorkContractScheduleDay.create()
                            .withTotalDayHours(Duration.ZERO)
                            .build());

            scheduleOneList.add(
                    WorkContractScheduleDay.create()
                            .withTotalDayHours(Duration.ZERO)
                            .build());

            scheduleTwoList.add(
                    WorkContractScheduleDay.create()
                            .withTotalDayHours(Duration.ZERO)
                            .build());

            scheduleThreeList.add(
                    WorkContractScheduleDay.create()
                            .withTotalDayHours(Duration.ZERO)
                            .build());

            scheduleFourList.add(
                    WorkContractScheduleDay.create()
                            .withTotalDayHours(Duration.ZERO)
                            .build());
        }

        ObservableList<WorkContractScheduleDay> dataForContractScheduleTable = FXCollections.observableArrayList(workContractScheduleDayList);
        refreshTable(dataForContractScheduleTable);
    }

    public List<WorkContractScheduleDay> getScheduleOneList() {
        return scheduleOneList;
    }

    public void setScheduleOneList(List<WorkContractScheduleDay> scheduleOneList) {
        this.scheduleOneList = scheduleOneList;
    }

    public List<WorkContractScheduleDay> getScheduleTwoList() {
        return scheduleTwoList;
    }

    public void setScheduleTwoList(List<WorkContractScheduleDay> scheduleTwoList) {
        this.scheduleTwoList = scheduleTwoList;
    }

    public List<WorkContractScheduleDay> getScheduleThreeList() {
        return scheduleThreeList;
    }

    public void setScheduleThreeList(List<WorkContractScheduleDay> scheduleThreeList) {
        this.scheduleThreeList = scheduleThreeList;
    }

    public List<WorkContractScheduleDay> getScheduleFourList() {
        return scheduleFourList;
    }

    public void setScheduleFourList(List<WorkContractScheduleDay> scheduleFourList) {
        this.scheduleFourList = scheduleFourList;
    }

    public TableView<WorkContractScheduleDay> getWorkContractScheduleTable() {
        return workContractScheduleTable;
    }

    public void setWorkContractScheduleTable(TableView<WorkContractScheduleDay> workContractScheduleTable) {
        this.workContractScheduleTable = workContractScheduleTable;
    }

    public TableColumn<WorkContractScheduleDay, DayOfWeek> getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(TableColumn<WorkContractScheduleDay, DayOfWeek> dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public TableColumn<WorkContractScheduleDay, LocalDate> getDate() {
        return date;
    }

    public TableColumn<WorkContractScheduleDay, LocalTime> getAmFrom() {
        return amFrom;
    }

    public TableColumn<WorkContractScheduleDay, LocalTime> getAmTo() {
        return amTo;
    }

    public TableColumn<WorkContractScheduleDay, LocalTime> getPmFrom() {
        return pmFrom;
    }

    public TableColumn<WorkContractScheduleDay, LocalTime> getPmTo() {
        return pmTo;
    }

    public void setDate(TableColumn<WorkContractScheduleDay, LocalDate> date) {
        this.date = date;
    }

    public void setAmFrom(TableColumn<WorkContractScheduleDay, LocalTime> amFrom) {
        this.amFrom = amFrom;
    }

    public void setAmTo(TableColumn<WorkContractScheduleDay, LocalTime> amTo) {
        this.amTo = amTo;
    }

    public void setPmFrom(TableColumn<WorkContractScheduleDay, LocalTime> pmFrom) {
        this.pmFrom = pmFrom;
    }

    public void setPmTo(TableColumn<WorkContractScheduleDay, LocalTime> pmTo) {
        this.pmTo = pmTo;
    }

    public TableColumn<WorkContractScheduleDay, Duration> getTotalDayHours() {
        return totalDayHours;
    }

    public void setTotalDayHours(TableColumn<WorkContractScheduleDay, Duration> totalDayHours) {
        this.totalDayHours = totalDayHours;
    }

    public Label getHoursWorkWeek() {
        return hoursWorkWeek;
    }

    public void setHoursWorkWeek(Label hoursWorkWeek) {
        this.hoursWorkWeek = hoursWorkWeek;
    }

    public static String getScheduleFxml() {
        return SCHEDULE_FXML;
    }

    public RadioButton getScheduleOne() {
        return scheduleOne;
    }

    public RadioButton getScheduleTwo() {
        return scheduleTwo;
    }

    public RadioButton getScheduleThree() {
        return scheduleThree;
    }

    public RadioButton getScheduleFour() {
        return scheduleFour;
    }

    private void updateTotalWeekHours(TableColumn.CellEditEvent event){
        int editedRow = event.getTablePosition().getRow();
        int editedColumn = event.getTablePosition().getColumn();

        WorkContractScheduleDay selectedItemDay = workContractScheduleTable.getItems().get(editedRow);

        if(editedColumn == AM_FROM_COLUMN){
            selectedItemDay.setAmFrom((LocalTime) event.getNewValue());
        }
        if(editedColumn == AM_TO_COLUMN){
            selectedItemDay.setAmTo((LocalTime) event.getNewValue());
        }
        if(editedColumn == PM_FROM_COLUMN){
            selectedItemDay.setPmFrom((LocalTime) event.getNewValue());
        }
        if(editedColumn == PM_TO_COLUMN){
            selectedItemDay.setPmTo((LocalTime) event.getNewValue());
        }

        Duration durationDay = retrieveDurationDay(selectedItemDay);
        selectedItemDay.setTotalDayHours(durationDay);

        refreshTable(workContractScheduleTable.getItems());

        refreshTotalWeekHours();
    }

    private void refreshTable(ObservableList<WorkContractScheduleDay> tableItemList){
        workContractScheduleTable.setItems(tableItemList);
    }

    private Duration retrieveDurationDay(WorkContractScheduleDay selectedItemRow){
        Duration durationAM = Duration.ZERO;
        Duration durationPM = Duration.ZERO;

        if(selectedItemRow.getAmFrom() != null && selectedItemRow.getAmTo() != null){
            if(Duration.between(selectedItemRow.getAmFrom(), selectedItemRow.getAmTo()).toMinutes() <= 0){
                deleteAllDataForSelectedRow();

                return Duration.ZERO;
            }

            durationAM = Duration.between(selectedItemRow.getAmFrom(), selectedItemRow.getAmTo());
        }

        if(selectedItemRow.getPmFrom() != null && selectedItemRow.getPmTo() != null){
            LocalTime pmFrom = selectedItemRow.getPmFrom();
            LocalTime pmTo =  selectedItemRow.getPmTo();

            if(pmTo != LocalTime.parse("00:00") && Duration.between(pmFrom, pmTo).toMinutes() <= 0){
                deleteAllDataForSelectedRow();

                return Duration.ZERO;
            }

            if(pmTo == LocalTime.parse("00:00"))
            {
                durationPM = Duration.between(pmFrom, LocalTime.MIDNIGHT).plus(Duration.ofDays(1));
            }
            else {
                durationPM = Duration.between(pmFrom, pmTo);
            }
        }

        return durationAM.plus(durationPM);

    }

    private void verifyRequestWithSpecialKeyCode(KeyEvent event){
        if(event.getCode() != REQUEST_DELETION_ALL_DATA &&
                event.getCode() != REQUEST_FOR_DATA_DUPLICATION)
        {
            return;
        }

        if(event.getCode() == REQUEST_DELETION_ALL_DATA) {
            deleteAllDataForSelectedRow();
            refreshTotalWeekHours();

            return;
        }

        Integer selectedRow = workContractScheduleTable.getSelectionModel().getSelectedIndex();
        if (verifyRowAndRowData(selectedRow)) {
            duplicateDataInFirstEmptyRow(selectedRow);
            refreshTotalWeekHours();
        }
    }

    private void deleteAllDataForSelectedRow(){
        Integer selectedRow = workContractScheduleTable.getSelectionModel().getSelectedIndex();
        if(selectedRow >= ContractConstants.FIRST_ROW_SCHEDULE_TABLE){
            WorkContractScheduleDay selectedItemRow = workContractScheduleTable.getItems().get(selectedRow);

            selectedItemRow.setDayOfWeek(null);
            selectedItemRow.setDate(null);
            selectedItemRow.setAmFrom(null);
            selectedItemRow.setAmTo(null);
            selectedItemRow.setPmFrom(null);
            selectedItemRow.setPmTo(null);
            selectedItemRow.setTotalDayHours(Duration.ZERO);
            refreshTable(workContractScheduleTable.getItems());
        }
    }

    private Boolean verifyRowAndRowData(Integer selectedRow){
        if(selectedRow >= ContractConstants.FIRST_ROW_SCHEDULE_TABLE &&
                selectedRow <= ContractConstants.LAST_ROW_SCHEDULE_TABLE &&
                verifySelectedRowContainsData(selectedRow)) {
            return true;
            }

            return false;
    }

    private Boolean verifySelectedRowContainsData(Integer selectedRow){
        ObservableList<WorkContractScheduleDay> tableItemList = workContractScheduleTable.getItems();
        if(!tableItemList.get(selectedRow).getTotalDayHours().equals(Duration.ZERO)){
            return true;
        }

        return false;
    }

    private void duplicateDataInFirstEmptyRow(Integer selectedRow){

        final Integer LAST_ROW_IN_TABLE = 6;

        WorkContractScheduleDay previousRowToFirstEmptyRow;

        Integer firstEmptyRowNumber = findFirstEmptyRowNumber();

        if(firstEmptyRowNumber == null){

            return;
        }

        WorkContractScheduleDay selectedItemRow = workContractScheduleTable.getItems().get(selectedRow);
        WorkContractScheduleDay firstEmptyRowTarget = workContractScheduleTable.getItems().get(firstEmptyRowNumber);

        if(firstEmptyRowNumber == 0 && workContractScheduleTable.getItems().get(LAST_ROW_IN_TABLE).getDayOfWeek() != null){
            firstEmptyRowTarget.setDayOfWeek(workContractScheduleTable.getItems().get(LAST_ROW_IN_TABLE).getDayOfWeek().plus(1));
        }

        if(firstEmptyRowNumber == 0 && workContractScheduleTable.getItems().get(LAST_ROW_IN_TABLE).getDayOfWeek() == null){
            firstEmptyRowTarget.setDayOfWeek(selectedItemRow.getDayOfWeek().minus(1));
        }

        if(firstEmptyRowNumber > 0){
            previousRowToFirstEmptyRow =  workContractScheduleTable.getItems().get(firstEmptyRowNumber -1);

            if(workContractScheduleTable.getItems().get(firstEmptyRowNumber -1 ).getDayOfWeek() != null) {
                firstEmptyRowTarget.setDayOfWeek(previousRowToFirstEmptyRow.getDayOfWeek().plus(1));
            }

            if(workContractScheduleTable.getItems().get(firstEmptyRowNumber -1 ).getDayOfWeek() == null) {
                if(selectedItemRow.getDayOfWeek() != null){
                    firstEmptyRowTarget.setDayOfWeek(selectedItemRow.getDayOfWeek().plus(1));
                }else{
                    firstEmptyRowTarget.setDayOfWeek(null);
                }
            }
        }

        firstEmptyRowTarget.setAmFrom(selectedItemRow.getAmFrom());
        firstEmptyRowTarget.setAmTo(selectedItemRow.getAmTo());
        firstEmptyRowTarget.setPmFrom(selectedItemRow.getPmFrom());
        firstEmptyRowTarget.setPmTo(selectedItemRow.getPmTo());
        firstEmptyRowTarget.setTotalDayHours(selectedItemRow.getTotalDayHours());
        refreshTable(workContractScheduleTable.getItems());
    }

    private Integer findFirstEmptyRowNumber(){
        ObservableList<WorkContractScheduleDay> tableItemList = workContractScheduleTable.getItems();
        for(WorkContractScheduleDay workContractScheduleDay : tableItemList){
            if(workContractScheduleDay.getTotalDayHours().equals(Duration.ZERO)){
                return tableItemList.indexOf(workContractScheduleDay);
            }
        }

        return null;
    }

    private void refreshTotalWeekHours(){
        Duration totalHours = Duration.ZERO;
        for(WorkContractScheduleDay workContractScheduleDay : workContractScheduleTable.getItems()){
            totalHours = totalHours.plus(workContractScheduleDay.getTotalDayHours());
        }
        hoursWorkWeek.setText(Utilities.durationToTimeStringConverter(totalHours));
        storeAllSchedule();

        if(totalHours != Duration.ZERO) {
            final ChangeScheduleDurationEvent changeScheduleDurationEvent = new ChangeScheduleDurationEvent(totalHours);
            onChangeScheduleDurationEventHandler.handle(changeScheduleDurationEvent);
        }
    }

    public Set<DayOfWeek> getTableColumnDayOfWeekData(){

        Set<DayOfWeek> dayOfWeekSet = new HashSet<>();
        for(Integer i = ContractConstants.FIRST_ROW_SCHEDULE_TABLE; i<= ContractConstants.LAST_ROW_SCHEDULE_TABLE; i++) {
            if(dayOfWeek.getCellData(i) != null) {
                dayOfWeekSet.add(dayOfWeek.getCellData(i));
            }
        }

        return dayOfWeekSet;
    }

    public Set<WorkDaySchedule> retrieveScheduleWithScheduleDays(List<WorkContractScheduleDay> workContractScheduleQuarter){

        Set<WorkDaySchedule> schedule = new HashSet<>();
            ObservableList<WorkContractScheduleDay> tableItemList = FXCollections.observableList(workContractScheduleQuarter);
            for(Integer i = ContractConstants.FIRST_ROW_SCHEDULE_TABLE; i < tableItemList.size(); i++){//ContractConstants.LAST_ROW_SCHEDULE_TABLE; i++){
                String dayOfWeek = "";
                LocalDate date = null;
                LocalTime amFrom = null;
                LocalTime amTo = null;
                LocalTime pmFrom = null;
                LocalTime pmTo = null;
                Duration durationHours = null;
                WorkContractScheduleDay selectedItemRow = tableItemList.get(i);
                if (selectedItemRow.getDayOfWeek() != null) {
                    dayOfWeek = selectedItemRow.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.getDefault());
                }
                if (selectedItemRow.getDate() != null) {
                    date = selectedItemRow.getDate();
                }
                if (selectedItemRow.getAmFrom() != null) {
                    amFrom = selectedItemRow.getAmFrom();
                }
                if (selectedItemRow.getAmTo() != null) {
                    amTo = selectedItemRow.getAmTo();
                }
                if (selectedItemRow.getPmFrom() != null) {
                    pmFrom = selectedItemRow.getPmFrom();
                }
                if (selectedItemRow.getPmTo() != null) {
                    pmTo = selectedItemRow.getPmTo();
                }
                if (selectedItemRow.getTotalDayHours() != Duration.ZERO) {
                    durationHours = selectedItemRow.getTotalDayHours();
                }

                WorkDaySchedule scheduleDay = WorkDaySchedule.create()
                        .withDayOfWeek(dayOfWeek)
                        .withDate(date)
                        .withAmFrom(amFrom)
                        .withAmTo(amTo)
                        .withPmFrom(pmFrom)
                        .withPmTo(pmTo)
                        .withDurationHours(durationHours)
                        .build();
                schedule.add(scheduleDay);
            }

        return schedule;
    }

    public void setOnChangeScheduleDuration(EventHandler<ChangeScheduleDurationEvent> handler){
       this.onChangeScheduleDurationEventHandler = handler;
    }

    private void onScheduleNumberChange(Toggle old_toggle, Toggle new_toggle){

        RadioButton previousRadioButton = (RadioButton) old_toggle;
        RadioButton selectedRadioButton = (RadioButton) new_toggle;

        if(scheduleGroup.getSelectedToggle() != null){
            storeSchedule(previousRadioButton);
            loadSchedule(selectedRadioButton);
        }
    }

    private void storeSchedule(RadioButton previousRadioButton){

        if(previousRadioButton.equals(scheduleOne)){
            scheduleOneList = newTableScheduleList(workContractScheduleTable.getItems());
        }

        if(previousRadioButton.equals(scheduleTwo)){
            scheduleTwoList = newTableScheduleList(workContractScheduleTable.getItems());
        }

        if(previousRadioButton.equals(scheduleThree)){
            scheduleThreeList = newTableScheduleList(workContractScheduleTable.getItems());
        }

        if(previousRadioButton.equals(scheduleFour)){
            scheduleFourList = newTableScheduleList(workContractScheduleTable.getItems());
        }
    }

    private void loadSchedule(RadioButton selectedRadioButton){

        scheduleTableClear();

        if(selectedRadioButton.equals(scheduleOne)){
            refreshTable(FXCollections.observableList(scheduleOneList));
        }

        if(selectedRadioButton.equals(scheduleTwo)) {
            refreshTable(FXCollections.observableList(scheduleTwoList));
        }

        if(selectedRadioButton.equals(scheduleThree)) {
            refreshTable(FXCollections.observableList(scheduleThreeList));
        }

        if(selectedRadioButton.equals(scheduleFour)) {
            refreshTable(FXCollections.observableList(scheduleFourList));
        }

        refreshTotalWeekHours();
    }

    private List<WorkContractScheduleDay> newTableScheduleList(List<WorkContractScheduleDay> scheduleTableList){

        List<WorkContractScheduleDay> workContractScheduleDayList = new ArrayList<>();
        for(WorkContractScheduleDay workContractScheduleDay : scheduleTableList){
            workContractScheduleDayList.add(WorkContractScheduleDay.create()
                    .withDayOfWeek(workContractScheduleDay.getDayOfWeek())
                    .withDate(workContractScheduleDay.getDate())
                    .withAmFrom(workContractScheduleDay.getAmFrom())
                    .withAmTo(workContractScheduleDay.getAmTo())
                    .withPmFrom(workContractScheduleDay.getPmFrom())
                    .withPmTo(workContractScheduleDay.getPmTo())
                    .withTotalDayHours(workContractScheduleDay.getTotalDayHours())
                    .build());
        }

        return workContractScheduleDayList;
    }

    public void cleanComponents(){
     scheduleTableClear();
    }

    private void scheduleTableClear() {
        for (Integer i = ContractConstants.FIRST_ROW_SCHEDULE_TABLE; i <= ContractConstants.LAST_ROW_SCHEDULE_TABLE; i++) {
            WorkContractScheduleDay selectedItemRow = workContractScheduleTable.getItems().get(i);
            selectedItemRow.setDate(null);
            selectedItemRow.setAmFrom(null);
            selectedItemRow.setAmTo(null);
            selectedItemRow.setPmFrom(null);
            selectedItemRow.setPmTo(null);
            selectedItemRow.setTotalDayHours(Duration.ZERO);
        }

        refreshTable(workContractScheduleTable.getItems());
        workContractScheduleTable.refresh();
    }

    public void storeAllSchedule(){
        if(scheduleOne.isSelected()) {
            scheduleOneList = newTableScheduleList(workContractScheduleTable.getItems());
        } else if(scheduleTwo.isSelected()) {
            scheduleTwoList = newTableScheduleList(workContractScheduleTable.getItems());
        } else if(scheduleThree.isSelected()) {
            scheduleThreeList = newTableScheduleList(workContractScheduleTable.getItems());
        } else
            scheduleFourList = newTableScheduleList(workContractScheduleTable.getItems());
    }

    public ContractSchedule retrieveWorkContractSchedule(){

        List<List<WorkContractScheduleDay>> workContractScheduleList = new ArrayList<>();
        workContractScheduleList.add(this.getScheduleOneList());
        workContractScheduleList.add(this.getScheduleTwoList());
        workContractScheduleList.add(this.getScheduleThreeList());
        workContractScheduleList.add(this.getScheduleFourList());

        Map<String, ContractDaySchedule> contractDayScheduleSet = new HashMap<>();

        ContractSchedule schedule = new ContractSchedule();

        Integer scheduleNumber = 1;
        for(List<WorkContractScheduleDay> workContractScheduleQuarter : workContractScheduleList) {

            Integer counter = 0;

            Set<WorkDaySchedule> scheduleSet = this.retrieveScheduleWithScheduleDays(workContractScheduleQuarter);
            for (WorkDaySchedule workDaySchedule : scheduleSet) {
                if (!workDaySchedule.getDayOfWeek().isEmpty()) {
                    String dayOfWeek = workDaySchedule.getDayOfWeek() != null ? workDaySchedule.getDayOfWeek() : "";
                    String date = workDaySchedule.getDate() != null ? workDaySchedule.getDate().toString() : "";
                    String amFrom = workDaySchedule.getAmFrom() != null ? workDaySchedule.getAmFrom().toString() : "";
                    String amTo = workDaySchedule.getAmTo() != null ? workDaySchedule.getAmTo().toString() : "";
                    String pmFrom = workDaySchedule.getPmFrom() != null ? workDaySchedule.getPmFrom().toString() : "";
                    String pmTo = workDaySchedule.getPmTo() != null ? workDaySchedule.getPmTo().toString() : "";
                    String durationHours = Utilities.durationToTimeStringConverter(workDaySchedule.getDurationHours());

                    ContractDaySchedule contractDayScheduleJson = ContractDaySchedule.create()
                            .withDayOfWeek(dayOfWeek)
                            .withDate(date)
                            .withAmFrom(amFrom)
                            .withAmTo(amTo)
                            .withPmFrom(pmFrom)
                            .withPmTo(pmTo)
                            .withDurationHours(durationHours)
                            .build();

                    contractDayScheduleSet.put("Schedule" + scheduleNumber + " - workDay" + counter, contractDayScheduleJson);
                }

                counter++;

                schedule.setSchedule(contractDayScheduleSet);
            }

            scheduleNumber++;
        }

        return schedule;
    }
}
