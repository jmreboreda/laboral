package laboral.component.work_contract.creation.components;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class WorkContractPrivateNotesForm extends AnchorPane {

    private static final String PRIVATE_NOTES_FXML = "/fxml/work_contract/creation/work_contract_private_notes.fxml";

    private Parent parent;

    @FXML
    private TextArea taPrivateNotes;

    public WorkContractPrivateNotesForm() {
        this.parent = ViewLoader.load(this, PRIVATE_NOTES_FXML);
    }

    @FXML
    public void initialize(){
        taPrivateNotes.setStyle("-fx-text-fill: #8b0000");
    }

    public TextArea getPrivateNotes(){
        return taPrivateNotes;
    }
}
