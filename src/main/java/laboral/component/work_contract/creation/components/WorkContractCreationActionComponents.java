package laboral.component.work_contract.creation.components;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class WorkContractCreationActionComponents extends AnchorPane {

    private static final String WORK_CONTRACT_CREATION_ACTION_COMPONENTS_FXML = "/fxml/work_contract/creation/work_contract_action_components.fxml";

    Parent parent;

    private EventHandler<MouseEvent> mouseExitEventEventHandler;
    private EventHandler<MouseEvent> mouseOkEventEventHandler;
    private EventHandler<MouseEvent> mouseSendMailEventEventHandler;


    @FXML
    private Button sendMailButton;
    @FXML
    private Button okButton;
    @FXML
    private Button viewPDFButton;
    @FXML
    private Button exitButton;


    public WorkContractCreationActionComponents() {
        this.parent = ViewLoader.load(this, WORK_CONTRACT_CREATION_ACTION_COMPONENTS_FXML);
    }

    @FXML
    public void initialize(){

        okButton.setOnMouseClicked(this::onOkButton);
        sendMailButton.setOnMouseClicked(this::onSendmailButton);
        exitButton.setOnMouseClicked(this::onExitButton);
    }

    public Button getSendMailButton() {
        return sendMailButton;
    }

    public void setSendMailButton(Button sendMailButton) {
        this.sendMailButton = sendMailButton;
    }

    public Button getOkButton() {
        return okButton;
    }

    public void setOkButton(Button okButton) {
        this.okButton = okButton;
    }

    public Button getViewPDFButton() {
        return viewPDFButton;
    }

    public void setViewPDFButton(Button viewPDFButton) {
        this.viewPDFButton = viewPDFButton;
    }

    public Button getExitButton() {
        return exitButton;
    }

    public void setExitButton(Button exitButton) {
        this.exitButton = exitButton;
    }

    private void onOkButton(MouseEvent event){
        mouseOkEventEventHandler.handle(event);
    }

    private void onSendmailButton(MouseEvent event){
        mouseSendMailEventEventHandler.handle(event);
    }

    private void onExitButton(MouseEvent event){
        mouseExitEventEventHandler.handle(event);
    }


    public void setOnExitButton(EventHandler<MouseEvent> mouseExitEventEventHandler){
        this.mouseExitEventEventHandler = mouseExitEventEventHandler;
    }

    public void setOnOkButton(EventHandler<MouseEvent> mouseOkEventEventHandler){
        this.mouseOkEventEventHandler = mouseOkEventEventHandler;
    }

    public void setOnSendMailButton(EventHandler<MouseEvent> mouseSendMailEventEventHandler){
        this.mouseSendMailEventEventHandler = mouseSendMailEventEventHandler;
    }
}
