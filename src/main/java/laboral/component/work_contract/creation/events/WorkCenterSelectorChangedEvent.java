package laboral.component.work_contract.creation.events;

import javafx.event.Event;
import javafx.event.EventType;
import laboral.domain.work_center.WorkCenter;

public class WorkCenterSelectorChangedEvent extends Event {

    public static final EventType<WorkCenterSelectorChangedEvent> WORK_CENTER_SELECTOR_CHANGE_EVENT_TYPE = new EventType<>("WORK_CENTER_SELECTOR_CHANGE_EVENT_TYPE");
    private final WorkCenter selectedWorkCenter;

    public WorkCenterSelectorChangedEvent(WorkCenter selectedWorkCenter) {
        super(WORK_CENTER_SELECTOR_CHANGE_EVENT_TYPE);
        this.selectedWorkCenter = selectedWorkCenter;

    }

    public WorkCenter getSelectedWorkCenter() {
        return selectedWorkCenter;
    }
}
