package laboral.component.work_contract.creation.events;

import javafx.event.Event;
import javafx.event.EventType;
import laboral.domain.contract_type.WorkContractType;

public class ContractTypeSelectorChangedEvent extends Event {

    public static final EventType<ContractTypeSelectorChangedEvent> CONTRACT_TYPE_SELECTOR_CHANGE_EVENT_TYPE = new EventType<>("CONTRACT_TYPE_SELECTOR_CHANGE_EVENT_TYPE");
    private final WorkContractType selectedWorkContractType;

    public ContractTypeSelectorChangedEvent(WorkContractType selectedWorkContractType) {
        super(CONTRACT_TYPE_SELECTOR_CHANGE_EVENT_TYPE);
        this.selectedWorkContractType = selectedWorkContractType;

    }

    public WorkContractType getSelectedContractType() {
        return selectedWorkContractType;
    }
}
