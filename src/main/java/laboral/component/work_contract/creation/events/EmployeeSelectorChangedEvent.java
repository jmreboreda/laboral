package laboral.component.work_contract.creation.events;

import javafx.event.Event;
import javafx.event.EventType;
import laboral.domain.employee.Employee;

public class EmployeeSelectorChangedEvent extends Event {

    public static final EventType<EmployeeSelectorChangedEvent> EMPLOYEE_SELECTOR_CHANGE_EVENT_TYPE = new EventType<>("EMPLOYEE_SELECTOR_CHANGE_EVENT_TYPE");
    private final Employee selectedEmployee;

    public EmployeeSelectorChangedEvent(Employee selectedEmployee) {
        super(EMPLOYEE_SELECTOR_CHANGE_EVENT_TYPE);
        this.selectedEmployee = selectedEmployee;

    }

    public Employee getSelectedEmployee() {
        return selectedEmployee;
    }
}
