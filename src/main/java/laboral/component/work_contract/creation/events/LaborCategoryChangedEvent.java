package laboral.component.work_contract.creation.events;

import javafx.event.Event;
import javafx.event.EventType;

public class LaborCategoryChangedEvent extends Event {

    public static final EventType<LaborCategoryChangedEvent> LABOR_CATEGORY_CHANGE_EVENT_TYPE = new EventType<>("LABOR_CATEGORY_CHANGE_EVENT_TYPE");
    private final String laborCategory;

    public LaborCategoryChangedEvent(String laborCategory) {
        super(LABOR_CATEGORY_CHANGE_EVENT_TYPE);
        this.laborCategory = laborCategory;

    }

    public String getLaborCategory() {
        return laborCategory;
    }
}
