package laboral.component.employer_management.common_components;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.util.StringConverter;
import laboral.component.ViewLoader;
import laboral.domain.address.Address;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.quote_account_code.QuoteAccountCodeRegimeEnum;
import laboral.domain.utilities.cells.EditableStringQuoteAccountCodeTableCell;

import java.util.ArrayList;
import java.util.List;

public class EmployerQuoteAccountCodeTableView extends AnchorPane {

    private static final String WORK_CENTERS_TABLE_VIEW_FXML = "/fxml/employer_management/employer_creation/employer_creation_qac_table_view.fxml";

    private static final Integer REGIME_COLUMN = 0;
    private static final Integer PROVINCE_COLUMN = 1;
    private static final Integer NUMBER_COLUMN = 2;
    private static final Integer CONTROL_DIGIT_COLUMN = 3;
    private static final Integer EPIGRAPH_COLUMN = 4;
    private static final Integer ACTIVITY_DESCRIPTION_COLUMN = 5;

    private Parent parent;

    EventHandler<TableColumn.CellEditEvent> cellEditEventEventHandler;
    private EventHandler<MouseEvent> onNewQACEventHandler;
    private EventHandler<MouseEvent> onDeleteQACEventHandler;

    @FXML
    private TitledPane headerPane;
    @FXML
    private TableView<QuoteAccountCode> quoteAccountCodeTableView;
    @FXML
    private TableColumn<QuoteAccountCode, QuoteAccountCodeRegimeEnum> regimeTableColumn;
    @FXML
    private TableColumn<QuoteAccountCode, String> provinceTableColumn;
    @FXML
    private TableColumn<QuoteAccountCode, String> numberTableColumn;
    @FXML
    private TableColumn<QuoteAccountCode, String> controlDigitTableColumn;
    @FXML
    private TableColumn<QuoteAccountCode, String> epigraphTableColumn;
    @FXML
    private TableColumn<QuoteAccountCode, String> activityDescriptionTableColumn;
    @FXML
    private Button newQACButton;
    @FXML
    private Button deleteQACButton;

    public EmployerQuoteAccountCodeTableView() {
        this.parent = ViewLoader.load(this, WORK_CENTERS_TABLE_VIEW_FXML);
    }

    public TitledPane getHeaderPane() {
        return headerPane;
    }

    public void setHeaderPane(TitledPane headerPane) {
        this.headerPane = headerPane;
    }

    public TableView<QuoteAccountCode> getQuoteAccountCodeTableView() {
        return quoteAccountCodeTableView;
    }

    public void setQuoteAccountCodeTableView(TableView<Address> addresses1) {
        this.quoteAccountCodeTableView = quoteAccountCodeTableView;
    }

    public TableColumn<QuoteAccountCode, QuoteAccountCodeRegimeEnum> getRegimeTableColumn() {
        return regimeTableColumn;
    }

    public void setRegimeTableColumn(TableColumn<QuoteAccountCode, QuoteAccountCodeRegimeEnum> regimeTableColumn) {
        this.regimeTableColumn = regimeTableColumn;
    }

    public TableColumn<QuoteAccountCode, String> getProvinceTableColumn() {
        return provinceTableColumn;
    }

    public void setProvinceTableColumn(TableColumn<QuoteAccountCode, String> provinceTableColumn) {
        this.provinceTableColumn = provinceTableColumn;
    }

    public TableColumn<QuoteAccountCode, String> getNumberTableColumn() {
        return numberTableColumn;
    }

    public void setNumberTableColumn(TableColumn<QuoteAccountCode, String> numberTableColumn) {
        this.numberTableColumn = numberTableColumn;
    }

    public TableColumn<QuoteAccountCode, String> getControlDigitTableColumn() {
        return controlDigitTableColumn;
    }

    public void setControlDigitTableColumn(TableColumn<QuoteAccountCode, String> controlDigitTableColumn) {
        this.controlDigitTableColumn = controlDigitTableColumn;
    }

    public TableColumn<QuoteAccountCode, String> getEpigraphTableColumn() {
        return epigraphTableColumn;
    }

    public void setEpigraphTableColumn(TableColumn<QuoteAccountCode, String> epigraphTableColumn) {
        this.epigraphTableColumn = epigraphTableColumn;
    }

    public TableColumn<QuoteAccountCode, String> getActivityDescriptionTableColumn() {
        return activityDescriptionTableColumn;
    }

    public void setActivityDescriptionTableColumn(TableColumn<QuoteAccountCode, String> activityDescriptionTableColumn) {
        this.activityDescriptionTableColumn = activityDescriptionTableColumn;
    }

    public Button getNewQACButton() {
        return newQACButton;
    }

    public void setNewQACButton(Button newQACButton) {
        this.newQACButton = newQACButton;
    }

    public Button getDeleteQACButton() {
        return deleteQACButton;
    }

    public void setDeleteQACButton(Button deleteQACButton) {
        this.deleteQACButton = deleteQACButton;
    }

    @FXML
    public void initialize() {

        getHeaderPane().setText("Códigos de Cuenta de Cotización (CCC) del cliente-empleador");
//        getNewAddressButton().setText("Añadir centro de trabajo");
//        getDeleteAddressButton().setText("Eliminar centro de trabajo");

        deleteQACButton.disableProperty().bind(quoteAccountCodeTableView.getSelectionModel().selectedItemProperty().isNull());

        newQACButton.setOnMouseClicked(this::onNewQACButton);
        deleteQACButton.setOnMouseClicked(this::onDeleteQACButton);

        quoteAccountCodeTableView.setEditable(true);

        regimeTableColumn.setOnEditCommit(this::regimeTableColumnOnEditCommit);


        List<QuoteAccountCodeRegimeEnum> quoteAccountCodeRegimeEnumList = new ArrayList<>();
        for(QuoteAccountCodeRegimeEnum quoteAccountCodeRegimeEnum : QuoteAccountCodeRegimeEnum.values()){
            if(quoteAccountCodeRegimeEnum.getActive()){
                quoteAccountCodeRegimeEnumList.add(quoteAccountCodeRegimeEnum);
            }
        }

        final ObservableList<QuoteAccountCodeRegimeEnum> regimenList = FXCollections.observableArrayList();
        regimenList.addAll(quoteAccountCodeRegimeEnumList);

        regimeTableColumn.setCellFactory(param -> {
            ComboBoxTableCell<QuoteAccountCode, QuoteAccountCodeRegimeEnum> comboBoxTableCell = new ComboBoxTableCell<>(regimenList);
            comboBoxTableCell.setPickOnBounds(true);
            comboBoxTableCell.updateSelected(true);
            comboBoxTableCell.setConverter(new StringConverter<>() {
                @Override
                public String toString(QuoteAccountCodeRegimeEnum object) {
                    if (object != null) {
                        regimeTableColumn.setStyle("-fx-text-fill: #000FFF;");
                        Tooltip tooltip = new Tooltip(object.getQuoteAccountCodeRegimeDescriptionResumed());
                        tooltip.setStyle("-fx-background-color: blue; -fx-text-fill: white;");
                        tooltip.setFont(Font.font("Noto Sans", 12));
                        comboBoxTableCell.setTooltip(tooltip);
                        return object.getQuoteAccountCodeRegimeDescriptionResumed();
                    }
                    return null;
                }

                @Override
                public QuoteAccountCodeRegimeEnum fromString(String string) {
                    return null;
                }
            });

            return comboBoxTableCell;
        });

        provinceTableColumn.setCellFactory(param-> new EditableStringQuoteAccountCodeTableCell<QuoteAccountCode, String>());
        numberTableColumn.setCellFactory(param-> new EditableStringQuoteAccountCodeTableCell<QuoteAccountCode, String>());
        controlDigitTableColumn.setCellFactory(param-> new EditableStringQuoteAccountCodeTableCell<QuoteAccountCode, String>());
        epigraphTableColumn.setCellFactory(param-> new EditableStringQuoteAccountCodeTableCell<QuoteAccountCode, String>());
        activityDescriptionTableColumn.setCellFactory(param-> new EditableStringQuoteAccountCodeTableCell<QuoteAccountCode, String>());

        regimeTableColumn.setCellValueFactory(new PropertyValueFactory<>("regime"));
        provinceTableColumn.setCellValueFactory(new PropertyValueFactory<>("province"));
        numberTableColumn.setCellValueFactory(new PropertyValueFactory<>("quoteAccountCodeNumber"));
        controlDigitTableColumn.setCellValueFactory(new PropertyValueFactory<>("controlDigit"));
        epigraphTableColumn.setCellValueFactory(new PropertyValueFactory<>("activityEpigraphNumber"));
        activityDescriptionTableColumn.setCellValueFactory(new PropertyValueFactory<>("activityDescription"));

        provinceTableColumn.setOnEditCommit(this::onTableCellEdited);
        numberTableColumn.setOnEditCommit(this::onTableCellEdited);
        controlDigitTableColumn.setOnEditCommit(this::onTableCellEdited);
        epigraphTableColumn.setOnEditCommit(this::onTableCellEdited);
        activityDescriptionTableColumn.setOnEditCommit(this::onTableCellEdited);

        // Unselect selected row
        quoteAccountCodeTableView.setRowFactory(param -> {
            final TableRow<QuoteAccountCode> row = new TableRow<>();
            row.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getButton().equals(MouseButton.SECONDARY)) {
                        final int index = row.getIndex();
                        if (index >= 0 && index < quoteAccountCodeTableView.getItems().size() && quoteAccountCodeTableView.getSelectionModel().isSelected(index)) {
                            quoteAccountCodeTableView.getSelectionModel().clearSelection();
                            event.consume();
                        }
                    }
                }
            });
            return row;
        });


        provinceTableColumn.getStyleClass().add("center");
        numberTableColumn.getStyleClass().add("center");
        controlDigitTableColumn.getStyleClass().add("center");
        epigraphTableColumn.getStyleClass().add("center");
    }

    public void initializeView(){


    }

    private void regimeTableColumnOnEditCommit(TableColumn.CellEditEvent event){

        onTableCellEdited(event);

        int editedRow = event.getTablePosition().getRow();
        int editedColumn = event.getTablePosition().getColumn();

        QuoteAccountCode editedQuoteAccountCode = quoteAccountCodeTableView.getItems().get(editedRow);

        editedQuoteAccountCode.setRegime(quoteAccountCodeTableView.getItems().get(editedRow).getRegime());
        editedQuoteAccountCode.setProvince(quoteAccountCodeTableView.getItems().get(editedRow).getProvince());
        editedQuoteAccountCode.setQuoteAccountCodeNumber(quoteAccountCodeTableView.getItems().get(editedRow).getQuoteAccountCodeNumber());
        editedQuoteAccountCode.setControlDigit(quoteAccountCodeTableView.getItems().get(editedRow).getControlDigit());
        editedQuoteAccountCode.setActivityEpigraphNumber(quoteAccountCodeTableView.getItems().get(editedRow).getActivityEpigraphNumber());
        editedQuoteAccountCode.setActivityDescription(quoteAccountCodeTableView.getItems().get(editedRow).getActivityDescription());

        int rowNumber;
        for(rowNumber = 0; rowNumber< quoteAccountCodeTableView.getItems().size(); rowNumber++){
            QuoteAccountCode notEditedQuoteAccountCode = quoteAccountCodeTableView.getItems().get(rowNumber);
            if(rowNumber != editedRow){
                notEditedQuoteAccountCode.setRegime(quoteAccountCodeTableView.getItems().get(rowNumber).getRegime());
                notEditedQuoteAccountCode.setProvince(quoteAccountCodeTableView.getItems().get(rowNumber).getProvince());
                notEditedQuoteAccountCode.setQuoteAccountCodeNumber(quoteAccountCodeTableView.getItems().get(rowNumber).getQuoteAccountCodeNumber());
                notEditedQuoteAccountCode.setControlDigit(quoteAccountCodeTableView.getItems().get(rowNumber).getControlDigit());
                notEditedQuoteAccountCode.setActivityEpigraphNumber(quoteAccountCodeTableView.getItems().get(rowNumber).getActivityEpigraphNumber());
                notEditedQuoteAccountCode.setActivityDescription(quoteAccountCodeTableView.getItems().get(rowNumber).getActivityDescription());
            }
        }

//        // It is not valid to remove the default address mark if the address is the only one.
//        if(event.getTableView().getItems().size() == 1 && event.getNewValue() == Boolean.FALSE){
//            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.DEFAULT_ADDRESS_INVALID_CHANGE);
//            quoteAccountCodeTableView.getItems().get(DEFAULT_ADDRESS_COLUMN).setDefaultAddress(Boolean.TRUE);
//        }

        refreshQuoteAccountCodeTable(quoteAccountCodeTableView.getItems());
    }

    private void onTableCellEdited(TableColumn.CellEditEvent cellEditEvent){

        cellEditEventEventHandler.handle(cellEditEvent);

        int editedRow = cellEditEvent.getTablePosition().getRow();
        int editedColumn = cellEditEvent.getTablePosition().getColumn();
        QuoteAccountCode selectedItemQuoteAccountCode = quoteAccountCodeTableView.getItems().get(editedRow);

        if(editedColumn == REGIME_COLUMN){
            selectedItemQuoteAccountCode.setRegime((QuoteAccountCodeRegimeEnum) cellEditEvent.getNewValue());
        }
        if(editedColumn == PROVINCE_COLUMN){
            selectedItemQuoteAccountCode.setProvince((String) cellEditEvent.getNewValue());
        }

        if(editedColumn == NUMBER_COLUMN){
            selectedItemQuoteAccountCode.setQuoteAccountCodeNumber((String) cellEditEvent.getNewValue());
        }

        if(editedColumn == CONTROL_DIGIT_COLUMN){
            selectedItemQuoteAccountCode.setControlDigit((String) cellEditEvent.getNewValue());
        }

        if(editedColumn == EPIGRAPH_COLUMN){
            selectedItemQuoteAccountCode.setActivityEpigraphNumber((String) cellEditEvent.getNewValue());
        }

        if(editedColumn == ACTIVITY_DESCRIPTION_COLUMN){
            selectedItemQuoteAccountCode.setActivityDescription((String) cellEditEvent.getNewValue());
        }

        refreshQuoteAccountCodeTable(quoteAccountCodeTableView.getItems());
    }

    private void onNewQACButton(MouseEvent event){
        this.onNewQACEventHandler.handle(event);
    }

    private void onDeleteQACButton(MouseEvent event){
        this.onDeleteQACEventHandler.handle(event);
    }

    public void refreshAddressTable(List<QuoteAccountCode> addressesItemList){
        quoteAccountCodeTableView.setItems(FXCollections.observableList(addressesItemList));
        quoteAccountCodeTableView.refresh();
    }

    public void refreshQuoteAccountCodeTable(List<QuoteAccountCode> addressesItemList){
        quoteAccountCodeTableView.setItems(FXCollections.observableList(addressesItemList));
        quoteAccountCodeTableView.refresh();
    }

    public void setOnNewQACButton(EventHandler<MouseEvent> onNewQACEventHandler){
        this.onNewQACEventHandler = onNewQACEventHandler;
    }

    public void setOnDeleteQACButton(EventHandler<MouseEvent> onDeleteQACEventHandler){
        this.onDeleteQACEventHandler = onDeleteQACEventHandler;
    }


    public Boolean validateNoMissingAddresses(){
        if(quoteAccountCodeTableView.getItems().size() == 0){
            return false;
        }

        return true;
    }

//    public Boolean validateDefaultAddressIsSet() {
//        ObservableList<QuoteAccountCode> quoteAccountCodes = this.quoteAccountCodeTableView.getItems();
//
//        for (QuoteAccountCode quoteAccountCode : quoteAccountCodes) {
//            if (quoteAccountCode.getDefaultAddress()) {
//                return Boolean.TRUE;
//            }
//        }
//
//        return Boolean.FALSE;
//    }

//    public Boolean validateAllAddressesAreComplete() {
//        ObservableList<Address> addressesOnTableView = quoteAccountCodeTableView.getItems();
//        for (Integer i = 0; i < addressesOnTableView.size(); i++) {
//            Address actualWorkCenter = addressesOnTableView.get(i);
//            if (actualWorkCenter.getStreetName().equals("") ||
//                    actualWorkCenter.getStreetExtended().equals("") ||
//                    actualWorkCenter.getLocation().equals("") ||
//                    actualWorkCenter.getMunicipality().equals("") ||
//                    actualWorkCenter.getPostalCode().equals("")) {
//
//                return Boolean.FALSE;
//            }
//        }
//
//        return Boolean.TRUE;
//    }

    public void setOnAnyCellChange(EventHandler<TableColumn.CellEditEvent> event){
        this.cellEditEventEventHandler = event;
    }
}
