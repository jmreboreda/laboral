package laboral.component.employer_management.employer_creation;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class EmployerCreationHeader extends AnchorPane {

    private static final String NEW_PERSON_HEADER_FXML = "/fxml/employer_management/employer_creation/employer_creation_header.fxml";

    private Parent parent;

    @FXML
    private Label headerText;

    public EmployerCreationHeader() {
        this.parent = ViewLoader.load(this, NEW_PERSON_HEADER_FXML);
    }

    public Label getHeaderText() {
        return headerText;
    }
}
