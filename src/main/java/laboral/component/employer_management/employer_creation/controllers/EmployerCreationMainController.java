package laboral.component.employer_management.employer_creation.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Callback;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.employer_management.EmployerManagementConstants;
import laboral.component.employer_management.common_components.EmployerQuoteAccountCodeTableView;
import laboral.component.employer_management.common_components.EmployerWorkCenterTableView;
import laboral.component.employer_management.employer_creation.EmployerCreationAction;
import laboral.component.employer_management.employer_creation.EmployerCreationHeader;
import laboral.domain.address.Address;
import laboral.domain.address.StreetType;
import laboral.domain.address.controller.AddressController;
import laboral.domain.address.mapper.MapperAddressDBOToAddress;
import laboral.domain.client.Client;
import laboral.domain.client.ClientService;
import laboral.domain.client.persistence.dbo.ClientDBO;
import laboral.domain.employer.Employer;
import laboral.domain.employer.EmployerCreationRequest;
import laboral.domain.employer.controller.EmployerController;
import laboral.domain.person.controller.PersonController;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.quote_account_code.QuoteAccountCodeRegimeEnum;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.validator.EmployerCreationDataValidator;
import laboral.domain.work_center.controller.WorkCenterController;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class EmployerCreationMainController extends AnchorPane {

    private static final Logger logger = Logger.getLogger(EmployerCreationMainController.class.getSimpleName());
    private static final String EMPLOYER_CREATION_MAIN_FXML = "/fxml/employer_management/employer_creation/employer_creation_main_controller.fxml";
    private static final String ADVISORY = "Asesoría";

    private Boolean loadingData = Boolean.FALSE;
    private Client clientToEmployerCreation;

    private final EmployerController employerController = new EmployerController();
    private final PersonController personController = new PersonController();
    private final AddressController addressController = new AddressController();
    private final WorkCenterController workCenterController = new WorkCenterController();

    @FXML
    private EmployerCreationHeader employerCreationHeader;
    @FXML
    private ComboBox<Employer> employerSelector;
    @FXML
    private EmployerWorkCenterTableView employerWorkCenterTableView;
    @FXML
    private EmployerQuoteAccountCodeTableView employerQuoteAccountCodeTableView;
    @FXML
    private EmployerCreationAction employerCreationAction;


    public EmployerCreationMainController() {
        logger.info("Initializing employer management main fxml");
        Parent parent = ViewLoader.load(this, EMPLOYER_CREATION_MAIN_FXML);

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());

        employerWorkCenterTableView.setOnAnyCellChange(this::onAnyCellAddressesChange);
        employerQuoteAccountCodeTableView.setOnAnyCellChange(this::onAnyCellQuoteAccountCodesChange);

        employerCreationAction.getOkButton().setOnMouseClicked(this::onOkButton);
//        employerCreationAction.getSaveButton().setOnMouseClicked(this::onSaveButton);
        employerCreationAction.getExitButton().setOnMouseClicked(this::onExitButton);
    }

    @FXML
    public void initialize(){

        employerSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 20));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.TOP_CENTER);
                    setText(item.toString());
                }
            }

        });

        employerSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<Employer> call(ListView<Employer> param) {
                        final ListCell<Employer> cell = new ListCell<Employer>() {
                            @Override
                            public void updateItem(Employer item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }

                });

        employerWorkCenterTableView.getAddresses().setEditable(false);
//
//        clientServiceTableView.setOnServiceCellTableChange(this::onServiceCellTableChange);
//        clientServiceTableView.setOnNewServiceButton(this::onNewServiceButton);
//        clientServiceTableView.setOnDeleteServiceButton(this::onDeleteServiceButton);
//
        employerWorkCenterTableView.setOnNewWorkCenterButton(this::onNewWorkCenterButton);
        employerWorkCenterTableView.setOnDeleteWorkCenterButton(this::onDeleteWorkCenterButton);

        employerQuoteAccountCodeTableView.setOnNewQACButton(this::onNewQACButton);
        employerQuoteAccountCodeTableView.setOnDeleteQACButton(this::onDeleteQACButton);
        employerCreationAction.getSaveButton().setOnMouseClicked(this::onSaveButton);

        employerCreationAction.getOkButton().setDisable(true);
    }

    public EmployerCreationHeader getEmployerCreationHeader() {
        return employerCreationHeader;
    }

    public void setClientManagementHeader(EmployerCreationHeader employerManagementHeader) {
        this.employerCreationHeader = employerManagementHeader;
    }

    public ComboBox<Employer> getEmployerSelector() {
        return employerSelector;
    }

    public void setEmployerSelector(ComboBox<Employer> employerSelector) {
        this.employerSelector = employerSelector;
    }

    public EmployerWorkCenterTableView getEmployerWorkCenterTableView() {
        return employerWorkCenterTableView;
    }

    public void setEmployerWorkCenterTableView(EmployerWorkCenterTableView employerWorkCenterTableView) {
        this.employerWorkCenterTableView = employerWorkCenterTableView;
    }

    public EmployerQuoteAccountCodeTableView getEmployerQuoteAccountCodeTableView() {
        return employerQuoteAccountCodeTableView;
    }

    public void setEmployerQuoteAccountCodeTableView(EmployerQuoteAccountCodeTableView employerQuoteAccountCodeTableView) {
        this.employerQuoteAccountCodeTableView = employerQuoteAccountCodeTableView;
    }

    public void loadClientToEmployer(Client client){

        ClientService clientService = ClientService.ClientServiceFactory.getInstance();

//        Client client = this.clientToEmployerCreation;

        ClientDBO clientDBO = clientService.findClientDBOById(client.getClientId());

        Employer employer = Employer.EmployerBuilder.create()
                .withId(null)
                .withClient(client)
                .withQuoteAccountCodes(null)
                .build();

        List<Employer> employerList = new ArrayList<>();
        employerList.add(employer);

        ObservableList<Employer> ClientToEmployerObservableList = FXCollections.observableList(employerList);
        employerSelector.getItems().setAll(ClientToEmployerObservableList);
        employerSelector.getSelectionModel().select(0);

        List<Address> workCenterClientList = new ArrayList<>();
        MapperAddressDBOToAddress mapperAddressDBOToAddress = new MapperAddressDBOToAddress();
        for(WorkCenterDBO workCenterDBO : clientDBO.getWorkCenters()){
            Address address = mapperAddressDBOToAddress.map(workCenterDBO.getAddressDBO());
            workCenterClientList.add(address);
        }

        employerWorkCenterTableView.getAddresses().getItems().addAll(workCenterClientList);
        employerWorkCenterTableView.getAddresses().setEditable(true);
    }


//
//    public void setClientServiceTableView(ClientServiceTableView clientServiceTableView) {
//        this.clientServiceTableView = clientServiceTableView;
//    }
//
//    public ClientWorkCenterTableView getClientWorkCenterTableView() {
//        return clientWorkCenterTableView;
//    }
//
//    public void setClientWorkCenterTableView(ClientWorkCenterTableView clientWorkCenterTableView) {
//        this.clientWorkCenterTableView = clientWorkCenterTableView;
//    }
//
//    public ClientManagementAction getClientManagementAction() {
//        return clientManagementAction;
//    }
//
//    public void setClientManagementAction(ClientManagementAction clientManagementAction) {
//        this.clientManagementAction = clientManagementAction;
//    }

    private void onActivityPeriodCellTableChange(CellEditEvent event){
        if(loadingData){
            return;
        }

        if(event.getOldValue() != null && event.getNewValue() == null ||
        event.getNewValue() != null && event.getOldValue() == null ||
                !event.getNewValue().equals(event.getOldValue())) {
            employerCreationAction.toFront();
            employerCreationAction.getOkButton().setVisible(true);
            employerCreationAction.getOkButton().setDisable(false);
            employerCreationAction.getSaveButton().setDisable(true);
        }
    }

    private void onServiceCellTableChange(CellEditEvent event){
        if(loadingData){
            return;
        }

        if(event.getOldValue() != null && event.getNewValue() == null ||
                event.getNewValue() != null && event.getOldValue() == null ||
                !event.getNewValue().equals(event.getOldValue())) {
            employerCreationAction.toFront();
            employerCreationAction.getOkButton().setVisible(true);
            employerCreationAction.getOkButton().setDisable(false);
            employerCreationAction.getSaveButton().setDisable(true);
        }
    }

    private void onAnyCellAddressesChange(CellEditEvent event){
        if(loadingData){
            return;
        }

        if(!event.getNewValue().equals(event.getOldValue())) {
            employerCreationAction.getOkButton().setVisible(true);
            employerCreationAction.getOkButton().setDisable(false);
            employerCreationAction.getSaveButton().setDisable(true);
        }


//        ClientManagementDataValidator clientManagementDataValidator = new ClientManagementDataValidator(this);
//        if(!clientManagementDataValidator.validateActivityPeriodDataEntry())
//        {
//            return;
//        };
//
//        if(!event.getNewValue().equals(event.getOldValue())) {
//            clientManagementAction.toFront();
//            clientManagementAction.getOkButton().setVisible(true);
//            clientManagementAction.getOkButton().setDisable(false);
//            clientManagementAction.getSaveButton().setDisable(true);
//        }
    }

    private void onAnyCellQuoteAccountCodesChange(CellEditEvent event){
        if(loadingData){
            return;
        }

        if(!event.getNewValue().equals(event.getOldValue())) {
            employerCreationAction.getOkButton().setVisible(true);
            employerCreationAction.getOkButton().setDisable(false);
            employerCreationAction.getSaveButton().setDisable(true);
        }
    }

//
//    private void onNewActivityPeriod(MouseEvent event){
//        Integer clientId = clientSelector.getSelectionModel().getSelectedItem().getId();
//        ActivityPeriod newActivityPeriod = new ActivityPeriod(null, LocalDate.now(), null, null, null);
//        clientActivityPeriodsTableView.getActivityPeriodTableView().getItems().add(newActivityPeriod);
//        clientActivityPeriodsTableView.getActivityPeriodTableView().refresh();
//
//        clientManagementAction.getOkButton().setVisible(true);
//        clientManagementAction.getOkButton().setDisable(false);
//        clientManagementAction.getSaveButton().setDisable(true);
//    }
//
//    public void onDeleteActivityPeriod(MouseEvent event){
//        if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.QUESTION_DELETE_ACTIVITY_PERIOD_IS_CORRECT)){
//            return;
//        }
//
//        ActivityPeriod activityPeriodSelectedItem = clientActivityPeriodsTableView.getActivityPeriodTableView().getSelectionModel().getSelectedItem();
//        clientActivityPeriodsTableView.getActivityPeriodTableView().getItems().remove(activityPeriodSelectedItem);
//        clientActivityPeriodsTableView.getActivityPeriodTableView().getSelectionModel().clearSelection();
//
//        clientManagementAction.getSaveButton().setDisable(true);
//        clientManagementAction.getOkButton().setVisible(true);
//        clientManagementAction.getOkButton().setDisable(false);
//    }
//
//     private void onNewServiceButton(MouseEvent event) {
//        ServiceGM serviceGM = new ServiceGM(null, ServiceGMEnum.SERVICE_NOT_ESTABLISHED,LocalDate.now(),LocalDate.now(),false,null);
//        clientServiceTableView.getServices().getItems().add(serviceGM);
//        clientServiceTableView.getServices().refresh();
//
//        clientManagementAction.getOkButton().setVisible(true);
//        clientManagementAction.getOkButton().setDisable(false);
//        clientManagementAction.getSaveButton().setDisable(true);
//     }
//
//     private void onDeleteServiceButton(MouseEvent event){
//         if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.QUESTION_DELETE_SERVICE_IS_CORRECT)){
//             return;
//         }
//
//         ServiceGM persistedServiceGMSelectedItem = clientServiceTableView.getServices().getSelectionModel().getSelectedItem();
//         clientServiceTableView.getServices().getItems().remove(persistedServiceGMSelectedItem);
//         clientServiceTableView.getServices().getSelectionModel().clearSelection();
//
//         clientManagementAction.getSaveButton().setDisable(true);
//         clientManagementAction.getOkButton().setVisible(true);
//         clientManagementAction.getOkButton().setDisable(false);
//
//     }
//
     private void onNewWorkCenterButton(MouseEvent event){
         Address newWorkCenter = new Address(null, StreetType.UNKN,"-----", "-----", "-----", "-----", null, "-----", false);
         employerWorkCenterTableView.getAddresses().getItems().add(newWorkCenter);

         employerCreationAction.getOkButton().setVisible(true);
         employerCreationAction.getOkButton().setDisable(false);
         employerCreationAction.getSaveButton().setDisable(true);
     }

    private void onDeleteWorkCenterButton(MouseEvent event){
        if(employerWorkCenterTableView.getAddresses().getItems().size() == 1){
            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, EmployerManagementConstants.CANNOT_DELETE_THE_ONLY_EXISTING_WORK_CENTER);
            return;
        }

        if(employerWorkCenterTableView.getAddresses().getSelectionModel().getSelectedItem().getDefaultAddress() == Boolean.TRUE) {
            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, EmployerManagementConstants.CANNOT_DELETE_THE_DEFAULT_ADDRESS);
            return;
        }

        if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, EmployerManagementConstants.QUESTION_DELETE_ADDRESS_IS_CORRECT)){
            return;
        }

        Address workCentersSelectedItem = employerWorkCenterTableView.getAddresses().getSelectionModel().getSelectedItem();
        employerWorkCenterTableView.getAddresses().getItems().remove(workCentersSelectedItem);
        employerWorkCenterTableView.getAddresses().getSelectionModel().clearSelection();

        employerCreationAction.getOkButton().setVisible(true);
        employerCreationAction.getOkButton().setDisable(false);
    }

    private void onNewQACButton(MouseEvent event){
        QuoteAccountCode quoteAccountCode = new QuoteAccountCode(null,
                QuoteAccountCodeRegimeEnum.GENERAL_REGIME,
                "99",
                "9999999",
                "99",
                "999",
                "------------------------------");
        employerQuoteAccountCodeTableView.getQuoteAccountCodeTableView().getItems().add(quoteAccountCode);

        employerCreationAction.getOkButton().setVisible(true);
        employerCreationAction.getOkButton().setDisable(false);
        employerCreationAction.getSaveButton().setDisable(true);
    }

    private void onDeleteQACButton(MouseEvent event){
        QuoteAccountCode quoteAccountCodeSelectedItem = employerQuoteAccountCodeTableView.getQuoteAccountCodeTableView().getSelectionModel().getSelectedItem();
        employerQuoteAccountCodeTableView.getQuoteAccountCodeTableView().getItems().remove(quoteAccountCodeSelectedItem);
        employerQuoteAccountCodeTableView.getQuoteAccountCodeTableView().getSelectionModel().clearSelection();

        employerCreationAction.getOkButton().setVisible(true);
        employerCreationAction.getOkButton().setDisable(false);

    }
//
//    private void onNewEmployerButton(MouseEvent event){
//        Client client = clientSelector.getSelectionModel().getSelectedItem();
//        System.out.println(client.toAlphabeticalName());
//
//    }
//
    private void onOkButton(MouseEvent event){

        EmployerCreationDataValidator employerCreationDataValidator = new EmployerCreationDataValidator(this);
        if(!employerCreationDataValidator.validateWorkCenterDataEntry() ||
        !employerCreationDataValidator.validateQuoteAccountCodeDataEntry())
        {

            employerCreationAction.getOkButton().setDisable(true);
            employerCreationAction.getSaveButton().setDisable(true);
            return;
        };

        employerCreationAction.getOkButton().setDisable(true);
        employerCreationAction.getSaveButton().setDisable(false);
    }

    private void onSaveButton(MouseEvent event){

        employerCreationAction.getSaveButton().setDisable(true);

        EmployerCreationRequest employerCreationRequest = new EmployerCreationRequest().create(employerSelector.getSelectionModel().getSelectedItem());

        Integer employerId = employerController.createEmployer(employerCreationRequest);

        if(employerId != null){
            Employer employer = employerController.findById(employerId);
            Message.informationMessage((Stage) employerCreationHeader.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, EmployerManagementConstants.NEW_EMPLOYER_SAVED_OK);
            logger.info("Employer creation: the employer has been successfully registered.");
        }
    }
//
    private void onExitButton(MouseEvent event){
        logger.info("Employer creation: exiting program.");

        Stage stage = (Stage) employerCreationHeader.getScene().getWindow();
        stage.close();
    }
//
//    private void interfaceInitialize(){
//        clientSelector.getItems().clear();
//        clientSelector.getSelectionModel().clearSelection();
//        clientActivityPeriodsTableView.getActivityPeriodTableView().getItems().clear();
//        clientServiceTableView.getServices().getItems().clear();
//        clientWorkCenterTableView.getAddresses().getItems().clear();
//    }
//
//    private Boolean isAdvisoryClient(Client selectedClient){
//        for(ServiceGM serviceGM : selectedClient.getServices()){
//            if(serviceGM.getServiceGM().getServiceGMDescription().contains(ADVISORY)){
//                if(serviceGM.getDateTo() == null){
//                    return Boolean.TRUE;
//                }
//            }
//        }
//
//        return Boolean.FALSE;
//    }
//
//    private Boolean isAlreadyEmployer(Client selectedClient){
//        EmployerController employerController = new EmployerController();
//        Employer clientIsEmployer = employerController.findEmployerByClientId(selectedClient.getClientId());
//        if(clientIsEmployer != null){
//
//            return Boolean.TRUE;
//        }
//
//        return Boolean.FALSE;
//    }
//
//    private Set<WorkCenter> prepareWorkCenterSetToUpdate(Integer personId){
//
//        Set<WorkCenter> workCenterSet = new HashSet<>();
//
//        Set<Address> addressesInWorkCenterTableView = new HashSet<>(clientWorkCenterTableView.getAddresses().getItems());
//
//        for(Address address : addressesInWorkCenterTableView){
//            if(address.getId() == null){
//                String postalCode = address.getPostalCode();
//                Province province = null;
//                if(!postalCode.isEmpty()) {
//                    String provinceInPostalCode = postalCode.substring(0, 2);
//                    for (Province provinceRead : Province.values()) {
//                        if (provinceRead.getCode().equals(provinceInPostalCode)) {
//                            province = provinceRead;
//                            break;
//                        }
//                    }
//                    address.setProvince(province);
//                }
//            }
//
//            WorkCenter newWorkCenter = new WorkCenter(null, address);
//            workCenterSet.add(newWorkCenter);
//        }
//
//        return workCenterSet;
//    }

//    public void setEmployerCreate(Client client){
//        this.clientToEmployerCreation = client;
//    }
}
