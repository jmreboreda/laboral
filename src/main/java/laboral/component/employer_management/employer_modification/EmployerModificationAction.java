package laboral.component.employer_management.employer_modification;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class EmployerModificationAction extends AnchorPane {

    private static final String NEW_PERSON_ACTION_FXML = "/fxml/employer_management/employer_modification/employer_modification_action_components.fxml";

    private Parent parent;


    @FXML
    private Button okButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button exitButton;

    public EmployerModificationAction() {
        this.parent = ViewLoader.load(this, NEW_PERSON_ACTION_FXML);
    }

    @FXML
    public void initialize(){
    }

    public Button getOkButton() {
        return okButton;
    }

    public Button getSaveButton() {
        return saveButton;
    }

    public Button getExitButton() {
        return exitButton;
    }

    public void setOkButton(Button okButton) {
        this.okButton = okButton;
    }

    public void setSaveButton(Button saveButton) {
        this.saveButton = saveButton;
    }

    public void setExitButton(Button exitButton) {
        this.exitButton = exitButton;
    }

    private void onMouseClicked(){

    }
}
