package laboral.component.employer_management.employer_modification.controller;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Callback;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.employer_management.EmployerManagementConstants;
import laboral.component.employer_management.common_components.EmployerQuoteAccountCodeTableView;
import laboral.component.employer_management.common_components.EmployerWorkCenterTableView;
import laboral.component.employer_management.employer_modification.EmployerModificationAction;
import laboral.component.employer_management.employer_modification.EmployerModificationHeader;
import laboral.domain.address.Address;
import laboral.domain.address.controller.AddressController;
import laboral.domain.address.mapper.MapperAddressDBOToAddress;
import laboral.domain.client.Client;
import laboral.domain.contract.WorkContract;
import laboral.domain.contract.work_contract.controller.WorkContractController;
import laboral.domain.employer.Employer;
import laboral.domain.employer.EmployerService;
import laboral.domain.employer.controller.EmployerController;
import laboral.domain.employer.mapper.MapperEmployerDBOToEmployer;
import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.person.controller.PersonController;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.quote_account_code.QuoteAccountCodeRegimeEnum;
import laboral.domain.quote_account_code.mapper.MapperQuoteAccountCodeDBOToQuoteAccountCode;
import laboral.domain.quote_account_code.persistence.dbo.QuoteAccountCodeDBO;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.validator.EmployerModificationDataValidator;
import laboral.domain.work_center.controller.WorkCenterController;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public class EmployerModificationMainController extends AnchorPane {

    private static final Logger logger = Logger.getLogger(EmployerModificationMainController.class.getSimpleName());
    private static final String EMPLOYER_CREATION_MAIN_FXML = "/fxml/employer_management/employer_modification/employer_modification_main_controller.fxml";
    private static final String ADVISORY = "Asesoría";

    private Boolean loadingData = Boolean.FALSE;
    private Employer employerToModify;

    private final EmployerController employerController = new EmployerController();
    private final PersonController personController = new PersonController();
    private final AddressController addressController = new AddressController();
    private final WorkCenterController workCenterController = new WorkCenterController();

    @FXML
    private EmployerModificationHeader employerModificationHeader;
    @FXML
    private ComboBox<Employer> employerSelector;
    @FXML
    private EmployerWorkCenterTableView employerWorkCenterTableView;
    @FXML
    private EmployerQuoteAccountCodeTableView employerQuoteAccountCodeTableView;
    @FXML
    private EmployerModificationAction employerModificationAction;


    public EmployerModificationMainController() {
        logger.info("Initializing employer management main fxml");
        Parent parent = ViewLoader.load(this, EMPLOYER_CREATION_MAIN_FXML);

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());

        employerWorkCenterTableView.setOnAnyCellChange(this::onAnyCellAddressesChange);
        employerQuoteAccountCodeTableView.setOnAnyCellChange(this::onAnyCellQuoteAccountCodesChange);

        employerModificationAction.getOkButton().setOnMouseClicked(this::onOkButton);
        employerModificationAction.getSaveButton().setOnMouseClicked(this::onSaveButton);
        employerModificationAction.getExitButton().setOnMouseClicked(this::onExitButton);
    }

    @FXML
    public void initialize(){

        this.addEventHandler(KeyEvent.KEY_PRESSED, t -> {
            if(t.getCode() == KeyCode.ESCAPE)
            {
             onExitButton(null);
            }
        });

        employerWorkCenterTableView.getNewAddressButton().setDisable(true);
        employerWorkCenterTableView.getDeleteAddressButton().setDisable(true);

        employerSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 20));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.TOP_CENTER);
                    setText(item.toString());
                }
            }

        });

        employerSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<Employer> call(ListView<Employer> param) {
                        final ListCell<Employer> cell = new ListCell<Employer>() {
                            @Override
                            public void updateItem(Employer item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }

                });

        employerWorkCenterTableView.getAddresses().setEditable(false);

        employerQuoteAccountCodeTableView.setOnNewQACButton(this::onNewQACButton);
        employerQuoteAccountCodeTableView.setOnDeleteQACButton(this::onDeleteQACButton);

        employerModificationAction.getOkButton().setDisable(true);
    }

    public EmployerModificationHeader getEmployerModificationHeader() {
        return employerModificationHeader;
    }

    public void setClientManagementHeader(EmployerModificationHeader employerModificationHeader) {
        this.employerModificationHeader = employerModificationHeader;
    }

    public EmployerWorkCenterTableView getEmployerWorkCenterTableView() {
        return employerWorkCenterTableView;
    }

    public void setEmployerWorkCenterTableView(EmployerWorkCenterTableView employerWorkCenterTableView) {
        this.employerWorkCenterTableView = employerWorkCenterTableView;
    }

    public EmployerQuoteAccountCodeTableView getEmployerQuoteAccountCodeTableView() {
        return employerQuoteAccountCodeTableView;
    }

    public void setEmployerQuoteAccountCodeTableView(EmployerQuoteAccountCodeTableView employerQuoteAccountCodeTableView) {
        this.employerQuoteAccountCodeTableView = employerQuoteAccountCodeTableView;
    }

    public void loadEmployerFromSelectedClient(Client client){

        MapperEmployerDBOToEmployer mapperEmployerDBOToEmployer = new MapperEmployerDBOToEmployer();

        EmployerService employerService = EmployerService.EmployerServiceFactory.getInstance();
        EmployerDBO employerDBO = employerService.findByClientId(client.getClientId());
        Employer employer = mapperEmployerDBOToEmployer.map(employerDBO);

        employerSelector.getItems().addAll(employer);
        employerSelector.getSelectionModel().select(0);

        List<Address> workCenterClientList = new ArrayList<>();
        MapperAddressDBOToAddress mapperAddressDBOToAddress = new MapperAddressDBOToAddress();
        for(WorkCenterDBO workCenterDBO : employerDBO.getClientDBO().getWorkCenters()){
            Address address = mapperAddressDBOToAddress.map(workCenterDBO.getAddressDBO());
            workCenterClientList.add(address);
        }

        List<QuoteAccountCode> quoteAccountCodeList = new ArrayList<>();
        MapperQuoteAccountCodeDBOToQuoteAccountCode mapperQuoteAccountCodeDBOToQuoteAccountCode = new MapperQuoteAccountCodeDBOToQuoteAccountCode();
        for(QuoteAccountCodeDBO quoteAccountCodeDBO : employerDBO.getQuoteAccountCodes()){
            QuoteAccountCode quoteAccountCode = mapperQuoteAccountCodeDBOToQuoteAccountCode.map(quoteAccountCodeDBO);
            quoteAccountCodeList.add(quoteAccountCode);
        }

        employerWorkCenterTableView.refreshAddressTable(workCenterClientList);
        employerQuoteAccountCodeTableView.refreshQuoteAccountCodeTable(quoteAccountCodeList);
        employerQuoteAccountCodeTableView.getQuoteAccountCodeTableView().setEditable(true);
    }

    public ComboBox<Employer> getEmployerSelector() {
        return employerSelector;
    }

    public void setEmployerSelector(ComboBox<Employer> employerSelector) {
        this.employerSelector = employerSelector;
    }

    private void onAnyCellAddressesChange(CellEditEvent event){
        if(loadingData){
            return;
        }

        if(!event.getNewValue().equals(event.getOldValue())) {
            employerModificationAction.getOkButton().setVisible(true);
            employerModificationAction.getOkButton().setDisable(false);
            employerModificationAction.getSaveButton().setDisable(true);
        }
    }

    private void onAnyCellQuoteAccountCodesChange(CellEditEvent event){
        if(loadingData){
            return;
        }

        if(!event.getNewValue().equals(event.getOldValue())) {
            employerModificationAction.getOkButton().setVisible(true);
            employerModificationAction.getOkButton().setDisable(false);
            employerModificationAction.getSaveButton().setDisable(true);
        }
    }



    private void onNewQACButton(MouseEvent event){
        QuoteAccountCode quoteAccountCode = new QuoteAccountCode(null,
                QuoteAccountCodeRegimeEnum.GENERAL_REGIME,
                "**",
                "*******",
                "**",
                "***.*",
                "**********************************");
        employerQuoteAccountCodeTableView.getQuoteAccountCodeTableView().getItems().add(quoteAccountCode);

        employerModificationAction.getOkButton().setVisible(true);
        employerModificationAction.getOkButton().setDisable(false);
        employerModificationAction.getSaveButton().setDisable(true);
    }

    private void onDeleteQACButton(MouseEvent event){
        QuoteAccountCode quoteAccountCodeSelectedItem = employerQuoteAccountCodeTableView.getQuoteAccountCodeTableView().getSelectionModel().getSelectedItem();

        WorkContractController workContractController =new WorkContractController();
        List<WorkContract> workContractList = workContractController.findByQuoteAccountCodeId(quoteAccountCodeSelectedItem.getId());
        if(!workContractList.isEmpty()){
            Message.errorMessage((Stage) employerModificationHeader.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, EmployerManagementConstants.IMPOSSIBLE_TO_DELETE_THERE_ARE_CONTRACTS_WITH_THIS_QAC);
            return;
        }

        employerQuoteAccountCodeTableView.getQuoteAccountCodeTableView().getItems().remove(quoteAccountCodeSelectedItem);
        employerQuoteAccountCodeTableView.getQuoteAccountCodeTableView().getSelectionModel().clearSelection();

        employerModificationAction.getOkButton().setVisible(true);
        employerModificationAction.getOkButton().setDisable(false);

    }

    private void onOkButton(MouseEvent event){

        EmployerModificationDataValidator employerModificationDataValidator = new EmployerModificationDataValidator(this);
        if(!employerModificationDataValidator.validateQuoteAccountCodeDataEntry()){

            employerModificationAction.getOkButton().setDisable(true);
            employerModificationAction.getSaveButton().setDisable(true);
            return;
        };

        employerModificationAction.getOkButton().setDisable(true);
        employerModificationAction.getSaveButton().setDisable(false);
    }

    private void onSaveButton(MouseEvent event){

        employerModificationAction.getSaveButton().setDisable(true);

        Employer employerSelected = employerSelector.getSelectionModel().getSelectedItem();
        Employer employerToUpdate = employerController.findByClientId(employerSelected.getClient().getClientId());

//        Set<Address> workCenterSetToUpdate = new HashSet<>(employerWorkCenterTableView.getAddresses().getItems());
//
//        WorkCenterService workCenterService = WorkCenterService.WorkCenterServiceFactory.getInstance();
//        Set<WorkCenterDBO> workCenterDBOSet = new HashSet<>();
//        for(Address address : workCenterSetToUpdate){
//            WorkCenterDBO workCenterDBO = workCenterService.findWorkCenterByAddressId(address.getId(), employerToUpdate.getClientDBO().getId());
//            workCenterDBOSet.add(workCenterDBO);
//        }
//        employerToUpdate.getClientDBO().setWorkCenters(workCenterDBOSet);

        Set<QuoteAccountCode> quoteAccountCodeSetToUpdate = new HashSet<>(employerQuoteAccountCodeTableView.getQuoteAccountCodeTableView().getItems());
        employerToUpdate.setQuoteAccountCodes(quoteAccountCodeSetToUpdate);

        Integer employerId = employerController.updateEmployer(employerToUpdate);

        if(employerId != null){
            Employer employer = employerController.findById(employerId);
            Message.informationMessage((Stage) employerModificationHeader.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, EmployerManagementConstants.EMPLOYER_SAVED_OK);
            logger.info("Employer management: the employer has been successfully updated.");
        }
    }

    private void onExitButton(MouseEvent event){
        logger.info("Employer management: exiting program.");

        Stage stage = (Stage) employerModificationHeader.getScene().getWindow();
        stage.close();
    }

    public void setEmployerToModify(Employer employer){
        this.employerToModify = employer;
    }
}
