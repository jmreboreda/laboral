package laboral.component.person_management.controllers;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;
import laboral.component.person_management.person_creation.ewl_person.EntityWithoutLegalPersonalityCreator;
import laboral.component.person_management.person_creation.legal_person.LegalPersonCreator;
import laboral.component.person_management.person_creation.natural_person.NaturalPersonCreator;
import laboral.domain.person.controller.PersonController;

public class PersonCreationController extends AnchorPane {

    private static final String PERSON_CREATOR_CONTROLLER_FXML = "/fxml/person_management/person_creation/person_creator_controller.fxml";

    private final PersonController personController = new PersonController();
    private Parent parent;

    @FXML
    private NaturalPersonCreator naturalPersonCreator;
    @FXML
    private LegalPersonCreator legalPersonCreator;
    @FXML
    private EntityWithoutLegalPersonalityCreator entityWithoutLegalPersonalityCreator;

    public PersonCreationController() {

        this.parent = ViewLoader.load(this, PERSON_CREATOR_CONTROLLER_FXML);
    }

    @FXML
    public void initialize(){

    }

    public NaturalPersonCreator getNaturalPersonCreator() {
        return naturalPersonCreator;
    }

    public void setNaturalPersonCreator(NaturalPersonCreator naturalPersonCreator) {
        this.naturalPersonCreator = naturalPersonCreator;
    }

    public LegalPersonCreator getLegalPersonCreator() {
        return legalPersonCreator;
    }

    public void setLegalPersonCreator(LegalPersonCreator legalPersonCreator) {
        this.legalPersonCreator = legalPersonCreator;
    }

    public EntityWithoutLegalPersonalityCreator getEntityWithoutLegalPersonalityCreator() {
        return entityWithoutLegalPersonalityCreator;
    }

    public void setEntityWithoutLegalPersonalityCreator(EntityWithoutLegalPersonalityCreator entityWithoutLegalPersonalityCreator) {
        this.entityWithoutLegalPersonalityCreator = entityWithoutLegalPersonalityCreator;
    }

    public void initializeViews(){
        naturalPersonCreator.getNaturalPersonDataEntryView().initializeView();
        naturalPersonCreator.getNaturalPersonDataEntryView().toBack();

        legalPersonCreator.getLegalPersonDataEntryView().initializeView();
        legalPersonCreator.getLegalPersonDataEntryView().toBack();

        entityWithoutLegalPersonalityCreator.getEntityWithoutLegalPersonalityDataEntryView().initializeView();
    }

    public void naturalPersonCreator(){
        naturalPersonCreator.toFront();
        naturalPersonCreator.setDisable(false);
        naturalPersonCreator.setMouseTransparent(false);
    }

    public void legalPersonCreator(){
        legalPersonCreator.toFront();
        legalPersonCreator.setDisable(false);
        legalPersonCreator.setMouseTransparent(false);
    }

    public void entityWithoutLegalPersonalityCreator(){
        entityWithoutLegalPersonalityCreator.toFront();
        entityWithoutLegalPersonalityCreator.setDisable(false);
        entityWithoutLegalPersonalityCreator.setMouseTransparent(false);
    }
}
