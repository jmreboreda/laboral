package laboral.component.person_management.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import laboral.App;
import laboral.component.ViewLoader;
import laboral.component.client_management.client_creation.controllers.ClientCreationMainController;
import laboral.component.person_management.PersonManagementConstants;
import laboral.component.person_management.common_components.PersonManagementAction;
import laboral.component.person_management.common_components.PersonManagementHeader;
import laboral.component.person_management.common_components.PersonManagementSelector;
import laboral.component.person_management.common_components.PersonTypeSelector;
import laboral.component.person_management.events.NamePatternChangedEvent;
import laboral.component.person_management.events.PersonDataChangedEvent;
import laboral.component.person_management.events.PersonToModifyEvent;
import laboral.domain.address.Address;
import laboral.domain.address.StreetType;
import laboral.domain.address.controller.AddressController;
import laboral.domain.client.Client;
import laboral.domain.client.ClientCreationRequest;
import laboral.domain.client.controller.ClientController;
import laboral.domain.employee.Employee;
import laboral.domain.employee.EmployeeCreationRequest;
import laboral.domain.employee.controller.EmployeeController;
import laboral.domain.person.*;
import laboral.domain.person.controller.PersonController;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.person_type.PersonType;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.validator.PersonDataValidator;

import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;

public class PersonManagementMainController extends AnchorPane {

    private static final Logger logger = Logger.getLogger(PersonManagementMainController.class.getSimpleName());
    private static final String PERSON_MANAGEMENT_MAIN_FXML = "/fxml/person_management/person_management_main.fxml";

    private Parent parent;
    private Boolean loadingPersonData = Boolean.TRUE;

    private PersonController personController = new PersonController();
    private AddressController addressController = new AddressController();
    private ClientController clientController = new ClientController();
    private EmployeeController employeeController = new EmployeeController();

    private final static String REGISTRATION_ELIMINATION_AND_MODIFICATION_OF_PERSON = "Alta, baja y modificación de personas";
    private final static String PERSON_MODIFICATION_TEXT = "Modificación de persona";

    @FXML
    private PersonManagementHeader personManagementHeader;
    @FXML
    private PersonManagementSelector personManagementSelector;
    @FXML
    private PersonTypeSelector personTypeSelector;
    @FXML
    private PersonCreationController personCreationController;
    @FXML
    private PersonModificationController personModificationController;
    @FXML
    private PersonManagementAction personManagementAction;

    @FXML
    public void initialize() {

        this.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>
                () {
            @Override
            public void handle(KeyEvent t) {
                if(t.getCode()== KeyCode.ESCAPE)
                {
                    onExitButton(null);
                }
            }
        });

        personManagementSelector.getNewPerson().disableProperty().bind(personManagementSelector.getModificationPerson().selectedProperty().or(
                personManagementSelector.getDeletePerson().selectedProperty()));

        personManagementSelector.getModificationPerson().disableProperty().bind(personManagementSelector.getNewPerson().selectedProperty().or(
                personManagementSelector.getDeletePerson().selectedProperty()));

        personManagementSelector.getDeletePerson().disableProperty().bind(personManagementSelector.getNewPerson().selectedProperty().or(
                personManagementSelector.getModificationPerson().selectedProperty()));

        personTypeSelector.getNaturalPerson().disableProperty().bind(personTypeSelector.getLegalPerson().selectedProperty().or(
                personTypeSelector.getEntityWithoutLegalPersonality().selectedProperty()));

        personTypeSelector.getLegalPerson().disableProperty().bind(personTypeSelector.getNaturalPerson().selectedProperty().or(
                personTypeSelector.getEntityWithoutLegalPersonality().selectedProperty()));

        personTypeSelector.getEntityWithoutLegalPersonality().disableProperty().bind(personTypeSelector.getNaturalPerson().selectedProperty().or(
                personTypeSelector.getLegalPerson().selectedProperty()));

        personTypeSelector.visibleProperty().bind(this.personManagementSelector.getNewPerson().selectedProperty());

        personCreationController.getNaturalPersonCreator().visibleProperty().bind(this.personTypeSelector.getNaturalPerson().selectedProperty());
        personCreationController.getNaturalPersonCreator().getNaturalPersonDataEntryView().onChanged(this::onPersonDataChanged);

        personCreationController.getLegalPersonCreator().visibleProperty().bind(this.personTypeSelector.getLegalPerson().selectedProperty());
        personCreationController.getLegalPersonCreator().getLegalPersonDataEntryView().onChanged(this::onPersonDataChanged);

        personCreationController.getEntityWithoutLegalPersonalityCreator().visibleProperty().bind(this.personTypeSelector.getEntityWithoutLegalPersonality().selectedProperty());

        personCreationController.getEntityWithoutLegalPersonalityCreator().getEntityWithoutLegalPersonalityDataEntryView().onChanged(this::onPersonDataChanged);
        personModificationController.getPersonModificationNameFinder().setOnNamePatternChanged(this::personPatternNameChanged);
        personModificationController.getPersonModificationNameFinder().setOnSelectPersonToModification(this::onSelectPersonToModification);

        List<PersonModificationView> views = personModificationController.getPersonModificationViews();
        views.stream()
                .map(PersonModificationView::getPersonDataEntryView)
                .forEach(ev -> ev.onChanged(this::onPersonDataChanged));
        views
                .forEach(v -> v.getAddress().setOnAnyCellChange(this::onAnyCellAddressesChange));
        views
                .forEach(v -> v.getAddress().setOnNewAddressButton(this::onNewAddressButton));
        views
                .forEach(v -> v.getAddress().setOnDeleteAddressButton(this::onDeleteAddressButton));

        personManagementSelector.getNewPerson().setOnAction(this::onNewPerson);
        personManagementSelector.getModificationPerson().setOnAction(this::onModificationPerson);
        personManagementSelector.getDeletePerson().setOnAction(this::onDeletePerson);

        personTypeSelector.getNaturalPerson().setOnAction(this::onNaturalPersonCreation);
        personTypeSelector.getLegalPerson().setOnAction(this::onLegalPersonCreation);
        personTypeSelector.getEntityWithoutLegalPersonality().setOnAction(this::onEntityWithoutLegalPersonalityCreation);

        personManagementAction.setOnNewClientButton(this::onNewClientButton);
        personManagementAction.setOnNewEmployeeButton(this::onNewEmployeeButton);
        personManagementAction.setOnOkButton(this::onOkButton);
        personManagementAction.setOnSaveButton(this::onSaveButton);
        personManagementAction.setOnExitButton(this::onExitButton);

        personManagementAction.getOkButton().setVisible(false);
        personManagementAction.getSaveButton().setVisible(false);
        personManagementAction.getNewClientButton().setVisible(false);
        personManagementAction.getNewEmployeeButton().setVisible(false);

    }

    public PersonManagementMainController() {
        logger.info("Initializing person management main fxml");
        this.parent = ViewLoader.load(this, PERSON_MANAGEMENT_MAIN_FXML);

        interfaceInitialState();
    }

    private void interfaceInitialState(){
        personManagementSelectorInitialState();
        personTypeSelectorInitialState();
        initializeViews();

        personManagementAction.getSaveButton().setDisable(true);
        personManagementAction.getOkButton().setDisable(true);
    }

    private void personManagementSelectorInitialState(){
        personManagementSelector.interfaceInitialState();
    }

    private void personTypeSelectorInitialState(){
        personTypeSelector.toFront();

        personManagementHeader.getHeaderText().setText(REGISTRATION_ELIMINATION_AND_MODIFICATION_OF_PERSON);
        personTypeSelector.getNaturalPerson().setSelected(false);
        personTypeSelector.getLegalPerson().setSelected(false);
        personTypeSelector.getEntityWithoutLegalPersonality().setSelected(false);
    }

    private void initializeViews(){
        personCreationController.initializeViews();
        personModificationController.initializeViews();
    }

    private void onNewPerson(ActionEvent event){
        personTypeSelector.toFront();
    }

    private void onModificationPerson(ActionEvent event){
        personTypeSelector.toBack();
        personModificationController.toFront();
        personModificationController.getPersonModificationNameFinder().setVisible(true);
        personModificationController.getPersonModificationNameFinder().initializeView();
        personManagementHeader.getHeaderText().setText(PERSON_MODIFICATION_TEXT);
        loadingPersonData = Boolean.TRUE;
        personModificationController.getPersonModificationNameFinder().getPersonName().requestFocus();
    }

    private void onPersonDataChanged(PersonDataChangedEvent event){
        if(loadingPersonData){
            return;
        }
        personManagementAction.toFront();
        personManagementAction.getOkButton().setVisible(true);
        personManagementAction.getOkButton().setDisable(false);
        personManagementAction.getSaveButton().setDisable(true);
    }

    private void onAnyCellAddressesChange(TableColumn.CellEditEvent event){

        if(!event.getNewValue().equals(event.getOldValue())) {
            personManagementAction.toFront();
            personManagementAction.getOkButton().setVisible(true);
            personManagementAction.getOkButton().setDisable(false);
            personManagementAction.getSaveButton().setDisable(true);
        }
    }

    private void onDeletePerson(ActionEvent event){
        personTypeSelector.toBack();
        Message.informationMessage((Stage) personManagementHeader.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.OPTION_NOT_IMPLEMENTED_YET);
        personManagementSelector.getDeletePerson().setSelected(false);
        System.out.println("Borrando ...");
    }

    private void onNaturalPersonCreation(ActionEvent event){
        personCreationController.toFront();
        personCreationController.naturalPersonCreator();
        personCreationController.getNaturalPersonCreator().toFront();

        personManagementAction.getOkButton().setVisible(true);
        personManagementAction.getOkButton().setDisable(false);
    }

    private void onLegalPersonCreation(ActionEvent event){
        personCreationController.toFront();
        personCreationController.legalPersonCreator();
        personCreationController.getLegalPersonCreator().toFront();

        personManagementAction.getOkButton().setVisible(true);
        personManagementAction.getOkButton().setDisable(false);
    }

    private void onEntityWithoutLegalPersonalityCreation(ActionEvent event){
        personCreationController.toFront();
        personCreationController.entityWithoutLegalPersonalityCreator();
        personCreationController.getEntityWithoutLegalPersonalityCreator().toFront();

        personManagementAction.getOkButton().setVisible(true);
        personManagementAction.getOkButton().setDisable(false);
    }

    private void personPatternNameChanged(NamePatternChangedEvent namePatternChangedEvent){
        loadingPersonData = Boolean.TRUE;
        String pattern = namePatternChangedEvent.getPattern();
        if (pattern.isEmpty()) {
            personModificationController.getPersonModificationNameFinder().getPersonName().clear();
            personModificationController.getPersonModificationNameFinder().getPersonsNames().getItems().clear();

            return;
        }

        List<Person> personList = personController.findPersonByPattern(pattern);
        personModificationController.getPersonModificationNameFinder().refreshPersonNames(personList);
    }

    private void onSelectPersonToModification(PersonToModifyEvent personToModifyEvent){

        personManagementAction.getNewClientButton().setDisable(false);
        personManagementAction.getNewEmployeeButton().setDisable(false);

        Person personToModify = personToModifyEvent.getPersonToModify();

        ObservableList<Address> addressesOrderedObservableList = personToModify.getAddresses()
                .stream()
                .sorted(Comparator.comparing(Address::getId))
                .collect(collectingAndThen(Collectors.toList(), FXCollections::observableArrayList));

        personModificationController.getPersonModificationNameFinder().setVisible(false);
        personModificationController.getPersonModificationNameFinder().toBack();
        personManagementAction.getOkButton().setVisible(false);

        PersonModificationView personModificationView = personModificationController.getPersonModificationView(personToModify.getPersonType());
        AnchorPane anchorPane = personModificationView.getAnchorPane();
        anchorPane.setVisible(true);
        anchorPane.toFront();
        PersonDataEntryView personDataEntryView = personModificationView.getPersonDataEntryView();
        personDataEntryView.refreshDataToModifyPerson(personToModify);
        personModificationView.getAddress().refreshAddressTable(addressesOrderedObservableList);

        personManagementHeader.getHeaderText().setText(personModificationView.getHeaderTitleText());

        Client client = clientController.findClientByPersonId(personToModify.getPersonId());
        if(client.getClientId() == null){
            personManagementAction.getNewClientButton().setVisible(true);
        }else{
            personManagementAction.getNewClientButton().setVisible(false);
        }

        if(personToModify.getPersonType().equals(PersonType.NATURAL_PERSON)) {

            Employee employee = employeeController.findEmployeeByPersonId(personToModify.getPersonId());
            if (employee == null) {
                personManagementAction.getNewEmployeeButton().setVisible(true);
            } else {
                personManagementAction.getNewEmployeeButton().setVisible(false);
            }
        }

        loadingPersonData = Boolean.FALSE;
    }


    private void onNewClientButton(MouseEvent event){
        personManagementAction.getNewClientButton().setDisable(true);

        PersonCreationRequest personCreationRequest = null;

        if(personModificationController.getNaturalPersonModificationView().isVisible()){
            personCreationRequest = personModificationController.getNaturalPersonModificationView().getNaturalPersonModificationRequest();
        }

        if(personModificationController.getLegalPersonModificationView().isVisible()){
            personCreationRequest = personModificationController.getLegalPersonModificationView().getLegalPersonModificationRequest();
        }

        if(personModificationController.getEntityWithoutLegalPersonalityModificationView().isVisible()){
            personCreationRequest = personModificationController.getEntityWithoutLegalPersonalityModificationView().getLEntityWithoutLegalPersonalityModificationRequest();
        }

        ClientCreationRequest clientCreationRequest = ClientCreationRequest.ClientCreationRequestBuilder.create()
                .withPersonId(personCreationRequest.getPersonId())
                .withPersonType(personCreationRequest.getPersonType())
                .withNieNif(personCreationRequest.getNieNif())
                .withAddresses(personCreationRequest.getAddresses())
                .withSg21Code(null)
                .build();

        ClientCreationMainController clientCreationMainController = new ClientCreationMainController();
        clientCreationMainController.setClientCreate(clientCreationRequest);
        Scene scene = new Scene(clientCreationMainController);
        scene.getStylesheets().add(App.class.getResource("/css_stylesheet/application.css").toExternalForm());
        Stage clientCreationStage = new Stage();
        clientCreationStage.setResizable(false);
        clientCreationStage.setTitle("Alta de clientes");
        clientCreationStage.setScene(scene);
        clientCreationStage.initOwner(this.getScene().getWindow());
        clientCreationStage.initModality(Modality.APPLICATION_MODAL);
        clientCreationStage.setOnCloseRequest(Event::consume);
        clientCreationMainController.loadPersonToClient();
        clientCreationStage.show();
    }

    private void onNewEmployeeButton(MouseEvent event){

        if (!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.QUESTION_SAVE_NEW_EMPLOYEE)) {
            return;
            }

        personManagementAction.getNewEmployeeButton().setDisable(true);

        PersonService personService = PersonService.PersonServiceFactory.getInstance();

        PersonCreationRequest personToSetEmployee = personModificationController.getNaturalPersonModificationView().getNaturalPersonModificationRequest();

        PersonDBO personDBO = personService.findPersonById(personToSetEmployee.getPersonId());
        EmployeeCreationRequest employeeCreationRequest = new EmployeeCreationRequest();
        employeeCreationRequest.setId(null);
        employeeCreationRequest.setPersonDBO(personDBO);

        Integer employeeId = employeeController.createEmployee(employeeCreationRequest);
        if(employeeId != null){
            Message.informationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.PERSON_TO_EMPLOYEE_OK);
        }else{
            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.PERSON_TO_EMPLOYEE_NOT_OK);
        }
    }

    private void onNewAddressButton(MouseEvent event) {
        Address newAddress = new Address(null, StreetType.UNKN, "----", "----", "----", "----", null, "----", false);

        if(personModificationController.getNaturalPersonModificationView().isVisible()){
            personModificationController.getNaturalPersonModificationView().getAddresses().getAddresses().getItems().add(newAddress);
        }

        if(personModificationController.getLegalPersonModificationView().isVisible()) {
            personModificationController.getLegalPersonModificationView().getAddresses().getAddresses().getItems().add(newAddress);
        }

        if(personModificationController.getEntityWithoutLegalPersonalityModificationView().isVisible()) {
            personModificationController.getEntityWithoutLegalPersonalityModificationView().getAddresses().getAddresses().getItems().add(newAddress);
        }
    }

    private void onDeleteAddressButton(MouseEvent event){

//            Message.informationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.OPTION_NOT_IMPLEMENTED_YET);
//
//            return;

        List<PersonModificationView> views = personModificationController.getPersonModificationViews();

        if(personModificationController.getNaturalPersonModificationView().isVisible() && personModificationController.getNaturalPersonModificationView().getAddresses().getAddresses().getItems().size() == 1 ||
                personModificationController.getLegalPersonModificationView().isVisible() && personModificationController.getLegalPersonModificationView().getAddresses().getAddresses().getItems().size() == 1 ||
                personModificationController.getEntityWithoutLegalPersonalityModificationView().isVisible() && personModificationController.getEntityWithoutLegalPersonalityModificationView().getAddresses().getAddresses().getItems().size() == 1){
            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.CANNOT_DELETE_THE_ONLY_EXISTING_ADDRESS);

            views.stream()
                    .map(PersonModificationView::getAddress)
                    .forEach(ev -> ev.getAddresses().getSelectionModel().clearSelection());
            return;
        }

        if(personModificationController.getNaturalPersonModificationView().isVisible() && personModificationController.getNaturalPersonModificationView().getAddresses().getAddresses().getSelectionModel().getSelectedItem().getDefaultAddress() == Boolean.TRUE ||
                personModificationController.getLegalPersonModificationView().isVisible() && personModificationController.getLegalPersonModificationView().getAddresses().getAddresses().getSelectionModel().getSelectedItem().getDefaultAddress() == Boolean.TRUE  ||
                personModificationController.getEntityWithoutLegalPersonalityModificationView().isVisible() && personModificationController.getEntityWithoutLegalPersonalityModificationView().getAddresses().getAddresses().getSelectionModel().getSelectedItem().getDefaultAddress() == Boolean.TRUE ) {
            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.CANNOT_DELETE_THE_DEFAULT_ADDRESS);

            views.stream()
                    .map(PersonModificationView::getAddress)
                    .forEach(ev -> ev.getAddresses().getSelectionModel().clearSelection());
            return;
        }

        if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.QUESTION_DELETE_ADDRESS_IS_CORRECT)){

            views.stream()
                    .map(PersonModificationView::getAddress)
                    .forEach(ev -> ev.getAddresses().getSelectionModel().clearSelection());
            return;
        }

        Address addressesSelectedItem = null;
        if(personModificationController.getNaturalPersonModificationView().isVisible()) {
            addressesSelectedItem = personModificationController.getNaturalPersonModificationView().getAddresses().getAddresses().getSelectionModel().getSelectedItem();
            personModificationController.getNaturalPersonModificationView().getAddresses().getAddresses().getItems().remove(addressesSelectedItem);
        }else if(personModificationController.getLegalPersonModificationView().isVisible()){
            addressesSelectedItem = personModificationController.getLegalPersonModificationView().getAddresses().getAddresses().getSelectionModel().getSelectedItem();
            personModificationController.getLegalPersonModificationView().getAddresses().getAddresses().getItems().remove(addressesSelectedItem);

        }else if(personModificationController.getEntityWithoutLegalPersonalityModificationView().isVisible()){
            addressesSelectedItem = personModificationController.getEntityWithoutLegalPersonalityModificationView().getAddresses().getAddresses().getSelectionModel().getSelectedItem();
            personModificationController.getEntityWithoutLegalPersonalityModificationView().getAddresses().getAddresses().getItems().remove(addressesSelectedItem);
        }

        views.stream()
                .map(PersonModificationView::getAddress)
                .forEach(ev -> ev.getAddresses().getSelectionModel().clearSelection());

        personManagementAction.getOkButton().setVisible(true);
        personManagementAction.getOkButton().setDisable(false);
    }

    private void onOkButton(MouseEvent event){
        PersonCreationRequest personCreationRequest = null;

        // New Person
        if(personManagementSelector.getNewPerson().isSelected()){
            PersonDataValidator personDataValidator = new PersonDataValidator((Stage)this.getScene().getWindow());
            // Natural Person
            if(personTypeSelector.getNaturalPerson().isSelected()){
                personCreationRequest = personCreationController.getNaturalPersonCreator().getNaturalPersonCreationRequest();
                if(!personDataValidator.naturalPersonDataValidator(personCreationRequest)){
                    return;
                }
            }

            // Legal Person
            if(personTypeSelector.getLegalPerson().isSelected()){
                personCreationRequest = personCreationController.getLegalPersonCreator().getLegalPersonCreationRequest();
                if(!personDataValidator.legalPersonDataValidator(personCreationRequest)){
                    return;
                }
            }

            // EWLP
            if(personTypeSelector.getEntityWithoutLegalPersonality().isSelected()){
                personCreationRequest = personCreationController.getEntityWithoutLegalPersonalityCreator().getEntityWithoutLegalPersonalityCreationRequest();
                if(!personDataValidator.entityWithoutLegalPersonalityDataValidator(personCreationRequest)){
                    return;
                }
            }

            personManagementAction.getSaveButton().setVisible(true);
            personManagementAction.getSaveButton().setDisable(false);
        }

        // Modification person
        if(personManagementSelector.getModificationPerson().isSelected()){
            // Natural person
            if(personModificationController.getNaturalPersonModificationView().isVisible()){
                // Validate data
                PersonDataValidator personDataValidator = new PersonDataValidator((Stage) this.getScene().getWindow());

                personCreationRequest = personModificationController.getNaturalPersonModificationView().getNaturalPersonModificationRequest();
                Boolean validateDataWithSpecificFormat = personDataValidator.naturalPersonDataValidator(personCreationRequest);
                if(!validateDataWithSpecificFormat){
                    personManagementAction.getOkButton().setDisable(true);
                    personManagementAction.getSaveButton().setDisable(true);
                    return;
                }
            }

            // Legal Person
            if(personModificationController.getLegalPersonModificationView().isVisible()){
                // Validate data
                PersonDataValidator personDataValidator = new PersonDataValidator((Stage) this.getScene().getWindow());

                personCreationRequest = personModificationController.getLegalPersonModificationView().getLegalPersonModificationRequest();
                Boolean validateDataWithSpecificFormat = personDataValidator.legalPersonDataValidator(personCreationRequest);
                if(!validateDataWithSpecificFormat){
                    personManagementAction.getOkButton().setDisable(true);
                    personManagementAction.getSaveButton().setDisable(true);
                    return;
                }
            }

            // EWLP
            if(personModificationController.getEntityWithoutLegalPersonalityModificationView().isVisible()){
                // Validate data
                PersonDataValidator personDataValidator = new PersonDataValidator((Stage) this.getScene().getWindow());

                personCreationRequest = personModificationController.getEntityWithoutLegalPersonalityModificationView().getLEntityWithoutLegalPersonalityModificationRequest();
                Boolean validateDataWithSpecificFormat = personDataValidator.legalPersonDataValidator(personCreationRequest);
                if(!validateDataWithSpecificFormat){
                    personManagementAction.getOkButton().setDisable(true);
                    personManagementAction.getSaveButton().setDisable(true);
                    return;
                }
            }

            personManagementAction.getSaveButton().setVisible(true);
            personManagementAction.getSaveButton().setDisable(false);

            return;
        }

        // Delete person
        if(personManagementSelector.getDeletePerson().isSelected()){

            return;
        }
    }

    private void onSaveButton(MouseEvent event){

        // Save new Person
        if(personManagementSelector.getNewPerson().isSelected()) {
            onSaveNewPerson();
            return;
        }

        // Save modification Person
        if(personManagementSelector.getModificationPerson().isSelected()){
            onSavePersonModification();
        }
    }

    private void onExitButton(MouseEvent event){
        if(personTypeSelector.isVisible() &&
                (personTypeSelector.getNaturalPerson().isSelected() ||
                        personTypeSelector.getLegalPerson().isSelected() ||
                        personTypeSelector.getEntityWithoutLegalPersonality().isSelected())){
            personTypeSelectorInitialState();
            return;
        }

        if(personModificationController.getNaturalPersonModificationView().isVisible() ||
        personModificationController.getLegalPersonModificationView().isVisible() ||
        personModificationController.getEntityWithoutLegalPersonalityModificationView().isVisible()){
            personModificationController.getNaturalPersonModificationView().setVisible(false);
            personModificationController.getLegalPersonModificationView().setVisible(false);
            personModificationController.getEntityWithoutLegalPersonalityModificationView().setVisible(false);
            personModificationController.getPersonModificationNameFinder().setVisible(true);
            personManagementAction.getOkButton().setVisible(false);
            personManagementAction.getSaveButton().setVisible(false);
            personManagementAction.getNewClientButton().setVisible(false);
            personManagementHeader.getHeaderText().setText(PERSON_MODIFICATION_TEXT);
            // Update finder data
            String textInFinder = personModificationController.getPersonModificationNameFinder().getPersonName().getText();
            personModificationController.getPersonModificationNameFinder().initializeView();
            personModificationController.getPersonModificationNameFinder().getPersonName().setText(textInFinder);
            personPatternNameChanged(new NamePatternChangedEvent(textInFinder));

            return;
        }

        if(personManagementSelector.getNewPerson().isSelected() ||
                personManagementSelector.getModificationPerson().isSelected() ||
                personManagementSelector.getDeletePerson().isSelected()){
            interfaceInitialState();

            return;
        }

        logger.info("Person management: exiting program.");

        Stage stage = (Stage) personManagementHeader.getScene().getWindow();
        stage.close();
    }

    private void onSaveNewPerson(){
        PersonDataValidator personDataValidator = new PersonDataValidator((Stage)this.getScene().getWindow());

        PersonCreationRequest personCreationRequest = null;

        // Natural person
        if(personTypeSelector.getNaturalPerson().isSelected()){
            personCreationRequest = personCreationController.getNaturalPersonCreator().getNaturalPersonCreationRequest();
            if(!personDataValidator.naturalPersonDataValidator(personCreationRequest)){
                return;
            }
        }

        // Legal person
        if(personTypeSelector.getLegalPerson().isSelected()){
            PersonCreationRequest legalNaturalPersonCreationRequest = personCreationController.getLegalPersonCreator().getLegalPersonCreationRequest();
            if(!personDataValidator.legalPersonDataValidator(legalNaturalPersonCreationRequest)){
                return;
            }
        }

        // EWLP
        if(personTypeSelector.getEntityWithoutLegalPersonality().isSelected()){
            PersonCreationRequest entityWithoutLegalPersonalityCreationRequest = personCreationController.getEntityWithoutLegalPersonalityCreator().getEntityWithoutLegalPersonalityCreationRequest();
            if(!personDataValidator.entityWithoutLegalPersonalityDataValidator(entityWithoutLegalPersonalityCreationRequest)){
                return;
            }
        }

        System.out.println("Pasados controles para persistencia ...");

        // Persist New Person
        Integer personId = persistPerson(personCreationRequest);
        if(personId != null){
            Message.informationMessage((Stage) personManagementHeader.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.PERSON_SAVED_OK);
            String persistedPersonType = personCreationRequest.getPersonType().toString();
            logger.info("Person management: new \"" + persistedPersonType.toLowerCase() + "\" saved ok.");

            personManagementAction.getOkButton().setDisable(true);
            personManagementAction.getSaveButton().setDisable(true);

        }else{
            Message.errorMessage((Stage) personManagementHeader.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.PERSON_NOT_SAVED_OK);
        }
    }

    private void onSavePersonModification(){

        PersonCreationRequest personCreationRequest = null;

        // Natural Person
        if(personModificationController.getNaturalPersonModificationView().isVisible()){
            personCreationRequest = personModificationController.getNaturalPersonModificationView().getNaturalPersonModificationRequest();
        }

        // Legal Person
        if(personModificationController.getLegalPersonModificationView().isVisible()){
            personCreationRequest = personModificationController.getLegalPersonModificationView().getLegalPersonModificationRequest();
        }

        // Entity Without Legal Personality
        if(personModificationController.getEntityWithoutLegalPersonalityModificationView().isVisible()){
            personCreationRequest = personModificationController.getEntityWithoutLegalPersonalityModificationView().getLEntityWithoutLegalPersonalityModificationRequest();
        }

        if(!personModificationController.validateCorrectDataEnteredFromPerson(personCreationRequest)){
            System.out.println("Mal asunto amigo ...");
            return;
        }

        Person personToUpdate = personController.findPersonById(personCreationRequest.getPersonId());
        if(personToUpdate.getPersonType().equals(PersonType.NATURAL_PERSON)){
            personToUpdate = retrieveNaturalPersonToUpdate(personCreationRequest);
        }
        else if(personToUpdate.getPersonType().equals(PersonType.LEGAL_PERSON)){
            personToUpdate = retrieveLegalPersonToUpdate(personCreationRequest);
        }
        else if(personToUpdate.getPersonType().equals(PersonType.ENTITY_WITHOUT_LEGAL_PERSONALITY)){
            personToUpdate = retrieveLegalPersonToUpdate(personCreationRequest);
        }

        // Update Person
        Integer updatedPersonId = updatePerson(personToUpdate);
        if(updatedPersonId != null) {
            Message.informationMessage((Stage) personManagementHeader.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.PERSON_MODIFICATION_SAVED_OK);

        }else {
            Message.errorMessage((Stage) personManagementHeader.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.PERSON_MODIFICATION_NOT_SAVED_OK);
            return;
        }

        personManagementAction.getOkButton().setDisable(true);
        personManagementAction.getSaveButton().setDisable(true);
        personModificationController.getNaturalPersonModificationView().getAddresses().getAddresses().getSelectionModel().clearSelection();
        personModificationController.getLegalPersonModificationView().getAddresses().getAddresses().getSelectionModel().clearSelection();
        personModificationController.getEntityWithoutLegalPersonalityModificationView().getAddresses().getAddresses().getSelectionModel().clearSelection();

    }

    private Person retrieveNaturalPersonToUpdate(PersonCreationRequest personCreationRequest){
        NaturalPerson naturalPersonToUpdate = (NaturalPerson) personController.findPersonById(personCreationRequest.getPersonId());
        naturalPersonToUpdate.setFirstSurname(personCreationRequest.getFirstSurname());
        naturalPersonToUpdate.setSecondSurname(personCreationRequest.getSecondSurname());
        naturalPersonToUpdate.setName(personCreationRequest.getName());
        naturalPersonToUpdate.setNieNif(personCreationRequest.getNieNif());
        naturalPersonToUpdate.setSocialSecurityAffiliationNumber(personCreationRequest.getSocialSecurityAffiliationNumber());
        naturalPersonToUpdate.setStudy(personCreationRequest.getStudy());
        naturalPersonToUpdate.setBirthDate(personCreationRequest.getBirthDate());
        naturalPersonToUpdate.setCivilStatus(personCreationRequest.getCivilStatus());
        naturalPersonToUpdate.setNationality(personCreationRequest.getNationality());
        naturalPersonToUpdate.setAddresses(personCreationRequest.getAddresses());

        return naturalPersonToUpdate;
    }

    private Person retrieveLegalPersonToUpdate(PersonCreationRequest personCreationRequest){
        LegalPerson legalPersonToUpdate = (LegalPerson) personController.findPersonById(personCreationRequest.getPersonId());
        legalPersonToUpdate.setLegalName(personCreationRequest.getLegalName());
        legalPersonToUpdate.setNieNif(personCreationRequest.getNieNif());
        legalPersonToUpdate.setAddresses(personCreationRequest.getAddresses());

        return legalPersonToUpdate;
    }

    private Integer persistPerson(PersonCreationRequest personCreationRequest){
        return personController.personCreator(personCreationRequest);
    }

    private Integer updatePerson(Person persistedPersonToUpdate){
        return personController.personUpdater(persistedPersonToUpdate);
    }
}
