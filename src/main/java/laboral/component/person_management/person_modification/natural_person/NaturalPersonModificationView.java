package laboral.component.person_management.person_modification.natural_person;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import javafx.util.converter.LocalDateStringConverter;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.client_management.client_modification.generic_components.AddressesGenericTableView;
import laboral.component.person_management.controllers.PersonDataEntryView;
import laboral.component.person_management.controllers.PersonModificationView;
import laboral.domain.address.Address;
import laboral.domain.address.Province;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person_type.PersonType;
import laboral.domain.utilities.Parameters;

import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NaturalPersonModificationView extends AnchorPane implements PersonModificationView {

    private static final String NATURAL_PERSON_MODIFICATION_VIEW_FXML = "/fxml/person_management/person_modification/natural_person/natural_person_modification_view.fxml";
    private static final String NATURAL_PERSON_MODIFICATION_DATA_ENTRY_VIEW_HEADER_PANE_TEXT = "Datos de la persona física a modificar";
    private static final String ADDRESSES_NATURAL_PERSON_MODIFICATION_TABLE_VIEW_HEADER_PANE_TEXT = "Direcciones";

    private Parent parent;

    @FXML
    private NaturalPersonModificationDataEntryView naturalPersonModificationDataEntryView;
    @FXML
    private AddressesNaturalPersonModificationTableView addresses;


    public NaturalPersonModificationView() {
        this.parent = ViewLoader.load(this, NATURAL_PERSON_MODIFICATION_VIEW_FXML);
    }

    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());

    @FXML
    public void initialize(){
        naturalPersonModificationDataEntryView.getHeaderPanel().setText(NATURAL_PERSON_MODIFICATION_DATA_ENTRY_VIEW_HEADER_PANE_TEXT);
        naturalPersonModificationDataEntryView.getHeaderPanel().setStyle(Parameters.FOREGROUND_TEXT_RED_COLOR);

        addresses.getHeaderPane().setText(ADDRESSES_NATURAL_PERSON_MODIFICATION_TABLE_VIEW_HEADER_PANE_TEXT);
        addresses.getHeaderPane().setStyle(Parameters.FOREGROUND_TEXT_RED_COLOR);

        naturalPersonModificationDataEntryView.getBirthDate().setConverter(new LocalDateStringConverter(dateFormatter, null));
    }

    public NaturalPersonModificationDataEntryView getNaturalPersonModificationDataEntryView() {
        return naturalPersonModificationDataEntryView;
    }

    public void setNaturalPersonModificationDataEntryView(NaturalPersonModificationDataEntryView naturalPersonModificationDataEntryView) {
        this.naturalPersonModificationDataEntryView = naturalPersonModificationDataEntryView;
    }

    public AddressesNaturalPersonModificationTableView getAddresses() {
        return addresses;
    }

    public void setAddresses(AddressesNaturalPersonModificationTableView addresses) {
        this.addresses = addresses;
    }

    public PersonCreationRequest getNaturalPersonModificationRequest(){

        Set<Address> addressSet = new HashSet<>();
        List<Address> addressList = addresses.getAddresses().getItems();

        for(Address address : addressList){
            String provinceInPostalCode = address.getPostalCode().substring(0,2);
            Province province = null;
            for(Province provinceRead : Province.values()){
                if(provinceRead.getCode().equals(provinceInPostalCode)){
                    province = provinceRead;
                    break;
                }
            }

            Address newAddress = new Address();
            newAddress.setId(address.getId());
            newAddress.setDefaultAddress(address.getDefaultAddress());
            newAddress.setStreetType(address.getStreetType());
            newAddress.setStreetName(address.getStreetName());
            newAddress.setStreetExtended(address.getStreetExtended());
            newAddress.setLocation(address.getLocation());
            newAddress.setMunicipality(address.getMunicipality());
            newAddress.setPostalCode(address.getPostalCode());
            newAddress.setProvince(province);

            addressSet.add(newAddress);
        }

        return PersonCreationRequest.PersonCreationRequestBuilder.create()
                .withId(naturalPersonModificationDataEntryView.getNaturalPersonId())
                .withPersonType(PersonType.NATURAL_PERSON)
                .withFirstSurname(naturalPersonModificationDataEntryView.getFirstSurname().getText())
                .withSecondSurname(naturalPersonModificationDataEntryView.getSecondSurname().getText())
                .withName(naturalPersonModificationDataEntryView.getName().getText())
                .withBirthDate(naturalPersonModificationDataEntryView.getBirthDate().getValue())
                .withNieNif(new NieNif(naturalPersonModificationDataEntryView.getNieNif().getText()))
                .withSocialSecurityAffiliationNumber(naturalPersonModificationDataEntryView.getSocialSecurityAffiliationNumber().getText())
                .withCivilStatus(naturalPersonModificationDataEntryView.getCivilStatus().getValue())
                .withStudy(naturalPersonModificationDataEntryView.getStudyLevel().getValue())
                .withAddresses(addressSet)
                .withNationality(naturalPersonModificationDataEntryView.getNationality().getText())
                .build();
    }

    @Override
    public PersonDataEntryView getPersonDataEntryView() {
        return getNaturalPersonModificationDataEntryView();
    }

    @Override
    public AnchorPane getAnchorPane() {
        return this;
    }

    @Override
    public AddressesGenericTableView getAddress() {
        return addresses;
    }

    @Override
    public String getHeaderTitleText() {
        return "Modificación de persona física";
    }
}
