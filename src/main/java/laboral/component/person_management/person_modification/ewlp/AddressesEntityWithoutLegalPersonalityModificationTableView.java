package laboral.component.person_management.person_modification.ewlp;

import javafx.event.EventHandler;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import laboral.component.client_management.client_modification.generic_components.AddressesGenericTableView;
import laboral.domain.address.Address;
import laboral.domain.address.StreetType;

import java.util.List;

public class AddressesEntityWithoutLegalPersonalityModificationTableView extends AddressesGenericTableView {

    public AddressesEntityWithoutLegalPersonalityModificationTableView() {
    }

    @Override
    public TitledPane getHeaderPane() {
        return super.getHeaderPane();
    }

    @Override
    public void setHeaderPane(TitledPane headerPane) {
        super.setHeaderPane(headerPane);
    }

    @Override
    public TableView<Address> getAddresses() {
        return super.getAddresses();
    }

    @Override
    public void setAddresses(TableView<Address> addresses1) {
        super.setAddresses(addresses1);
    }

    @Override
    public TableColumn<Address, Boolean> getPrincipalTableColumn() {
        return super.getPrincipalTableColumn();
    }

    @Override
    public void setPrincipalTableColumn(TableColumn<Address, Boolean> principalTableColumn) {
        super.setPrincipalTableColumn(principalTableColumn);
    }

    @Override
    public TableColumn<Address, StreetType> getStreetTypeTableColumn() {
        return super.getStreetTypeTableColumn();
    }

    @Override
    public void setStreetTypeTableColumn(TableColumn<Address, StreetType> streetTypeTableColumn) {
        super.setStreetTypeTableColumn(streetTypeTableColumn);
    }

    @Override
    public TableColumn<Address, String> getStreetNameTableColumn() {
        return super.getStreetNameTableColumn();
    }

    @Override
    public void setStreetNameTableColumn(TableColumn<Address, String> streetNameTableColumn) {
        super.setStreetNameTableColumn(streetNameTableColumn);
    }

    @Override
    public TableColumn<Address, String> getStreetExtendedTableColumn() {
        return super.getStreetExtendedTableColumn();
    }

    @Override
    public void setStreetExtendedTableColumn(TableColumn<Address, String> streetExtendedTableColumn) {
        super.setStreetExtendedTableColumn(streetExtendedTableColumn);
    }

    @Override
    public TableColumn<Address, String> getLocationTableColumn() {
        return super.getLocationTableColumn();
    }

    @Override
    public void setLocationTableColumn(TableColumn<Address, String> locationTableColumn) {
        super.setLocationTableColumn(locationTableColumn);
    }

    @Override
    public TableColumn<Address, String> getMunicipalityTableColumn() {
        return super.getMunicipalityTableColumn();
    }

    @Override
    public void setMunicipalityTableColumn(TableColumn<Address, String> municipalityTableColumn) {
        super.setMunicipalityTableColumn(municipalityTableColumn);
    }

    @Override
    public TableColumn<Address, String> getPostalCodeTableColumn() {
        return super.getPostalCodeTableColumn();
    }

    @Override
    public void setPostalCodeTableColumn(TableColumn<Address, String> postalCodeTableColumn) {
        super.setPostalCodeTableColumn(postalCodeTableColumn);
    }

    @Override
    public void initialize() {
        super.initialize();
    }

    @Override
    public void initializeView() {
        super.initializeView();
    }

    @Override
    public void refreshAddressTable(List<Address> addressesItemList) {
        super.refreshAddressTable(addressesItemList);
    }

    @Override
    public Boolean validateNoMissingAddresses() {
        return super.validateNoMissingAddresses();
    }

    @Override
    public Boolean validateDefaultAddressIsSet() {
        return super.validateDefaultAddressIsSet();
    }

    @Override
    public Boolean validateAllAddressesAreComplete() {
        return super.validateAllAddressesAreComplete();
    }

    @Override
    public void setOnAnyCellChange(EventHandler<TableColumn.CellEditEvent> event) {
        super.setOnAnyCellChange(event);
    }
}
