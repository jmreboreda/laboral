package laboral.component.person_management.person_creation.ewl_person;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import laboral.component.client_management.client_modification.generic_components.EntityWithoutLegalPersonalityGenericDataEntryView;
import laboral.component.person_management.events.PersonDataChangedEvent;
import laboral.domain.person.EntityWithoutLegalPersonality;
import laboral.domain.person.Person;
import laboral.domain.person.PersonCreationRequest;

public class EntityWithoutLegalPersonalityDataEntryView extends EntityWithoutLegalPersonalityGenericDataEntryView {

    public EntityWithoutLegalPersonalityDataEntryView() {
    }

    @Override
    public void initialize() {
        super.initialize();
    }

    @Override
    public Group getLegalPersonGroup() {
        return super.getLegalPersonGroup();
    }

    @Override
    public TitledPane getHeaderPane() {
        return super.getHeaderPane();
    }

    @Override
    public TextField getLegalName() {
        return super.getLegalName();
    }

    @Override
    public TextField getNieNif() {
        return super.getNieNif();
    }

    @Override
    public CheckBox getNormalizeText() {
        return super.getNormalizeText();
    }

    @Override
    public void initializeView() {
        super.initializeView();
    }

    @Override
    public Boolean validateNoMissingData() {
        return super.validateNoMissingData();
    }

    @Override
    public void refreshDataToModifyPerson(Person person) {
        EntityWithoutLegalPersonality entityWithoutLegalPersonality = (EntityWithoutLegalPersonality) person;
        super.refreshDataToModifyPerson(entityWithoutLegalPersonality);
    }

    @Override
    public PersonCreationRequest getPersonCreationRequest() {
        return super.getPersonCreationRequest();
    }

    @Override
    public void setOnEntityWithoutLegalPersonalityDataChanged(EventHandler<PersonDataChangedEvent> personDataEventHandler) {
        super.setOnEntityWithoutLegalPersonalityDataChanged(personDataEventHandler);
    }
}
