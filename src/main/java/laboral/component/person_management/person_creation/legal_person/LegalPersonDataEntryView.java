package laboral.component.person_management.person_creation.legal_person;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import laboral.component.client_management.client_modification.generic_components.LegalPersonGenericDataEntryView;
import laboral.component.person_management.events.PersonDataChangedEvent;
import laboral.domain.person.Person;
import laboral.domain.person.PersonCreationRequest;

public class LegalPersonDataEntryView extends LegalPersonGenericDataEntryView {

    public LegalPersonDataEntryView() {
    }

    @Override
    public void initialize() {
        super.initialize();
    }

    @Override
    public Group getLegalPersonGroup() {
        return super.getLegalPersonGroup();
    }

    @Override
    public TitledPane getHeaderPane() {
        return super.getHeaderPane();
    }

    @Override
    public TextField getLegalName() {
        return super.getLegalName();
    }

    @Override
    public TextField getNieNif() {
        return super.getNieNif();
    }

    @Override
    public CheckBox getNormalizeText() {
        return super.getNormalizeText();
    }

    @Override
    public void initializeView() {
        super.initializeView();
    }

    @Override
    public Boolean validateNoMissingData() {
        return super.validateNoMissingData();
    }

    @Override
    public void refreshDataToModifyPerson(Person person) {
        super.refreshDataToModifyPerson(person);
    }

    @Override
    public PersonCreationRequest getPersonCreationRequest() {
        return super.getPersonCreationRequest();
    }

    @Override
    public void setOnLegalPersonDataChanged(EventHandler<PersonDataChangedEvent> personDataEventHandler) {
        super.setOnLegalPersonDataChanged(personDataEventHandler);
    }
}
