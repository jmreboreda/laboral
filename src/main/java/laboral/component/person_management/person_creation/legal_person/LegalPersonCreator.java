package laboral.component.person_management.person_creation.legal_person;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;
import laboral.component.client_management.client_modification.generic_components.AddressGenericEntryDataView;
import laboral.component.client_management.client_modification.generic_components.LegalPersonGenericDataEntryView;
import laboral.domain.address.Address;
import laboral.domain.address.Province;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person_type.PersonType;

import java.util.HashSet;
import java.util.Set;

public class LegalPersonCreator extends AnchorPane {

    private static final String NEW_LEGAL_PERSON_FXML = "/fxml/person_management/person_creation/legal_person/new_legal_person_creator.fxml";

    private Parent parent;

    @FXML
    private LegalPersonDataEntryView legalPersonDataEntryView;
    @FXML
    private AddressLegalPersonDataEntryView addressLegalPersonDataEntryView;

    public LegalPersonCreator() {
        this.parent = ViewLoader.load(this, NEW_LEGAL_PERSON_FXML);
    }

    @FXML
    public void initialize(){
    }

    public LegalPersonGenericDataEntryView getLegalPersonDataEntryView() {
        return legalPersonDataEntryView;
    }

    public void setLegalPersonDataEntryView(LegalPersonDataEntryView legalPersonDataEntryView) {
        this.legalPersonDataEntryView = legalPersonDataEntryView;
    }

    public AddressGenericEntryDataView getAddressLegalPersonDataEntryView() {
        return addressLegalPersonDataEntryView;
    }

    public void setAddressLegalPersonDataEntryView(AddressLegalPersonDataEntryView addressLegalPersonDataEntryView) {
        this.addressLegalPersonDataEntryView = addressLegalPersonDataEntryView;
    }

    public PersonCreationRequest getLegalPersonCreationRequest(){

        Set<Address> addresses = new HashSet<>();

        String postalCode =  addressLegalPersonDataEntryView.getPostalCode().getText();
        Province province = null;
        if(!postalCode.isEmpty()) {
            String provinceInPostalCode = addressLegalPersonDataEntryView.getPostalCode().getText().substring(0, 2);
            for (Province provinceRead : Province.values()) {
                if (provinceRead.getCode().equals(provinceInPostalCode)) {
                    province = provinceRead;
                    break;
                }
            }
        }

        Address address = new Address();
        address.setStreetType(addressLegalPersonDataEntryView.getStreetType().getSelectionModel().getSelectedItem());
        address.setStreetName(addressLegalPersonDataEntryView.getStreetName().getText());
        address.setStreetExtended(addressLegalPersonDataEntryView.getStreetExtended().getText());
        address.setLocation(addressLegalPersonDataEntryView.getLocality().getText());
        address.setMunicipality(addressLegalPersonDataEntryView.getMunicipality().getText());
        address.setPostalCode(addressLegalPersonDataEntryView.getPostalCode().getText());
        address.setProvince(province);
        address.setDefaultAddress(Boolean.TRUE);

        addresses.add(address);

        return PersonCreationRequest.PersonCreationRequestBuilder.create()
                .withPersonType(PersonType.LEGAL_PERSON)
                .withLegalName(legalPersonDataEntryView.getLegalName().getText())
                .withNieNif(new NieNif(legalPersonDataEntryView.getNieNif().getText()))
                .withAddresses(addresses)
                .build();
    }
}
