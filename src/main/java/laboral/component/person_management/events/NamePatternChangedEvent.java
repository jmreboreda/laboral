package laboral.component.person_management.events;

import javafx.event.Event;
import javafx.event.EventType;

public class NamePatternChangedEvent extends Event {

    public static final EventType<NamePatternChangedEvent> PERSON_NAME_PATTERN_EVENT_EVENT_TYPE = new EventType<>("PERSON_NAME_PATTERN_EVENT_EVENT_TYPE");
    private final String pattern;

    public NamePatternChangedEvent(String pattern) {
        super(PERSON_NAME_PATTERN_EVENT_EVENT_TYPE);
        this.pattern = pattern;

    }

    public String getPattern() {
        return pattern;
    }
}
