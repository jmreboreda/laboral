package laboral.component.person_management.events;

import javafx.event.Event;
import javafx.event.EventType;

public class PersonDataChangedEvent extends Event {

    public static final EventType<PersonDataChangedEvent> PERSON_DATA_CHANGED_EVENT_EVENT_TYPE = new EventType<>("PERSON_DATA_CHANGED_EVENT_EVENT_TYPE");
    private final String newValue;

    public PersonDataChangedEvent(String newValue) {
        super(PERSON_DATA_CHANGED_EVENT_EVENT_TYPE);
        this.newValue = newValue;

    }

    public String getPersonNamePattern() {
        return newValue;
    }
}
