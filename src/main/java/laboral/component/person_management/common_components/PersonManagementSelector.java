package laboral.component.person_management.common_components;

import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

public class PersonManagementSelector extends AnchorPane {

    private static final String NEW_PERSON_MANAGEMENT_SELECTOR_FXML = "/fxml/person_management/person_management_selector.fxml";

    private Parent parent;

    private EventHandler<ActionEvent> selectorActionEventHandler;

    @FXML
    private ToggleGroup personManagementGroup;
    @FXML
    private RadioButton newPerson;
    @FXML
    private RadioButton modificationPerson;
    @FXML
    private RadioButton deletePerson;

    public PersonManagementSelector() {
        this.parent = ViewLoader.load(this, NEW_PERSON_MANAGEMENT_SELECTOR_FXML);
    }

    @FXML
    public void initialize() {

    }

    public RadioButton getNewPerson() {
        return newPerson;
    }

    public RadioButton getModificationPerson() {
        return modificationPerson;
    }

    public RadioButton getDeletePerson() {
        return deletePerson;
    }

    public ToggleGroup getPersonManagementGroup() {
        return personManagementGroup;
    }

    public void interfaceInitialState(){

        newPerson.setSelected(false);
        modificationPerson.setSelected(false);
        deletePerson.setSelected(false);
    }

    private void onSelectorAction(ActionEvent event){
        selectorActionEventHandler.handle(event);
    }

    public void setOnSelectorAction(EventHandler<ActionEvent> selectorActionEventHandler){
        this.selectorActionEventHandler = selectorActionEventHandler;
    }
}
