package laboral.component.initial_menu;


import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import laboral.App;
import laboral.component.ViewLoader;
import laboral.component.client_invoice_check_list.controllers.ClientInvoiceCheckListMainController;
import laboral.component.client_management.client_modification.controllers.ClientModificationMainController;
import laboral.component.person_management.controllers.PersonManagementMainController;
import laboral.component.time_record.controller.TimeRecordController;
import laboral.component.work_contract.creation.controllers.InitialWorkContractCreationMainController;
import laboral.component.work_contract.variation.controller.WorkContractVariationMainController;


import java.util.logging.Logger;

public class InitialMenuController extends AnchorPane {


    private static final Logger logger = Logger.getLogger(InitialMenuController.class.getSimpleName());
    private static final String INITIAL_MENU_FXML = "/fxml/initial_menu/initial_menu.fxml";

    private Stage primaryStage;
//    private ClientManager clientManager = new ClientManager();
//    private PersonManager personManager = new PersonManager();

    private Parent parent;

    @FXML
    private Button newContractButton;
    @FXML
    private Button timeRecordButton;
    @FXML
    private Button consultationContractButton;
    @FXML
    private Button contractVariationButton;
    @FXML
    private Button clientInvoiceCheckListButton;
    @FXML
    private Button payrollCheckListButton;
    @FXML
    private Button applicationAgendaButton;
    @FXML
    private Button personManagementButton;
    @FXML
    private Button clientManagementButton;
    @FXML
    private Button contractDocumentationControlButton;
    @FXML
    private Button exitButton;



    public InitialMenuController() {
        logger.info("Initilizing initial_menu fxml");
        this.parent = ViewLoader.load(this, INITIAL_MENU_FXML);

        //contractVariationButton.setVisible(false);
//        JsonTest jsonTest = new JsonTest();
//        jsonTest.testJson();
    }

    @FXML
    public void initialize() {

        newContractButton.setOnMouseClicked(this::onNewContract);
//        consultationContractButton.setOnMouseClicked(this::onConsultationContract);
        timeRecordButton.setOnMouseClicked(this::onTimeRecord);
        contractVariationButton.setOnMouseClicked(this::onContractVariation);
        clientInvoiceCheckListButton.setOnMouseClicked(this::onClientInvoiceCheckList);
//        payrollCheckListButton.setOnMouseClicked(this::onPayrollCheckList);
//        applicationAgendaButton.setOnMouseClicked(this::onApplicationAgenda);

        personManagementButton.setOnMouseClicked(this::onPersonManagement);
        clientManagementButton.setOnMouseClicked(this::onClientManagement);

//        contractDocumentationControlButton.setOnMouseClicked(this::onContractDocumentationControl);
        exitButton.setOnMouseClicked(this::onExit);
    }

    private void onNewContract(MouseEvent event){
        InitialWorkContractCreationMainController newContractController = new InitialWorkContractCreationMainController();
        Scene scene = new Scene(newContractController);
        scene.getStylesheets().add(App.class.getResource("/css_stylesheet/application.css").toExternalForm());
        Stage newContractStage = new Stage();
        newContractStage.setResizable(false);
        newContractStage.setTitle("Nuevo contrato de trabajo");
        newContractStage.setScene(scene);
        newContractStage.initOwner(primaryStage);
        newContractStage.initModality(Modality.APPLICATION_MODAL);
        newContractStage.show();
    }

    private void onClientInvoiceCheckList(MouseEvent event){
        ClientInvoiceCheckListMainController clientInvoiceCheckListMainController = new ClientInvoiceCheckListMainController();
        Scene scene = new Scene(clientInvoiceCheckListMainController);
        scene.getStylesheets().add(App.class.getResource("/css_stylesheet/application.css").toExternalForm());
        Stage contractVariationStage = new Stage();
        contractVariationStage.setResizable(false);
        contractVariationStage.setTitle("Relación de empresas y empresarios para control de envío de facturas de la actividad");
        contractVariationStage.setScene(scene);
        contractVariationStage.initOwner(primaryStage);
        contractVariationStage.initModality(Modality.APPLICATION_MODAL);
        contractVariationStage.show();
    }
//
//    private void onPayrollCheckList(MouseEvent event){
//        PayrollCheckListMainController payrollCheckListMainController = new PayrollCheckListMainController();
//        Scene scene = new Scene(payrollCheckListMainController);    private void onNewContract(MouseEvent event){
//            NewContractMainController newContractController = new NewContractMainController();
//            Scene scene = new Scene(newContractController);
//            scene.getStylesheets().add(App.class.getResource("/css_stylesheet/application.css").toExternalForm());
//            Stage newContractStage = new Stage();
//            newContractStage.setResizable(false);
//            newContractStage.setTitle("Nuevo contrato de trabajo");
//            newContractStage.setScene(scene);
//            newContractStage.initOwner(primaryStage);
//            newContractStage.initModality(Modality.APPLICATION_MODAL);
//            newContractStage.show();
//        }
//
        private void onTimeRecord(MouseEvent event){
            TimeRecordController timeRecordController = new TimeRecordController();
            Scene scene = new Scene(timeRecordController);
            scene.getStylesheets().add(App.class.getResource("/css_stylesheet/application.css").toExternalForm());
            Stage timeRecordStage = new Stage();
            timeRecordStage.setResizable(false);
            timeRecordStage.setTitle("Registro horario");
            timeRecordStage.setScene(scene);
            timeRecordStage.initOwner(primaryStage);
            timeRecordStage.initModality(Modality.APPLICATION_MODAL);
            timeRecordStage.show();
        }
//
//        private void onConsultationContract(MouseEvent event){
//            ConsultationContractMainController consultationContractMainController = new ConsultationContractMainController();
//            Scene scene = new Scene(consultationContractMainController);
//            scene.getStylesheets().add(App.class.getResource("/css_stylesheet/application.css").toExternalForm());
//            Stage consultationContractStage = new Stage();
//            consultationContractStage.setResizable(false);
//            consultationContractStage.setTitle("Consulta de contratos");
//            consultationContractStage.setScene(scene);
//            consultationContractStage.initOwner(primaryStage);
//            consultationContractStage.initModality(Modality.APPLICATION_MODAL);
//            consultationContractStage.show();
//        }
//
        private void onContractVariation(MouseEvent event){
            WorkContractVariationMainController workContractVariationMainController = new WorkContractVariationMainController();
            Scene scene = new Scene(workContractVariationMainController);
            scene.getStylesheets().add(App.class.getResource("/css_stylesheet/application.css").toExternalForm());
            Stage contractVariationStage = new Stage();
            contractVariationStage.setResizable(false);
            contractVariationStage.setTitle("Variaciones de contratos de trabajo");
            contractVariationStage.setScene(scene);
            contractVariationStage.initOwner(primaryStage);
            contractVariationStage.initModality(Modality.APPLICATION_MODAL);
            contractVariationStage.show();
        }
//        scene.getStylesheets().add(App.class.getResource("/css_stylesheet/application.css").toExternalForm());
//        Stage contractVariationStage = new Stage();
//        contractVariationStage.setResizable(false);
//        contractVariationStage.setTitle("Relación de personas para control de nóminas");
//        contractVariationStage.setScene(scene);
//        contractVariationStage.initOwner(primaryStage);
//        contractVariationStage.initModality(Modality.APPLICATION_MODAL);
//        contractVariationStage.show();
//    }
//
//    private void onApplicationAgenda(MouseEvent event){
//        Stage thisStage = (Stage) this.getScene().getWindow();
//
//        InitialChecks.alertByContractNewVersionWithPendingIDC(thisStage);
//        InitialChecks.alertOfWeeklyOfWorkingDayScheduleWithEndDate(thisStage);
//        InitialChecks.alertByContractNewVersionExpiration(thisStage);
//        InitialChecks.alertByDelaySendingLaborDocumentationToClients(thisStage);
//
//        Message.informationMessage((Stage) thisStage.getOwner(), CheckConstants.INITIAL_CHECK_HEADER_TEXT.concat("Agenda de la aplicación"), CheckConstants.FINALIZED_CHECK_APPLICATION_AGENDA);
//    }

    private void onPersonManagement(MouseEvent event){
        PersonManagementMainController personManagementMainController = new PersonManagementMainController();
        Scene scene = new Scene(personManagementMainController);
        scene.getStylesheets().add(App.class.getResource("/css_stylesheet/application.css").toExternalForm());
        Stage personManagementStage = new Stage();
        personManagementStage.setResizable(false);
        personManagementStage.setTitle("Mantenimiento de personas");
        personManagementStage.setScene(scene);
        personManagementStage.initOwner(primaryStage);
        personManagementStage.initModality(Modality.APPLICATION_MODAL);
        personManagementStage.setOnCloseRequest(Event::consume);
        personManagementStage.show();
    }

    private void onClientManagement(MouseEvent event){
        ClientModificationMainController clientModificationMainController = new ClientModificationMainController();
        Scene scene = new Scene(clientModificationMainController);
        scene.getStylesheets().add(App.class.getResource("/css_stylesheet/application.css").toExternalForm());
        Stage clientManagementStage = new Stage();
        clientManagementStage.setResizable(false);
        clientManagementStage.setTitle("Mantenimiento de clientes");
        clientManagementStage.setScene(scene);
        clientManagementStage.initOwner(primaryStage);
        clientManagementStage.initModality(Modality.APPLICATION_MODAL);
        clientManagementStage.setOnCloseRequest(Event::consume);
        clientManagementStage.show();
    }

//    private void onContractDocumentationControl(MouseEvent event){
//        ContractDocumentationControlMainController contractDocumentationControlMainController = new ContractDocumentationControlMainController();
//        Scene scene = new Scene(contractDocumentationControlMainController);
//        scene.getStylesheets().add(App.class.getResource("/css_stylesheet/application.css").toExternalForm());
//        Stage personManagementStage = new Stage();
//        personManagementStage.setResizable(false);
//        personManagementStage.setTitle("Control de entrega y recepción de documentación");
//        personManagementStage.setScene(scene);
//        personManagementStage.initOwner(primaryStage);
//        personManagementStage.initModality(Modality.APPLICATION_MODAL);
//        personManagementStage.show();
//    }

    private void onExit(MouseEvent event) {

        logger.info("Application GMoldes_laboral: exiting program.");

        Platform.exit();
        System.exit(0);
    }
}
