package laboral.component.client_management.client_creation.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Callback;
import laboral.component.ViewLoader;
import laboral.component.client_management.ClientManagementConstants;
import laboral.component.client_management.client_creation.ClientCreationHeader;
import laboral.component.client_management.client_creation.ClientCreationVerifier;
import laboral.component.client_management.client_modification.generic_components.ClientModificationAction;
import laboral.component.client_management.common_components.ClientActivityPeriodsTableView;
import laboral.component.client_management.common_components.ClientServiceTableView;
import laboral.component.client_management.common_components.ClientWorkCenterTableView;
import laboral.domain.activity_period.ActivityPeriod;
import laboral.domain.address.Address;
import laboral.domain.address.Province;
import laboral.domain.address.StreetType;
import laboral.domain.address.controller.AddressController;
import laboral.domain.client.Client;
import laboral.domain.client.ClientCreationRequest;
import laboral.domain.client.controller.ClientController;
import laboral.domain.person.Person;
import laboral.domain.person.controller.PersonController;
import laboral.domain.service_gm.ServiceGM;
import laboral.domain.service_gm.ServiceGMEnum;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.work_center.WorkCenter;
import laboral.domain.work_center.controller.WorkCenterController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public class ClientCreationMainController extends AnchorPane {

    private static final Logger logger = Logger.getLogger(ClientCreationMainController.class.getSimpleName());
    private static final String CLIENT_CREATION_MAIN_FXML = "/fxml/client_management/client_creation/client_creation_main_controller.fxml";
    ClientCreationRequest clientCreationRequest;

    private Boolean loadingData = Boolean.FALSE;

    private final ClientController clientController = new ClientController();
    private final PersonController personController = new PersonController();
    private final AddressController addressController = new AddressController();
    private final WorkCenterController workCenterController = new WorkCenterController();

    @FXML
    private ClientCreationHeader clientCreationHeader;
    @FXML
    private ComboBox<Person> personToClientCreate;
    @FXML
    private ClientActivityPeriodsTableView clientActivityPeriodsTableView;
    @FXML
    private ClientServiceTableView clientServiceTableView;
    @FXML
    private ClientWorkCenterTableView clientWorkCenterTableView;
    @FXML
    private ClientModificationAction clientModificationAction;


    public ClientCreationMainController() {
        logger.info("Initializing client creation main fxml");
        Parent parent = ViewLoader.load(this, CLIENT_CREATION_MAIN_FXML);

        clientWorkCenterTableView.setOnAnyCellChange(this::onAnyCellAddressesChange);

        clientModificationAction.getOkButton().setOnMouseClicked(this::onOkButton);
        clientModificationAction.getSaveButton().setOnMouseClicked(this::onSaveButton);
        clientModificationAction.getExitButton().setOnMouseClicked(this::onExitButton);
    }

    @FXML
    public void initialize(){

        clientActivityPeriodsTableView.getDeletePeriod().disableProperty().bind(
                clientActivityPeriodsTableView.getActivityPeriodTableView().getSelectionModel().selectedItemProperty().isNull());

        clientServiceTableView.getServiceDelete().disableProperty().bind(
                clientServiceTableView.getServices().getSelectionModel().selectedItemProperty().isNull());

        clientWorkCenterTableView.getDeleteAddressButton().disableProperty().bind(
                clientWorkCenterTableView.getAddresses().getSelectionModel().selectedItemProperty().isNull());

        personToClientCreate.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 20));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.TOP_CENTER);
                    setText(item.toString());
                }
            }

        });

        personToClientCreate.setCellFactory(
                new Callback<ListView<Person>, ListCell<Person>>() {
                    @Override public ListCell<Person> call(ListView<Person> param) {
                        final ListCell<Person> cell = new ListCell<Person>() {
                            @Override public void updateItem(Person item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font ("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }

                });

        clientActivityPeriodsTableView.getActivityPeriodTableView().setEditable(false);
        clientServiceTableView.getServices().setEditable(false);
        clientWorkCenterTableView.getAddresses().setEditable(false);

        clientActivityPeriodsTableView.setOnActivityPeriodCellTableChange(this::onActivityPeriodCellTableChange);
        clientActivityPeriodsTableView.setOnNewActivityPeriodButton(this::onNewActivityPeriod);
        clientActivityPeriodsTableView.setOnDeleteActivityPeriod(this::onDeleteActivityPeriod);

        clientServiceTableView.setOnServiceCellTableChange(this::onServiceCellTableChange);
        clientServiceTableView.setOnNewServiceButton(this::onNewServiceButton);
        clientServiceTableView.setOnDeleteServiceButton(this::onDeleteServiceButton);


        clientWorkCenterTableView.setOnNewAddressButton(this::onNewWorkCenterButton);
        clientWorkCenterTableView.setOnDeleteAddressButton(this::onDeleteWorkCenterButton);
    }

    public ClientCreationHeader getClientCreationHeader() {
        return clientCreationHeader;
    }

    public void setClientCreationHeader(ClientCreationHeader clientCreationHeader) {
        this.clientCreationHeader = clientCreationHeader;
    }

    public ComboBox<Person> getPersonToClientCreate() {
        return personToClientCreate;
    }

    public void setPersonToClientCreate(ComboBox<Person> personToClientCreate) {
        this.personToClientCreate = personToClientCreate;
    }

    public ClientActivityPeriodsTableView getClientActivityPeriodsTableView() {
        return clientActivityPeriodsTableView;
    }

    public void setClientActivityPeriodsTableView(ClientActivityPeriodsTableView clientActivityPeriodsTableView) {
        this.clientActivityPeriodsTableView = clientActivityPeriodsTableView;
    }

    public ClientServiceTableView getClientServiceTableView() {
        return clientServiceTableView;
    }

    public void setClientServiceTableView(ClientServiceTableView clientServiceTableView) {
        this.clientServiceTableView = clientServiceTableView;
    }

    public ClientWorkCenterTableView getClientWorkCenterTableView() {
        return clientWorkCenterTableView;
    }

    public void setClientWorkCenterTableView(ClientWorkCenterTableView clientWorkCenterTableView) {
        this.clientWorkCenterTableView = clientWorkCenterTableView;
    }

    public ClientModificationAction getClientModificationAction() {
        return clientModificationAction;
    }

    public void setClientModificationAction(ClientModificationAction clientModificationAction) {
        this.clientModificationAction = clientModificationAction;
    }

    private void onActivityPeriodCellTableChange(CellEditEvent event){
        if(loadingData){
            return;
        }

        if(event.getOldValue() != null && event.getNewValue() == null ||
        event.getNewValue() != null && event.getOldValue() == null ||
//                event.getNewValue() != null && event.getOldValue() != null &&
                        !event.getNewValue().equals(event.getOldValue())) {
            clientModificationAction.toFront();
            clientModificationAction.getOkButton().setVisible(true);
            clientModificationAction.getOkButton().setDisable(false);
            clientModificationAction.getSaveButton().setDisable(true);
        }
    }

    private void onServiceCellTableChange(CellEditEvent event){
        if(loadingData){
            return;
        }

        if(event.getOldValue() != null && event.getNewValue() == null ||
                event.getNewValue() != null && event.getOldValue() == null ||
                !event.getNewValue().equals(event.getOldValue())) {
            clientModificationAction.toFront();
            clientModificationAction.getOkButton().setVisible(true);
            clientModificationAction.getOkButton().setDisable(false);
            clientModificationAction.getSaveButton().setDisable(true);
        }
    }

    private void onAnyCellAddressesChange(CellEditEvent event){
        if(loadingData){
            return;
        }
        List<ActivityPeriod> activityPeriodList = this.getClientActivityPeriodsTableView().getActivityPeriodTableView().getItems();

        ClientCreationVerifier clientCreationVerifier = new ClientCreationVerifier((Stage)this.getScene().getWindow());
        if(!clientCreationVerifier.validateActivityPeriodDataEntry(activityPeriodList))
        {
            return;
        };

        if(!event.getNewValue().equals(event.getOldValue())) {
            clientModificationAction.toFront();
            clientModificationAction.getOkButton().setVisible(true);
            clientModificationAction.getOkButton().setDisable(false);
            clientModificationAction.getSaveButton().setDisable(true);
        }
    }

    public void loadPersonToClient(){

        Person person = personController.findPersonById(this.clientCreationRequest.getPersonId());
        List<Person> personList = new ArrayList<>();
        personList.add(person);
        ObservableList<Person> persistedClientOrderedObservableList = FXCollections.observableList(personList);
        personToClientCreate.getItems().setAll(persistedClientOrderedObservableList);
        personToClientCreate.getSelectionModel().select(0);

        clientActivityPeriodsTableView.getActivityPeriodTableView().setEditable(true);
        clientServiceTableView.getServices().setEditable(true);
        clientWorkCenterTableView.getAddresses().setEditable(true);
    }

    private void onNewActivityPeriod(MouseEvent event){
        ActivityPeriod newActivityPeriod = new ActivityPeriod(null, null, LocalDate.now(), null, null);
        clientActivityPeriodsTableView.getActivityPeriodTableView().getItems().add(newActivityPeriod);
        clientActivityPeriodsTableView.getActivityPeriodTableView().refresh();

        clientModificationAction.getOkButton().setVisible(true);
        clientModificationAction.getOkButton().setDisable(false);
        clientModificationAction.getSaveButton().setDisable(true);
    }

    public void onDeleteActivityPeriod(MouseEvent event){
        if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.QUESTION_DELETE_ACTIVITY_PERIOD_IS_CORRECT)){
            return;
        }

        ActivityPeriod activityPeriodSelectedItem = clientActivityPeriodsTableView.getActivityPeriodTableView().getSelectionModel().getSelectedItem();
        clientActivityPeriodsTableView.getActivityPeriodTableView().getItems().remove(activityPeriodSelectedItem);
        clientActivityPeriodsTableView.getActivityPeriodTableView().getSelectionModel().clearSelection();

        clientModificationAction.getSaveButton().setDisable(true);
        clientModificationAction.getOkButton().setVisible(true);
        clientModificationAction.getOkButton().setDisable(false);
    }

     private void onNewServiceButton(MouseEvent event) {
        ServiceGM serviceGM = new ServiceGM(null, ServiceGMEnum.SERVICE_NOT_ESTABLISHED, LocalDate.now(), LocalDate.now(), false, null);
        clientServiceTableView.getServices().getItems().add(serviceGM);
        clientServiceTableView.getServices().refresh();

        clientModificationAction.getOkButton().setVisible(true);
        clientModificationAction.getOkButton().setDisable(false);
        clientModificationAction.getSaveButton().setDisable(true);
     }

     private void onDeleteServiceButton(MouseEvent event){
         if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.QUESTION_DELETE_SERVICE_IS_CORRECT)){
             return;
         }

         ServiceGM serviceGMSelectedItem = clientServiceTableView.getServices().getSelectionModel().getSelectedItem();
         clientServiceTableView.getServices().getItems().remove(serviceGMSelectedItem);
         clientServiceTableView.getServices().getSelectionModel().clearSelection();

         clientModificationAction.getSaveButton().setDisable(true);
         clientModificationAction.getOkButton().setVisible(true);
         clientModificationAction.getOkButton().setDisable(false);

     }

     private void onNewWorkCenterButton(MouseEvent event){
         Address newWorkCenter = new Address(null, StreetType.UNKN, "----", "----", "----", "----", null, "----", false);
         clientWorkCenterTableView.getAddresses().getItems().add(newWorkCenter);

         clientModificationAction.getOkButton().setVisible(true);
         clientModificationAction.getOkButton().setDisable(false);
         clientModificationAction.getSaveButton().setDisable(true);
     }

    private void onDeleteWorkCenterButton(MouseEvent event){
        //TODO Avoid deletion of work center if it has been included in a contract.
//        if(clientWorkCenterTableView.getAddresses().getItems().size() == 1){
//            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.CANNOT_DELETE_THE_ONLY_EXISTING_ADDRESS);
//            return;
//        }

//        if(clientWorkCenterTableView.getAddresses().getSelectionModel().getSelectedItem().getDefaultAddress() == Boolean.TRUE) {
//            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.CANNOT_DELETE_THE_DEFAULT_ADDRESS);
//            return;
//        }

        if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.QUESTION_DELETE_ADDRESS_IS_CORRECT)){
            return;
        }

        Address workCentersSelectedItem = clientWorkCenterTableView.getAddresses().getSelectionModel().getSelectedItem();
        clientWorkCenterTableView.getAddresses().getItems().remove(workCentersSelectedItem);
        clientWorkCenterTableView.getAddresses().getSelectionModel().clearSelection();

        clientModificationAction.getOkButton().setVisible(true);
        clientModificationAction.getOkButton().setDisable(false);
    }

    private void onOkButton(MouseEvent event){

        List<ActivityPeriod> activityPeriodList = this.getClientActivityPeriodsTableView().getActivityPeriodTableView().getItems();
        List<ServiceGM> servicesTableList = this.getClientServiceTableView().getServices().getItems();
        List<Address> workCenterTableList = this.getClientWorkCenterTableView().getAddresses().getItems();

        ClientCreationVerifier clientCreationVerifier = new ClientCreationVerifier((Stage)this.getScene().getWindow());

        if(!clientCreationVerifier.validateActivityPeriodDataEntry(activityPeriodList) ||
        !clientCreationVerifier.validateServicesDataEntry(servicesTableList) ||
        !clientCreationVerifier.validateWorkCenterDataEntry(workCenterTableList))
        {
            clientModificationAction.getOkButton().setDisable(true);
            clientModificationAction.getSaveButton().setDisable(true);
            return;
        }

        clientModificationAction.getOkButton().setDisable(true);
        clientModificationAction.getSaveButton().setDisable(false);
    }

    private void onSaveButton(MouseEvent event){

        clientModificationAction.getSaveButton().setDisable(true);

        Person personToCreateClient = this.personToClientCreate.getSelectionModel().getSelectedItem();

        Set<ActivityPeriod> activityPeriodSet = new HashSet<>(clientActivityPeriodsTableView.getActivityPeriodTableView().getItems());

        Set<ServiceGM> serviceGMSetToUpdate = new HashSet<>(clientServiceTableView.getServices().getItems());

        Set<WorkCenter> workCenterSet = prepareWorkCenterSetToUpdate(personToCreateClient.getPersonId());

        ClientCreationRequest clientCreationRequest = ClientCreationRequest.ClientCreationRequestBuilder.create()
                .withPersonId(personToCreateClient.getPersonId())
                .withPersonType(personToCreateClient.getPersonType())
                .withNieNif(personToCreateClient.getNieNif())
                .withAddresses(personToCreateClient.getAddresses())
                .withSg21Code("****")
                .withActivityPeriods(activityPeriodSet)
                .withServices(serviceGMSetToUpdate)
                .withWorkCenters(workCenterSet)
                .build();

        Integer clientId = clientController.clientCreator(clientCreationRequest);

        if(clientId != null){
            Client client = clientController.findClientById(clientId);
            Message.informationMessage((Stage) clientCreationHeader.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.CLIENT_SAVED_OK);
            logger.info("Client management: the client \"" + client.toString() + "\" has been successfully updated.");
        }
    }

    private void onExitButton(MouseEvent event){
        logger.info("Client management: exiting program.");

        Stage stage = (Stage) clientCreationHeader.getScene().getWindow();
        stage.close();
    }

    private void interfaceInitialize(){
        personToClientCreate.getItems().clear();
        personToClientCreate.getSelectionModel().clearSelection();
        clientActivityPeriodsTableView.getActivityPeriodTableView().getItems().clear();
        clientServiceTableView.getServices().getItems().clear();
        clientWorkCenterTableView.getAddresses().getItems().clear();
    }

    private Set<WorkCenter> prepareWorkCenterSetToUpdate(Integer personId){

        Set<WorkCenter> workCenterSet = new HashSet<>();

        Set<Address> addressesInWorkCenterTableView = new HashSet<>(clientWorkCenterTableView.getAddresses().getItems());

        for(Address address : addressesInWorkCenterTableView){
            if(address.getId() == null){
                String postalCode = address.getPostalCode();
                Province province = null;
                if(!postalCode.isEmpty()) {
                    String provinceInPostalCode = postalCode.substring(0, 2);
                    for (Province provinceRead : Province.values()) {
                        if (provinceRead.getCode().equals(provinceInPostalCode)) {
                            province = provinceRead;
                            break;
                        }
                    }
                    address.setProvince(province);
                }
            }

            WorkCenter workCenter = new WorkCenter(null, address);
            workCenterSet.add(workCenter);
        }

        return workCenterSet;
    }

    public void setClientCreate(ClientCreationRequest clientCreationRequest){
        this.clientCreationRequest = clientCreationRequest;
    }
}
