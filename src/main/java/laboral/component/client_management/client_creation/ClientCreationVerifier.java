package laboral.component.client_management.client_creation;

import javafx.stage.Stage;
import laboral.component.client_management.ClientManagementConstants;
import laboral.domain.activity_period.ActivityPeriod;
import laboral.domain.address.Address;
import laboral.domain.address.StreetType;
import laboral.domain.service_gm.ServiceGM;
import laboral.domain.service_gm.ServiceGMEnum;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.utilities.validations.PostalCodeValidator;

import java.util.*;

public class ClientCreationVerifier {

    Stage stage;

    public ClientCreationVerifier(Stage stage) {
        this.stage = stage;
    }

    public Boolean validateActivityPeriodDataEntry(List<ActivityPeriod> activityPeriodTableList) {

        if(activityPeriodTableList.size() == 0){
            Message.errorMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.NO_ACTIVITY_PERIOD_HAS_BEEN_ESTABLISHED);

            return Boolean.FALSE;
        }

        for(int counter = 0; counter < activityPeriodTableList.size(); counter++){
            if(activityPeriodTableList.get(counter).getDateFrom() == null &&  activityPeriodTableList.get(counter).getDateTo() == null ||
                    activityPeriodTableList.get(counter).getDateFrom() == null &&  activityPeriodTableList.get(counter).getDateTo() != null ){
                Message.errorMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.THERE_ARE_ANY_PERIOD_OF_ACTIVITY_WITHOUT_DATES);

                return Boolean.FALSE;
            }
        }

        Integer activityPeriodsOpen = 0;
        for (ActivityPeriod activityPeriod : activityPeriodTableList) {
            if (activityPeriod.getDateTo() == null && activityPeriod.getWithoutActivityDate() == null ||
                    activityPeriod.getDateTo() == null && activityPeriod.getWithoutActivityDate() != null) {
                activityPeriodsOpen++;
            }

            if (activityPeriodsOpen > 1) {
                Message.errorMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.TOO_MANY_OPEN_ACTIVITY_PERIODS);

                return Boolean.FALSE;
            }
        }

        for (ActivityPeriod activityPeriod : activityPeriodTableList) {
            if (activityPeriod.getDateTo() != null && activityPeriod.getDateFrom() != null &&
                    activityPeriod.getDateTo().isBefore(activityPeriod.getDateFrom()) ||
                    activityPeriod.getDateTo() != null && activityPeriod.getWithoutActivityDate() != null &&
                            activityPeriod.getDateTo().isBefore(activityPeriod.getWithoutActivityDate()) ||
                    activityPeriod.getDateFrom() != null && activityPeriod.getWithoutActivityDate() != null &&
                            activityPeriod.getWithoutActivityDate().isBefore(activityPeriod.getDateFrom())
            ) {
                Message.errorMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.THERE_ARE_INCONSISTENT_DATES_IN_ANY_PERIOD_OF_ACTIVITY);

                return Boolean.FALSE;
            }
        }

        if(activityPeriodTableList.size() > 1) {
            for (int counter = 1; counter < activityPeriodTableList.size(); counter++) {
                if(activityPeriodTableList.get(counter).getDateFrom().isBefore(activityPeriodTableList.get(counter-1).getDateFrom()) ||
                        activityPeriodTableList.get(counter-1).getDateTo() != null && activityPeriodTableList.get(counter).getDateFrom().isBefore(activityPeriodTableList.get(counter-1).getDateTo())){
                    Message.errorMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.THERE_ARE_INCONSISTENT_DATES_IN_ANY_PERIOD_OF_ACTIVITY);

                    return Boolean.FALSE;
                }
            }
        }

        return Boolean.TRUE;
    }

    public Boolean validateServicesDataEntry( List<ServiceGM> servicesTableList){

        // No service established
        if(servicesTableList.isEmpty()){
            Message.errorMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.NO_SERVICE_HAS_BEEN_ESTABLISHED);

            return Boolean.FALSE;
        }

        // Start date missing
        for(ServiceGM serviceGM : servicesTableList){
            if(serviceGM.getDateFrom() == null){
                Message.errorMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.SERVICES_WITHOUT_INITIAL_DATE);

                return Boolean.FALSE;
            }
        }

        // Too many equal services open
        Map<ServiceGMEnum, Integer> frequencyMap = new HashMap<>();
        for (ServiceGM serviceGM : servicesTableList) {
            if(serviceGM.getDateTo() == null) {
                Integer count = frequencyMap.get(serviceGM.getServiceGM());
                if (count == null) {
                    count = 0;
                }

                frequencyMap.put(serviceGM.getServiceGM(), count + 1);
            }
        }

        for (Map.Entry<ServiceGMEnum, Integer> serviceGMEnum : frequencyMap.entrySet()) {
            if(serviceGMEnum.getValue() > 1) {
                Message.errorMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.TOO_MANY_EQUAL_SERVICES_OPEN);

                return Boolean.FALSE;
            }
        }

        // DateTo > DateFrom
        for (ServiceGM serviceGM : servicesTableList) {
            if (serviceGM.getDateTo() != null && serviceGM.getDateFrom() != null &&
                    serviceGM.getDateTo().isBefore(serviceGM.getDateFrom())) {
                Message.errorMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.THERE_ARE_INCONSISTENT_DATES_IN_ANY_SERVICE);

                return Boolean.FALSE;
            }
        }

        // Verify the correct continuity of dates for the same service
        List<ServiceGM> servicesOrderedList = new ArrayList<>(servicesTableList);
        Collections.sort(servicesOrderedList, Comparator.comparing(ServiceGM::getServiceGM)
                .thenComparing(ServiceGM::getDateFrom)
                .thenComparing(ServiceGM::getDateTo));

        for(int counter = 1; counter < servicesOrderedList.size(); counter++){
            if(servicesOrderedList.get(counter).getServiceGM().equals(servicesOrderedList.get(counter-1).getServiceGM())){
                if(servicesOrderedList.get(counter).getDateFrom() != null && servicesOrderedList.get(counter-1).getDateTo() != null) {
                    if (servicesOrderedList.get(counter).getDateFrom().isBefore(servicesOrderedList.get(counter - 1).getDateTo())) {
                        Message.errorMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.CONTINUITY_ERROR_IN_DATES_OF_SOME_SERVICE);

                        return Boolean.FALSE;
                    }
                }
            }
        }

        // Not established
        for(ServiceGM serviceGM : servicesTableList){
            if(serviceGM.getServiceGM().equals(ServiceGMEnum.SERVICE_NOT_ESTABLISHED)){
                Message.errorMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.THERE_ARE_SOME_SERVICES_NOT_ESTABLISHED);

                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    public Boolean validateWorkCenterDataEntry(List<Address> workCenterTableList){

        if(!validateNoMissingWorkCenterData(workCenterTableList)){
            Message.errorMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.SOME_ADDRESS_OR_ADDRESSES_ARE_INCOMPLETE);

            return Boolean.FALSE;
        }

        Boolean isDefaultWorkCenterSet = Boolean.FALSE;
        for(Address address : workCenterTableList){
            if(address.getDefaultAddress()){
                isDefaultWorkCenterSet = Boolean.TRUE;
            }
        }

        if(!isDefaultWorkCenterSet){
            Message.informationMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.DEFAULT_WORK_CENTER_IS_NOT_SET);

//            return Boolean.FALSE;
        }

        PostalCodeValidator postalCodeValidator = new PostalCodeValidator();
        for(Address address : workCenterTableList){
            if(!postalCodeValidator.validatePostalCode(address.getPostalCode())){
                Message.errorMessage((Stage) stage.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.POSTAL_CODE_IS_NOT_VALID);

                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    private Boolean validateNoMissingWorkCenterData(List<Address> workCenterTableList) {

        String pattern = "\\W*";

        for (Address address : workCenterTableList) {
            if (address.getStreetName().isEmpty() ||
                    address.getStreetName().matches(pattern) ||
                    address.getStreetType().equals(StreetType.UNKN) ||
                    address.getStreetExtended().isEmpty() ||
                    address.getStreetExtended().matches(pattern) ||
                    address.getLocation().isEmpty() ||
                    address.getLocation().matches(pattern) ||
                    address.getMunicipality().isEmpty() ||
                    address.getMunicipality().matches(pattern) ||
                    address.getPostalCode().isEmpty()) {

                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

}
