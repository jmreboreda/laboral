package laboral.component.client_management.client_creation;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;
import laboral.domain.activity_period.ActivityPeriod;
import laboral.domain.utilities.cells.ActivityPeriodDateCell;

import java.time.LocalDate;

public class ClientCreationActivityPeriodsTableView extends AnchorPane {

    private static final String CLIENT_CREATION_ACTIVITY_PERIODS_TABLE_VIEW = "/fxml/client_management/common_components/client_activity_periods_table_view.fxml";

    private static final Integer DATE_FROM_COLUMN = 0;
    private static final Integer DATE_TO_COLUMN = 1;
    private static final Integer DATE_SUSPENSION_COLUMN = 2;

    private EventHandler<MouseEvent> newActivityEventEventHandler;
    private EventHandler<MouseEvent> deleteActivityEventEventHandler;

    private EventHandler<TableColumn.CellEditEvent> cellEditEventEventHandler;

    Parent parent;

    @FXML
    private TitledPane activityPeriodHeaderPane;
    @FXML
    private TableView<ActivityPeriod> activityPeriodTableView;
    @FXML
    private TableColumn<ActivityPeriod, LocalDate> periodDateFrom;
    @FXML
    private TableColumn<ActivityPeriod, LocalDate> periodDateTo;
    @FXML
    private TableColumn<ActivityPeriod, LocalDate> periodSuspensionDate;
    @FXML
    private Button addPeriod;
    @FXML
    private Button deletePeriod;


    public ClientCreationActivityPeriodsTableView() {
        this.parent = ViewLoader.load(this, CLIENT_CREATION_ACTIVITY_PERIODS_TABLE_VIEW);
    }

    @FXML
    public void initialize(){
        activityPeriodHeaderPane.setText("Histórico de períodos de actividad del cliente");

        activityPeriodTableView.setEditable(false);

        periodDateFrom.setCellFactory(param-> new ActivityPeriodDateCell());
        periodDateTo.setCellFactory(param-> new ActivityPeriodDateCell());
        periodSuspensionDate.setCellFactory(param-> new ActivityPeriodDateCell());

        periodDateFrom.setCellValueFactory(new PropertyValueFactory<>("dateFrom"));
        periodDateTo.setCellValueFactory(new PropertyValueFactory<>("dateTo"));
        periodSuspensionDate.setCellValueFactory(new PropertyValueFactory<>("withoutActivityDate"));

        periodDateFrom.getStyleClass().add("center");
        periodDateTo.getStyleClass().add("center");
        periodSuspensionDate.getStyleClass().add("center");

        periodDateFrom.setOnEditCommit(this::onTableCellEdited);
        periodDateTo.setOnEditCommit(this::onTableCellEdited);
        periodSuspensionDate.setOnEditCommit(this::onTableCellEdited);

        addPeriod.setOnMouseClicked(this::onNewActivityPeriod);
        deletePeriod.setOnMouseClicked(this::onDeletePeriod);

        // Unselect selected row
        activityPeriodTableView.setRowFactory(param -> {
            final TableRow<ActivityPeriod> row = new TableRow<>();
            row.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getButton().equals(MouseButton.SECONDARY)) {
                        final int index = row.getIndex();
                        if (index >= 0 && index < activityPeriodTableView.getItems().size() && activityPeriodTableView.getSelectionModel().isSelected(index)) {
                            activityPeriodTableView.getSelectionModel().clearSelection();
                            event.consume();
                        }
                    }
                }
            });
            return row;
        });
    }

    public TableView<ActivityPeriod> getActivityPeriodTableView() {
        return activityPeriodTableView;
    }

    public void setActivityPeriodTableView(TableView<ActivityPeriod> activityPeriodTableView) {
        this.activityPeriodTableView = activityPeriodTableView;
    }

    public TableColumn<ActivityPeriod, LocalDate> getPeriodDateFrom() {
        return periodDateFrom;
    }

    public void setPeriodDateFrom(TableColumn<ActivityPeriod, LocalDate> periodDateFrom) {
        this.periodDateFrom = periodDateFrom;
    }

    public TableColumn<ActivityPeriod, LocalDate> getPeriodDateTo() {
        return periodDateTo;
    }

    public void setPeriodDateTo(TableColumn<ActivityPeriod, LocalDate> periodDateTo) {
        this.periodDateTo = periodDateTo;
    }

    public TableColumn<ActivityPeriod, LocalDate> getPeriodSuspensionDate() {
        return periodSuspensionDate;
    }

    public void setPeriodSuspensionDate(TableColumn<ActivityPeriod, LocalDate> periodSuspensionDate) {
        this.periodSuspensionDate = periodSuspensionDate;
    }

    public Button getAddPeriod() {
        return addPeriod;
    }

    public void setAddPeriod(Button addPeriod) {
        this.addPeriod = addPeriod;
    }

    public Button getDeletePeriod() {
        return deletePeriod;
    }

    public void setDeletePeriod(Button deletePeriod) {
        this.deletePeriod = deletePeriod;
    }

    public void refreshActivityPeriodsTable(ObservableList<ActivityPeriod> sortedActivityPeriodObservableList){
        activityPeriodTableView.setItems(FXCollections.observableArrayList(sortedActivityPeriodObservableList));
        activityPeriodTableView.refresh();
    }

    private void onNewActivityPeriod(MouseEvent event){
        newActivityEventEventHandler.handle(event);
    }

    private void onDeletePeriod(MouseEvent event){
        this.deleteActivityEventEventHandler.handle(event);
//        Message.informationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.OPTION_NOT_IMPLEMENTED_YET);
//        activityPeriodTableView.getSelectionModel().clearSelection();
    }

    private void onTableCellEdited(TableColumn.CellEditEvent cellEditEvent){

        cellEditEventEventHandler.handle(cellEditEvent);

        int editedRow = cellEditEvent.getTablePosition().getRow();
        int editedColumn = cellEditEvent.getTablePosition().getColumn();
        ActivityPeriod activityPeriod = activityPeriodTableView.getItems().get(editedRow);

        if(editedColumn == DATE_FROM_COLUMN){
            activityPeriod.setDateFrom((LocalDate) cellEditEvent.getNewValue());
        }
        if(editedColumn == DATE_TO_COLUMN){
            activityPeriod.setDateTo((LocalDate) cellEditEvent.getNewValue());
        }

        if(editedColumn == DATE_SUSPENSION_COLUMN){
            activityPeriod.setWithoutActivityDate((LocalDate) cellEditEvent.getNewValue());
        }

        refreshActivityPeriodsTable(activityPeriodTableView.getItems());
    }

    public void setOnNewActivityPeriodButton(EventHandler<MouseEvent> newActivityEventEventHandler){
        this.newActivityEventEventHandler = newActivityEventEventHandler;
    }

    public void setOnDeleteActivityPeriod(EventHandler<MouseEvent> deleteActivityEventEventHandler){
        this.deleteActivityEventEventHandler = deleteActivityEventEventHandler;
    }

    public void setOnActivityPeriodCellTableChange(EventHandler<TableColumn.CellEditEvent> cellEditEventEventHandler){
        this.cellEditEventEventHandler = cellEditEventEventHandler;
    }
}
