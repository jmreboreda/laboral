package laboral.component.client_management.common_components;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import laboral.component.ViewLoader;
import laboral.component.person_management.PersonManagementConstants;
import laboral.domain.address.Address;
import laboral.domain.address.StreetType;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.utilities.cells.EditableStringWorkCenterTableCell;

import java.util.Arrays;

public class ClientWorkCenterTableView extends AnchorPane {

    private static final String WORK_CENTERS_TABLE_VIEW_FXML = "/fxml/client_management/common_components/client_management_work_center_table_view.fxml";

    private static final Integer DEFAULT_ADDRESS_COLUMN = 0;
    private static final Integer STREET_TYPE_COLUMN = 1;
    private static final Integer STREET_NAME_COLUMN = 2;
    private static final Integer STREET_EXTENDED_COLUMN = 3;
    private static final Integer LOCATION_COLUMN = 4;
    private static final Integer MUNICIPALITY_COLUMN = 5;
    private static final Integer POSTAL_CODE_COLUMN = 6;

    private Parent parent;

    EventHandler<TableColumn.CellEditEvent> cellEditEventEventHandler;
    private EventHandler<MouseEvent> onNewAddressEventHandler;
    private EventHandler<MouseEvent> onDeleteAddressEventHandler;

    @FXML
    private TitledPane headerPane;
    @FXML
    private TableView<Address> addresses;
    @FXML
    private TableColumn<Address,Boolean> principalTableColumn;
    @FXML
    private TableColumn<Address, StreetType> streetTypeTableColumn;
    @FXML
    private TableColumn<Address, String> streetNameTableColumn;
    @FXML
    private TableColumn<Address, String> streetExtendedTableColumn;
    @FXML
    private TableColumn<Address, String> locationTableColumn;
    @FXML
    private TableColumn<Address, String> municipalityTableColumn;
    @FXML
    private TableColumn<Address, String> postalCodeTableColumn;
    @FXML
    private Button newAddressButton;
    @FXML
    private Button deleteAddressButton;

    public ClientWorkCenterTableView() {
        this.parent = ViewLoader.load(this, WORK_CENTERS_TABLE_VIEW_FXML);
    }

    public TitledPane getHeaderPane() {
        return headerPane;
    }

    public void setHeaderPane(TitledPane headerPane) {
        this.headerPane = headerPane;
    }

    public TableView<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(TableView<Address> addresses1) {
        this.addresses = addresses;
    }

    public TableColumn<Address, Boolean> getPrincipalTableColumn() {
        return principalTableColumn;
    }

    public void setPrincipalTableColumn(TableColumn<Address, Boolean> principalTableColumn) {
        this.principalTableColumn = principalTableColumn;
    }

    public TableColumn<Address, StreetType> getStreetTypeTableColumn() {
        return streetTypeTableColumn;
    }

    public void setStreetTypeTableColumn(TableColumn<Address, StreetType> streetTypeTableColumn) {
        this.streetTypeTableColumn = streetTypeTableColumn;
    }

    public TableColumn<Address, String> getStreetNameTableColumn() {
        return streetNameTableColumn;
    }

    public void setStreetNameTableColumn(TableColumn<Address, String> streetNameTableColumn) {
        this.streetNameTableColumn = streetNameTableColumn;
    }

    public TableColumn<Address, String> getStreetExtendedTableColumn() {
        return streetExtendedTableColumn;
    }

    public void setStreetExtendedTableColumn(TableColumn<Address, String> streetExtendedTableColumn) {
        this.streetExtendedTableColumn = streetExtendedTableColumn;
    }

    public TableColumn<Address, String> getLocationTableColumn() {
        return locationTableColumn;
    }

    public void setLocationTableColumn(TableColumn<Address, String> locationTableColumn) {
        this.locationTableColumn = locationTableColumn;
    }

    public TableColumn<Address, String> getMunicipalityTableColumn() {
        return municipalityTableColumn;
    }

    public void setMunicipalityTableColumn(TableColumn<Address, String> municipalityTableColumn) {
        this.municipalityTableColumn = municipalityTableColumn;
    }

    public TableColumn<Address, String> getPostalCodeTableColumn() {
        return postalCodeTableColumn;
    }

    public void setPostalCodeTableColumn(TableColumn<Address, String> postalCodeTableColumn) {
        this.postalCodeTableColumn = postalCodeTableColumn;
    }

    public Button getNewAddressButton() {
        return newAddressButton;
    }

    public void setNewAddressButton(Button newAddressButton) {
        this.newAddressButton = newAddressButton;
    }

    public Button getDeleteAddressButton() {
        return deleteAddressButton;
    }

    public void setDeleteAddressButton(Button deleteAddressButton) {
        this.deleteAddressButton = deleteAddressButton;
    }

    @FXML
    public void initialize() {

        getHeaderPane().setText("Centros de trabajo del cliente");
        getNewAddressButton().setText("Añadir centro de trabajo");
        getDeleteAddressButton().setText("Eliminar centro de trabajo");

        deleteAddressButton.disableProperty().bind(addresses.getSelectionModel().selectedItemProperty().isNull());

        newAddressButton.setOnMouseClicked(this::onNewWorkCenterButton);
        deleteAddressButton.setOnMouseClicked(this::onDeleteWorkCenterButton);

        addresses.setEditable(true);

        principalTableColumn.setOnEditCommit(this::principalTableColumnOnEditCommit);

        final ObservableList<StreetType> streetTypes = FXCollections.observableArrayList();
        streetTypes.addAll(Arrays.asList(StreetType.values()));

        final ObservableList<Boolean> defaultAddress = FXCollections.observableArrayList();
        defaultAddress.add(Boolean.TRUE);
        defaultAddress.add(Boolean.FALSE);

        principalTableColumn.setCellFactory(param -> {
            String dot = "\u2022";
            String doubleArrow ="\u00BB";
            ComboBoxTableCell<Address, Boolean> comboBoxTableCell = new ComboBoxTableCell<Address, Boolean>(defaultAddress);
            comboBoxTableCell.setPickOnBounds(true);
            comboBoxTableCell.updateSelected(true);
            comboBoxTableCell.setConverter(new StringConverter<Boolean>() {
                @Override
                public String toString(Boolean object) {
                    if(object == Boolean.TRUE){
                        return doubleArrow;
                    }

                    return "";
                }

                @Override
                public Boolean fromString(String string) {
                    if(string.equals("")){
                        return Boolean.FALSE;
                    }

                    return Boolean.TRUE;
                }
            });

            return comboBoxTableCell;
        });

        streetTypeTableColumn.setCellFactory(param -> {
            ComboBoxTableCell<Address, StreetType> comboBoxTableCell = new ComboBoxTableCell<>(streetTypes);
            comboBoxTableCell.setPickOnBounds(true);
            comboBoxTableCell.updateSelected(true);
            comboBoxTableCell.setConverter(new StringConverter<StreetType>() {
                @Override
                public String toString(StreetType object) {
                    if(object != null) {
                        streetTypeTableColumn.setStyle("-fx-text-fill: #000FFF;");
                        return object.getStreetTypeDescription();
                    }
                    return null;
                }

                @Override
                public StreetType fromString(String string) {
                    return null;
                }
            });

            return comboBoxTableCell;
        });

        streetNameTableColumn.setCellFactory(param-> new EditableStringWorkCenterTableCell<Address, String>());
        streetExtendedTableColumn.setCellFactory(param-> new EditableStringWorkCenterTableCell<Address, String>());
        locationTableColumn.setCellFactory(param-> new EditableStringWorkCenterTableCell<Address, String>());
        municipalityTableColumn.setCellFactory(param-> new EditableStringWorkCenterTableCell<Address, String>());
        postalCodeTableColumn.setCellFactory(param-> new EditableStringWorkCenterTableCell<Address, String>());

        principalTableColumn.setCellValueFactory(new PropertyValueFactory<>("defaultAddress"));
        streetTypeTableColumn.setCellValueFactory(new PropertyValueFactory<>("streetType"));
        streetNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("streetName"));
        streetExtendedTableColumn.setCellValueFactory(new PropertyValueFactory<>("streetExtended"));
        locationTableColumn.setCellValueFactory(new PropertyValueFactory<>("location"));
        municipalityTableColumn.setCellValueFactory(new PropertyValueFactory<>("municipality"));
        postalCodeTableColumn.setCellValueFactory(new PropertyValueFactory<>("postalCode"));

        streetTypeTableColumn.setOnEditCommit(this::onTableCellEdited);
        streetNameTableColumn.setOnEditCommit(this::onTableCellEdited);
        streetExtendedTableColumn.setOnEditCommit(this::onTableCellEdited);
        locationTableColumn.setOnEditCommit(this::onTableCellEdited);
        municipalityTableColumn.setOnEditCommit(this::onTableCellEdited);
        postalCodeTableColumn.setOnEditCommit(this::onTableCellEdited);

        // Unselect selected row
        addresses.setRowFactory(param -> {
            final TableRow<Address> row = new TableRow<>();
            row.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getButton().equals(MouseButton.SECONDARY)) {
                        final int index = row.getIndex();
                        if (index >= 0 && index < addresses.getItems().size() && addresses.getSelectionModel().isSelected(index)) {
                            addresses.getSelectionModel().clearSelection();
                            event.consume();
                        }
                    }
                }
            });
            return row;
        });


        principalTableColumn.getStyleClass().add("center");
        principalTableColumn.getStyleClass().add("bold");
        postalCodeTableColumn.getStyleClass().add("center");
    }

    public void initializeView(){


    }

    private void principalTableColumnOnEditCommit(TableColumn.CellEditEvent event){

        onTableCellEdited(event);

        int editedRow = event.getTablePosition().getRow();
        int editedColumn = event.getTablePosition().getColumn();

        Boolean isDefaultAddressSelected = (Boolean) event.getNewValue();

        Address editedWorkCenter = addresses.getItems().get(editedRow);

        editedWorkCenter.setDefaultAddress(isDefaultAddressSelected);
        editedWorkCenter.setStreetType(addresses.getItems().get(editedRow).getStreetType());
        editedWorkCenter.setStreetName(addresses.getItems().get(editedRow).getStreetName());
        editedWorkCenter.setStreetExtended(addresses.getItems().get(editedRow).getStreetExtended());
        editedWorkCenter.setLocation(addresses.getItems().get(editedRow).getLocation());
        editedWorkCenter.setMunicipality(addresses.getItems().get(editedRow).getMunicipality());
        editedWorkCenter.setPostalCode(addresses.getItems().get(editedRow).getPostalCode());

        int rowNumber;
        for(rowNumber = 0; rowNumber< addresses.getItems().size(); rowNumber++){
            Address notEditedWorkCenter = addresses.getItems().get(rowNumber);
            if(rowNumber != editedRow && isDefaultAddressSelected){
                addresses.getItems().get(rowNumber).setDefaultAddress(!isDefaultAddressSelected);
                notEditedWorkCenter.setStreetType(addresses.getItems().get(rowNumber).getStreetType());
                notEditedWorkCenter.setStreetName(addresses.getItems().get(rowNumber).getStreetName());
                notEditedWorkCenter.setStreetExtended(addresses.getItems().get(rowNumber).getStreetExtended());
                notEditedWorkCenter.setLocation(addresses.getItems().get(rowNumber).getLocation());
                notEditedWorkCenter.setMunicipality(addresses.getItems().get(rowNumber).getMunicipality());
                notEditedWorkCenter.setPostalCode(addresses.getItems().get(rowNumber).getPostalCode());
            }
        }

        // It is not valid to remove the default address mark if the address is the only one.
        if(event.getTableView().getItems().size() == 1 && event.getNewValue() == Boolean.FALSE){
            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PersonManagementConstants.DEFAULT_ADDRESS_INVALID_CHANGE);
            addresses.getItems().get(DEFAULT_ADDRESS_COLUMN).setDefaultAddress(Boolean.TRUE);
        }

        refreshAddressTable(addresses.getItems());
    }

    private void onTableCellEdited(TableColumn.CellEditEvent cellEditEvent){

        cellEditEventEventHandler.handle(cellEditEvent);

        int editedRow = cellEditEvent.getTablePosition().getRow();
        int editedColumn = cellEditEvent.getTablePosition().getColumn();
        Address selectedItemAddress = addresses.getItems().get(editedRow);

        if(editedColumn == DEFAULT_ADDRESS_COLUMN){
            selectedItemAddress.setDefaultAddress((Boolean) cellEditEvent.getNewValue());
        }
        if(editedColumn == STREET_TYPE_COLUMN){
            selectedItemAddress.setStreetType((StreetType) cellEditEvent.getNewValue());
        }

        if(editedColumn == STREET_NAME_COLUMN){
            selectedItemAddress.setStreetName((String) cellEditEvent.getNewValue());
        }

        if(editedColumn == STREET_EXTENDED_COLUMN){
            selectedItemAddress.setStreetExtended((String) cellEditEvent.getNewValue());
        }

        if(editedColumn == LOCATION_COLUMN){
            selectedItemAddress.setLocation((String) cellEditEvent.getNewValue());
        }

        if(editedColumn == MUNICIPALITY_COLUMN){
            selectedItemAddress.setMunicipality((String) cellEditEvent.getNewValue());
        }

        if(editedColumn == POSTAL_CODE_COLUMN){
            selectedItemAddress.setPostalCode((String) cellEditEvent.getNewValue());
        }

        refreshAddressTable(addresses.getItems());
    }

    private void onNewWorkCenterButton(MouseEvent event){
        this.onNewAddressEventHandler.handle(event);
    }

    private void onDeleteWorkCenterButton(MouseEvent event){
        this.onDeleteAddressEventHandler.handle(event);
    }

    public void refreshAddressTable(ObservableList<Address> addressesItemList){

        addresses.setItems(FXCollections.observableList(addressesItemList));
        addresses.refresh();
    }

    public void setOnNewAddressButton(EventHandler<MouseEvent> onNewAddressEventHandler){
        this.onNewAddressEventHandler = onNewAddressEventHandler;
    }

    public void setOnDeleteAddressButton(EventHandler<MouseEvent> onDeleteAddressEventHandler){
        this.onDeleteAddressEventHandler = onDeleteAddressEventHandler;
    }


    public Boolean validateNoMissingAddresses(){
        if(addresses.getItems().size() == 0){
            return false;
        }

        return true;
    }

    public Boolean validateDefaultAddressIsSet() {
        ObservableList<Address> workCenters = this.addresses.getItems();

        for (Address address : workCenters) {
            if (address.getDefaultAddress()) {
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public Boolean validateAllAddressesAreComplete() {
        ObservableList<Address> addressesOnTableView = addresses.getItems();
        for (Integer i = 0; i < addressesOnTableView.size(); i++) {
            Address actualWorkCenter = addressesOnTableView.get(i);
            if (actualWorkCenter.getStreetName().equals("") ||
                    actualWorkCenter.getStreetExtended().equals("") ||
                    actualWorkCenter.getLocation().equals("") ||
                    actualWorkCenter.getMunicipality().equals("") ||
                    actualWorkCenter.getPostalCode().equals("")) {

                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    public void setOnAnyCellChange(EventHandler<TableColumn.CellEditEvent> event){
        this.cellEditEventEventHandler = event;
    }
}
