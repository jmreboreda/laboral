package laboral.component.client_management.common_components;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import laboral.component.ViewLoader;
import laboral.domain.service_gm.ServiceGM;
import laboral.domain.service_gm.ServiceGMEnum;
import laboral.domain.utilities.cells.ServiceGMDateCell;

import java.time.LocalDate;
import java.util.Arrays;

public class ClientServiceTableView extends AnchorPane {

    private static final String CLIENT_SERVICE_TABLE_VIEW = "/fxml/client_management/common_components/client_service_table_view.fxml";

    private static final Integer SERVICE_DESCRIPTION_COLUMN = 0;
    private static final Integer DATE_FROM_COLUMN = 1;
    private static final Integer DATE_TO_COLUMN = 2;
    private static final Integer CLAIM_INVOICES_COLUMN = 3;


    private EventHandler<MouseEvent> mouseEventNewServiceEventHandler;
    private EventHandler<MouseEvent> mouseEventDeleteServiceEventHandler;
    private EventHandler<TableColumn.CellEditEvent> cellEditEventEventHandler;

    Parent parent;

    @FXML
    private TitledPane servicesHeaderPane;
    @FXML
    private TableView<ServiceGM> services;
    @FXML
    private TableColumn<ServiceGM, ServiceGMEnum> tableColumnServiceDescription;
    @FXML
    private TableColumn<ServiceGM, LocalDate> tableColumnDateFrom;
    @FXML
    private TableColumn<ServiceGM, LocalDate> tableColumnDateTo;
    @FXML
    private TableColumn<ServiceGM, Boolean> tableColumnClaimInvoices;
    @FXML
    private Button serviceAdd;
    @FXML
    private Button serviceDelete;

    public ClientServiceTableView() {
        this.parent = ViewLoader.load(this, CLIENT_SERVICE_TABLE_VIEW);
    }

    @FXML
    public void initialize(){
        servicesHeaderPane.setText("Histórico de servicios del cliente");

        services.setEditable(false);

        final ObservableList<ServiceGMEnum> serviceGMDescription = FXCollections.observableArrayList();
        serviceGMDescription.addAll(Arrays.asList(ServiceGMEnum.values()));

        final ObservableList<Boolean> claimInvoices = FXCollections.observableArrayList();
        claimInvoices.add(Boolean.TRUE);
        claimInvoices.add(Boolean.FALSE);

        tableColumnServiceDescription.setCellFactory(param -> {
            ComboBoxTableCell<ServiceGM, ServiceGMEnum> comboBoxTableCell = new ComboBoxTableCell<>(serviceGMDescription);
            comboBoxTableCell.setTextFill(Color.BLUE);
            comboBoxTableCell.setPickOnBounds(true);
            comboBoxTableCell.updateSelected(true);
            comboBoxTableCell.setConverter(new StringConverter<ServiceGMEnum>() {
                @Override
                public String toString(ServiceGMEnum serviceGMEnum) {
                    ServiceGM serviceGM = comboBoxTableCell.getTableRow().getItem();
                    if(serviceGM == null) {
                        comboBoxTableCell.setTextFill(Color.BLUE);
                    }
                    else if (serviceGM.getDateTo() != null) {
                        comboBoxTableCell.setTextFill(Color.DARKRED);
                    }

                    if(serviceGMEnum != null) {
                        return serviceGMEnum.getServiceGMDescription();
                    }

                    return null;
                }

                @Override
                public ServiceGMEnum fromString(String string) {
                    return null;
                }
            });

            return comboBoxTableCell;
        });

        tableColumnDateFrom.setCellFactory(param-> new ServiceGMDateCell());

        tableColumnDateTo.setCellFactory(param-> new ServiceGMDateCell());

        tableColumnClaimInvoices.setCellFactory(param -> {
            ComboBoxTableCell<ServiceGM, Boolean> comboBoxTableCell = new ComboBoxTableCell<>(claimInvoices);
            comboBoxTableCell.setTextFill(Color.BLUE);
            comboBoxTableCell.setStyle("-fx-alignment: center;");
            comboBoxTableCell.setPickOnBounds(true);
            comboBoxTableCell.updateSelected(true);
            comboBoxTableCell.setConverter(new StringConverter<Boolean>() {

                @Override
                public String toString(Boolean object) {
                    ServiceGM serviceGM = comboBoxTableCell.getTableRow().getItem();
                    if(serviceGM == null) {
                        comboBoxTableCell.setTextFill(Color.BLUE);
                    }
                    else if (serviceGM.getDateTo() != null) {
                                comboBoxTableCell.setTextFill(Color.DARKRED);
                            }

                    if(object.equals(Boolean.TRUE)) {
                        return "Si";
                    }
                    return "No";
                }

                @Override
                public Boolean fromString(String string) {
                    if(string.equals("Si")){
                        return Boolean.TRUE;
                    }else
                        return Boolean.FALSE;
                }
            });

            return comboBoxTableCell;
        });

        tableColumnServiceDescription.setCellValueFactory(new PropertyValueFactory<>("serviceGM"));
        tableColumnDateFrom.setCellValueFactory(new PropertyValueFactory<>("dateFrom"));
        tableColumnDateTo.setCellValueFactory(new PropertyValueFactory<>("dateTo"));
        tableColumnClaimInvoices.setCellValueFactory(new PropertyValueFactory<>("claimInvoices"));

        tableColumnDateFrom.getStyleClass().add("center");
        tableColumnDateTo.getStyleClass().add("center");
        tableColumnClaimInvoices.getStyleClass().add("center");


        // Unselect selected row
        services.setRowFactory(param -> {
            final TableRow<ServiceGM> row = new TableRow<>();
            row.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if(event.getButton().equals(MouseButton.SECONDARY)) {
                        final int index = row.getIndex();
                        if (index >= 0 && index < services.getItems().size() && services.getSelectionModel().isSelected(index)) {
                            services.getSelectionModel().clearSelection();
                            event.consume();
                        }
                    }
                }
            });
            return row;
        });

        tableColumnServiceDescription.setOnEditCommit(this::onTableCellEdited);
        tableColumnDateFrom.setOnEditCommit(this::onTableCellEdited);
        tableColumnDateTo.setOnEditCommit(this::onTableCellEdited);
        tableColumnClaimInvoices.setOnEditCommit(this::onTableCellEdited);

        serviceAdd.setOnMouseClicked(this::onAddServiceButton);
        serviceDelete.setOnMouseClicked(this::onDeleteServiceButton);
    }

    public TitledPane getServicesHeaderPane() {
        return servicesHeaderPane;
    }

    public void setServicesHeaderPane(TitledPane servicesHeaderPane) {
        this.servicesHeaderPane = servicesHeaderPane;
    }

    public TableView<ServiceGM> getServices() {
        return services;
    }

    public void setServices(TableView<ServiceGM> services) {
        this.services = services;
    }

    public TableColumn<ServiceGM, ServiceGMEnum> getTableColumnServiceDescription() {
        return tableColumnServiceDescription;
    }

    public void setTableColumnServiceDescription(TableColumn<ServiceGM, ServiceGMEnum> tableColumnServiceDescription) {
        this.tableColumnServiceDescription = tableColumnServiceDescription;
    }

    public TableColumn<ServiceGM, LocalDate> getTableColumnDateFrom() {
        return tableColumnDateFrom;
    }

    public void setTableColumnDateFrom(TableColumn<ServiceGM, LocalDate> tableColumnDateFrom) {
        this.tableColumnDateFrom = tableColumnDateFrom;
    }

    public TableColumn<ServiceGM, LocalDate> getTableColumnDateTo() {
        return tableColumnDateTo;
    }

    public void setTableColumnDateTo(TableColumn<ServiceGM, LocalDate> tableColumnDateTo) {
        this.tableColumnDateTo = tableColumnDateTo;
    }

    public TableColumn<ServiceGM, Boolean> getTableColumnClaimInvoices() {
        return tableColumnClaimInvoices;
    }

    public void setTableColumnClaimInvoices(TableColumn<ServiceGM, Boolean> tableColumnClaimInvoices) {
        this.tableColumnClaimInvoices = tableColumnClaimInvoices;
    }

    public Button getServiceAdd() {
        return serviceAdd;
    }

    public void setServiceAdd(Button serviceAdd) {
        this.serviceAdd = serviceAdd;
    }

    public Button getServiceDelete() {
        return serviceDelete;
    }

    public void setServiceDelete(Button serviceDelete) {
        this.serviceDelete = serviceDelete;
    }

    private void onAddServiceButton(MouseEvent event){
        mouseEventNewServiceEventHandler.handle(event);
    }

    private void onDeleteServiceButton(MouseEvent event){
        mouseEventDeleteServiceEventHandler.handle(event);
    }

    public void setOnNewServiceButton(EventHandler<MouseEvent> mouseEventEventHandler){
        this.mouseEventNewServiceEventHandler = mouseEventEventHandler;
    }

    public void setOnDeleteServiceButton(EventHandler<MouseEvent> mouseEventEventHandler){
        this.mouseEventDeleteServiceEventHandler = mouseEventEventHandler;
    }

    public void setOnServiceCellTableChange(EventHandler<TableColumn.CellEditEvent> cellEditEventEventHandler){
        this.cellEditEventEventHandler = cellEditEventEventHandler;
    }

    public void refreshServicesTable(ObservableList<ServiceGM> serviceGMObservableList){
        services.setItems(FXCollections.observableArrayList(serviceGMObservableList));
        services.refresh();
    }

    private void onTableCellEdited(TableColumn.CellEditEvent cellEditEvent){

        cellEditEventEventHandler.handle(cellEditEvent);

        int editedRow = cellEditEvent.getTablePosition().getRow();
        int editedColumn = cellEditEvent.getTablePosition().getColumn();
        ServiceGM serviceGM = services.getItems().get(editedRow);

        if(editedColumn == SERVICE_DESCRIPTION_COLUMN){
            serviceGM.setServiceGM((ServiceGMEnum) cellEditEvent.getNewValue());
        }
        if(editedColumn == DATE_FROM_COLUMN){
            serviceGM.setDateFrom((LocalDate) cellEditEvent.getNewValue());
        }
        if(editedColumn == DATE_TO_COLUMN){
            serviceGM.setDateTo((LocalDate) cellEditEvent.getNewValue());
        }
        if(editedColumn == CLAIM_INVOICES_COLUMN){
            serviceGM.setClaimInvoices((Boolean) cellEditEvent.getNewValue());
        }

        refreshServicesTable(services.getItems());
    }
}
