package laboral.component.client_management.client_modification.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.LocalDateStringConverter;
import laboral.App;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.client_management.ClientManagementConstants;
import laboral.component.client_management.client_modification.generic_components.ClientModificationAction;
import laboral.component.client_management.client_modification.generic_components.ClientModificationHeader;
import laboral.component.client_management.common_components.ClientActivityPeriodsTableView;
import laboral.component.client_management.common_components.ClientServiceTableView;
import laboral.component.client_management.common_components.ClientWorkCenterTableView;
import laboral.component.employer_management.employer_creation.controllers.EmployerCreationMainController;
import laboral.component.employer_management.employer_modification.controller.EmployerModificationMainController;
import laboral.domain.activity_period.ActivityPeriod;
import laboral.domain.address.Address;
import laboral.domain.address.Province;
import laboral.domain.address.StreetType;
import laboral.domain.address.controller.AddressController;
import laboral.domain.client.Client;
import laboral.domain.client.ClientService;
import laboral.domain.client.controller.ClientController;
import laboral.domain.employer.Employer;
import laboral.domain.employer.controller.EmployerController;
import laboral.domain.person.controller.PersonController;
import laboral.domain.service_gm.ServiceGM;
import laboral.domain.service_gm.ServiceGMEnum;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.validator.ClientModificationDataValidator;
import laboral.domain.work_center.WorkCenter;
import laboral.domain.work_center.controller.WorkCenterController;
import java.text.Collator;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ClientModificationMainController extends AnchorPane {

    private static final Logger logger = Logger.getLogger(ClientModificationMainController.class.getSimpleName());
    private static final String CLIENT_MANAGEMENT_MAIN_FXML = "/fxml/client_management/client_modification/client_modification_main_controller.fxml";
    private static final String ADVISORY = "Asesoría";

    private Boolean loadingData = Boolean.FALSE;
    private String clientSg21InitialValue = null;


    private final ClientController clientController = new ClientController();
    private final PersonController personController = new PersonController();
    private final AddressController addressController = new AddressController();
    private final WorkCenterController workCenterController = new WorkCenterController();

    @FXML
    private ClientModificationHeader clientModificationHeader;
    @FXML
    private TextField clientNumber;
    @FXML
    private TextField clientSg21;
    @FXML
    private ComboBox<Client> clientSelector;
    @FXML
    private CheckBox advisoryClientsOnly;
    @FXML
    private CheckBox activeClientAtDate;
    @FXML
    private DatePicker activeClientDatePicker;
    @FXML
    private HBox historicalHBox;
    @FXML
    private ClientActivityPeriodsTableView clientActivityPeriodsTableView;
    @FXML
    private ClientServiceTableView clientServiceTableView;
    @FXML
    private ClientWorkCenterTableView clientWorkCenterTableView;
    @FXML
    private ClientModificationAction clientModificationAction;


    public ClientModificationMainController() {
        logger.info("Initializing client management main fxml");
        Parent parent = ViewLoader.load(this, CLIENT_MANAGEMENT_MAIN_FXML);

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());
        activeClientDatePicker.setConverter(new LocalDateStringConverter(dateFormatter, null));
        activeClientDatePicker.setValue(LocalDate.now());


        clientWorkCenterTableView.setOnAnyCellChange(this::onAnyCellAddressesChange);

        clientModificationAction.getOkButton().setOnMouseClicked(this::onOkButton);
        clientModificationAction.getSaveButton().setOnMouseClicked(this::onSaveButton);
        clientModificationAction.getExitButton().setOnMouseClicked(this::onExitButton);
    }

    @FXML
    public void initialize(){

        this.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>
                () {
            @Override
            public void handle(KeyEvent t) {
                if(t.getCode()== KeyCode.ESCAPE)
                {
                    onExitButton(null);
                }
            }
        });

        clientActivityPeriodsTableView.getDeletePeriod().disableProperty().bind(
                clientActivityPeriodsTableView.getActivityPeriodTableView().getSelectionModel().selectedItemProperty().isNull());

        clientServiceTableView.getServiceDelete().disableProperty().bind(
                clientServiceTableView.getServices().getSelectionModel().selectedItemProperty().isNull());

        clientWorkCenterTableView.getDeleteAddressButton().disableProperty().bind(
                clientWorkCenterTableView.getAddresses().getSelectionModel().selectedItemProperty().isNull());

        clientActivityPeriodsTableView.getActivityPeriodTableView().editableProperty().bind(clientSelector.getSelectionModel().selectedItemProperty().isNotNull());
        clientServiceTableView.getServices().editableProperty().bind(clientSelector.getSelectionModel().selectedItemProperty().isNotNull());
        clientWorkCenterTableView.getAddresses().editableProperty().bind(clientSelector.getSelectionModel().selectedItemProperty().isNotNull());

        clientSg21.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.equals(clientSg21InitialValue)) {
                clientModificationAction.getOkButton().setDisable(false);
            }else{
                clientModificationAction.getOkButton().setDisable(true);
            }

        });

        clientSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 20));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.TOP_CENTER);
                    setText(item.toString());
                }
            }

        });

        clientSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<Client> call(ListView<Client> param) {
                        final ListCell<Client> cell = new ListCell<Client>() {
                            @Override
                            public void updateItem(Client item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }

                });

        loadAdvisoryClientsOnlyAtDate(LocalDate.now());

        clientModificationAction.getExitButton().requestFocus();

        advisoryClientsOnly.setOnAction(this::onActiveClientsAtDateChanged);
        activeClientAtDate.setOnAction(this::onActiveClientsAtDateChanged);
        activeClientDatePicker.valueProperty().addListener((observable, oldDate, newDate)->{
            onActiveClientDatePickerChanged(newDate);
        });

        clientActivityPeriodsTableView.setOnActivityPeriodCellTableChange(this::onActivityPeriodCellTableChange);
        clientActivityPeriodsTableView.setOnNewActivityPeriodButton(this::onNewActivityPeriod);
        clientActivityPeriodsTableView.setOnDeleteActivityPeriod(this::onDeleteActivityPeriod);

        clientServiceTableView.setOnServiceCellTableChange(this::onServiceCellTableChange);
        clientServiceTableView.setOnNewServiceButton(this::onNewServiceButton);
        clientServiceTableView.setOnDeleteServiceButton(this::onDeleteServiceButton);

        clientWorkCenterTableView.setOnNewAddressButton(this::onNewWorkCenterButton);
        clientWorkCenterTableView.setOnDeleteAddressButton(this::onDeleteWorkCenterButton);

        clientModificationAction.getNewEmployerButton().setOnMouseClicked(this::onNewEmployerButton);
        clientModificationAction.getEmployerManagementButton().setOnMouseClicked(this::onEmployerManagementButton);


        clientModificationAction.getNewEmployerButton().setDisable(true);
        clientModificationAction.getEmployerManagementButton().setDisable(true);
        clientModificationAction.getOkButton().setDisable(true);

        clientSelector.setOnAction(this::onSelectClient);

        clientNumber.setMouseTransparent(true);
    }

    public ClientModificationHeader getClientModificationHeader() {
        return clientModificationHeader;
    }

    public void setClientModificationHeader(ClientModificationHeader clientModificationHeader) {
        this.clientModificationHeader = clientModificationHeader;
    }

    public TextField getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(TextField clientNumber) {
        this.clientNumber = clientNumber;
    }

    public TextField getClientSg21() {
        return clientSg21;
    }

    public void setClientSg21(TextField clientSg21) {
        this.clientSg21 = clientSg21;
    }

    public ComboBox<Client> getClientSelector() {
        return clientSelector;
    }

    public void setClientSelector(ComboBox<Client> clientSelector) {
        this.clientSelector = clientSelector;
    }

    public CheckBox getAdvisoryClientsOnly() {
        return advisoryClientsOnly;
    }

    public void setAdvisoryClientsOnly(CheckBox advisoryClientsOnly) {
        this.advisoryClientsOnly = advisoryClientsOnly;
    }

    public CheckBox getActiveClientAtDate() {
        return activeClientAtDate;
    }

    public void setActiveClientAtDate(CheckBox activeClientAtDate) {
        this.activeClientAtDate = activeClientAtDate;
    }

    public DatePicker getActiveClientDatePicker() {
        return activeClientDatePicker;
    }

    public void setActiveClientDatePicker(DatePicker activeClientDatePicker) {
        this.activeClientDatePicker = activeClientDatePicker;
    }

    public HBox getHistoricalHBox() {
        return historicalHBox;
    }

    public void setHistoricalHBox(HBox historicalHBox) {
        this.historicalHBox = historicalHBox;
    }

    public ClientActivityPeriodsTableView getClientActivityPeriodsTableView() {
        return clientActivityPeriodsTableView;
    }

    public void setClientActivityPeriodsTableView(ClientActivityPeriodsTableView clientActivityPeriodsTableView) {
        this.clientActivityPeriodsTableView = clientActivityPeriodsTableView;
    }

    public ClientServiceTableView getClientServiceTableView() {
        return clientServiceTableView;
    }

    public void setClientServiceTableView(ClientServiceTableView clientServiceTableView) {
        this.clientServiceTableView = clientServiceTableView;
    }

    public ClientWorkCenterTableView getClientWorkCenterTableView() {
        return clientWorkCenterTableView;
    }

    public void setClientWorkCenterTableView(ClientWorkCenterTableView clientWorkCenterTableView) {
        this.clientWorkCenterTableView = clientWorkCenterTableView;
    }

    public ClientModificationAction getClientModificationAction() {
        return clientModificationAction;
    }

    public void setClientModificationAction(ClientModificationAction clientModificationAction) {
        this.clientModificationAction = clientModificationAction;
    }

    private void onActiveClientsAtDateChanged(ActionEvent event){
        if(advisoryClientsOnly.isSelected() && activeClientAtDate.isSelected()){
            activeClientDatePicker.setDisable(false);
            loadAdvisoryClientsOnlyAtDate(activeClientDatePicker.getValue());

            return;
        }

        if(!advisoryClientsOnly.isSelected() && activeClientAtDate.isSelected()){
            activeClientDatePicker.setDisable(false);
            loadAllActiveClientsAtDate(activeClientDatePicker.getValue());

            return;
        }

        if(advisoryClientsOnly.isSelected() && !activeClientAtDate.isSelected()){
            activeClientDatePicker.setDisable(true);
            loadAllAdvisoryClientHistory();

            return;
        }

        if(!advisoryClientsOnly.isSelected() && !activeClientAtDate.isSelected()){
            activeClientDatePicker.setDisable(true);
            loadAllCustomerHistory();

            return;
        }
    }

    private void onActiveClientDatePickerChanged(LocalDate atDate){
        onActiveClientsAtDateChanged(new ActionEvent());
    }

    private void onActivityPeriodCellTableChange(CellEditEvent event){
        if(loadingData){
            return;
        }

        if(event.getNewValue() == null && event.getOldValue() != null ||
        event.getNewValue() != null && event.getOldValue() == null ||
                (event.getNewValue() != null && event.getOldValue() != null &&
                        !event.getNewValue().equals(event.getOldValue()))) {
            clientModificationAction.toFront();
            clientModificationAction.getOkButton().setVisible(true);
            clientModificationAction.getOkButton().setDisable(false);
            clientModificationAction.getSaveButton().setDisable(true);
        }
    }

    private void onServiceCellTableChange(CellEditEvent event){
        if(loadingData){
            return;
        }

        if(event.getOldValue() != null && event.getNewValue() == null ||
                event.getNewValue() != null && event.getOldValue() == null ||
                !event.getNewValue().equals(event.getOldValue())) {
            clientModificationAction.toFront();
            clientModificationAction.getOkButton().setVisible(true);
            clientModificationAction.getOkButton().setDisable(false);
            clientModificationAction.getSaveButton().setDisable(true);
        }
    }

    private void onAnyCellAddressesChange(CellEditEvent event){
        if(loadingData){
            return;
        }

        ClientModificationDataValidator clientModificationDataValidator = new ClientModificationDataValidator(this);
        if(!clientModificationDataValidator.validateActivityPeriodDataEntry())
        {
            return;
        };

        if(!event.getNewValue().equals(event.getOldValue())) {
            clientModificationAction.toFront();
            clientModificationAction.getOkButton().setVisible(true);
            clientModificationAction.getOkButton().setDisable(false);
            clientModificationAction.getSaveButton().setDisable(true);
        }
    }

    private void loadAdvisoryClientsOnlyAtDate(LocalDate atDate) {

        interfaceInitialize();

        List<Client> activeClientList = new ArrayList<>();
        List<Client> clientList = clientController.findAll();

        for (Client client : clientList) {
            for (ActivityPeriod activityPeriod : client.getActivityPeriods()) {
                if (activityPeriod.getDateFrom() == null) {
//                    System.out.println("Hay períodos de actividad sin fecha de inicio en el cliente: " + persistedClient.getClientAlphabeticalName());
                } else if ((activityPeriod.getDateFrom().isBefore(atDate) || activityPeriod.getDateFrom().isEqual(atDate)) &&
                        ((activityPeriod.getDateTo() == null || activityPeriod.getDateTo().isAfter(atDate)) || activityPeriod.getDateTo().isEqual(atDate))) {
                    for (ServiceGM serviceGM : client.getServices()) {
                        if (serviceGM.getServiceGM().getServiceGMDescription().contains(ADVISORY)) {
                            activeClientList.add(client);
                            break;
                        }
                    }
                }
            }

            Collator primaryCollator = Collator.getInstance(new Locale("es", "ES"));
            primaryCollator.setStrength(Collator.PRIMARY);

            List<Client> clientOrderedList = activeClientList
                    .stream()
                    .sorted(Comparator.comparing(Client::toAlphabeticalName, primaryCollator)).collect(Collectors.toList());

            ObservableList<Client> persistedClientOrderedObservableList = FXCollections.observableList(clientOrderedList);
            clientSelector.getItems().setAll(persistedClientOrderedObservableList);
        }
    }

    private void loadAllAdvisoryClientHistory() {

        interfaceInitialize();

        List<Client> clientAdvisoryList = new ArrayList<>();

        List<Client> clientList = clientController.findAll();
        for (Client client : clientList) {
            for (ServiceGM serviceGM : client.getServices()) {
                if (serviceGM.getServiceGM().getServiceGMDescription().contains(ADVISORY)) {
                    clientAdvisoryList.add(client);
                    break;
                }
            }

            Collator primaryCollator = Collator.getInstance(new Locale("es", "ES"));
            primaryCollator.setStrength(Collator.PRIMARY);

            List<Client> persistedClientOrderedList = clientAdvisoryList
                    .stream()
                    .sorted(Comparator.comparing(Client::toAlphabeticalName, primaryCollator)).collect(Collectors.toList());

            ObservableList<Client> persistedClientOrderedObservableList = FXCollections.observableList(persistedClientOrderedList);
            clientSelector.getItems().setAll(persistedClientOrderedObservableList);
        }
    }

    private void loadAllCustomerHistory(){

        interfaceInitialize();

        List<Client> clientList = clientController.findAll();

        Collator primaryCollator = Collator.getInstance(new Locale("es", "ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        List<Client> persistedClientOrderedList = clientList
                .stream()
                .sorted(Comparator.comparing(Client::toAlphabeticalName, primaryCollator)).collect(Collectors.toList());

        ObservableList<Client> persistedClientOrderedObservableList = FXCollections.observableList(persistedClientOrderedList);
        clientSelector.getItems().setAll(persistedClientOrderedObservableList);
    }

    private void loadAllActiveClientsAtDate(LocalDate atDate) {

        interfaceInitialize();

        List<Client> persistedActiveClientList = new ArrayList<>();
        List<Client> clientList = clientController.findAll();

        for (Client client : clientList) {
            for (ActivityPeriod activityPeriod : client.getActivityPeriods()) {
                if (activityPeriod.getDateFrom() == null) {
//                    System.out.println("Hay períodos de actividad sin fecha de inicio en el cliente: " + persistedClient.getClientAlphabeticalName());
                } else if ((activityPeriod.getDateFrom().isBefore(atDate) || activityPeriod.getDateFrom().isEqual(atDate)) &&
                        ((activityPeriod.getDateTo() == null || activityPeriod.getDateTo().isAfter(atDate)) || activityPeriod.getDateTo().isEqual(atDate))) {
                        persistedActiveClientList.add(client);
                }
            }

            Collator primaryCollator = Collator.getInstance(new Locale("es", "ES"));
            primaryCollator.setStrength(Collator.PRIMARY);

            List<Client> persistedClientOrderedList = persistedActiveClientList
                    .stream()
                    .sorted(Comparator.comparing(Client::toAlphabeticalName, primaryCollator)).collect(Collectors.toList());

            ObservableList<Client> persistedClientOrderedObservableList = FXCollections.observableList(persistedClientOrderedList);
            clientSelector.getItems().setAll(persistedClientOrderedObservableList);
        }
    }

    private void onSelectClient(ActionEvent actionEvent){
        Client selectedClient = clientSelector.getSelectionModel().getSelectedItem();
        if(selectedClient != null){
            showClientData(selectedClient.getClientId());
            if(isAdvisoryClient(selectedClient)){
                if(!isAlreadyEmployer(selectedClient)){
                    clientModificationAction.getNewEmployerButton().setDisable(false);
                    clientModificationAction.getEmployerManagementButton().setDisable(true);
                    return;
                }
            }
            clientModificationAction.getNewEmployerButton().setDisable(true);
            clientModificationAction.getEmployerManagementButton().setDisable(false);
        }
    }

    private void showClientData(Integer clientId){

        loadingData = Boolean.TRUE;

        Client client = clientController.findClientById(clientId);

        clientNumber.setText(client.getClientId().toString());
        clientSg21.setText(client.getSg21Code());
        clientSg21InitialValue = clientSg21.getText();

        // Activity periods
        List<ActivityPeriod> activityPeriodList = new ArrayList<>(client.getActivityPeriods());
        Comparator<ActivityPeriod> persistedActivityPeriodComparatorDateFrom = Comparator.comparing(ActivityPeriod::getDateFrom);
        activityPeriodList.sort(persistedActivityPeriodComparatorDateFrom);
        ObservableList<ActivityPeriod> sortedActivityPeriodObservableList = FXCollections.observableList(activityPeriodList);
        clientActivityPeriodsTableView.refreshActivityPeriodsTable(sortedActivityPeriodObservableList);

        // Services
        List<ServiceGM> serviceGMList = new ArrayList<>(client.getServices());
        Comparator<ServiceGM> serviceGMDateFrom = Comparator.comparing(ServiceGM::getDateFrom);
        serviceGMList.sort(serviceGMDateFrom);
        ObservableList<ServiceGM> sortedServiceGMObservableList = FXCollections.observableList(serviceGMList);
        clientServiceTableView.refreshServicesTable(sortedServiceGMObservableList);

        // Work centers
        List<Address> addressOfWorkCenterList = new ArrayList<>(client.getAddressesOfWorkCenters());
        ObservableList<Address> addressOfWorkCenterObservableList = FXCollections.observableList(addressOfWorkCenterList);
        clientWorkCenterTableView.refreshAddressTable(addressOfWorkCenterObservableList);

        clientModificationAction.getOkButton().setDisable(true);
        clientModificationAction.getSaveButton().setDisable(true);

        loadingData = Boolean.FALSE;

        advisoryClientsOnly.requestFocus();
    }

    private void onNewActivityPeriod(MouseEvent event){
        Integer clientId = clientSelector.getSelectionModel().getSelectedItem().getClientId();
        ActivityPeriod newActivityPeriod = new ActivityPeriod(null, LocalDate.now(), null, null, null);
        clientActivityPeriodsTableView.getActivityPeriodTableView().getItems().add(newActivityPeriod);
        clientActivityPeriodsTableView.getActivityPeriodTableView().refresh();

        clientModificationAction.getOkButton().setVisible(true);
        clientModificationAction.getOkButton().setDisable(false);
        clientModificationAction.getSaveButton().setDisable(true);
    }

    public void onDeleteActivityPeriod(MouseEvent event){
        if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.QUESTION_DELETE_ACTIVITY_PERIOD_IS_CORRECT)){
            return;
        }

        ActivityPeriod activityPeriodSelectedItem = clientActivityPeriodsTableView.getActivityPeriodTableView().getSelectionModel().getSelectedItem();
        clientActivityPeriodsTableView.getActivityPeriodTableView().getItems().remove(activityPeriodSelectedItem);
        clientActivityPeriodsTableView.getActivityPeriodTableView().getSelectionModel().clearSelection();

        clientModificationAction.getSaveButton().setDisable(true);
        clientModificationAction.getOkButton().setVisible(true);
        clientModificationAction.getOkButton().setDisable(false);
    }

     private void onNewServiceButton(MouseEvent event) {
        ServiceGM serviceGM = new ServiceGM(null, ServiceGMEnum.SERVICE_NOT_ESTABLISHED,LocalDate.now(),LocalDate.now(),false,null);
        clientServiceTableView.getServices().getItems().add(serviceGM);
        clientServiceTableView.getServices().refresh();

        clientModificationAction.getOkButton().setVisible(true);
        clientModificationAction.getOkButton().setDisable(false);
        clientModificationAction.getSaveButton().setDisable(true);
     }

     private void onDeleteServiceButton(MouseEvent event){
         if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.QUESTION_DELETE_SERVICE_IS_CORRECT)){
             return;
         }

         ServiceGM persistedServiceGMSelectedItem = clientServiceTableView.getServices().getSelectionModel().getSelectedItem();
         clientServiceTableView.getServices().getItems().remove(persistedServiceGMSelectedItem);
         clientServiceTableView.getServices().getSelectionModel().clearSelection();

         clientModificationAction.getSaveButton().setDisable(true);
         clientModificationAction.getOkButton().setVisible(true);
         clientModificationAction.getOkButton().setDisable(false);

     }

     private void onNewWorkCenterButton(MouseEvent event){
        Address newWorkCenter = new Address(null, StreetType.UNKN,"-----", "-----", "-----", "-----", null, "-----", false);
        clientWorkCenterTableView.getAddresses().getItems().add(newWorkCenter);

        clientModificationAction.getOkButton().setVisible(true);
        clientModificationAction.getOkButton().setDisable(false);
        clientModificationAction.getSaveButton().setDisable(true);
     }

    private void onDeleteWorkCenterButton(MouseEvent event){
//        if(clientWorkCenterTableView.getAddresses().getItems().size() == 1){
//            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.CANNOT_DELETE_THE_ONLY_EXISTING_ADDRESS);
//            return;
//        }

//        if(clientWorkCenterTableView.getAddresses().getSelectionModel().getSelectedItem().getDefaultAddress() == Boolean.TRUE) {
//            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.CANNOT_DELETE_THE_DEFAULT_ADDRESS);
//            return;
//        }

        if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.QUESTION_DELETE_ADDRESS_IS_CORRECT)){
            return;
        }

        Address workCentersSelectedItem = clientWorkCenterTableView.getAddresses().getSelectionModel().getSelectedItem();
        clientWorkCenterTableView.getAddresses().getItems().remove(workCentersSelectedItem);
        clientWorkCenterTableView.getAddresses().getSelectionModel().clearSelection();

        clientModificationAction.getOkButton().setVisible(true);
        clientModificationAction.getOkButton().setDisable(false);
    }

    private void onNewEmployerButton(MouseEvent event){
        Client client = clientSelector.getSelectionModel().getSelectedItem();
        EmployerCreationMainController employerCreationMainController = new EmployerCreationMainController();
//        employerCreationMainController.setEmployerCreate(client);
        Scene scene = new Scene(employerCreationMainController);
        scene.getStylesheets().add(App.class.getResource("/css_stylesheet/application.css").toExternalForm());
        Stage employerCreationStage = new Stage();
        employerCreationStage.setResizable(false);
        employerCreationStage.setTitle("Alta de empleador");
        employerCreationStage.setScene(scene);
        employerCreationStage.initOwner(this.getScene().getWindow());
        employerCreationStage.initModality(Modality.APPLICATION_MODAL);
        employerCreationStage.setOnCloseRequest(Event::consume);
        employerCreationMainController.loadClientToEmployer(client);
        employerCreationStage.show();
    }

    private void onEmployerManagementButton(MouseEvent event){
        Client client = clientSelector.getSelectionModel().getSelectedItem();
        EmployerModificationMainController employerModificationMainController = new EmployerModificationMainController();
        EmployerController employerController = new EmployerController();
        Employer employer = employerController.findByClientId(client.getClientId());
        employerModificationMainController.setEmployerToModify(employer);
        Scene scene = new Scene(employerModificationMainController);
        scene.getStylesheets().add(App.class.getResource("/css_stylesheet/application.css").toExternalForm());
        Stage employerCreationStage = new Stage();
        employerCreationStage.setResizable(false);
        employerCreationStage.setTitle("Modificación de empleador");
        employerCreationStage.setScene(scene);
        employerCreationStage.initOwner(this.getScene().getWindow());
        employerCreationStage.initModality(Modality.APPLICATION_MODAL);
        employerCreationStage.setOnCloseRequest(Event::consume);
        employerModificationMainController.loadEmployerFromSelectedClient(clientSelector.getSelectionModel().getSelectedItem());
        employerCreationStage.show();
    }

    private void onOkButton(MouseEvent event){

        ClientModificationDataValidator clientModificationDataValidator = new ClientModificationDataValidator(this);
        if(!clientModificationDataValidator.validateSg21Code() ||
                !clientModificationDataValidator.validateActivityPeriodDataEntry() ||
                !clientModificationDataValidator.validateServicesDataEntry() ||
                !clientModificationDataValidator.validateWorkCenterDataEntry())
        {
            clientModificationAction.getOkButton().setDisable(true);
            clientModificationAction.getSaveButton().setDisable(true);
            return;
        };

        clientModificationAction.getOkButton().setDisable(true);
        clientModificationAction.getSaveButton().setDisable(false);
    }

    private void onSaveButton(MouseEvent event){

        clientModificationAction.getSaveButton().setDisable(true);

        Client clientSelected = clientSelector.getSelectionModel().getSelectedItem();
        Client clientToUpdate = clientController.findClientById(clientSelected.getClientId());

        Set<ActivityPeriod> activityPeriodSetToUpdate = new HashSet<>(clientActivityPeriodsTableView.getActivityPeriodTableView().getItems());
        Set<ServiceGM> serviceGMSetToUpdate = new HashSet<>(clientServiceTableView.getServices().getItems());
        Set<WorkCenter> workCenterSetToUpdate = prepareWorkCenterSetToUpdate();

        clientToUpdate.setSg21Code(getClientSg21().getText());
        clientToUpdate.setActivityPeriods(activityPeriodSetToUpdate);
        clientToUpdate.setServices(serviceGMSetToUpdate);
        clientToUpdate.setWorkCenters(workCenterSetToUpdate);

        Integer clientId = clientController.updateClient(clientToUpdate);

        if(clientId != null){
            Client client = clientController.findClientById(clientId);
            Message.informationMessage((Stage) clientModificationHeader.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ClientManagementConstants.CLIENT_SAVED_OK);
            logger.info("Client management: the client \"" + client.toString() +  "\" has been successfully updated.");
        }
    }

    private void onExitButton(MouseEvent event){
        logger.info("Client management: exiting program.");

        Stage stage = (Stage) clientModificationHeader.getScene().getWindow();
        stage.close();
    }

    private void interfaceInitialize(){
        clientSelector.getItems().clear();
        clientSelector.getSelectionModel().clearSelection();
        clientNumber.setText("");
        clientSg21.setText("");
        clientActivityPeriodsTableView.getActivityPeriodTableView().getItems().clear();
        clientServiceTableView.getServices().getItems().clear();
        clientWorkCenterTableView.getAddresses().getItems().clear();
    }

    private Boolean isAdvisoryClient(Client selectedClient){
        for(ServiceGM serviceGM : selectedClient.getServices()){
            if(serviceGM.getServiceGM().getServiceGMDescription().contains(ADVISORY)){
                if(serviceGM.getDateTo() == null){
                    return Boolean.TRUE;
                }
            }
        }

        return Boolean.FALSE;
    }

    private Boolean isAlreadyEmployer(Client selectedClient){
        ClientService clientService = ClientService.ClientServiceFactory.getInstance();
        if(clientService.isEmployer(selectedClient)){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    private Set<WorkCenter> prepareWorkCenterSetToUpdate(){

        Set<WorkCenter> workCenterSet = new HashSet<>();

        Set<Address> addressesInWorkCenterTableView = new HashSet<>(clientWorkCenterTableView.getAddresses().getItems());

        for(Address address : addressesInWorkCenterTableView){
            if(address.getId() == null){
                String postalCode = address.getPostalCode();
                Province province = null;
                if(!postalCode.isEmpty()) {
                    String provinceInPostalCode = postalCode.substring(0, 2);
                    for (Province provinceRead : Province.values()) {
                        if (provinceRead.getCode().equals(provinceInPostalCode)) {
                            province = provinceRead;
                            break;
                        }
                    }
                    address.setProvince(province);
                }
            }

            WorkCenter newWorkCenter = new WorkCenter(null, address);
            workCenterSet.add(newWorkCenter);
        }

        return workCenterSet;
    }
}
