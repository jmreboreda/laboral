package laboral.component.client_management.client_modification.generic_components;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class ClientModificationAction extends AnchorPane {

    private static final String NEW_PERSON_ACTION_FXML = "/fxml/client_management/client_modification/client_modification_action_components.fxml";

    private Parent parent;


    @FXML
    private Button newEmployerButton;
    @FXML
    private Button employerManagementButton;
    @FXML
    private Button okButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button exitButton;

    public ClientModificationAction() {
        this.parent = ViewLoader.load(this, NEW_PERSON_ACTION_FXML);
    }

    @FXML
    public void initialize(){
    }

    public Button getNewEmployerButton() {
        return newEmployerButton;
    }

    public void setNewEmployerButton(Button newEmployerButton) {
        this.newEmployerButton = newEmployerButton;
    }

    public Button getEmployerManagementButton() {
        return employerManagementButton;
    }

    public void setEmployerManagementButton(Button employerManagementButton) {
        this.employerManagementButton = employerManagementButton;
    }

    public Button getOkButton() {
        return okButton;
    }

    public Button getSaveButton() {
        return saveButton;
    }

    public Button getExitButton() {
        return exitButton;
    }

    public void setOkButton(Button okButton) {
        this.okButton = okButton;
    }

    public void setSaveButton(Button saveButton) {
        this.saveButton = saveButton;
    }

    public void setExitButton(Button exitButton) {
        this.exitButton = exitButton;
    }
}
