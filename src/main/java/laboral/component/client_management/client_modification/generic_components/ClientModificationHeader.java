package laboral.component.client_management.client_modification.generic_components;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class ClientModificationHeader extends AnchorPane {

    private static final String NEW_PERSON_HEADER_FXML = "/fxml/client_management/client_modification/client_modification_header.fxml";

    private Parent parent;

    @FXML
    private Label headerText;

    public ClientModificationHeader() {
        this.parent = ViewLoader.load(this, NEW_PERSON_HEADER_FXML);
    }

    public Label getHeaderText() {
        return headerText;
    }
}
