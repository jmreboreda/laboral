package laboral.component.client_management.client_modification.generic_components;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Callback;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.domain.address.StreetType;

import java.time.format.DateTimeFormatter;

public class AddressGenericEntryDataView extends AnchorPane {

    private static final String GENERIC_ADDRESS_DATA_ENTRY_VIEW_FXML = "/fxml/generic/address_generic_entry_data_view.fxml";

    private Parent parent;

    @FXML
    private ComboBox <StreetType> streetType;
    @FXML
    private TextField streetName;
    @FXML
    private TextField streetExtended;
    @FXML
    private TextField postalCode;
    @FXML
    private TextField locality;
    @FXML
    private TextField municipality;

    public AddressGenericEntryDataView() {
        this.parent = ViewLoader.load(this, GENERIC_ADDRESS_DATA_ENTRY_VIEW_FXML);
    }

    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());

    @FXML
    public void initialize(){

        streetType.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font ("Noto Sans", 14));
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", 14));
                    setTextFill(Color.BLUE);
//                    setAlignment(Pos.TOP_CENTER);
                    setText(item.toString());
                }
            }

        });

        streetType.setCellFactory(
                new Callback<ListView<StreetType>, ListCell<StreetType>>() {
                    @Override public ListCell<StreetType> call(ListView<StreetType> param) {
                        final ListCell<StreetType> cell = new ListCell<StreetType>() {
                            @Override public void updateItem(StreetType item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font ("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }
                });

        loadStreetType();
    }

    public ComboBox<StreetType> getStreetType() {
        return streetType;
    }

    public TextField getStreetName() {
        return streetName;
    }

    public TextField getStreetExtended() {
        return streetExtended;
    }

    public TextField getPostalCode() {
        return postalCode;
    }

    public TextField getLocality() {
        return locality;
    }

    public TextField getMunicipality() {
        return municipality;
    }

    public void setStreetType(ComboBox<StreetType> streetType) {
        this.streetType = streetType;
    }

    public void setStreetName(TextField streetName) {
        this.streetName = streetName;
    }

    public void setStreetExtended(TextField streetExtended) {
        this.streetExtended = streetExtended;
    }

    public void setPostalCode(TextField postalCode) {
        this.postalCode = postalCode;
    }

    public void setLocality(TextField locality) {
        this.locality = locality;
    }

    public void setMunicipality(TextField municipality) {
        this.municipality = municipality;
    }

    private void loadStreetType(){
        getStreetType().getItems().setAll(StreetType.values());
    }

    public void initializeView(){

        getStreetType().getSelectionModel().clearSelection();
        getStreetName().clear();
        getStreetExtended().clear();
        getPostalCode().clear();
        getLocality().clear();
        getMunicipality().clear();
    }

//    private void normalizeAddressDataEntry() {
//        if(getNormalizeText().isSelected()) {
//            getFirstSurname().setText(WordUtils.capitalizeFully(getFirstSurname().getText()));
//            getSecondSurname().setText(WordUtils.capitalizeFully(getSecondSurname().getText()));
//            getName().setText(WordUtils.capitalizeFully(getName().getText()));
//            getNieNif().setText((getNieNif().getText().toUpperCase()));
//            getNationality().setText(WordUtils.capitalizeFully(getNationality().getText()));
//            getStreetName().setText(WordUtils.capitalizeFully(getStreetName().getText()));
//            getStreetExtended().setText(WordUtils.capitalizeFully(getStreetExtended().getText()));
//            getLocality().setText(WordUtils.capitalizeFully(getLocality().getText()));
//            getMunicipality().setText(WordUtils.capitalizeFully(getMunicipality().getText()));
//    }

    public Boolean validateNoMissingData(){

        if(getStreetType().getSelectionModel().getSelectedItem() == null ||
                getStreetName().getText().isEmpty() ||
                getStreetExtended().getText().isEmpty() ||
                getPostalCode().getText().isEmpty() ||
                getLocality().getText().isEmpty() ||
                getMunicipality().getText().isEmpty()){

            return false;
        }

        return true;
    }
}
