package laboral.component.client_management.client_modification.generic_components;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.util.converter.LocalDateStringConverter;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.person_management.PersonManagementConstants;
import laboral.component.person_management.controllers.PersonDataEntryView;
import laboral.component.person_management.events.PersonDataChangedEvent;
import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.NaturalPerson;
import laboral.domain.person.Person;
import laboral.domain.person.controller.PersonController;
import laboral.domain.person.persistence.dao.PersonDAO;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.study.StudyLevelType;
import org.apache.commons.lang.WordUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class NaturalPersonGenericDataEntryView extends AnchorPane implements PersonDataEntryView {

    private static final String NATURAL_PERSON_GENERIC_DATA_ENTRY_VIEW_FXML = "/fxml/generic/natural_person_generic_data_entry_view.fxml";
    private final PersonController personController = new PersonController();


    private Parent parent;
    private Integer naturalPersonId;

    private EventHandler<PersonDataChangedEvent> personDataEventHandler;

    @FXML
    private Group naturalPersonGroup;
    @FXML
    private TitledPane headerPanel;
    @FXML
    private TextField firstSurname;
    @FXML
    private TextField secondSurname;
    @FXML
    private TextField name;
    @FXML
    private CheckBox normalizeText;
    @FXML
    private TextField nieNif;
    @FXML
    private TextField socialSecurityAffiliationNumber;
    @FXML
    private DatePicker birthDate;
    @FXML
    private ChoiceBox<CivilStatusType> civilStatus;
    @FXML
    private TextField nationality;
    @FXML
    private ChoiceBox<StudyLevelType> studyLevel;

    public NaturalPersonGenericDataEntryView() {
        this.parent = ViewLoader.load(this, NATURAL_PERSON_GENERIC_DATA_ENTRY_VIEW_FXML);
    }

    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());

    @FXML
    public void initialize(){
        Tooltip tooltip = new Tooltip(PersonManagementConstants.TOOLTIP_NORMALIZE_TEXT);
        tooltip.setStyle("-fx-background-color: blue; -fx-text-fill: white;");
        tooltip.setFont(Font.font("Noto Sans", 12));
        normalizeText.setTooltip(tooltip);        birthDate.setConverter(new LocalDateStringConverter(dateFormatter, null));


        firstSurname.textProperty().addListener((observable, oldValue, newValue) -> {
            personDataEventHandler.handle(new PersonDataChangedEvent(newValue));
        });

        secondSurname.textProperty().addListener((observable, oldValue, newValue) -> {
            personDataEventHandler.handle(new PersonDataChangedEvent(newValue));
        });

        name.textProperty().addListener((observable, oldValue, newValue) -> {
            personDataEventHandler.handle(new PersonDataChangedEvent(newValue));
        });

        nieNif.textProperty().addListener((observable, oldValue, newValue) -> {
            personDataEventHandler.handle(new PersonDataChangedEvent(newValue));
        });

        socialSecurityAffiliationNumber.textProperty().addListener((observable, oldValue, newValue) -> {
            personDataEventHandler.handle(new PersonDataChangedEvent(newValue));
        });

        birthDate.valueProperty().addListener((ov, oldValue, newValue) -> {
            personDataEventHandler.handle(null);
        });

        civilStatus.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<CivilStatusType>() {
            @Override
            public void changed(ObservableValue<? extends CivilStatusType> observableValue, CivilStatusType oldValue, CivilStatusType newValue) {
                personDataEventHandler.handle(null);
            }
        });

        nationality.textProperty().addListener((observable, oldValue, newValue) -> {
            personDataEventHandler.handle(new PersonDataChangedEvent(newValue));
        });

        studyLevel.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<StudyLevelType>() {
            @Override
            public void changed(ObservableValue<? extends StudyLevelType> observableValue, StudyLevelType oldValue, StudyLevelType newValue) {
                personDataEventHandler.handle(null);
            }
        });

        loadCivilStatus();
        loadStudy();
    }

    public Integer getNaturalPersonId() {
        return naturalPersonId;
    }

    public void setNaturalPersonId(Integer naturalPersonId) {
        this.naturalPersonId = naturalPersonId;
    }

    public Group getNaturalPersonGroup() {
        return naturalPersonGroup;
    }

    public TitledPane getHeaderPanel() {
        return headerPanel;
    }

    public TextField getFirstSurname() {
        return firstSurname;
    }

    public TextField getSecondSurname() {
        return secondSurname;
    }

    public TextField getName() {
        return name;
    }

    public CheckBox getNormalizeText() {
        return normalizeText;
    }

    public TextField getNieNif() {
        return nieNif;
    }

    public TextField getSocialSecurityAffiliationNumber() {
        return socialSecurityAffiliationNumber;
    }

    public DatePicker getBirthDate() {
        return birthDate;
    }

    public ChoiceBox<CivilStatusType> getCivilStatus() {
        return civilStatus;
    }

    public TextField getNationality() {
        return nationality;
    }

    public ChoiceBox<StudyLevelType> getStudyLevel() {
        return studyLevel;
    }

    public void setFirstSurname(TextField firstSurname) {
        this.firstSurname = firstSurname;
    }

    public void setSecondSurname(TextField secondSurname) {
        this.secondSurname = secondSurname;
    }

    public void setName(TextField name) {
        this.name = name;
    }

    public void setNormalizeText(CheckBox normalizeText) {
        this.normalizeText = normalizeText;
    }

    public void setNieNif(TextField nieNif) {
        this.nieNif = nieNif;
    }

    public void setSocialSecurityAffiliationNumber(TextField socialSecurityAffiliationNumber) {
        this.socialSecurityAffiliationNumber = socialSecurityAffiliationNumber;
    }

    public void setBirthDate(DatePicker birthDate) {
        this.birthDate = birthDate;
    }

    public void setCivilStatus(ChoiceBox<CivilStatusType> civilStatus) {
        this.civilStatus = civilStatus;
    }

    public void setNationality(TextField nationality) {
        this.nationality = nationality;
    }

    public void setStudyLevel(ChoiceBox<StudyLevelType> studyLevel) {
        this.studyLevel = studyLevel;
    }


    public void setDateFormatter(DateTimeFormatter dateFormatter) {
        this.dateFormatter = dateFormatter;
    }

    private void loadCivilStatus(){
        getCivilStatus().getItems().setAll(CivilStatusType.values());
    }
    private void loadStudy(){
        getStudyLevel().getItems().setAll(StudyLevelType.values());
    }

    public void initializeView(){

        getFirstSurname().clear();
        getSecondSurname().clear();
        getName().clear();
        getNieNif().clear();
        getSocialSecurityAffiliationNumber().clear();
        getBirthDate().setValue(null);
        getCivilStatus().getSelectionModel().clearSelection();
        getNationality().clear();
        getStudyLevel().getSelectionModel().clearSelection();
    }

    private void normalizeDataEntry(){
        if(getNormalizeText().isSelected()) {
            getFirstSurname().setText(WordUtils.capitalizeFully(getFirstSurname().getText()));
            getSecondSurname().setText(WordUtils.capitalizeFully(getSecondSurname().getText()));
            getName().setText(WordUtils.capitalizeFully(getName().getText()));
            getNieNif().setText((getNieNif().getText().toUpperCase()));
            getNationality().setText(WordUtils.capitalizeFully(getNationality().getText()));
        }
    }

    @Override
    public List<PersonDBO> finPersonByNieNif(NieNif nieNif) {
        PersonDAO personDAO = PersonDAO.PersonDAOFactory.getInstance();

        return personDAO.findPersonByNieNif(nieNif.getNif());
    }

    @Override
    public void refreshDataToModifyPerson(Person person) {

            NaturalPerson naturalPerson = (NaturalPerson) person;

            StudyLevelType studyLevel = naturalPerson.getStudy() == null ? null : naturalPerson.getStudy();
            LocalDate birthDate = naturalPerson.getBirthDate() == null ? null : naturalPerson.getBirthDate();
            CivilStatusType civilStatus = naturalPerson.getCivilStatus() == null ? null : naturalPerson.getCivilStatus();

            setNaturalPersonId(naturalPerson.getPersonId());
            getFirstSurname().setText(naturalPerson.getFirstSurname());
            getSecondSurname().setText(naturalPerson.getSecondSurname());
            getName().setText(naturalPerson.getName());
            getNieNif().setText(naturalPerson.getNieNif().getNif());
            getSocialSecurityAffiliationNumber().setText(naturalPerson.getSocialSecurityAffiliationNumber());
            getStudyLevel().getSelectionModel().select(studyLevel);
            getBirthDate().setValue(birthDate);
            getCivilStatus().setValue(civilStatus);
            getNationality().setText(naturalPerson.getNationality());
    }


    @Override
    public void onChanged(EventHandler<PersonDataChangedEvent> personDataEventHandler) {
        this.personDataEventHandler = personDataEventHandler;
    }

//    public void setOnNaturalPersonDataChanged(EventHandler<PersonDataChangedEvent> personDataEventHandler){
//        this.personDataEventHandler = personDataEventHandler;
//    }
}
