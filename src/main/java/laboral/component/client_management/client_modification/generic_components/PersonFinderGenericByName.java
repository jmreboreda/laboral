package laboral.component.client_management.client_modification.generic_components;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;
import laboral.component.person_management.events.NamePatternChangedEvent;
import laboral.component.person_management.events.PersonToModifyEvent;
import laboral.domain.person.Person;

import java.util.List;

public class PersonFinderGenericByName extends AnchorPane {

    private static final String MODIFICATION_PERSON_MANAGEMENT_SELECTOR_FXML = "/fxml/person_management/person_modification/person_finder_generic_by_name.fxml";

    private EventHandler<NamePatternChangedEvent> namePatternEventEventHandler;
    private EventHandler<PersonToModifyEvent> personToModifyEventEventHandler;

    private Parent parent;

    @FXML
    private TitledPane headerPane;
    @FXML
    private TextField personName;
    @FXML
    private ListView<Person> personsNames;

    public PersonFinderGenericByName() {
        this.parent = ViewLoader.load(this, MODIFICATION_PERSON_MANAGEMENT_SELECTOR_FXML);
    }

    @FXML
    public void initialize() {
        personName.setOnKeyReleased(this::onPersonNameChanged);
        personsNames.setOnMouseClicked(this::onSelectedPersonToModification);
        }

    public void initializeView(){
        getPersonName().clear();
        getPersonsNames().getItems().clear();
    }

    public TitledPane getHeaderPane() {
        return headerPane;
    }

    public void setHeaderPane(TitledPane headerPane) {
        this.headerPane = headerPane;
    }

    public TextField getPersonName() {
        return personName;
    }

    public void setPersonName(TextField personName) {
        this.personName = personName;
    }

    public ListView<Person> getPersonsNames() {
        return personsNames;
    }

    public void setPersonsNames(ListView<Person> personsNames) {
        this.personsNames = personsNames;
    }

    private void onPersonNameChanged(Event event){
        this.namePatternEventEventHandler.handle(new NamePatternChangedEvent(personName.getText()));
    }

    private void onSelectedPersonToModification(MouseEvent event){
        if(event.getClickCount() == 2){
            personToModifyEventEventHandler.handle(new PersonToModifyEvent(personsNames.getSelectionModel().getSelectedItem()));
        }
    }

    public void refreshPersonNames(List<Person> personList){
        ObservableList<Person> listPersonsWhoMatchPattern = FXCollections.observableList(personList);
        personsNames.setItems(listPersonsWhoMatchPattern);
    }

    public void setOnNamePatternChanged(EventHandler<NamePatternChangedEvent> namePatternEventEventHandler){
        this.namePatternEventEventHandler = namePatternEventEventHandler;
    }

    public void setOnSelectPersonToModification(EventHandler<PersonToModifyEvent> personToModifyEvent){
        this.personToModifyEventEventHandler = personToModifyEvent;
    }
}
