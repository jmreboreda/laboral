package laboral.component.client_management.client_modification.generic_components;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import laboral.component.ViewLoader;
import laboral.component.person_management.PersonManagementConstants;
import laboral.component.person_management.controllers.PersonDataEntryView;
import laboral.component.person_management.events.PersonDataChangedEvent;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.LegalPerson;
import laboral.domain.person.Person;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person.persistence.dao.PersonDAO;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.person_type.PersonType;
import org.apache.commons.lang.WordUtils;

import java.util.List;

public class LegalPersonGenericDataEntryView extends AnchorPane implements PersonDataEntryView {

    private static final String LEGAL_PERSON_GENERIC_DATA_ENTRY_VIEW_FXML = "/fxml/generic/legal_person_generic_data_entry_view.fxml";

    private Parent parent;
    private Integer legalPersonId;

    private EventHandler<PersonDataChangedEvent> personDataEventHandler;

    @FXML
    private Group legalPersonGroup;
    @FXML
    private TitledPane headerPane;
    @FXML
    private TextField legalName;
    @FXML
    private TextField nieNif;
    @FXML
    private CheckBox normalizeText;

    public LegalPersonGenericDataEntryView() {
        this.parent = ViewLoader.load(this, LEGAL_PERSON_GENERIC_DATA_ENTRY_VIEW_FXML);
    }

    @FXML
    public void initialize(){
        Tooltip tooltip = new Tooltip(PersonManagementConstants.TOOLTIP_NORMALIZE_TEXT);
        tooltip.setStyle("-fx-background-color: blue; -fx-text-fill: white;");
        tooltip.setFont(Font.font("Noto Sans", 12));
        normalizeText.setTooltip(tooltip);

        legalName.textProperty().addListener((observable, oldValue, newValue) -> {
            personDataEventHandler.handle(new PersonDataChangedEvent(newValue));
        });

        nieNif.textProperty().addListener((observable, oldValue, newValue) -> {
            personDataEventHandler.handle(new PersonDataChangedEvent(newValue));
        });
    }

    public Integer getLegalPersonId() {
        return legalPersonId;
    }

    public void setLegalPersonId(Integer legalPersonId) {
        this.legalPersonId = legalPersonId;
    }

    public Group getLegalPersonGroup() {
        return legalPersonGroup;
    }

    public TitledPane getHeaderPane() {
        return headerPane;
    }

    public TextField getLegalName() {
        return legalName;
    }

    public TextField getNieNif() {
        return nieNif;
    }

    public CheckBox getNormalizeText() {
        return normalizeText;
    }

    public void initializeView() {
        getLegalName().clear();
        getNieNif().clear();
    }

    public Boolean validateNoMissingData(){
        normalizeDataEntry();

        if(getLegalName().getText().isEmpty()||
                getNieNif().getText().isEmpty()){

            return false;
        }

        return true;
    }

    private void normalizeDataEntry(){
        if(getNormalizeText().isSelected()) {
            getLegalName().setText(WordUtils.capitalizeFully(getLegalName().getText()));
            getNieNif().setText((getNieNif().getText().toUpperCase()));
        }
    }

    @Override
    public List<PersonDBO> finPersonByNieNif(NieNif nieNif) {
        PersonDAO personDAO = PersonDAO.PersonDAOFactory.getInstance();

        return personDAO.findPersonByNieNif(nieNif.getNif());
    }

    public void refreshDataToModifyPerson(Person person){

        LegalPerson legalPerson = (LegalPerson) person;

        setLegalPersonId(legalPerson.getPersonId());
        getLegalName().setText(legalPerson.getLegalName());
        getNieNif().setText(legalPerson.getNieNif().toString());
    }

    @Override
    public void onChanged(EventHandler<PersonDataChangedEvent> personDataEventHandler) {
        this.setOnLegalPersonDataChanged(personDataEventHandler);
    }

    public PersonCreationRequest getPersonCreationRequest(){

        return PersonCreationRequest.PersonCreationRequestBuilder.create()
                .withPersonType(PersonType.LEGAL_PERSON)
                .withLegalName(getLegalName().getText())
                .withNieNif(new NieNif(getNieNif().getText()))

                .build();
    }

    public void setOnLegalPersonDataChanged(EventHandler<PersonDataChangedEvent> personDataEventHandler){
        this.personDataEventHandler = personDataEventHandler;
    }
}
