package laboral.component.client_management.client_modification.generic_components;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import laboral.component.ViewLoader;
import laboral.component.person_management.PersonManagementConstants;
import laboral.component.person_management.controllers.PersonDataEntryView;
import laboral.component.person_management.events.PersonDataChangedEvent;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.EntityWithoutLegalPersonality;
import laboral.domain.person.Person;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.person_type.PersonType;
import org.apache.commons.lang.WordUtils;

import java.util.List;

public class EntityWithoutLegalPersonalityGenericDataEntryView extends AnchorPane implements PersonDataEntryView {

    private static final String LEGAL_PERSON_GENERIC_DATA_ENTRY_VIEW_FXML = "/fxml/generic/legal_person_generic_data_entry_view.fxml";

    private Parent parent;
    private Integer entityWithoutLegalPersonalityId;

    private EventHandler<PersonDataChangedEvent> personDataEventHandler;

    @FXML
    private Group legalPersonGroup;
    @FXML
    private TitledPane headerPane;
    @FXML
    private TextField legalName;
    @FXML
    private TextField nieNif;
    @FXML
    private CheckBox normalizeText;

    public EntityWithoutLegalPersonalityGenericDataEntryView() {
        this.parent = ViewLoader.load(this, LEGAL_PERSON_GENERIC_DATA_ENTRY_VIEW_FXML);
    }

    @FXML
    public void initialize(){
        Tooltip tooltip = new Tooltip(PersonManagementConstants.TOOLTIP_NORMALIZE_TEXT);
        tooltip.setStyle("-fx-background-color: blue; -fx-text-fill: white;");
        tooltip.setFont(Font.font("Noto Sans", 12));
        normalizeText.setTooltip(tooltip);

        legalName.textProperty().addListener((observable, oldValue, newValue) -> {
            personDataEventHandler.handle(new PersonDataChangedEvent(newValue));
        });

        nieNif.textProperty().addListener((observable, oldValue, newValue) -> {
            personDataEventHandler.handle(new PersonDataChangedEvent(newValue));
        });
    }

    public Integer getEntityWithoutLegalPersonalityId() {
        return entityWithoutLegalPersonalityId;
    }

    public void setEntityWithoutLegalPersonalityId(Integer entityWithoutLegalPersonalityId) {
        this.entityWithoutLegalPersonalityId = entityWithoutLegalPersonalityId;
    }

    public Group getLegalPersonGroup() {
        return legalPersonGroup;
    }

    public TitledPane getHeaderPane() {
        return headerPane;
    }

    public TextField getLegalName() {
        return legalName;
    }

    public TextField getNieNif() {
        return nieNif;
    }

    public CheckBox getNormalizeText() {
        return normalizeText;
    }

    public void initializeView() {
        getLegalName().clear();
        getNieNif().clear();
    }

    public Boolean validateNoMissingData(){
        normalizeDataEntry();

        if(getLegalName().getText().isEmpty()||
                getNieNif().getText().isEmpty()){

            return false;
        }

        return true;
    }

    private void normalizeDataEntry(){
        if(getNormalizeText().isSelected()) {
            getLegalName().setText(WordUtils.capitalizeFully(getLegalName().getText()));
            getNieNif().setText((getNieNif().getText().toUpperCase()));
        }
    }

    public PersonCreationRequest getPersonCreationRequest(){

        return PersonCreationRequest.PersonCreationRequestBuilder.create()
                .withPersonType(PersonType.LEGAL_PERSON)
                .withLegalName(getLegalName().getText())
                .withNieNif(new NieNif(getNieNif().getText()))

                .build();
    }

    public void setOnEntityWithoutLegalPersonalityDataChanged(EventHandler<PersonDataChangedEvent> personDataEventHandler){
        this.personDataEventHandler = personDataEventHandler;
    }

    @Override
    public List<PersonDBO> finPersonByNieNif(NieNif nieNif) {
        return null;
    }

    @Override
    public void refreshDataToModifyPerson(Person person) {

        EntityWithoutLegalPersonality entityWithoutLegalPersonality = (EntityWithoutLegalPersonality) person;

        setEntityWithoutLegalPersonalityId(entityWithoutLegalPersonality.getPersonId());
        getLegalName().setText(entityWithoutLegalPersonality.getLegalName());
        getNieNif().setText(entityWithoutLegalPersonality.getNieNif().getNif());
    }

    @Override
    public void onChanged(EventHandler<PersonDataChangedEvent> personDataEventHandler) {
        this.personDataEventHandler = personDataEventHandler;

    }
}
