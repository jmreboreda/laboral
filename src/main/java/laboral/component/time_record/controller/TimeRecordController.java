package laboral.component.time_record.controller;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.VBox;
import laboral.component.ViewLoader;
import laboral.component.time_record.component.TimeRecordData;
import laboral.component.time_record.component.TimeRecordHeader;
import laboral.domain.contract.work_contract.manager.WorkContractManager;

import java.util.logging.Logger;

public class TimeRecordController extends VBox {


    private static final Logger logger = Logger.getLogger(TimeRecordController.class.getSimpleName());
    private static final String TIME_RECORD_MAIN_FXML = "/fxml/time_record/time_record_main.fxml";

    private Parent parent;

    private WorkContractManager workContractManager = new WorkContractManager();

    @FXML
    private TimeRecordHeader timeRecordHeader;
    @FXML
    private TimeRecordData timeRecord;


    public TimeRecordController() {
        logger.info("Initilizing Main fxml");
        this.parent = ViewLoader.load(this, TIME_RECORD_MAIN_FXML);
    }

    @FXML
    public void initialize(){

    }

//    public List<ContractNewVersionDTO> findAllContractNewVersionByClientIdInMonthOfDate(Integer clientId, LocalDate date){
//
//        return workContractManager.findAllContractNewVersionByClientIdInMonthOfDate(clientId, date);
//    }

}
