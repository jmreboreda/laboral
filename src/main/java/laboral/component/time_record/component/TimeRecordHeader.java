package laboral.component.time_record.component;

import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import laboral.component.ViewLoader;

public class TimeRecordHeader extends HBox {

    private static final String TIME_RECORD_HEADER_FXML = "/fxml/time_record/time_record_header.fxml";

    private Parent parent;

    public TimeRecordHeader() {
        this.parent = ViewLoader.load(this, TIME_RECORD_HEADER_FXML);
    }

}
