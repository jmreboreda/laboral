package laboral.component.time_record.component;

import com.lowagie.text.DocumentException;
import javafx.beans.binding.BooleanExpression;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.time_record.TimeRecord;
import laboral.component.time_record.TimeRecordConstants;
import laboral.component.time_record.controller.TimeRecordController;
import laboral.component.work_contract.ContractConstants;
import laboral.domain.employee.manager.EmployeeFinderByEmployerForTimeRecord;
import laboral.domain.employer.manager.EmployerFinderForSelectorInTimeRecord;
import laboral.domain.services.printer.PrinterConstants;
import laboral.domain.time_record.dto.EmployeeDataForTimeRecord;
import laboral.domain.time_record.dto.EmployerForSelectorInTimeRecord;
import laboral.domain.time_record.service.TimeRecordPDFCreator;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;

import java.io.IOException;
import java.nio.file.Path;
import java.text.Collator;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.*;
import java.util.stream.Collectors;

public class TimeRecordData extends VBox {

    private static final String TIME_RECORD_FXML = "/fxml/time_record/time_record_data.fxml";
    private static final Integer FIRST_MONTH_INDEX_IN_MONTHNAME = 0;
    private static final Integer LAST_MONTH_INDEX_IN_MONTHNAME = 11;
    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());

    private Parent parent;
    private Stage stage;

    @FXML
    private ChoiceBox<TimeRecord> monthName;
    @FXML
    private TextField yearNumber;
    @FXML
    private ChoiceBox<EmployerForSelectorInTimeRecord> employerForTimeRecord;
    @FXML
    private TableColumn<EmployeeDataForTimeRecord, String> employeeFullName;
    @FXML
    private TableColumn<EmployeeDataForTimeRecord, String> workDayType;
    @FXML
    private TableColumn<EmployeeDataForTimeRecord, String> hoursByWeek;
    @FXML
    private TableColumn<EmployeeDataForTimeRecord, String> contractType;
    @FXML
    private TableColumn<EmployeeDataForTimeRecord, String> dateFrom;
    @FXML
    private TableColumn<EmployeeDataForTimeRecord, String> dateTo;
    @FXML
    private TableView<EmployeeDataForTimeRecord> dataByTimeRecord;
    @FXML
    private Button createPDFButton;
    @FXML
    private Button printButton;
    @FXML
    private Button exitButton;

    public TimeRecordData() {
        this.parent = ViewLoader.load(this, TIME_RECORD_FXML);
    }

    @FXML
    public void initialize() {

        employerForTimeRecord.setOnAction(this::onChangeEmployer);
        yearNumber.setOnAction(this::loadEmployerForTimeRecord);
        monthName.setOnAction(this::loadEmployerForTimeRecord);
        createPDFButton.disableProperty().bind(BooleanExpression.booleanExpression(this.dataByTimeRecord.getSelectionModel().selectedItemProperty().isNull()));
        createPDFButton.setOnMouseClicked(this::onCreateTimeRecordPDF);
        printButton.disableProperty().bind(BooleanExpression.booleanExpression(this.dataByTimeRecord.getSelectionModel().selectedItemProperty().isNull()));
        printButton.setOnMouseClicked(this::onPrintTimeRecord);
        exitButton.setOnMouseClicked(this::onExit);

        monthName.setItems(FXCollections.observableArrayList(
                TimeRecord.TimeRecordBuilder.create().withNameOfMonth(Month.JANUARY).build(),
                TimeRecord.TimeRecordBuilder.create().withNameOfMonth(Month.FEBRUARY).build(),
                TimeRecord.TimeRecordBuilder.create().withNameOfMonth(Month.MARCH).build(),
                TimeRecord.TimeRecordBuilder.create().withNameOfMonth(Month.APRIL).build(),
                TimeRecord.TimeRecordBuilder.create().withNameOfMonth(Month.MAY).build(),
                TimeRecord.TimeRecordBuilder.create().withNameOfMonth(Month.JUNE).build(),
                TimeRecord.TimeRecordBuilder.create().withNameOfMonth(Month.JULY).build(),
                TimeRecord.TimeRecordBuilder.create().withNameOfMonth(Month.AUGUST).build(),
                TimeRecord.TimeRecordBuilder.create().withNameOfMonth(Month.SEPTEMBER).build(),
                TimeRecord.TimeRecordBuilder.create().withNameOfMonth(Month.OCTOBER).build(),
                TimeRecord.TimeRecordBuilder.create().withNameOfMonth(Month.NOVEMBER).build(),
                TimeRecord.TimeRecordBuilder.create().withNameOfMonth(Month.DECEMBER).build())
        );

        LocalDate localDate = LocalDate.now();
        if(localDate.getMonthValue() > LAST_MONTH_INDEX_IN_MONTHNAME){
            monthName.getSelectionModel().select(FIRST_MONTH_INDEX_IN_MONTHNAME);
        }else {
            monthName.getSelectionModel().select(localDate.getMonthValue());
        }
        if(localDate.getMonthValue() > LAST_MONTH_INDEX_IN_MONTHNAME){
            yearNumber.setText(String.valueOf(localDate.getYear() + 1));
        }else {
            yearNumber.setText(String.valueOf(localDate.getYear()));
        }

        employeeFullName.setCellValueFactory(new PropertyValueFactory<>("employeeFullName"));
        workDayType.setCellValueFactory(new PropertyValueFactory<>("workDayType"));
        hoursByWeek.setCellValueFactory(new PropertyValueFactory<>("hoursByWeek"));
        contractType.setCellValueFactory(new PropertyValueFactory<>("contractType"));
        dateFrom.setCellValueFactory(new PropertyValueFactory<>("dateFrom"));
        dateTo.setCellValueFactory(new PropertyValueFactory<>("dateTo"));
        hoursByWeek.setStyle("-fx-alignment: CENTER;");
        dateFrom.setStyle("-fx-alignment: CENTER;");
        dateTo.setStyle("-fx-alignment: CENTER;");

        loadEmployerForTimeRecord(new ActionEvent());
    }

    private void onCreateTimeRecordPDF(MouseEvent event) {

        if(!verificationCorrectnessSelectedDate()){
            return;
        };

        createPDFButton.disableProperty().bind(BooleanExpression.booleanExpression(this.dataByTimeRecord.getSelectionModel().selectedItemProperty().isNotNull()));

        TimeRecord timeRecord = createTimeRecord();
        Path pathToTimeRecordPDF = null;
        try {
            pathToTimeRecordPDF = TimeRecordPDFCreator.createTimeRecordPDF(timeRecord);
            if(pathToTimeRecordPDF == null){
                Message.errorMessage((Stage) this.createPDFButton.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, TimeRecordConstants.TIME_RECORD_PDF_NOT_CREATED);
                return;
            }

        } catch (IOException | DocumentException e) {
            Message.errorMessage((Stage) this.createPDFButton.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, TimeRecordConstants.TIME_RECORD_PDF_NOT_CREATED);
            e.printStackTrace();
        }
        Message.warningMessage((Stage) createPDFButton.getScene().getWindow(),Parameters.SYSTEM_INFORMATION_TEXT, PrinterConstants.TIME_RECORD_CREATED_IN + "\n" + pathToTimeRecordPDF + "\n");
        createPDFButton.disableProperty().bind(BooleanExpression.booleanExpression(this.dataByTimeRecord.getSelectionModel().selectedItemProperty().isNull()));
    }

    private void onPrintTimeRecord(MouseEvent event){

        if(!verificationCorrectnessSelectedDate()){
            return;
        };

        printButton.disableProperty().bind(BooleanExpression.booleanExpression(this.dataByTimeRecord.getSelectionModel().selectedItemProperty().isNotNull()));

        TimeRecord timeRecord = createTimeRecord();
        Path pathToTimeRecordPDF = null;
        try {
            pathToTimeRecordPDF = TimeRecordPDFCreator.createTimeRecordPDF(timeRecord);
            if(pathToTimeRecordPDF == null){
                Message.errorMessage((Stage) this.createPDFButton.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, TimeRecordConstants.TIME_RECORD_PDF_NOT_CREATED);
            }else{
                String resultPrint = TimeRecordPDFCreator.printTimeRecord(pathToTimeRecordPDF);
                if(resultPrint.equals("ok")) {
                    Message.warningMessage((Stage) printButton.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PrinterConstants.TIME_RECORD_SEND_TO_PRINTER + "\n");
                }else{
                    Message.warningMessage((Stage) printButton.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, PrinterConstants.NO_PRINTER_EXISTS_TO_PRINT_THIS_TIME_RECORD + "\n");
                }
            }
        } catch (IOException | DocumentException e) {
            Message.errorMessage((Stage) this.createPDFButton.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, TimeRecordConstants.TIME_RECORD_PDF_NOT_CREATED);
            e.printStackTrace();
        }

        printButton.disableProperty().bind(BooleanExpression.booleanExpression(this.dataByTimeRecord.getSelectionModel().selectedItemProperty().isNull()));
    }

    private void onExit(MouseEvent event){
        Stage stage = (Stage) exitButton.getScene().getWindow();
        stage.close();
    }

    private Boolean verificationCorrectnessSelectedDate(){

        Boolean isCorrectDate = true;

        LocalDateTime timeNow =  LocalDateTime.now();

        Integer selectedMonth = monthName.getSelectionModel().getSelectedItem().getNameOfMonth().getValue();
        Integer monthMaximumDay = monthName.getSelectionModel().getSelectedItem().getNameOfMonth().maxLength();
        Integer selectedYear = Integer.parseInt(yearNumber.getText());

        LocalDateTime minimumDateTime = LocalDate.of(selectedYear, selectedMonth, 1).atTime(0,0,0);
        LocalDateTime maximumDateTime = LocalDate.of(selectedYear, selectedMonth, monthMaximumDay).atTime(0,0,0).minusSeconds(1L);

        if(Math.abs((int) Duration.between(timeNow, maximumDateTime).toDays()) > 30 ||
                (Math.abs((int) Duration.between(timeNow, minimumDateTime).toDays()) > 30)){

            if (Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, TimeRecordConstants.QUESTION_IS_CORRECT_DATE)) {
                isCorrectDate = true;
            }else{
                isCorrectDate = false;
            }
        }

        return isCorrectDate;
    }

    private TimeRecord createTimeRecord(){
        EmployeeDataForTimeRecord data = dataByTimeRecord.getSelectionModel().getSelectedItem();

        Month timeRecordMonth = this.monthName.getSelectionModel().getSelectedItem().getNameOfMonth();
        String yearMonthReceiptCopyText = "";
        if(this.monthName.getSelectionModel().getSelectedItem().getNameOfMonth() == Month.DECEMBER){
            Integer nextYear = Integer.parseInt(this.yearNumber.getText()) + 1;
            yearMonthReceiptCopyText = "de " + Month.JANUARY.getDisplayName(TextStyle.FULL, Locale.getDefault()) + " de " + nextYear;
        }
        else{
            yearMonthReceiptCopyText = "de " + timeRecordMonth.plus(1L).getDisplayName(TextStyle.FULL, Locale.getDefault()) + " de " + this.yearNumber.getText();
        }

        return TimeRecord.TimeRecordBuilder.create()
                .withNameOfMonth(this.monthName.getSelectionModel().getSelectedItem().getNameOfMonth())
                .withYearNumber(this.yearNumber.getText())
                .withEnterpriseName(employerForTimeRecord.getSelectionModel().getSelectedItem().toString())
                .withQuoteAccountCode(data.getQuoteAccountCode())
                .withEmployeeName(data.getEmployeeFullName())
                .withEmployeeNIF(data.getEmployeeNif())
                .withNumberHoursPerWeek(data.getHoursByWeek() + ContractConstants.HOURS_WORK_WEEK_TEXT.toLowerCase())
                .withMonthYearReceiptCopyText(yearMonthReceiptCopyText)
                .build();
    }

    private void onChangeEmployer(ActionEvent event){
        dataByTimeRecord.getItems().clear();
        if(employerForTimeRecord.getSelectionModel().getSelectedItem() == null){
            return;
        }

        TimeRecordController timeRecordController = new TimeRecordController();

        Integer selectedEmployerId = employerForTimeRecord.getSelectionModel().getSelectedItem().getEmployerId();

        LocalDate date = retrieveDateForTimeRecordFromSelector();
        LocalDate startDate = LocalDate.of(date.getYear(), date.getMonthValue(), 1);
        LocalDate endDate = YearMonth.of(date.getYear(), date.getMonth()).atEndOfMonth();

        EmployeeFinderByEmployerForTimeRecord employeeFinderByEmployerForTimeRecord = new EmployeeFinderByEmployerForTimeRecord();
        List<EmployeeDataForTimeRecord> employeeDataForTimeRecordList = employeeFinderByEmployerForTimeRecord
                .findEmployeeByEmployerForTimeRecord(selectedEmployerId, startDate, endDate);

        refreshCandidatesData(employeeDataForTimeRecordList);
    }

    private void loadEmployerForTimeRecord(ActionEvent event) {

        dataByTimeRecord.getItems().clear();

        LocalDate yearMonthDayDate = retrieveDateForTimeRecordFromSelector();
        LocalDate startDate = LocalDate.of(yearMonthDayDate.getYear(), yearMonthDayDate.getMonthValue(), 1);
        LocalDate endDate = YearMonth.of(yearMonthDayDate.getYear(), yearMonthDayDate.getMonth()).atEndOfMonth();

        EmployerFinderForSelectorInTimeRecord employerFinderForSelectorInTimeRecord = new EmployerFinderForSelectorInTimeRecord();
        List<EmployerForSelectorInTimeRecord> employerForSelectorInTimeRecordList = employerFinderForSelectorInTimeRecord.findAllEmployerWithWorkContractWithTimeRecordInMonth(startDate, endDate);

        ObservableList<EmployerForSelectorInTimeRecord> clientDTOS = FXCollections.observableArrayList(employerForSelectorInTimeRecordList);
        employerForTimeRecord.setItems(clientDTOS);
    }

    private void refreshCandidatesData(List<EmployeeDataForTimeRecord> candidates){

        Collator primaryCollator = Collator.getInstance(new Locale("es","ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        List<EmployeeDataForTimeRecord> sortedCandidatesByName = candidates
                .stream()
                .sorted(Comparator.comparing(EmployeeDataForTimeRecord::getEmployeeFullName, primaryCollator)).collect(Collectors.toList());

        ObservableList<EmployeeDataForTimeRecord> candidateObList = FXCollections.observableArrayList(sortedCandidatesByName);
        dataByTimeRecord.setItems(candidateObList);
        for(EmployeeDataForTimeRecord employeeDataForTimeRecord : candidates){
        }
    }

    private List<EmployerForSelectorInTimeRecord> retrieveClientListWithActiveContractWithoutDuplicates(List<EmployerForSelectorInTimeRecord> activeClientList ){

        List<EmployerForSelectorInTimeRecord> activeClientListWithoutDuplicates = new ArrayList<>();

        Map<Integer, EmployerForSelectorInTimeRecord> clientMap = new HashMap<>();

        for (EmployerForSelectorInTimeRecord timeRecordClientDTO : activeClientList) {
            clientMap.put(timeRecordClientDTO.getEmployerId(), timeRecordClientDTO);
        }

        for (Map.Entry<Integer, EmployerForSelectorInTimeRecord> itemMap : clientMap.entrySet()) {
            activeClientListWithoutDuplicates.add(itemMap.getValue());
        }

        Collator primaryCollator = Collator.getInstance(new Locale("es","ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        return activeClientListWithoutDuplicates
                .stream()
                .sorted(Comparator.comparing(EmployerForSelectorInTimeRecord::getEmployerFullName, primaryCollator)).collect(Collectors.toList());
    }

    private LocalDate retrieveDateForTimeRecordFromSelector(){

        Integer numberOfMonth = (monthName.getSelectionModel().getSelectedItem().getNameOfMonth().getValue());
        Integer numberOfYear = null;

        try{

            numberOfYear = Integer.parseInt(yearNumber.getText());

        }catch (NumberFormatException e){

            yearNumber.setText(String.valueOf(LocalDate.now().getYear()));
            employerForTimeRecord.getSelectionModel().clearSelection();
            return LocalDate.of(LocalDate.now().getYear(), numberOfMonth, 15);
        }

        return  LocalDate.of(numberOfYear, numberOfMonth, 15);
    }
}
