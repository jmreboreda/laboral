package laboral.component.time_record;

public class TimeRecordConstants {

    public static final String TIME_RECORD_PDF_NOT_CREATED = "No se ha podido crear el PDF del registro horario.";
    public static final String TIME_RECORD_CREATED_IN = "Registro horario creado en:\n";
    public static final String TIME_RECORD_SENT_TO_PRINTER = "Registro horario enviado a la impresora.\n";
    public static final String NO_PRINTER_FOR_THESE_ATTRIBUTES = "No existe impresora para imprimir el registro horario" +
            " con los atributos indicados.\n";
    public static final String QUESTION_IS_CORRECT_DATE = "¿Es correcta la fecha solicitada para el registro horario?";
    public static final Integer ADMINISTRATOR_OR_PARTNER_CONTRACT_CODE = 101;
    public static final String SPECIAL_REGIME_SEA = "08";
}
