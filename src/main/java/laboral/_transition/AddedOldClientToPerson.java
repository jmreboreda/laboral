package laboral._transition;

import laboral.domain.activity_period.persistence.dbo.ActivityPeriodDBO;
import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.client.persistence.dao.ClientDAO;
import laboral.domain.client.persistence.dbo.ClientDBO;
import laboral.domain.person.persistence.dao.PersonDAO;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.person_type.PersonType;
import laboral.domain.service_gm.persistence.dbo.ServiceGMDBO;
import laboral.domain.study.StudyLevelType;
import laboral.domain.utilities.OSUtils;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Set;

public class AddedOldClientToPerson {

    public static void addedOldClient() {

        String fileName;

        if(OSUtils.getOSName().contains("linux") ){
            fileName = "/home/jmrb/IdeaProjects/laboral/src/main/resources/client.csv";
        }else{
            fileName = "C:/Users/Jose.Jose-PC/IdeaProjects/laboral/src/main/resources/client.csv";
        }

        PersonDAO personDAO = PersonDAO.PersonDAOFactory.getInstance();
        ClientDAO clientDAO = ClientDAO.ClientDAOFactory.getInstance();

        Path pathToFile = Paths.get(fileName);

        try (BufferedReader br = Files.newBufferedReader(pathToFile,
                StandardCharsets.UTF_8)) {

            String line = br.readLine();

            while (line != null) {

                String[] attributes = line.split(";");

                PersonDBO personDBO = createPersonDBOFromClient(attributes);
                Integer personId = personDAO.create(personDBO);
//                System.out.println("Create Person " + personId);

                PersonDBO personDBORead = personDAO.findById(personId);
                ClientDBO clientDBO = createClientDBO(attributes);
                clientDBO.setPersonDBO(personDBORead);
                Integer clientId = clientDAO.create(clientDBO);
//                System.out.println("Create Client " + clientId);

                line = br.readLine();
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private static PersonDBO createPersonDBOFromClient(String[] metadata) {

        Integer id  = null;
        Integer oldClientId = Integer.parseInt(metadata[1]);
        String firstSurname = metadata[7].equals("") ? null : metadata[7];
        String secondSurname = metadata[8].equals("") ? null : metadata[8];
        String name = metadata[9].equals("") ? null : metadata[9];
        String legalName = metadata[10].equals("") ? null : metadata[10];

        PersonType personType = null;
        if(metadata[2].equals("True")) {
            personType = PersonType.NATURAL_PERSON;
        }else if(metadata[3].equals("True")){
            personType = PersonType.LEGAL_PERSON;
        }else{
            personType = PersonType.ENTITY_WITHOUT_LEGAL_PERSONALITY;
        }

        String nieNif = metadata[5].equals("") ? null  : metadata[5];
        String socialSecurityAffiliationNumber = null;
        Date birthDate = null;
        CivilStatusType civilStatus = null;
        StudyLevelType studyLevelType = null;
        String nationality = null;

        return new PersonDBO(id, oldClientId, firstSurname, secondSurname, name, legalName, personType, nieNif, socialSecurityAffiliationNumber, birthDate, civilStatus,
                studyLevelType, null, nationality);
    }

    private static ClientDBO createClientDBO(String[] metadata) {

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        Integer id = null;
        Date dateFrom = Date.valueOf(LocalDate.parse(metadata[11], dateFormatter));
        Date dateTo = Date.valueOf(LocalDate.parse(metadata[12], dateFormatter));
        String sg21Code = metadata[13];
        Date withoutActivity = metadata[14].equals("1900-01-01") ? null : Date.valueOf(LocalDate.parse(metadata[14], dateFormatter));
        PersonDBO personDBO = null;
        Set<ServiceGMDBO> services = null;
        Set<ActivityPeriodDBO> activityPeriods = null;
        Set<WorkCenterDBO> workCenters = null;

        return new ClientDBO(id, sg21Code, personDBO, services, activityPeriods, workCenters);
    }

//    private static Set<ServiceGMDBO> createServiceGMDBO(Integer clientId) {
//
//        String fileName;
//
//        if (OSUtils.getOSName().contains("linux")) {
//            fileName = "/home/jmrb/IdeaProjects/laboral/src/main/resources/servicegm.csv";
//        } else {
//            fileName = "C:/Users/Jose.Jose-PC/IdeaProjects/laboral/src/main/resources/servicegm.csv";
//        }
//
//        PersonDAO personDAO = PersonDAO.PersonDAOFactory.getInstance();
//        ClientDAO clientDAO = ClientDAO.ClientDAOFactory.getInstance();
//        ServiceGMDAO serviceGMDAO = ServiceGMDAO.ServiceGMDAOFactory.getInstance();
//
//
//        Path pathToFile = Paths.get(fileName);
//
//        try (BufferedReader br = Files.newBufferedReader(pathToFile,
//                StandardCharsets.UTF_8)) {
//
//            String line = br.readLine();
//
//            while (line != null) {
//
//                String[] attributes = line.split(";");
//
//                Set<ServiceGMDBO> serviceGMDBOSet = createServiceGMToClient(attributes, clientId);
//
//                line = br.readLine();
//            }
//
//        } catch (IOException ioe) {
//            ioe.printStackTrace();
//        }
//
//        return null;
//    }
//
//    private static Set<ServiceGMDBO> createServiceGMToClient(String[] metadata, Integer clientId) {
//
//        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        Set<ServiceGMDBO> serviceGMDBOSet = new HashSet<>();
//
//        while(Integer.parseInt(metadata[1]) == clientId) {
//            Integer id = null;
//            Date dateFrom = metadata[2].isEmpty() ? null : Date.valueOf(LocalDate.parse(metadata[2], dateFormatter));
//            Date dateTo = metadata[3].isEmpty() ? null : Date.valueOf(LocalDate.parse(metadata[3], dateFormatter));
//            ServiceGMEnum service = ServiceGMEnum.valueOf(metadata[4]);
//            Boolean claimInvoices = metadata[5].equals("True");
//            ClientDBO clientDBO = null;
//
//            ServiceGMDBO serviceGMDBO = new ServiceGMDBO(id, dateFrom, dateTo, service, claimInvoices, clientDBO);
//            serviceGMDBOSet.add(serviceGMDBO);
//        }
//
//        return serviceGMDBOSet;
//    }

}
