package laboral._transition;

import laboral.domain.contract_type.persistence.dao.WorkContractTypeDAO;
import laboral.domain.contract_type.persistence.dbo.WorkContractTypeDBO;
import laboral.domain.utilities.OSUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CreateContractTypeTable {

    public static void createContractTypeTable() {

        String fileName;

        if(OSUtils.getOSName().contains("linux") ){
            fileName = "/home/jmrb/IdeaProjects/laboral/src/main/resources/contract_type.csv";
        }else{
            fileName = "C:/Users/Jose.Jose-PC/IdeaProjects/laboral/src/main/resources/contract_type.csv";
        }
        WorkContractTypeDAO workContractTypeDAO = WorkContractTypeDAO.WorkContractTypeDAOFactory.getInstance();

        Path pathToFile = Paths.get(fileName);

        try (BufferedReader br = Files.newBufferedReader(pathToFile,
                StandardCharsets.UTF_8)) {

            String line = br.readLine();

            while (line != null) {

                String[] attributes = line.split(";");

                WorkContractTypeDBO workContractTypeDBO = createContractTypeDBO(attributes);
                Integer contractTypeId = workContractTypeDAO.create(workContractTypeDBO);

                line = br.readLine();
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    private static WorkContractTypeDBO createContractTypeDBO(String[] metadata) {

        Integer id  = null;
        Integer contractCode = Integer.parseInt(metadata[1]);
        String contractDescription = metadata[2];
        String colloquial = metadata[3];

        Boolean isInitialContract;
        if(metadata[4].equals("True")) {
            isInitialContract = true;
        }else {
            isInitialContract = false;
        }

        Boolean isTemporal;
        if(metadata[5].equals("True")) {
            isTemporal = true;
        }else {
            isTemporal = false;
        }

        Boolean isUndefined;
        if(metadata[6].equals("True")) {
            isUndefined = true;
        }else {
            isUndefined = false;
        }

        Boolean isPartialTime;
        if(metadata[7].equals("True")) {
            isPartialTime = true;
        }else {
            isPartialTime = false;
        }

        Boolean isFullTime;
        if(metadata[8].equals("True")) {
            isFullTime = true;
        }else {
            isFullTime = false;
        }

        Boolean isMenuSelectable;
        if(metadata[9].equals("True")) {
            isMenuSelectable = true;
        }else {
            isMenuSelectable = false;
        }

        Boolean isDeterminedDuration;
        if(metadata[10].equals("True")) {
            isDeterminedDuration = true;
        }else {
            isDeterminedDuration = false;
        }

        Boolean isSurrogate;
        if(metadata[11].equals("True")) {
            isSurrogate = true;
        }else {
            isSurrogate = false;
        }

        Boolean isAdminPartnerSimilar;
        if(metadata[12].equals("True")) {
            isAdminPartnerSimilar = true;
        }else {
            isAdminPartnerSimilar = false;
        }

        Boolean isInterim;
        if(metadata[13].equals("True")) {
            isInterim = true;
        }else {
            isInterim = false;
        }

        return new WorkContractTypeDBO(id, contractCode, contractDescription, colloquial, isInitialContract, isTemporal, isUndefined, isPartialTime, isFullTime, isMenuSelectable,
                isDeterminedDuration, isSurrogate, isAdminPartnerSimilar, isInterim);
    }
}
