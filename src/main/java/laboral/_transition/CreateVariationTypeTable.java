package laboral._transition;

import laboral.domain.utilities.OSUtils;
import laboral.domain.variation_type.persistence.dao.VariationTypeDAO;
import laboral.domain.variation_type.persistence.dbo.VariationTypeDBO;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CreateVariationTypeTable {

    public static void createVariationTypeTable() {

        String fileName;

        if(OSUtils.getOSName().contains("linux") ){
            fileName = "/home/jmrb/IdeaProjects/laboral/src/main/resources/variation_type.csv";
        }else{
            fileName = "C:/Users/Jose.Jose-PC/IdeaProjects/laboral/src/main/resources/variation_type.csv";
        }

        VariationTypeDAO variationTypeDAO = VariationTypeDAO.VariationTypeDAOFactory.getInstance();

        Path pathToFile = Paths.get(fileName);

        try (BufferedReader br = Files.newBufferedReader(pathToFile,
                StandardCharsets.UTF_8)) {

            String line = br.readLine();

            while (line != null) {

                String[] attributes = line.split(";");

                VariationTypeDBO variationTypeDBO = createVariationTypeDBO(attributes);
                Integer variationTypeId = variationTypeDAO.create(variationTypeDBO);

                line = br.readLine();
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    private static VariationTypeDBO createVariationTypeDBO(String[] metadata) {

        Integer id  = null;
        Integer oldVariationId = Integer.parseInt(metadata[1]);
        String variationDescription = metadata[2];

        Boolean extinction;
        if(metadata[3].equals("True")) {
            extinction = true;
        }else {
            extinction = false;
        }

        Boolean conversion;
        if(metadata[4].equals("True")) {
            conversion = true;
        }else {
            conversion = false;
        }

        Boolean special;
        if(metadata[5].equals("True")) {
            special = true;
        }else {
            special = false;
        }

        Boolean extension;
        if(metadata[6].equals("True")) {
            extension = true;
        }else {
            extension = false;
        }

        Boolean category;
        if(metadata[7].equals("True")) {
            category = true;
        }else {
            category = false;
        }

        Boolean initial;
        if(metadata[8].equals("True")) {
            initial = true;
        }else {
            initial = false;
        }

        Boolean reincorporation;
        if(metadata[9].equals("True")) {
            reincorporation = true;
        }else {
            reincorporation = false;
        }

        Boolean workingDay;
        if(metadata[10].equals("True")) {
            workingDay = true;
        }else {
            workingDay = false;
        }

        Boolean idcRequired;
        if(metadata[11].equals("True")) {
            idcRequired = true;
        }else {
            idcRequired = false;
        }

        return null;
//        return new VariationTypeDBO(id, oldVariationId, variationDescription, extinction, conversion, special, extension, category, initial, reincorporation, workingDay, idcRequired);
    }
}
