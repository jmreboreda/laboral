package laboral._transition.contract_variation_gmoldes;

import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.interface_pattern.GenericMapper;

public class MapperContractVariationGmoldesDBOToWorkContractDBO implements GenericMapper<ContractVariationGmoldesDBO, WorkContractDBO> {

    @Override
    public WorkContractDBO map(ContractVariationGmoldesDBO contractVariationGmoldesDBOToMap) {

//        List<WorkContractScheduleDay> scheduleOneList = new ArrayList<>();
//
//        Map<String, DayOfWeek> dayOfWeekConverter = new HashMap<>();
//        dayOfWeekConverter.put("lunes", DayOfWeek.MONDAY);
//        dayOfWeekConverter.put("martes", DayOfWeek.TUESDAY);
//        dayOfWeekConverter.put("miércoles", DayOfWeek.WEDNESDAY);
//        dayOfWeekConverter.put("jueves", DayOfWeek.THURSDAY);
//        dayOfWeekConverter.put("viernes", DayOfWeek.FRIDAY);
//        dayOfWeekConverter.put("sábado", DayOfWeek.SATURDAY);
//        dayOfWeekConverter.put("domingo", DayOfWeek.SUNDAY);
//
//        WorkContractSchedule workContractSchedule = new WorkContractSchedule();
//
//        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
//        DateTimeFormatter hourFormatter = DateTimeFormatter.ofPattern("HH:mm");
//
//        Map<String, ContractDayScheduleJsonData>  oldContractDaySchedule = contractVariationGmoldesDBOToMap.getContractScheduleJsonData().getSchedule();
//
//        if(oldContractDaySchedule.size() > 0) {
//            for (Integer i = 0; i <= 6; i++) {
//                ContractDayScheduleJsonData contractDayScheduleJsonData = oldContractDaySchedule.get("workDay" + i);
//                if(oldContractDaySchedule.get("workDay" + i) != null) {
//                        DayOfWeek dayOfWeek = contractDayScheduleJsonData.getDayOfWeek() == null ? null : dayOfWeekConverter.get(contractDayScheduleJsonData.getDayOfWeek());
//                        LocalDate date = contractDayScheduleJsonData.getDate().equals("") ? null : LocalDate.parse(contractDayScheduleJsonData.getDate(), dateFormatter);
//                        LocalTime amFrom = contractDayScheduleJsonData.getAmFrom().equals("") ? null : LocalTime.parse(contractDayScheduleJsonData.getAmFrom(), hourFormatter);
//                        LocalTime amTo = contractDayScheduleJsonData.getAmTo().equals("") ? null : LocalTime.parse(contractDayScheduleJsonData.getAmTo(), hourFormatter);
//                        LocalTime pmFrom = contractDayScheduleJsonData.getPmFrom().equals("") ? null : LocalTime.parse(contractDayScheduleJsonData.getPmFrom(), hourFormatter);
//                        LocalTime pmTo = contractDayScheduleJsonData.getPmTo().equals("") ? null : LocalTime.parse(contractDayScheduleJsonData.getPmTo(), hourFormatter);
//
//                        WorkContractScheduleDay workContractScheduleDay = WorkContractScheduleDay.create()
//                                .withDayOfWeek(dayOfWeek)
//                                .withDate(date)
//                                .withAmFrom(amFrom)
//                                .withAmTo(amTo)
//                                .withPmFrom(pmFrom)
//                                .withPmTo(pmTo)
//                                .withTotalDayHours(Utilities.converterTimeStringToDuration(contractDayScheduleJsonData.getDurationHours()))
//                                .build();
//
//                        scheduleOneList.add(workContractScheduleDay);
//                }
//            }
//
//            workContractSchedule.setScheduleOneList(scheduleOneList);
//        }
//
//        WorkContractScheduleRetrieve workContractScheduleRetrieve = new WorkContractScheduleRetrieve();
//        ContractSchedule contractSchedule = workContractScheduleRetrieve.retrieveWorkContractSchedule(workContractSchedule);
//
//        QuoteAccountCodeService quoteAccountCodeService = new QuoteAccountCodeService();
//        QuoteAccountCodeDBO quoteAccountCodeDBO = null;
//        if(contractVariationGmoldesDBOToMap.getContractJsonData().getQuoteAccountCode() != null){
//            quoteAccountCodeDBO = quoteAccountCodeService.findByQuoteAccountCode(contractVariationGmoldesDBOToMap.getContractJsonData().getQuoteAccountCode());
//        }
//
//        LocalTime notificationTime = Utilities.convertStringToLocalTime("00:00");
//        LocalDateTime clientNotification = LocalDateTime.of(contractVariationGmoldesDBOToMap.getStartDate().toLocalDate(), notificationTime);
//        Timestamp registrationTimeStamp = Timestamp.valueOf(clientNotification);
//
//        Date expectedEndDate = contractVariationGmoldesDBOToMap.getExpectedEndDate() == null ? null : contractVariationGmoldesDBOToMap.getExpectedEndDate();
//        Date modificationDate = contractVariationGmoldesDBOToMap.getModificationDate() == null ? null : contractVariationGmoldesDBOToMap.getModificationDate();
//        Date endingDate = contractVariationGmoldesDBOToMap.getEndingDate() == null ? null : contractVariationGmoldesDBOToMap.getEndingDate();
//
//        EmployerService employerService = EmployerService.EmployerServiceFactory.getInstance();
//        EmployerDBO employerDBO = employerService.findByClientId(contractVariationGmoldesDBOToMap.getContractJsonData().getClientGMId());
//
//        EmployeeService employeeService = new EmployeeService();
//        EmployeeDBO employeeDBO = employeeService.findById(contractVariationGmoldesDBOToMap.getContractJsonData().getWorkerId());
//
//        WorkContractDBO workContractDBO = new WorkContractDBO();
//        workContractDBO.setId(null);
//        workContractDBO.setClientNotification(registrationTimeStamp);
//        workContractDBO.setContractNumber(contractVariationGmoldesDBOToMap.getContractNumber());
//        workContractDBO.setContractTypeCode(contractVariationGmoldesDBOToMap.getContractJsonData().getContractType());
//        workContractDBO.setVariationTypeCode(contractVariationGmoldesDBOToMap.getVariationType());
//        workContractDBO.setEmployerDBO(employerDBO);
//        workContractDBO.setEmployee(employeeDBO);
//        workContractDBO.setStartDate(contractVariationGmoldesDBOToMap.getStartDate());
//        workContractDBO.setExpectedEndDate(expectedEndDate);
//        workContractDBO.setModificationDate(modificationDate);
//        workContractDBO.setEndingDate(endingDate);
//        workContractDBO.setTraceability(null);
//        workContractDBO.setWorkCenter(null);
//        workContractDBO.setLaborCategory(contractVariationGmoldesDBOToMap.getContractJsonData().getLaborCategory());
//        workContractDBO.setOfficialIdentificationNumber(contractVariationGmoldesDBOToMap.getContractJsonData().getIdentificationContractNumberINEM());
//        workContractDBO.setPublicNotes(contractVariationGmoldesDBOToMap.getContractJsonData().getNotesForContractManager());
//        workContractDBO.setPrivateNotes(contractVariationGmoldesDBOToMap.getContractJsonData().getPrivateNotes());
//        workContractDBO.setQuoteAccountCode(quoteAccountCodeDBO);
//        workContractDBO.setContractSchedule(contractSchedule);
//
//        return workContractDBO;
        return null;
    }
}
