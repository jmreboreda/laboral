package laboral._transition.contract_variation_gmoldes;

import laboral.domain.utilities.HibernateUtil;
import org.hibernate.ObjectDeletedException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.sql.Date;

public class TraceabilityContractDocumentationDAOGmoldes {

    private SessionFactory sessionFactory;
    private Session session;

    public TraceabilityContractDocumentationDAOGmoldes() {
    }

    public static class TraceabilityContractDocumentationDAOGmoldesFactory {

        private static TraceabilityContractDocumentationDAOGmoldes traceabilityContractDocumentationGmoldesDAO;

        public static TraceabilityContractDocumentationDAOGmoldes getInstance() {
            if(traceabilityContractDocumentationGmoldesDAO == null) {
                traceabilityContractDocumentationGmoldesDAO = new TraceabilityContractDocumentationDAOGmoldes(HibernateUtil.retrieveGlobalSession());
            }
            return traceabilityContractDocumentationGmoldesDAO;
        }
    }

    public TraceabilityContractDocumentationDAOGmoldes(Session session) {
        this.session = session;
    }

    public TraceabilityContractDocumentationDBOGmoldes findByThreeData(Integer contractNumber, Integer variationType, Date startDate){
        TypedQuery<TraceabilityContractDocumentationDBOGmoldes> query = session.createNamedQuery(TraceabilityContractDocumentationDBOGmoldes.FIND_BY_THREE_DATA, TraceabilityContractDocumentationDBOGmoldes.class);
        query.setParameter("contractNumber", contractNumber);
        query.setParameter("variationType", variationType);
        query.setParameter("startDate", startDate);

        try{
            return query.getSingleResult();

        }catch(NoResultException e){
            System.out.println("No hay trazabilidad ...");
            return null;
        }catch (ObjectNotFoundException e){
            return null;
        }
    }
}
