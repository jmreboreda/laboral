package laboral._transition.contract_variation_gmoldes;


import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import javax.persistence.TypedQuery;
import java.util.List;

public class ContractVariationGmoldesDAO implements GenericDAO<ContractVariationGmoldesDBO, Integer> {

    private SessionFactory sessionFactory;
    private Session session;

    public ContractVariationGmoldesDAO() {
    }

    public static class ContractVariationGmoldesDAOFactory {

        private static ContractVariationGmoldesDAO contractVariationGmoldesDAO;

        public static ContractVariationGmoldesDAO getInstance() {
            if(contractVariationGmoldesDAO == null) {
                contractVariationGmoldesDAO = new ContractVariationGmoldesDAO(HibernateUtil.retrieveGlobalSession());
            }
            return contractVariationGmoldesDAO;
        }

    }

    public ContractVariationGmoldesDAO(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    @Override
    public Integer create(ContractVariationGmoldesDBO entity) {
        return null;
    }

    @Override
    public Integer update(ContractVariationGmoldesDBO entity) {
        return null;
    }

    @Override
    public Boolean delete(ContractVariationGmoldesDBO entity) {
        return null;
    }

    @Override
    public ContractVariationGmoldesDBO findById(Integer integer) {
        return null;
    }

    @Override
    public List<ContractVariationGmoldesDBO> findAll() {
        TypedQuery<ContractVariationGmoldesDBO> query = session.createNamedQuery(ContractVariationGmoldesDBO.FIND_ALL, ContractVariationGmoldesDBO.class);

        return query.getResultList();
    }

    public List<ContractVariationGmoldesDBO> findByContractNumber(Integer contractNumber){
        TypedQuery<ContractVariationGmoldesDBO> query = session.createNamedQuery(ContractVariationGmoldesDBO.FIND_BY_CONTRACT_NUMBER, ContractVariationGmoldesDBO.class);
        query.setParameter("contractNumber", contractNumber);


        return query.getResultList();
    }
}
