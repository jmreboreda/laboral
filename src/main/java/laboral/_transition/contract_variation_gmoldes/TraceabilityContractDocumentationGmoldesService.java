package laboral._transition.contract_variation_gmoldes;

import laboral.domain.traceability_contract_documentation.persistence.dao.TraceabilityContractDocumentationDAO;
import laboral.domain.traceability_contract_documentation.persistence.dbo.TraceabilityContractDocumentationDBO;

import java.sql.Date;

public class TraceabilityContractDocumentationGmoldesService {

    private TraceabilityContractDocumentationGmoldesService() {
    }

    public static class TraceabilityContractDocumentationGmoldesServiceFactory {

        private static TraceabilityContractDocumentationGmoldesService traceabilityContractDocumentationGmoldesService;

        public static TraceabilityContractDocumentationGmoldesService getInstance() {
            if(traceabilityContractDocumentationGmoldesService == null) {
                traceabilityContractDocumentationGmoldesService = new TraceabilityContractDocumentationGmoldesService();
            }
            return traceabilityContractDocumentationGmoldesService;
        }
    }

    private TraceabilityContractDocumentationDAOGmoldes traceabilityContractDocumentationDAOGmoldes = TraceabilityContractDocumentationDAOGmoldes.TraceabilityContractDocumentationDAOGmoldesFactory.getInstance();

    public TraceabilityContractDocumentationDBOGmoldes findByThreeData(Integer contractNumber, Integer variationType, Date startDate){

        return traceabilityContractDocumentationDAOGmoldes.findByThreeData(contractNumber, variationType, startDate);
    }
}
