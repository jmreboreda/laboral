package laboral._transition.contract_variation_gmoldes;

import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "traceability_contract_documentation_gmoldes")
@NamedQueries(value = {
        @NamedQuery(
                name = TraceabilityContractDocumentationDBOGmoldes.FIND_TRACEABILITY_BY_ID,
                query = "select p from TraceabilityContractDocumentationDBOGmoldes p  where p.id = :traceabilityContractDocumentationId"
        ),
        @NamedQuery(
                name = TraceabilityContractDocumentationDBOGmoldes.FIND_ALL_RECORD,
                query = "select p from TraceabilityContractDocumentationDBOGmoldes p"
        ),
        @NamedQuery(
                name = TraceabilityContractDocumentationDBOGmoldes.FIND_ALL_RECORD_BY_CONTRACT_NUMBER,
                query = "select p from TraceabilityContractDocumentationDBOGmoldes p where contractNumber = :contractNumber"
        ),
        @NamedQuery(
                name = TraceabilityContractDocumentationDBOGmoldes.FIND_ALL_CONTRACT_WITH_PENDING_IDC,
                query = "select p from TraceabilityContractDocumentationDBOGmoldes p where IDCReceptionDate is null order by contractNumber, variationType"
        ),
        @NamedQuery(
                name = TraceabilityContractDocumentationDBOGmoldes.FIND_ALL_CONTRACT_WITH_PENDING_CONTRACT_END_NOTICE,
                query = "select p from TraceabilityContractDocumentationDBOGmoldes p where contractEndNoticeReceptionDate is null order by contractNumber, variationType"
        ),
        @NamedQuery(
                name = TraceabilityContractDocumentationDBOGmoldes.FIND_ALL_CONTRACT_WITH_WORKING_DAY_SCHEDULE_WITH_END_DATE,
                query = "select p from TraceabilityContractDocumentationDBOGmoldes p where variationType = 230 and expectedEndDate >= now() order by contractNumber"
        ),
        @NamedQuery(
                name = TraceabilityContractDocumentationDBOGmoldes.FIND_ALL_CONTRACT_WITH_PENDING_CONTRACT_DOCUMENTATION_TO_CLIENT,
                query = "select p from TraceabilityContractDocumentationDBOGmoldes p where dateDeliveryContractDocumentationToClient is null order by contractNumber, variationType"
        ),
        @NamedQuery(
                name = TraceabilityContractDocumentationDBOGmoldes.FIND_BY_THREE_DATA,
                query = "select p from TraceabilityContractDocumentationDBOGmoldes p where p.contractNumber = :contractNumber and p.variationType = :variationType and p.startDate = :startDate"
        )
})

public class TraceabilityContractDocumentationDBOGmoldes implements Serializable{

    public static final String FIND_TRACEABILITY_BY_ID = "TraceabilityContractDocumentationDBOGmoldes.FIND_TRACEABILITY_BY_ID";
    public static final String FIND_ALL_RECORD = "TraceabilityContractDocumentationDBOGmoldes.FIND_ALL_RECORD";
    public static final String FIND_ALL_RECORD_BY_CONTRACT_NUMBER = "TraceabilityContractDocumentationDBOGmoldes.FIND_ALL_RECORD_BY_CONTRACT_NUMBER";
    public static final String FIND_ALL_CONTRACT_WITH_PENDING_IDC = "TraceabilityContractDocumentationDBOGmoldes.FIND_ALL_CONTRACT_WITH_PENDING_IDC";
    public static final String FIND_ALL_CONTRACT_WITH_PENDING_CONTRACT_END_NOTICE = "TraceabilityContractDocumentationDBOGmoldes.FIND_ALL_CONTRACT_WITH_PENDING_CONTRACT_END_NOTICE";
    public static final String FIND_ALL_CONTRACT_WITH_PENDING_CONTRACT_DOCUMENTATION_TO_CLIENT = "TraceabilityContractDocumentationDBOGmoldes.FIND_ALL_CONTRACT_WITH_PENDING_CONTRACT_DOCUMENTATION_TO_CLIENT";
    public static final String FIND_ALL_CONTRACT_WITH_WORKING_DAY_SCHEDULE_WITH_END_DATE = "TraceabilityContractDocumentationDBOGmoldes.FIND_ALL_CONTRACT_WITH_WORKING_DAY_SCHEDULE_WITH_END_DATE";
    public static final String FIND_BY_THREE_DATA = "TraceabilityContractDocumentationDBOGmoldes.FIND_BY_THREE_DATA";

    @Id
    @SequenceGenerator(name = "traceability_contract_documentation_id_seq", sequenceName = "traceability_contract_documentation_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "traceability_contract_documentation_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;
    private Integer contractNumber;
    private Integer variationType;
    private Date startDate;
    private Date expectedEndDate;
    private Date IDCReceptionDate;
    private Date dateDeliveryContractDocumentationToClient;
    private Date contractEndNoticeReceptionDate;
    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name="contractId")
//    @OneToOne(mappedBy = "contractNumber", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
//    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)

    private WorkContractDBO contract;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(Integer contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Integer getVariationType() {
        return variationType;
    }

    public void setVariationType(Integer variationType) {
        this.variationType = variationType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(Date expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public Date getIDCReceptionDate() {
        return IDCReceptionDate;
    }

    public void setIDCReceptionDate(Date IDCReceptionDate) {
        this.IDCReceptionDate = IDCReceptionDate;
    }

    public Date getDateDeliveryContractDocumentationToClient() {
        return dateDeliveryContractDocumentationToClient;
    }

    public void setDateDeliveryContractDocumentationToClient(Date dateDeliveryContractDocumentationToClient) {
        this.dateDeliveryContractDocumentationToClient = dateDeliveryContractDocumentationToClient;
    }

    public Date getContractEndNoticeReceptionDate() {
        return contractEndNoticeReceptionDate;
    }

    public void setContractEndNoticeReceptionDate(Date contractEndNoticeReceptionDate) {
        this.contractEndNoticeReceptionDate = contractEndNoticeReceptionDate;
    }

    public WorkContractDBO getContract() {
        return contract;
    }

    public void setContract(WorkContractDBO contract) {
        this.contract = contract;
    }
}
