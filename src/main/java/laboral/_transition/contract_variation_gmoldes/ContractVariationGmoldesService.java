package laboral._transition.contract_variation_gmoldes;

import java.util.List;

public class ContractVariationGmoldesService {

    private ContractVariationGmoldesService() {
    }

    public static class ContractVariationServiceFactory {

        private static ContractVariationGmoldesService contractVariationService;

        public static ContractVariationGmoldesService getInstance() {
            if(contractVariationService == null) {
                contractVariationService = new ContractVariationGmoldesService();
            }
            return contractVariationService;
        }
    }

    private final ContractVariationGmoldesDAO contractVariationGmoldesDAO = ContractVariationGmoldesDAO.ContractVariationGmoldesDAOFactory.getInstance();

    public List<ContractVariationGmoldesDBO> findByContractNumber(Integer contractNumber){

        return contractVariationGmoldesDAO.findByContractNumber(contractNumber);
    }

    public List<ContractVariationGmoldesDBO> findAll(){

        return contractVariationGmoldesDAO.findAll();
    }
}
